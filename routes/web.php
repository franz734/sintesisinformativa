<?php
//use App\Directorio;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/registrarUsuario', function () {
    return view('auth.customRegister');
});
Auth::routes();
Route::get('/','CustomHomeController@index')->name('customHome');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/delegado','CustomHomeController@indexDelegado')->name('customHomeDelegado');
//Route::get('/subProc', 'CustomHomeController@indexSubProc')->name('customHomeSubProc');
Route::post('/customLogin', 'CustomAuthController@login')->name('customLogin');
/* Route::post('/customLogin', function(Request $request){
    dd($request->all());
})->name('customLogin'); */

//Route::get('/carga/presentacion', 'PresentacionController@inicio');

/////////////
//* Admin *//
/////////////
Route::group(['middleware' => ['role:Administrador|SuperAdmin|Colaborador','auth']], function () {
    Route::group(['middleware' => ['role:Administrador|SuperAdmin','auth']], function () {
        /// GET ///
        /* Notas */
        Route::get('/inicio', 'NotaController@inicio');
        Route::get('/notas', 'NotaController@notas');
        Route::get('/nuevaNota', 'NotaController@nuevaNota');
        Route::get('/getNotas', 'NotaController@getNotas');
        Route::get('/editaNota/{id}', 'NotaController@editaNota');
        Route::get('/getNotasByEdo/{id}', 'NotaController@notasByEdo');
        Route::get('/getNotasByEdoArea/{idEntidad}/{idArea}', 'NotaController@notasByEdoArea');
        Route::get('/notaExtemporanea', 'NotaController@notaExtemporanea');
        Route::get('/notasCSV', 'NotaController@notasFromCSV');
        Route::get('/completaNotasCSV', 'NotaController@completaNotasCSV');
        Route::get('/getNotasCSV', 'NotaController@getNotasCVS');
        Route::get('/editaNotaCSV/{id}', 'NotaController@editaNotaCSV');
        Route::get('/getFilteredNotas/{a}', 'NotaController@getFilteredNotas');
        Route::get('/getExcelNotas/{a}', 'NotaController@getExcelNotas');
        Route::get('/notasAPI', 'NotaController@notasFromAPI');
        Route::get('/getNotasAPIt', 'NotaController@getNotasApi');
        Route::get('/editaNotaAPI/{id}', 'NotaController@editaNotaAPI');
        Route::get('/getLugares/{a}', 'NotaController@getLugares');
        Route::get('/getSucesos/{a}', 'NotaController@getSucesos');
        /* Columnas */
        Route::get('/ochoColumnas', 'OchoColumnasController@ochoColumnas');
        Route::get('/nuevasOchoColumnas', 'OchoColumnasController@nuevasColumnas');
        Route::get('/getOchoColumnas', 'OchoColumnasController@getOchocolumnas');
        Route::get('/editaOchoColumnas/{id}', 'OchoColumnasController@editaOchoColumnas');
        /* Información Ambienta */
        Route::get('/ambiental', 'InfoAmbientalController@ambientales');
        Route::get('/nuevaInfoAmbiental', 'InfoAmbientalController@nuevaAmbiental');
        Route::get('/getAmbientales', 'InfoAmbientalController@getAmbientales');
        Route::get('/editaAmbiental/{id}', 'InfoAmbientalController@editaAmbiental');
        Route::get('/infoAmbientalCSV', 'InfoAmbientalController@ambientalFromCSV');
        Route::get('/completaInfoAmbientalCSV', 'InfoAmbientalController@completaAmbientalesCSV');
        Route::get('/getAmbientalesCSV', 'InfoAmbientalController@getAmbientalesCSV');
        Route::get('/editaAmbientalCSV/{id}', 'InfoAmbientalController@editaAmbientalCSV');
        /* Boletín */
        Route::get('/boletines', 'BoletinController@boletines');
        Route::get('/nuvoBoletin', 'BoletinController@nuevoBoletin');
        Route::get('/getBoletines', 'BoletinController@getBoletines');
        Route::get('/editaBoletin/{id}', 'BoletinController@editaBoletin');
        /* Estados */
        Route::get('/entidad/{id}', 'EntidadController@entidad');
        /* PDF */
        Route::get('/validaPdf','PDFController@validaPdf');
        //Route::get('/validaPdf','PDFController@validaPdfAndMerge');
        
        Route::get('/validaPdfRespuestas', 'PDFController@validaPdfRespuestas');
        /* Propuestas */
        Route::get('/verPropuesta/{id}', 'PropuestaController@verPropuesta');
        /* Revista */
        Route::get('/enviaRevista', 'RevistaController@sendMiProfepa');
        /* Respuestas*/
        Route::get('/respuestas', 'RespuestaController@respuestas');
        Route::get('/getRespuestas', 'RespuestaController@getRespuestas');
        Route::get('/verRespuesta/{id}', 'RespuestaController@verRespuesta');
        Route::get('/generaPDFRespuestas/{a}','PDFController@generatePDFRespuestas');
        Route::get('/getFilteredRespuestas/{a}', 'RespuestaController@getFilteredRespuestas');
        Route::get('/getExcelRespuestas/{a}', 'RespuestaController@getExcelRespuestas');
        /* Temas Relevantes */
        Route::get('/temasRelevantes','TemaRelevanteController@temasRelevantes');
        Route::get('/getTemasRel','TemaRelevanteController@getTemasRel');
        Route::get('/verRespuestaTemaRel/{id}', 'RespuestaController@verRespuestaTemaRel');
        Route::get('/nuevoTemaRelevante', 'TemaRelevanteController@nuevoTemaRel');
        /* Notas Informativas */
        Route::get('/notasInformativas', 'NotaInfoController@notasInformativas');
        Route::get('/getNotasInformativas', 'NotaInfoController@getNotasInformativas');
        Route::get('/verNotaInformativa/{a}', 'NotaInfoController@verNotaInformativa');
        Route::get('/nuevaNotaInformativa', 'NotaInfoController@nuevaNotaInfo');
        Route::get('/getFilteredNotasInfo/{a}', 'NotaInfoController@getFilteredNotasInfo');
        Route::get('/getExcelNotasInfo/{a}', 'NotaInfoController@getExcelNotasInfo');
        /* Presentaciones */
        Route::get('/cargaPresentacion', 'PresentacionController@inicio');
        Route::get('/presentaciones', 'PresentacionController@presentaciones');
        Route::get('/getPresentaciones', 'PresentacionController@getPresentaciones');

        ///////////////////////////////////////////////////////////
    
        /// POST ///
        /* Notas */
        Route::post('/guardaNota', 'NotaController@guardaNota');
        Route::post('/modificaNota', 'NotaController@modificaNota');
        Route::post('/borraNota', 'NotaController@borraNota');
        Route::post('/asignaTipo', 'NotaController@asignaTipo');
        Route::post('/enviaNota', 'NotaController@enviaNota');
        Route::post('/cargaCSV', 'NotaController@cargaCSV');
        Route::post('/modificaNotaCSV', 'NotaController@modificaNotaCSV');
        Route::post('/filtraNotas', 'NotaController@filtraNotas');
        Route::post('/modificaNotaAPI', 'NotaController@modificaNotaAPI');
        Route::post('/borraNotaAPI', 'NotaController@borraNotaAPI');
        /* Columnas */
        Route::post('/guardaOchoColumnas', 'OchoColumnasController@guardaColumnas');
        Route::post('/modificaColumnas', 'OchoColumnasController@modificaColumnas');
        Route::post('/borraColumnas', 'OchoColumnasController@borraColumnas');
        /* Información Ambienta */
        Route::post('/guardaAmbiental', 'InfoAmbientalController@guardaAmbiental');
        Route::post('/modificaAmbiental', 'InfoAmbientalController@modificaAmbiental');
        Route::post('/borraAmbiental', 'InfoAmbientalController@borraAmbiental');
        Route::post('/cargaCSVAmbiental', 'InfoAmbientalController@cargaCSV');
        Route::post('/modificaAmbientalCSV', 'InfoAmbientalController@modificaAmbientalCSV');
        /* Boletín */
        Route::post('/guardaBoletin', 'BoletinController@guardaBoletin');
        Route::post('/modificaBoletin', 'BoletinController@modificaBoletin');
        Route::post('/borraBoletin', 'BoletinController@borraBoletin');
        /* Estados */
        /* Respuestas*/
        Route::post('/filtraRespuestas', 'RespuestaController@filtraRespuestas');
        /* Temas Relevantes */
        Route::post('/cierraTemaRel', 'TemaRelevanteController@cierraTemaRel');
        Route::post('/abreTemaRel', 'TemaRelevanteController@abreTemaRel');
        Route::post('/guardaTemaRel', 'TemaRelevanteController@guardaTemaRel');
        Route::post('/enviaRespuestaTemaRel', 'TemaRelevanteController@enviaRespuestaTemaRel');
        Route::post('/masInfoTemaRel', 'TemaRelevanteController@masInfoTemaRel');
        /* Notas Informativas */
        Route::post('/guardaNotaInformativa', 'NotaInfoController@guardaNotaInfo');
        Route::post('/filtraNotasInfo', 'NotaInfoController@filtraNotasInfo');
        /* Presentaciones */
        Route::post('/guardaPresentacion', 'PresentacionController@guardaPresentacion');
    });
    
    /// GET ///
    /* Incidencias */
    Route::get('/incidencias', 'IncidenciaController@incidencias');
    Route::get('/getIncidencias', 'IncidenciaController@getIncidencias');    
    Route::get('/verIncidencia/{id}', 'IncidenciaController@verIncidencia');
    Route::get('/getFilteredIncidencias/{a}', 'IncidenciaController@getFilteredIncidencias');
    Route::get('/getExcelIncidencias/{a}', 'IncidenciaController@getExcelIncidencias');
    /* PDF */
    Route::get('/validaPdfIncidencias', 'PDFController@validaPdfIncidencias');
    
    ///////////////////////////////////////////////////////////
    
    /// POST ///    
    /* Incidencias */
    Route::post('/borraIncidencia', 'IncidenciaController@borraIncidencia');
    Route::post('/filtraIncidencias', 'IncidenciaController@filtraIncidencias');
});
/* */
Route::get('/getNotasAPI', 'NotaController@getNotasFromApi');
///////////////////////////////////////////////////////////

///////////////
//* Reader *//
//////////////
/// GET ///
Route::get('/sintesisInformativa/inicio', 'SintesisController@panelSintesis');
Route::get('/sintesisInformativa/entidad/{id}', 'SintesisController@entidad');
Route::get('/sintesisInformativa/ochoColumnas', 'SintesisController@ochoColumnas');
Route::get('/sintesisInformativa/boletines', 'SintesisController@boletin');
Route::get('/sintesisInformativa/informacionAmbiental', 'SintesisController@infoAmbiental');
Route::get('/sintesisInformativa/generaPDF/{f}/{t}','PDFController@generatePDF');
//Route::get('/sintesisInformativa/generaPDF','PDFController@PdfAndMerge');
Route::get('/sintesisInformativa/enviaSintesis/{t}/{f1}/{f2}', 'SintesisController@sendMailSintesis');
Route::get('/sintesisInformativa/revista', 'SintesisController@revista');
//Route::get('/sintesisInformativa/generate-pdf2','PDFController@generatePDF2');

/////////////////////
/// Incidecncias ///
///////////////////
Route::group(['middleware' => ['auth']], function () {
    /// GET ///
    Route::get('/incidencias/inicio', 'IncidenciaController@panelIncidencias');
    Route::get('/incidencias/entidad/{id}', 'IncidenciaController@entidad');
    Route::get('/incidencias/getIncidenciasByEdo/{id}', 'IncidenciaController@getIncidenciasByEdo');
    Route::get('/incidencias/verIncidencia/{id}', 'IncidenciaController@verIncidenciaReader');
    Route::get('/incidencias/generaPDFIncidencias', 'PDFController@generatePDFIncidencias');

});

Route::group(['middleware' => ['role:Colaborador|SuperAdmin|Administrador|Delegado|MonitorZona','auth']], function () {
    /// GET ///
    Route::get('/incidencia/inicio', 'IncidenciaController@inicio');
    Route::get('/incidencia/nuevaIncidencia', 'IncidenciaController@nuevaIncidencia');
    Route::get('/incidencia/listaSubAreas/{id}', 'IncidenciaController@getSubAreas');

    /// POST ///
    Route::post('/incidencia/guardaIncidencia', 'IncidenciaController@guardaIncidencia');
});

/////////////////
//* Delegado *//
///////////////
Route::group(['middleware' => ['role:Delegado|SuperAdmin','auth']], function () {
    /// GET ///
    Route::get('/delegado/inicio', 'DelegadoController@inicio');
    Route::get('/delegado/inicioCS', 'DelegadoController@inicioCS');
    Route::get('/delegado/nuevaIncidencia', 'DelegadoController@nuevaIncidencia');
    Route::get('/delegado/notas', 'DelegadoController@notas');
    Route::get('/delegado/getNotas', 'DelegadoController@getNotas');
    Route::get('/delegado/incidencias', 'DelegadoController@incidencias');
    Route::get('/delegado/getIncidencias', 'DelegadoController@getIncidencias');
    Route::get('/delegado/verIncidencia/{id}', 'DelegadoController@verIncidencia');
    Route::get('/delegado/solicitudes', 'DelegadoController@solicitudes');
    Route::get('/delegado/getSolicitudes', 'DelegadoController@getSolicitudes');
    Route::get('/delegado/responderSolicitud/{id}', 'DelegadoController@responderSolicitud');
    Route::get('/delegado/temasRelevantes', 'DelegadoController@temasRelevantes');
    Route::get('/delegado/getTemasRel', 'DelegadoController@getTemasRel');
    Route::get('/delegado/responderTemaRelevante/{id}', 'DelegadoController@responderTemaRelevante');
    Route::get('/delegado/verRespuestaTemaRel/{id}', 'DelegadoController@verRespuestaTemaRel');
    Route::get('/delegado/notasInformativas', 'NotaInfoController@notasInformativas');
    Route::get('/delegado/getNotasInformativas', 'DelegadoController@getNotasInformativas');
    Route::get('/delegado/verNotaInformativa/{a}', 'NotaInfoController@verNotaInformativa');
    Route::get('/delegado/nuevaNotaInformativa', 'NotaInfoController@nuevaNotaInfo');

    /// POST ///
    Route::post('/delegado/guardaRespuesta', 'DelegadoController@guardaRespuesta');
    Route::post('/delegado/guardaRespuestaTemaRel', 'DelegadoController@guardaRespuestaTemaRel');
    Route::post('/delegado/cierraTemaRel', 'DelegadoController@cierraTemaRel');
    Route::post('/delegado/guardaNotaInformativa', 'NotaInfoController@guardaNotaInfo');
});

///////////////////////
//* Monitor de Zona *//
//////////////////////
Route::group(['middleware' => ['role:MonitorZona|SuperAdmin','auth']], function () {
    /// GET ///    
    Route::get('/monitorZona/inicio', 'MonitorZonaController@inicio');
    Route::get('/monitorZona/notas', 'MonitorZonaController@notas');
    Route::get('/monitorZona/getNotas', 'MonitorZonaController@getNotas');
    Route::get('/monitorZona/temasRelevantes', 'MonitorZonaController@temasRelevantes');
    Route::get('/monitorZona/getTemasRel', 'MonitorZonaController@getTemasRel');
    Route::get('/monitorZona/verRespuesta/{id}','MonitorZonaController@verRespuesta');
    Route::get('/monitorZona/responderTemaRelevante/{id}', 'MonitorZonaController@responderTemaRelevante');
    Route::get('/monitorZona/respuestas','MonitorZonaController@respuestas');
    Route::get('/monitorZona/getRespuestas', 'MonitorZonaController@getRespuestas');
    Route::get('/monitorZona/verRespuestaSolInfo/{id}','MonitorZonaController@verRespuestaSolInfo');
    Route::get('/monitorZona/incidencias', 'MonitorZonaController@incidencias');
    Route::get('/monitorZona/getIncidencias', 'MonitorZonaController@getIncidencias');
    Route::get('/monitorZona/verIncidencia/{id}', 'MonitorZonaController@verIncidencia');
    Route::get('/monitorZona/verPropuesta/{id}', 'PropuestaController@verPropuestaMZ');
    Route::get('/monitorZona/getLugares/{a}', 'NotaController@getLugares');
    Route::get('/monitorZona/getFilteredNotas/{a}', 'NotaController@getFilteredNotas');
    Route::get('/monitorZona/getExcelNotas/{a}', 'NotaController@getExcelNotas');
    Route::get('/monitorZona/getFilteredRespuestas/{a}', 'RespuestaController@getFilteredRespuestas');
    Route::get('/monitorZona/getExcelRespuestas/{a}', 'RespuestaController@getExcelRespuestas');
    Route::get('/monitorZona/getFilteredIncidencias/{a}', 'IncidenciaController@getFilteredIncidencias');
    Route::get('/monitorZona/getExcelIncidencias/{a}', 'IncidenciaController@getExcelIncidencias');
    Route::get('/monitorZona/nuevaNotaInformativa', 'NotaInfoController@nuevaNotaInfo');
    Route::get('/monitorZona/notasInformativas', 'NotaInfoController@notasInformativas');
    Route::get('/monitorZona/getNotasInformativas', 'NotaInfoController@getNotasInformativas');
    Route::get('/monitorZona/verNotaInformativa/{a}', 'NotaInfoController@verNotaInformativa');
    Route::get('/monitorZona/getFilteredNotasInfo/{a}', 'NotaInfoController@getFilteredNotasInfo');
    Route::get('/monitorZona/getExcelNotasInfo/{a}', 'NotaInfoController@getExcelNotasInfo');

    /// POST ///
    Route::post('/monitorZona/asignaTipo', 'NotaController@asignaTipo');
    Route::post('/monitorZona/enviaNota', 'NotaController@enviaNota');
    Route::post('/monitorZona/guardaRespuestaTemaRel', 'MonitorZonaController@guardaRespuestaTemaRel');
    Route::post('/monitorZona/masInfoTemaRel', 'TemaRelevanteController@masInfoTemaRel');
    Route::post('/monitorZona/enviaRespuestaTemaRel', 'TemaRelevanteController@enviaRespuestaTemaRel');
    Route::post('/monitorZona/filtraNotas', 'NotaController@filtraNotas');
    Route::post('/monitorZona/filtraRespuestas', 'RespuestaController@filtraRespuestas');
    Route::post('/monitorZona/filtraIncidencias', 'IncidenciaController@filtraIncidencias');
    Route::post('/monitorZona/guardaNotaInformativa', 'NotaInfoController@guardaNotaInfo');
    Route::post('/monitorZona/filtraNotasInfo', 'NotaInfoController@filtraNotasInfo');
    
});

//////////////////
//* Procurador *//
/////////////////
Route::group(['middleware' => ['role:Procurador|SuperAdmin','auth']], function () {
    /// GET ///
    Route::get('/procurador/inicio', 'ProcuradorController@inicio');
    Route::get('/procurador/notas', 'ProcuradorController@notas');
    Route::get('/procurador/getNotas', 'ProcuradorController@getNotas');
    Route::get('/procurador/incidencias', 'ProcuradorController@incidencias');
    Route::get('/procurador/getIncidencias', 'ProcuradorController@getIncidencias');
    Route::get('/procurador/verIncidencia/{id}', 'ProcuradorController@verIncidencia');
    Route::get('/procurador/respuestas', 'ProcuradorController@respuestas');
    Route::get('/procurador/getRespuestas', 'ProcuradorController@getRespuestas');
    Route::get('/procurador/verRespuesta/{id}', 'ProcuradorController@verRespuesta');
    Route::get('/procurador/temasRelevantes', 'ProcuradorController@temasRelevantes');
    Route::get('/procurador/getTemasRel', 'ProcuradorController@getTemasRel');
    Route::get('/procurador/verRespuestaTemaRel/{id}','ProcuradorController@verRespuestaTemaRel');    

    /// POST ///
    
});

//////////////////
//* Consultor *//
/////////////////
Route::group(['middleware' => ['role:Consultor|SuperAdmin','auth']], function () {
    /// GET ///
    Route::get('/consultas/inicio', 'ConsultasController@inicio');
    Route::get('/consultas/notas/{keyWord}', 'ConsultasController@notas');
    Route::get('/consultas/getResultados/{keyWord}', 'ConsultasController@getResultados');
    Route::get('/consultas/verNota/{a}', 'ConsultasController@verNota');
    Route::get('/consultas/incidencias/{keyWord}', 'ConsultasController@incidencias');
    Route::get('/consultas/verIncidencia/{a}', 'ConsultasController@verIncidencia');
    Route::get('/consultas/temasRel/{keyWord}', 'ConsultasController@temasRel');
    Route::get('/consultas/verTemaRel/{a}', 'ConsultasController@verTemaRel');
    Route::get('/consultas/verRespuesta/{a}', 'ConsultasController@verRespuesta');
    Route::get('/consultas/notasInformativas/{keyWord}', 'ConsultasController@notasInformativas');
    Route::get('/consultas/verNotaInformativa/{a}', 'ConsultasController@verNotaInformativa');
    Route::get('/consultas/normas/{keyWord}', 'ConsultasController@normas');
    Route::get('/consultas/presentaciones/{keyWord}', 'ConsultasController@presentaciones');
    /// POST ///
    Route::post('/consultas/masInfoTemaRel', 'TemaRelevanteController@masInfoTemaRel');
    Route::post('/consultas/comparteInfo', 'ConsultasController@comparteInfo');
    
});

//////////////////////
//* SubProcurador *//
////////////////////
Route::group(['middleware' => ['role:SubProcurador|SuperAdmin','auth']], function () {
    /// GET ///
    Route::get('/subProc/inicio', 'SubProcController@inicio');
    Route::get('/subProc/notas', 'SubProcController@notas');
    Route::get('/subProc/getNotas', 'SubProcController@getNotas');
    Route::get('/subProc/getFilteredNotas/{a}', 'SubProcController@getFilteredNotas');
    Route::get('/subProc/getExcelNotas/{a}', 'SubProcController@getExcelNotas');
    Route::get('/subProc/incidencias', 'SubProcController@incidencias');
    Route::get('/subProc/getIncidencias', 'SubProcController@getIncidencias');
    Route::get('/subProc/verIncidencia/{id}', 'SubProcController@verIncidencia');
    Route::get('/subProc/getFilteredIncidencias/{a}', 'SubProcController@getFilteredIncidencias');
    Route::get('/subProc/getExcelIncidencias/{a}', 'SubProcController@getExcelIncidencias');
    /// POST ///
    Route::post('/subProc/filtraNotas', 'SubProcController@filtraNotas');
    Route::post('/subProc/filtraIncidencias', 'SubProcController@filtraIncidencias');

    /* JURIDICO */
    Route::get('/subProc/nuevaDenuncia', 'DenunciaController@nuevaDenuncia');
    
});

////////////////////
//* Denunciante *//
//////////////////
Route::group(['middleware' => ['role:Denunciante|SuperAdmin','auth']], function () {
    /// GET ///
    Route::get('/denunciante/nuevaDenuncia', 'DenunciaController@nuevaDenuncia');
    Route::get('/denunciante/listaSubAreas/{id}', 'IncidenciaController@getSubAreas');
    Route::get('/denunciante/listaSubTipos/{idA}/{idB}', 'DenunciaController@getSubTipos');
    Route::get('/denunciante/listaMunicipios/{id}', 'DenunciaController@getMunicipios');
    /// POST ///    
});

Route::post('/logoutColab', function(){
    Auth::logout();
    return redirect('/sintesisInformativa/inicio');
})->name('logoutColab');
Route::post('/logoutDeleg', function(){
    Auth::logout();
    return redirect('/delegado');
})->name('logoutDeleg');

/* Route::get('/aux', function () {    
    $dir = Directorio::all();    
    foreach($dir as $item){
        $ent = getNomEdo($item->idEntidad);
        $ent = str_replace(' ','',$ent);
        		//Reemplazamos la A y a
		$ent = str_replace(
            array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
            array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
            $ent
            );
     
            //Reemplazamos la E y e
            $ent = str_replace(
            array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
            array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
            $ent );
     
            //Reemplazamos la I y i
            $ent = str_replace(
            array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
            array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
            $ent );
     
            //Reemplazamos la O y o
            $ent = str_replace(
            array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
            array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
            $ent );
     
            //Reemplazamos la U y u
            $ent = str_replace(
            array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
            array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
            $ent );
     
            //Reemplazamos la N, n, C y c
            $ent = str_replace(
            array('Ñ', 'ñ', 'Ç', 'ç'),
            array('N', 'n', 'C', 'c'),
            $ent
            );
        $cad = 'del'.trim($ent);
        $item->usuario = $cad;
        $item->save();
        echo $cad.'<br>';
    }
}); */

/* Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
 */