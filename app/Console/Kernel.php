<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\Storage;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {        
        $schedule->call(function () {
            $var = app('App\Http\Controllers\NotaController')->getNotasFromApi();
            $cad = '['.date('Y-m-d H:i:s').']: Realizado';
            echo $cad.' - '.$var."\n";
            $file = "/var/www/html/sintesisinformativa/storage/logs/logRegAPI.txt";            
            file_put_contents($file,$cad.' - '.$var."\n",FILE_APPEND);            
        })->everyFiveMinutes();
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function(){
            $var = app('App\Http\Controllers\HelperController')->enviaSolInfo();
            $cad = '['.date('Y-m-d H:i:s').']:';
            echo $cad.' - '.$var."\n";
            $file = "/var/www/html/sintesisinformativa/storage/logs/logSendSolInfo.txt";            
            file_put_contents($file,$cad.' - '.$var."\n",FILE_APPEND); 
        })->everyMinute();//->dailyAt('09:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
