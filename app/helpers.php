<?php
use App\Entidad;
use App\Directorio;
use App\Area;
use Carbon\Carbon;
use App\Ley;

// Consultas //
/// obtinene los medios dada una entidad y una fecha //
function mediosControl($id, $a, $b)
{
    $mediosControl = DB::connection('mysql')->select("SELECT nota.idMedio, ct_medio.nomMedio 
                                FROM nota JOIN ct_medio
                                WHERE nota.idMedio = ct_medio.idMedio
                                AND nota.idEntidad = $id
                                AND nota.fcAlta BETWEEN '$a' AND '$b' 
                                AND nota.fcBorra IS NULL
                                ORDER BY ct_medio.idMedio ASC;");
    return $mediosControl;
}

// Helpers //
function getNomEdo($id)
{
    $entidad = Entidad::where('idEntidad', $id)->pluck('nomEntidad')->first();
    return $entidad;
}
function getRango($dt)
{
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(19, 15, 0);
        $fin = Carbon::today();
        $fcFin = $fin->setTime(19, 0, 0);
    } else {
        $ayer= Carbon::yesterday();
        $fcIni = $ayer->setTime(19, 15, 0);
        $hoy= Carbon::today();
        $fcFin = $hoy->setTime(19, 0, 0);
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(10, 0, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(9, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}
function getRangoM($dt)
{
    $ayer= Carbon::yesterday();
    $fcIni = $ayer->setTime(19, 15, 0);
    $hoy= Carbon::today();
    $fcFin = $hoy->setTime(9, 15, 0);
    
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(5, 45, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(9, 15, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}
function getRangoV($dt)
{
    $ayer= Carbon::today();
    $fcIni = $ayer->setTime(9, 30, 0);
    $hoy= Carbon::today();
    $fcFin = $hoy->setTime(19, 30, 0);
    
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(9, 30, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(5, 30, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getRangoInc($dt)
{
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today();
        $fcFin = $fin->setTime(7, 0, 0);
    } else {
        $ayer= Carbon::yesterday();
        $fcIni = $ayer->setTime(7, 05, 0);
        $hoy= Carbon::today();
        $fcFin = $hoy->setTime(7, 0, 0);
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getRangoResp($dt, $flg)
{    
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today();
        $fcFin = $fin->setTime(23, 30, 0);
    } else {
        if($flg == 's'){
            $ayer= Carbon::yesterday();
            $fcIni = $ayer->setTime(7, 05, 0);
            $hoy= Carbon::today()->subDay(1);
            $fcFin = $hoy->setTime(23, 0, 0);
        } elseif($flg == 'b'){
            $ayer= Carbon::yesterday()->addDay(1);
            $fcIni = $ayer->setTime(7, 05, 0);
            $hoy= Carbon::today();
            $fcFin = $hoy->setTime(23, 0, 0);
        }
        
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today();
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getRangoVerif($dt)
{
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(10, 0, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(9, 0, 0);
    } else {
        $ayer= Carbon::yesterday()->addDay(1);
        $fcIni = $ayer->setTime(10, 0, 0);
        $hoy= Carbon::today()->addDay(1);
        $fcFin = $hoy->setTime(9, 0, 0);
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(10, 0, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(9, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getRangoVerifInc($dt)
{
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    } else {
        $ayer= Carbon::yesterday()->addDay(1);
        $fcIni = $ayer->setTime(7, 05, 0);
        $hoy= Carbon::today()->addDay(1);
        $fcFin = $hoy->setTime(7, 0, 0);
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getRangoVerifResp($dt)
{
    if ($dt->dayOfWeek === Carbon::MONDAY) {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(3);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    } elseif ($dt->dayOfWeek === Carbon::FRIDAY) {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today();
        $fcFin = $fin->setTime(7, 0, 0);
    } else {
        $ayer= Carbon::yesterday()->addDay(1);
        $fcIni = $ayer->setTime(7, 05, 0);
        $hoy= Carbon::today()->addDay(1);
        $fcFin = $hoy->setTime(7, 0, 0);
    }
    if ($dt->toDateString() == '2020-11-03' || $dt->toDateString() == '2020-11-17' || $dt->toDateString() == '2021-03-16') {
        $hoy = Carbon::today()->addDay(1);
        $inicio = $hoy->subDays(4);
        $fcIni = $inicio->setTime(7, 05, 0);
        $fin = Carbon::today()->addDay(1);
        $fcFin = $fin->setTime(7, 0, 0);
    }
    $rangoArr = [
        'inicio'=>$fcIni->toDateTimeString(),
        'fin'=>$fcFin->toDateTimeString()
    ];
    return $rangoArr;
}

function getEntidad($mail)
{
    $entidad = str_replace('@profepa.gob.mx', '', $mail);
    $delegacion = Directorio::with(['entidadRel'])->where('usuario', $entidad)->first();
    return $delegacion;
}

function getMailEntidad($idEntidad)
{
    $mail = Directorio::where('idEntidad', $idEntidad)->pluck('correoElectronico');
    return $mail[0];
}
function getMailZona($idEntidad)
{
    $idEdosNorte = [1,2,3,8,5,6,10,11,14,18,19,24,25,26,32,28];
    $idEdosSur = [4,7,12,13,15,16,17,20,21,22,27,29,30,31,9,23];
    if (in_array($idEntidad, $idEdosNorte)) {
        $mail = 'lady.esquivel@profepa.gob.mx';
    } elseif (in_array($idEntidad, $idEdosSur)) {
        $mail = 'fernando.reyna@profepa.gob.mx';
    }
    return $mail;
}
function getArea($mail)
{
    $infoUser = Area::where('correoResponsable', $mail)->first();
    return $infoUser;
}

function cadToIdEntidad($a)
{
    switch ($a) {
        case "Aguascalientes":
            $id = 1;
            break;
        case "aguascalientes":
            $id = 1;
            break;
        case "Baja California":
            $id = 2;
            break;
        case "baja california":
            $id = 2;
            break;
        case "Baja California Sur":
            $id = 3;
            break;
        case "baja california sur":
            $id = 3;
            break;
        case "Campeche":
            $id = 4;
            break;
        case "campeche":
            $id = 4;
            break;
        case "Coahuila":
            $id = 5;
            break;
        case "coahuila":
            $id = 5;
            break;
        case "Colima":
            $id = 6;
            break;
        case "colima":
            $id = 6;
            break;
        case "Chiapas":
            $id = 7;
            break;
        case "chiapas":
            $id = 7;
            break;
        case "Chihuahua":
            $id = 8;
            break;
        case "chihuahua":
            $id = 8;
            break;
        case "Ciudad de México":
            $id = 9;
            break;
        case "ciudad de méxico":
            $id = 9;
            break;
        case "ciudad de mexico":
            $id = 9;
            break;
        case "CDMX":
            $id = 9;
            break;
        case "cdmx":
            $id = 9;
            break;
        case "Durango":
            $id = 10;
            break;
        case "durango":
            $id = 10;
            break;
        case "Guanajuato":
            $id = 11;
            break;
        case "guanajuato":
            $id = 11;
            break;
        case "Guerrero":
            $id = 12;
            break;
        case "guerrero":
            $id = 12;
            break;
        case "Hidalgo":
            $id = 13;
            break;
        case "hidalgo":
            $id = 13;
            break;
        case "Jalisco":
            $id = 14;
            break;
        case "jalisco":
            $id = 14;
            break;
        case "Estado de México":
            $id = 15;
            break;
        case "estado de méxico":
            $id = 15;
            break;
        case "estado de mexico":
            $id = 15;
            break;
        case "Michoacán":
            $id = 16;
            break;
        case "michoacán":
            $id = 16;
            break;
        case "michoacan":
            $id = 16;
            break;
        case "Morelos":
            $id = 17;
            break;
        case "morelos":
            $id = 17;
            break;
        case "Nayarit":
            $id = 18;
            break;
        case "nayarit":
            $id = 18;
            break;
        case "Nuevo León":
            $id = 19;
            break;
        case "nuevo león":
            $id = 19;
            break;
        case "nuevo leon":
            $id = 19;
            break;
        case "Oaxaca":
            $id = 20;
            break;
        case "oaxaca":
            $id = 20;
            break;
        case "Puebla":
            $id = 21;
            break;
        case "puebla":
            $id = 21;
            break;
        case "Querétaro":
            $id = 22;
            break;
        case "querétaro":
            $id = 22;
            break;
        case "queretaro":
            $id = 22;
            break;
        case "Quintana Roo":
            $id = 23;
            break;
        case "quintana roo":
            $id = 23;
            break;
        case "San Luis Potosí":
            $id = 24;
            break;
        case "san luis potosí":
            $id = 24;
            break;
        case "Sinaloa":
            $id = 25;
            break;
        case "sinaloa":
            $id = 25;
            break;
        case "Sonora":
            $id = 26;
            break;
        case "sonora":
            $id = 26;
            break;
        case "Tabasco":
            $id = 27;
            break;
        case "tabasco":
            $id = 27;
            break;
        case "Tamaulipas":
            $id = 28;
            break;
        case "tamaulipas":
            $id = 28;
            break;
        case "Tlaxcala":
            $id = 29;
            break;
        case "tlaxcala":
            $id = 29;
            break;
        case "Veracruz":
            $id = 30;
            break;
        case "veracruz":
            $id = 30;
            break;
        case "Yucatán":
            $id = 31;
            break;
        case "yucatán":
            $id = 31;
            break;
        case "Zacatecas":
            $id = 32;
            break;
        case "zacatecas":
            $id = 32;
            break;
        default:
            $id = 33;
    }
    //dd($id);
    return $id;
}

function cadToIdMedio($a)
{
    switch ($a) {
        case "PRENSA":
            $id = 1;
            break;
        case "RADIO":
            $id = 2;
            break;
        case "TV":
            $id = 2;
            break;
        case "INTERNET":
            $id = 3;
            break;
        case "SOCIAL":
            $id = 4;
            break;
        default:
            $id = 3;
    }
    return $id;
}

function encriptar($cad)
{
    $simple_string = $cad;
    $ciphering = "AES-128-CTR";
    $iv_length = openssl_cipher_iv_length($ciphering);
    $options = 0;
    $encryption_iv = '1234567891011121';
    $encryption_key = "sintesisInformativa";
    $encryption = openssl_encrypt(
        $simple_string,
        $ciphering,
        $encryption_key,
        $options,
        $encryption_iv
    );
    return $encryption;
}

function getPrefix($uri)
{
    $uri = explode('/',$uri);
    switch ($uri[0]) {
        case 'monitorZona':
            $prfx = 'monitorZona';
            break;
        case 'delegado':
            $prfx = 'delegado';
            break;
        default:
            $prfx = 'admin';
            break;
    }
    return $prfx;
}

function cadLeyToId($ley)
{
    $idLey = Ley::select('idLey')->where('ley',$ley)->first();
    return $idLey->idLey;
}