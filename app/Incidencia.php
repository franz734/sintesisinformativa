<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Config;
use Auth;
use Mail;
use DB;
use App\Propuesta;

class Incidencia extends Model
{
    protected $guarded = [];
    protected $table = 'incidencia';
    protected $connection = 'mysql';
    protected $primaryKey = 'idIncidencia';
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    public function entidadRel()
    {
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function areaRel()
    {
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel()
    {
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function materiaRel()
    {
        return $this->belongsTo('App\Materia', 'materia');
    }
    public function propuestaRel()
    {
        return $this->hasOne('App\Propuesta', 'idIncidencia', 'idIncidencia');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    public static function getIncidencias($a, $b)
    {
        $incidencias = Incidencia::with(['entidadRel', 'areaRel', 'SubAreaRel'])->whereBetween('fcCrea', [$a, $b])->get();
        return $incidencias;
    }
    public static function getAllIncidencias()
    {
        $incidencias = Incidencia::with(['entidadRel', 'propuestaRel'])->orderBy('idIncidencia', 'desc')->get();
        return $incidencias;
    }
    public static function getIncidecnias()
    {
        $incidencias = DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, count(incidencia.idIncidencia) AS incidencias 
                                    FROM ct_entidad LEFT JOIN incidencia
                                    ON ct_entidad.idEntidad = incidencia.idEntidad 
                                    AND incidencia.fcBorra  IS NULL
                                    GROUP BY ct_entidad.idEntidad ORDER BY incidencias DESC;");
        return $incidencias;
    }
    public static function getIncidecniasByDate($a, $b)
    {
        //$today = Carbon::now()->toDateString();
        $incidencias = DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, count(incidencia.idIncidencia) AS incidencias
                                    FROM ct_entidad JOIN incidencia
                                    ON ct_entidad.idEntidad = incidencia.idEntidad 
                                    AND incidencia.fcBorra  IS NULL
                                    AND incidencia.fcCrea BETWEEN '$a' AND '$b'
                                    GROUP BY ct_entidad.idEntidad ORDER BY incidencias DESC;");
        return $incidencias;
    }
    public static function getIncidenciasByEdoByDate($id, $a, $b)
    {
        $today = Carbon::now()->toDateString();
        $incidencias = Incidencia::with(['entidadRel'])->where('idEntidad', $id)->whereBetween('fcCrea', [$a, $b])->get();
        if ($incidencias->isEmpty()) {
            $flag = 'noIncidencias';
            return $flag;
        } else {
            return $incidencias;
        }
    }
    // total de incidencias anual dado un estado //
    public static function incidenciasAnioByEdo($idEntidad)
    {
        $from = date('Y') . '-01' . '-01';
        $to = date('Y') . '-12' . '-31';
        $incidenciasAnio = Incidencia::where('idEntidad', $idEntidad)->whereBetween('fcCrea', [$from, $to])->count();
        return $incidenciasAnio;
    }
    // incidencias dado un estado //
    public static function getIncidenciasByEdo($id)
    {
        $incidencias = Incidencia::with(['entidadRel'])->where('idEntidad', $id)->orderBy('idIncidencia', 'desc')->get();
        return $incidencias;
    }
    // total de incidencias anual dada una area //
    public static function incidenciasAnioByArea($idArea)
    {
        $from = date('Y') . '-01' . '-01';
        $to = date('Y') . '-12' . '-31';
        $incidenciasAnio = Incidencia::where('idArea', $idArea)->whereBetween('fcCrea', [$from, $to])->count();
        return $incidenciasAnio;
    }
    // todas las incidencias dada un area //
    public static function getAllIncidenciasByArea($idArea)
    {
        $incidencias = Incidencia::with(['entidadRel', 'propuestaRel'])->where('idArea', $idArea)->orderBy('idIncidencia', 'desc')->get();
        return $incidencias;
    }

    /* MODULO CONSULTAS */
    public static function getIncidenciasByKeyWord($keyWord)
    {
        $idEntidad = cadToIdEntidad($keyWord);
        $comp = ($idEntidad == 33) ? "WHERE (incidencia.tema LIKE '%$keyWord%'OR incidencia.problematica LIKE '%$keyWord%' OR incidencia.actuacion LIKE '%$keyWord%')" : "WHERE incidencia.idEntidad = $idEntidad";
        $incidencias = DB::select("SELECT incidencia.idIncidencia, incidencia.tema, incidencia.problematica, incidencia.fcSuceso, ct_entidad.nomEntidad
                                   FROM incidencia  join ct_entidad
                                   $comp
                                   AND incidencia.idEntidad = ct_entidad.idEntidad
                                   AND incidencia.fcBorra IS NULL
                                   ORDER BY incidencia.fcSuceso DESC, ct_entidad.nomEntidad,incidencia.idIncidencia");
        foreach ($incidencias as $incidencia) {
            $incidencia->nomEntidad = trim($incidencia->nomEntidad);
            $incidencia->fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        }
        return $incidencias;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
    // Crear //
    public static function crearIncidencia($request)
    {
        //dd($request->documento);
        $objProp = (object) [];
        $propuestaSend = null;
        if ($request->flgPropuesta == 1) {
            $tipoProp = $request->propuesta;
            switch ($tipoProp) {
                case 1:
                    $objProp->texto = $request->txtTweet;
                    break;
                case 2:
                    $objProp->titulo = $request->titBoletin;
                    $objProp->textHtml = $request->textoHtmlBoletin;
                    $objProp->texto = $request->textoBoletin;
                    break;
            }
        }
        //dd($objProp);
        $request->fechaSuc = Carbon::createFromFormat('d/m/Y', $request->fechaSuc)->format('Y-m-d H:i:s');
        $incidencia = Incidencia::create([
            'tema' => $request->tema,
            'materia' => $request->materia,
            'problematica' => $request->problematica,
            'actuacion' => $request->texto,
            'actuacionHtml' => $request->textoHtml,
            'acciones' => $request->acciones,
            'resumen' => $request->resumen,
            'evidencia' => $request->evidencia,
            'docIncidencia' => $request->documento,
            'fcSuceso' => $request->fechaSuc,
            'idEntidad' => $request->entidadFed,
            'idArea' => $request->area,
            'idSubArea' => $request->subArea,
            'accionVS' => $request->accionVS,
            'flgPropuesta' => $request->flgPropuesta
        ]);
        if ($incidencia->flgPropuesta == 1) {
            DB::beginTransaction();
            try {
                $propArrIn = [
                    'propuesta' => json_encode($objProp),
                    'idTipoPropuesta' => $request->propuesta,
                    'idIncidencia' => $incidencia->idIncidencia,
                    'idNota' => null,
                    'idRespuesta' => null,
                    'idRespuestaTemaRel' => null,
                    'idArea' => $request->area,
                    'idSubarea' => $request->subArea,
                    'idEntidad' => $request->entidadFed,
                    'statusRev' => null
                ];
                $propuesta = Propuesta::crearPropuesta($propArrIn);
            } catch (ValidationException $e) {
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg, 401);
            }
            DB::commit();
            $propuestaSend = Propuesta::where('idPropuesta', $propuesta->idPropuesta)->first();
        }
        $incidenciaSend = Incidencia::with(['entidadRel', 'areaRel', 'subAreaRel'])->where('idIncidencia', $incidencia->idIncidencia)->first();
        $mailEntidad = getMailEntidad($request->entidadFed);
        $dir = 'https://189.254.22.36/incidencias/verIncidencia/' . $incidenciaSend->idIncidencia;
        if ($incidenciaSend->docIncidencia != null) {
            $dirDoc = '/var/www/html/sintesisinformativa/public/' . $incidenciaSend->docIncidencia;
        } else {
            $dirDoc = '';
        }
        //dd($incidenciaSend, $dir, $dirDoc);
        $mailTo = [
            'blanca.mendoza@profepa.gob.mx',
            'raquel.soto@profepa.gob.mx',
            'florisel.santiago@profepa.gob.mx',
            'miguel.dorantes@profepa.gob.mx',
            'comunicacion.social@profepa.gob.mx',
            'lady.esquivel@profepa.gob.mx',
            'fernando.reyna@profepa.gob.mx',
            'nestor.guerrero@profepa.gob.mx'
        ];
        array_push($mailTo, $incidenciaSend->areaRel->correoResponsable);
        array_push($mailTo, $mailEntidad);
        //$copyTo = ['miguel.dorantes@profepa.gob.mx', 'comunicacion.social@profepa.gob.mx'];
        $subject = 'Incidencia registrada';
        Config::set('mail.encryption', 'tls');
        Config::set('mail.host', 'smtp.office365.com');
        Config::set('mail.port', '587');
        Config::set('mail.username', 'incidencias@profepa.gob.mx');
        Config::set('mail.password', '05bX&1X8SHDx');
        Config::set('mail.from', ['address' => 'incidencias@profepa.gob.mx', 'name' => 'Registro de Incidencias']);
        Mail::send('reader.correoIncidencia', ['incidencia' => $incidenciaSend, 'propuesta' => $propuestaSend, 'dir' => $dir], function ($message) use ($mailTo, $subject, $dirDoc) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject);
            if ($dirDoc != '') {
                $message->attach($dirDoc);
            }
        });
        return $incidencia;
    }
    /// Borrar ///
    public static function deleteIncidencia($request)
    {
        $incidencia = Incidencia::where('idIncidencia', $request->idIncidencia)->first();
        $incidencia->delete();
        return $incidencia;
    }
    /// Compartir ///
    static public function shareIncidencia($request)
    {
        //dd($request);                
        $incSend = Incidencia::with(['entidadRel', 'areaRel'])->where('idIncidencia', $request->id)->first();
        $mailTo = $request->mail;
        $subject = 'Incidencia';
        $userMail = Auth::user()->email;
        $copyTo = ['miguel.dorantes@profepa.gob.mx', $userMail];
        //dd($incSend);
        try {
            Mail::send('admin.correoComparteIncidencia', ['incidencia' => $incSend], function ($message) use ($mailTo, $subject, $copyTo) {
                $message->to($mailTo)->subject($subject)->cc($copyTo);
            });
        } catch (\Exception $e) {
            return response()->json('Error al enviar', 401);
        }
        return $incSend;
    }
}
