<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Referencia extends Model
{
    protected $guarded = [];
    protected $table = 'ct_referencia';
    protected $connection = 'mysql';
    protected $primaryKey = 'idReferencia';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    // Obtiene las referencias y su numero de notas dada una fecha //
    static public function referenciasNotasByDate($a, $b){              
        $mediosNotas =  DB::select("SELECT ct_referencia.idReferencia, ct_referencia.nomReferencia, COUNT(nota.idNota) AS notas
                                    FROM ct_referencia LEFT JOIN nota
                                    ON ct_referencia.idReferencia = nota.idReferencia
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.add = 1
                                    GROUP BY ct_referencia.idReferencia
                                    ORDER BY ct_referencia.idReferencia DESC;");     
    return $mediosNotas;
    }
    // Obtiene las referencias y su numero de notas dada una fecha y area //
    static public function referenciasNotasByAreaDate($a, $b, $area){              
        $mediosNotas =  DB::select("SELECT ct_referencia.idReferencia, ct_referencia.nomReferencia, COUNT(nota.idNota) AS notas
                                    FROM ct_referencia LEFT JOIN nota
                                    ON ct_referencia.idReferencia = nota.idReferencia
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idArea = '$area'
                                    AND nota.add = 1
                                    GROUP BY ct_referencia.idReferencia
                                    ORDER BY ct_referencia.idReferencia DESC;");     
    return $mediosNotas;
    }
    // Obtiene las referencias y su numero de notas dada una fecha y estados //
    static public function referenciasNotasByZonaDate($a, $b, $idEdos){       
        $idEdos = collect($idEdos)->implode(',');
        $mediosNotas =  DB::select("SELECT ct_referencia.idReferencia, ct_referencia.nomReferencia, COUNT(nota.idNota) AS notas
                                    FROM ct_referencia LEFT JOIN nota
                                    ON ct_referencia.idReferencia = nota.idReferencia
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idEntidad IN($idEdos)
                                    AND nota.add = 1
                                    GROUP BY ct_referencia.idReferencia
                                    ORDER BY ct_referencia.idReferencia DESC;");     
    return $mediosNotas;
    }
    // Obtiene las referencias e infoAmbiental dada una fecha
    static public function referenciasAmbientalByDate($a, $b){
        $refsAmbiental =  DB::select("SELECT ct_referencia.idReferencia, ct_referencia.nomReferencia, COUNT(infoAmbiental.idInfoAmbiental) AS cant
                                      FROM ct_referencia LEFT JOIN infoAmbiental
                                      ON ct_referencia.idReferencia = infoAmbiental.idReferencia
                                      WHERE infoAmbiental.fcAlta BETWEEN '$a' AND '$b'
                                      AND infoAmbiental.fcBorra IS NULL
                                      GROUP BY ct_referencia.idReferencia
                                      ORDER BY ct_referencia.idReferencia;");
        return $refsAmbiental;
    }
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
