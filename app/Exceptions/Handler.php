<?php

namespace App\Exceptions;

use Exception;
use Mail;
use Auth;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        if ($exception) {
            
            $data = [
                'exception' => $exception,
                'name' => (Auth::user() ) ? Auth::user()->name : "nombre",                
                "ip" => $_SERVER['REMOTE_ADDR']
            ];

            $classes = [
                "Illuminate\Auth\AuthenticationException",
                "Illuminate\Auth\Access\AuthorizationException",
                "Symfony\Component\HttpKernel\Exception\HttpException",
                "Symfony\Component\HttpKernel\Exception\NotFoundHttpException",
                "Illuminate\Database\Eloquent\ModelNotFoundException",
                "Illuminate\Validation\ValidationException",
                "Illuminate\Session\TokenMismatchException",
            ];

            if (!in_array(get_class($exception), $classes)) {
                //dd($data['exception']->getMessage());
                //return view('adminsie.mailReport', ['error'=>$data]);
                $destino = 'sefrancisco734@gmail.com';
                /* Mail::send('admin.mailReport', ['error'=>$data], function ($message) use($destino) {                    
                    $message->to($destino);                    
                    $message->subject('Error Sintesís Informativa');            
                }); */
            }
            
            
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }
}
