<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class TipoPropuesta extends Model
{
    protected $guarded = [];
    protected $table = 'ct_tipoPropuesta';
    protected $connection = 'mysql';
    protected $primaryKey = 'idTipoPropuesta';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';  
    const DELETED_AT = 'fcBorra';
    use SoftDeletes; 

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    
}
