<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class NotaEntidad extends Model
{
    protected $guarded = [];
    protected $table = 'notaEntidad';
    protected $connection = 'mysql';
    protected $primaryKey = 'idNotaEntidad';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    /* public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    } */
    public function notaRel(){
        return $this->belongsTo('App\Nota', 'idNota');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    // Obtiene todos los estados y su numero de notas //
    static public function entidadesNotas(){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $entidadesNotas =  DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, COUNT(notaEntidad.idNota) AS notas
                                    FROM ct_entidad LEFT JOIN notaEntidad
                                    ON ct_entidad.idEntidad = notaEntidad.idEntidad
                                    AND notaEntidad.fcBorra IS NULL                                    
                                    AND notaEntidad.fcAlta BETWEEN '$from' AND '$to'                                    
                                    GROUP BY ct_entidad.idEntidad ORDER BY notas DESC;");                                        
    return $entidadesNotas;
    }
    // Obtiene el estado con mas notas //
    static public function entidadMaxNotas(){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $entidadesNotas =  DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, COUNT(notaEntidad.idNota) AS notas
                                    FROM ct_entidad LEFT JOIN notaEntidad
                                    ON ct_entidad.idEntidad = notaEntidad.idEntidad
                                    AND notaEntidad.fcBorra IS NULL
                                    AND notaEntidad.fcAlta BETWEEN '$from' AND '$to'
                                    GROUP BY ct_entidad.idEntidad;");
        $a = collect($entidadesNotas);
        $max = $a->where('notas',$a->max('notas'))->first();    
    return $max;
    }
    // Obtiene todos los estados y su numero de notas dada una fecha //
    static public function entidadesNotasByDate($a, $b){   
        //dd($a);     
        $entidadesNotas =  DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, COUNT(notaEntidad.idNota) AS notas
                                    FROM ct_entidad LEFT JOIN notaEntidad
                                    ON ct_entidad.idEntidad = notaEntidad.idEntidad                                    
                                    WHERE notaEntidad.fcAlta BETWEEN '$a' AND '$b'  
                                    AND notaEntidad.fcBorra IS NULL
                                    AND notaEntidad.add = 1                               
                                    GROUP BY ct_entidad.idEntidad ORDER BY notas DESC;");                                        
    //dd($entidadesNotas);
    return $entidadesNotas;    
    }
    // Obtiene todos los estados y su numero de notas dada una fecha y area //
    static public function entidadesNotasByAreaDate($a, $b, $area){   
        //dd($a);     
        $entidadesNotas =  DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, COUNT(notaEntidad.idNota) AS notas
                                    FROM ct_entidad JOIN notaEntidad
                                    JOIN nota
                                    WHERE ct_entidad.idEntidad = notaEntidad.idEntidad   
                                    AND  notaEntidad.idNota = nota.idNota
                                    AND nota.idArea = '$area'                                 
                                    AND notaEntidad.fcAlta BETWEEN '$a' AND '$b'  
                                    AND notaEntidad.fcBorra IS NULL                                  
                                    GROUP BY ct_entidad.idEntidad ORDER BY notas DESC;");                                        
    //dd($entidadesNotas);
    return $entidadesNotas;    
    }
    // Obtiene todos los estados y su numero de notas dada una fecha y estados //
    static public function entidadesNotasByZonaDate($a, $b, $idEdos){   
        $idEdos = collect($idEdos)->implode(',');     
        $entidadesNotas =  DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, COUNT(notaEntidad.idNota) AS notas
                                    FROM ct_entidad JOIN notaEntidad
                                    JOIN nota
                                    WHERE ct_entidad.idEntidad = notaEntidad.idEntidad   
                                    AND  notaEntidad.idNota = nota.idNota
                                    AND nota.idEntidad IN($idEdos)                                 
                                    AND notaEntidad.fcAlta BETWEEN '$a' AND '$b'  
                                    AND notaEntidad.fcBorra IS NULL                                  
                                    GROUP BY ct_entidad.idEntidad ORDER BY notas DESC;");                                        
    //dd($entidadesNotas);
    return $entidadesNotas;    
    }
    
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
