<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Area extends Model
{
    protected $guarded = [];
    protected $table = 'ct_area';
    protected $connection = 'mysql';
    protected $primaryKey = 'idArea';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';  
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;  

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    // Obtiene todos las areas y su numero de notas //
    static public function areasNotas(){
        $areasNotas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(nota.idNota) AS notas
                                    FROM ct_area LEFT JOIN nota
                                    ON ct_area.idArea = nota.idArea
                                    GROUP BY ct_area.idArea;");                                        
    return $areasNotas;
    }
    // Obtiene el estado con mas notas //
    static public function areaMaxNotas(){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $areasNotas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(nota.idNota) AS notas
                                        FROM ct_area LEFT JOIN nota
                                        ON ct_area.idArea = nota.idArea
                                        AND nota.fcBorra IS NULL
                                        AND nota.fcAlta BETWEEN '$from' AND '$to'
                                        GROUP BY ct_area.idArea;");
        $a = collect($areasNotas);
        $max = $a->where('notas',$a->max('notas'))->first();    
    return $max;
    }
    // Obtiene todos las areas y su numero de notas dada una fecha //
    static public function areasNotasByDate($a, $b){
        $areasNotas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(nota.idNota) AS notas
                                    FROM ct_area LEFT JOIN nota
                                    ON ct_area.idArea = nota.idArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    GROUP BY ct_area.idArea;"); 
    //dd($areasNotas);
    return $areasNotas;
    }
    // Obtiene todos las areas y su numero de incidencias dada una fecha //
    static public function areasIncidenciasByDate($a, $b){
        $areasIncidencias =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(incidencia.idIncidencia) AS incidencias
                                    FROM ct_area LEFT JOIN incidencia
                                    ON ct_area.idArea = incidencia.idArea
                                    WHERE incidencia.fcCrea BETWEEN '$a' AND '$b'
                                    AND incidencia.fcBorra IS NULL
                                    GROUP BY ct_area.idArea;"); 
    //dd($areasNotas);
    return $areasIncidencias;
    }
    // Obtiene todos las areas y su numero de respuestas dada una fecha //
    static public function areasRespuestasByDate($a, $b){
        $areasRespuestas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(respuesta.idRespuesta) AS respuestas
                                    FROM ct_area LEFT JOIN respuesta
                                    ON ct_area.idArea = respuesta.idArea
                                    WHERE respuesta.fcCrea BETWEEN '$a' AND '$b'
                                    AND respuesta.fcBorra IS NULL
                                    GROUP BY ct_area.idArea;");     
    return $areasRespuestas;
    }
    // Obtiene todos las areas y su numero de notas dada una fecha //
    static public function areasNotasByAreaDate($a, $b, $area){
        $areasNotas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(nota.idNota) AS notas
                                    FROM ct_area LEFT JOIN nota
                                    ON ct_area.idArea = nota.idArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.idArea = '$area'
                                    GROUP BY ct_area.idArea;"); 
    //dd($areasNotas);
    return $areasNotas;
    }
    // Obtiene todos las areas y su numero de notas dada una fecha //
    static public function areasNotasByZonaDate($a, $b, $idEdos){
        $idEdos = collect($idEdos)->implode(',');
        $areasNotas =  DB::select("SELECT ct_area.idArea, ct_area.nomArea, COUNT(nota.idNota) AS notas
                                    FROM ct_area LEFT JOIN nota
                                    ON ct_area.idArea = nota.idArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.idEntidad IN($idEdos)
                                    GROUP BY ct_area.idArea;"); 
    //dd($areasNotas);
    return $areasNotas;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
