<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Entidad extends Model
{
    protected $guarded = [];
    protected $table = 'ct_entidad';
    protected $connection = 'mysql';
    protected $primaryKey = 'idEntidad';
    //public $timestamps = false;
    /* const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza'; */
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function entidadById($id){
        $entidad = Entidad::where('idEntidad', $id)->first();
        return $entidad;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
