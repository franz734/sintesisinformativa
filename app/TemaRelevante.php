<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Mail;

class TemaRelevante extends Model
{
    protected $guarded = [];
    protected $table = 'ct_temaRel';
    protected $connection = 'mysql';
    protected $primaryKey = 'idTemaRel';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel(){
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function respuestaRel(){
        return $this->hasOne('App\RespuestaTemaRelevante','idTemaRel', 'idTemaRel');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function totalTemasByEdo($idEntidad){
        $temasRel = TemaRelevante::where('idEntidad',$idEntidad)->where('status',null)->get();
        return $temasRel;
        
    }

    static public function getTemasRelByKeyWord($keyWord){
        $idEntidad = cadToIdEntidad($keyWord);        
        $comp = ($idEntidad == 33) ? "WHERE ct_temaRel.temaRel LIKE '%$keyWord%'" : "WHERE ct_temaRel.idEntidad = $idEntidad";
        //dd($comp);
        $temasRel = DB::select("SELECT ct_temaRel.idTemaRel, ct_temaRel.temaRel, ct_entidad.nomEntidad
                                FROM ct_temaRel join ct_entidad
                                $comp
                                AND ct_temaRel.idEntidad = ct_entidad.idEntidad
                                AND ct_temaRel.fcBorra IS NULL
                                ORDER BY ct_entidad.nomEntidad,ct_temaRel.idTemaRel");
        foreach($temasRel as $tema){
            $tema->nomEntidad = trim($tema->nomEntidad);            
        }
        return $temasRel;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
    // Crear //
    static public function crearTemaRel($request){
        //dd($request->all());
        if($request->entidadFed == 'null'){
            $request->entidadFed = null;
        }
        $temaRel = TemaRelevante::create([
            'temaRel'=>$request->temaRel,
            'idEntidad'=>$request->entidadFed,
            'status'=>null
        ]);
        $temaRelSend = TemaRelevante::with(['entidadRel'])->where('idTemaRel',$temaRel->idTemaRel)->first();
        if($request->entidadFed != null){
            $mailZona = getMailZona($request->entidadFed);
            $mailEntidad = getMailEntidad($request->entidadFed);
            $mailTo = [$mailZona, $mailEntidad, 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }
        else{
            $mailTo = ['lady.esquivel@profepa.gob.mx', 'fernando.reyna@profepa.gob.mx', 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }
        $subject = 'Nuevo Tema Relevante';
        Mail::send('admin.correoNuevoTemaRel',['temaRel'=> $temaRelSend], function($message) use($mailTo, $subject, $copyTo) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject)->cc($copyTo);                     
        });  
        return $temaRel;
    }
    // Actualizar //

    // Borrar //

    // Cerrrar //
    static public function cerrarTemaRel($idTema){
        $tema = TemaRelevante::where('idTemaRel',$idTema)->first();
        $tema->status = 2;
        $tema->save();
        return $tema;
    }
    // Abrir //
    static public function abrirTemaRel($idTema){
        $tema = TemaRelevante::where('idTemaRel',$idTema)->first();
        $tema->status = 1;
        $tema->save();
        return $tema;
    }
}
