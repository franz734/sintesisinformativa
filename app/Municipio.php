<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Municipio extends Model
{
    protected $guarded = [];
    protected $table = 'ct_municipio';
    protected $connection = 'mysql_den';
    protected $primaryKey = 'idMunicipio';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function getMunicipiosByEdo($id){        
        $municipios = Municipio::where('idEntidad',(int)$id)->get();        
        return $municipios;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
