<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Norma extends Model
{
    protected $guarded = [];
    protected $table = 'norma';
    protected $connection = 'mysql_nor';
    protected $primaryKey = 'idNorma';
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    public function leyRel()
    {
        return $this->belongsTo('App\Ley', 'idLey');
    }
    public function articuloRel()
    {
        return $this->belongsTo('App\Articulo', 'idArticulo');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /* MODULO CONSULTAS */
    public static function getNormasByKeyWord($keyWord, $flg=null)
    {
        $compSel = ($flg == null) ? ' cl.ley ,ca.articulo ,n.contenido' : 'cl.ley ,COUNT(ca.articulo) as cant';
        $compOrderGrup = ($flg == null) ? '' : 'GROUP BY cl.ley ORDER BY cl.ley';
        $normas = DB::connection('mysql_nor')->select("SELECT $compSel
                              FROM norma n
                              JOIN ct_ley cl 
                              JOIN ct_articulo ca 
                              WHERE n.contenido LIKE '%$keyWord%'
                              AND n.idLey = cl.idLey 
                              AND n.idArticulo = ca.idArticulo
                              $compOrderGrup");
        return $normas;
    }

    public static function getNormasByKeyWordIdLey($idLey, $keyWord=null)
    {
        $com = ($keyWord == null) ? 'WHERE n.idLey = cl.idLey' : "WHERE n.contenido LIKE '%$keyWord%' AND n.idLey = cl.idLey";
        $data = DB::connection('mysql_nor')
                    ->select("SELECT ca.articulo ,n.fraccion ,n.inciso ,n.contenido 
                          FROM normatividad.norma n
                          JOIN normatividad.ct_ley cl 
                          JOIN normatividad.ct_articulo ca 
                          $com 
                          AND n.idArticulo = ca.idArticulo
                          AND cl.idLey = $idLey");        
        return $data;
    }

    public static function getAllNormas()
    {
        $normas = DB::connection('mysql_nor')
                    ->select("SELECT cl.ley ,COUNT(n.idNorma) as cant
                            FROM normatividad.norma n
                            JOIN normatividad.ct_ley cl 
                            JOIN normatividad.ct_articulo ca 
                            WHERE n.idLey = cl.idLey 
                            AND n.idArticulo = ca.idArticulo 
                            GROUP BY cl.ley
                            ORDER BY cl.ley;");
        return $normas;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
