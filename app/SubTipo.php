<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\SubTipoParte;

class SubTipo extends Model
{
    protected $guarded = [];
    protected $table = 'ct_subTipo';
    protected $connection = 'mysql_den';
    protected $primaryKey = 'idSubTipo';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function getSubTipoByTipoPer($idA,$idB){     
        $ids = SubTipoParte::where('idTipoParte',$idB)->pluck('idSubTipo');           
        $subTipos = SubTipo::whereIn('idSubTipo',$ids)->where('idTipoPersona',$idA)->get();        
        //dd($subTipos);
        return $subTipos;
    }
    
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
