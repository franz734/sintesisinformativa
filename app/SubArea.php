<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class SubArea extends Model
{
    protected $guarded = [];
    protected $table = 'ct_subArea';
    protected $connection = 'mysql';
    protected $primaryKey = 'idSubArea';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';  
    const DELETED_AT = 'fcBorra';
    use SoftDeletes; 

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function getSubAreaByArea($id){        
        $subAreas = SubArea::where('idArea',(int)$id)->get();        
        return $subAreas;
    }
    static public function getSubAreasIncidenciasByDate($a,$b){
        $subAreasIncidencias =  DB::select("SELECT ct_subArea.idSubArea, ct_subArea.nomSubArea,ct_subArea.cveSubArea, COUNT(incidencia.idIncidencia) AS incidencias
                                    FROM ct_subArea LEFT JOIN incidencia
                                    ON ct_subArea.idSubArea = incidencia.idSubArea
                                    WHERE incidencia.fcCrea BETWEEN '$a' AND '$b'
                                    AND incidencia.fcBorra IS NULL
                                    GROUP BY ct_subArea.idSubArea;");         
        return $subAreasIncidencias;
    }
    static public function getSubAreasNotasByDate($a,$b){
        $subAreasNotas =  DB::select("SELECT ct_subArea.idSubArea, ct_subArea.cveSubArea, COUNT(nota.idNota) AS notas
                                    FROM ct_subArea LEFT JOIN nota
                                    ON ct_subArea.idSubArea = nota.idSubArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    GROUP BY ct_subArea.idSubArea;");         
        return $subAreasNotas;
    }
    static public function getSubAreasNotasByAreaDate($a,$b,$area){
        $subAreasNotas =  DB::select("SELECT ct_subArea.idSubArea, ct_subArea.cveSubArea, COUNT(nota.idNota) AS notas
                                    FROM ct_subArea LEFT JOIN nota
                                    ON ct_subArea.idSubArea = nota.idSubArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.idArea = '$area'
                                    GROUP BY ct_subArea.idSubArea;");         
        return $subAreasNotas;
    }
    static public function getSubAreasRespuestasByDate($a,$b){
        $subAreasRespuestas =  DB::select("SELECT ct_subArea.idSubArea, ct_subArea.nomSubArea,ct_subArea.cveSubArea, COUNT(respuesta.idRespuesta) AS respuestas
                                    FROM ct_subArea LEFT JOIN respuesta
                                    ON ct_subArea.idSubArea = respuesta.idSubArea
                                    WHERE respuesta.fcCrea BETWEEN '$a' AND '$b'
                                    AND respuesta.fcBorra IS NULL
                                    GROUP BY ct_subArea.idSubArea;");         
        return $subAreasRespuestas;
    }
    static public function getSubAreasNotasByZonaDate($a,$b,$idEdos){
        $idEdos = collect($idEdos)->implode(',');
        $subAreasNotas =  DB::select("SELECT ct_subArea.idSubArea, ct_subArea.cveSubArea, COUNT(nota.idNota) AS notas
                                    FROM ct_subArea LEFT JOIN nota
                                    ON ct_subArea.idSubArea = nota.idSubArea
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.idEntidad IN($idEdos)
                                    GROUP BY ct_subArea.idSubArea;");         
        return $subAreasNotas;
    }
}
