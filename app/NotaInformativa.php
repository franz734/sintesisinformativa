<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;
use Mail;
use DB;

class NotaInformativa extends Model
{
    protected $guarded = [];
    protected $table = 'notaInformativa';
    protected $connection = 'mysql';
    protected $primaryKey = 'idNotaInformativa';
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel()
    {
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function areaRel()
    {
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel()
    {
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    // Obtiene todas las notas informativas //
    static public function getAllNotasInfo()
    {
        $notas = NotaInformativa::with(['entidadRel', 'areaRel', 'subAreaRel'])->orderBy('idNotaInformativa', 'desc')->get();
        return $notas;
    }

    // obtienen una nota informativa dado un id //
    static public function getNotaInfoById($idNotaInfo)
    {
        $nota = NotaInformativa::with(['entidadRel', 'areaRel', 'subAreaRel'])->where('idNotaInformativa', $idNotaInfo)->first();
        $nota->entidadRel->nomEntidad = trim($nota->entidadRel->nomEntidad);
        return $nota;
    }

    // Notas informativas dada una entidad //
    static public function totalNotasInfoByEdo($idEntidad)
    {
        $notasInfo = NotaInformativa::with(['entidadRel', 'areaRel', 'subAreaRel'])->where('idEntidad', $idEntidad)->get();
        return $notasInfo;
    }

    /* MODULO CONSULTAS */
    static public function getNotasInfoByKeyWord($keyWord)
    {
        $idEntidad = cadToIdEntidad($keyWord);
        $comp = ($idEntidad == 33) ? "WHERE (notaInformativa.encabezado LIKE '%$keyWord%'OR notaInformativa.texto LIKE '%$keyWord%')" : "WHERE notaInformativa.idEntidad = $idEntidad";
        $notasInfo = DB::select("SELECT notaInformativa.idNotaInformativa , notaInformativa.encabezado ,notaInformativa.texto 
                     ,ct_entidad.nomEntidad ,notaInformativa.fcSuceso 
                     FROM notaInformativa JOIN ct_entidad
                     $comp
                     AND notaInformativa.idEntidad = ct_entidad.idEntidad
                     AND notaInformativa.fcBorra IS NULL
                     ORDER BY notaInformativa.fcSuceso DESC, ct_entidad.nomEntidad,notaInformativa.idNotaInformativa");
        foreach ($notasInfo as $nota) {
            $nota->nomEntidad = trim($nota->nomEntidad);
            $nota->fcSuceso = Carbon::parse($nota->fcSuceso)->locale('es')->isoFormat('LL');;
        }
        return $notasInfo;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    //Crear//
    static public function crearNotaInformativa($request)
    {
        $name = Auth::user()->name;
        $request->fechaSuc = Carbon::createFromFormat('d/m/Y', $request->fechaSuc)->format('Y-m-d H:i:s');
        $notaInfo = NotaInformativa::create([
            'encabezado' => $request->encabezado,
            //'resumen'=>$request->resumen,
            'texto' => $request->texto,
            'idEntidad' => $request->entidadFed,
            'idArea' => $request->subProc,
            'idSubArea' => $request->subArea,
            'imgNota' => $request->imgNota,
            'fcSuceso' => $request->fechaSuc,
            'usuario' => $name
        ]);
        $subject = 'Nota Informativa Registrada';
        $mailTo = 'miguel.dorantes@profepa.gob.mx';
        //dd($notaInfo);
        try {
            Mail::send('admin.correoNuevaNotaInfo', ['nota' => $notaInfo], function ($message) use ($mailTo, $subject) {
                $message->to($mailTo)->subject($subject);
            });
        } catch (\Exception $e) {
            return response()->json('Error al enviar', 401);
        }
        return $notaInfo;
    }

    // Compartir //
    static public function shareNotaInfo($request)
    {
        //dd($request);                
        $notaSend = NotaInformativa::with(['entidadRel', 'areaRel'])->where('idNotaInformativa', $request->id)->first();
        $mailTo = $request->mail;
        $subject = 'Nota informativa';
        $userMail = Auth::user()->email;
        $copyTo = ['miguel.dorantes@profepa.gob.mx', $userMail];
        try {
            Mail::send('admin.correoComparteNotaInfo', ['nota' => $notaSend], function ($message) use ($mailTo, $subject, $copyTo) {
                $message->to($mailTo)->subject($subject)->cc($copyTo);
            });
        } catch (\Exception $e) {
            return response()->json('Error al enviar', 401);
        }
        return $notaSend;
    }
}
