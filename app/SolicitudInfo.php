<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
use Mail;

class SolicitudInfo extends Model
{
    protected $guarded = [];
    protected $table = 'solicitudInfo';
    protected $connection = 'mysql';
    protected $primaryKey = 'idSolicitudInfo';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function notaRel(){
        return $this->belongsTo('App\Nota', 'idNota');
    }
    public function temaRelevanteRel(){
        return $this->belongsTo('App\TemaRelevante', 'idTemaRel');
    }
    public function incidenciaRel(){
        return $this->belongsTo('App\Incidencia', 'idIncidencia');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    // Crear //
    static public function crearSolicitud($request){
        //dd($request->all());
        switch ($request->task) {
            case 'nota':
                $indx = 'idNota';
                break;
            case 'temaRel':
                $indx = 'idTemaRel';
                break;            
        }
        //dd($indx, $request->id) ;
        $solicitud = SolicitudInfo::create([        
            $indx=>$request->id,
            'fcSolicita'=>$request->fecha
        ]);
        return $solicitud;
    }
}
