<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;
use DB;

class Presentacion extends Model
{
    protected $guarded = [];
    protected $table = 'presentacion';
    protected $connection = 'mysql';
    protected $primaryKey = 'idPresentacion';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    public static function getPresentacionesByKeyWord($keyWord)
    {
        $presentaciones = DB::select("SELECT titulo ,texto , pathFile FROM presentacion
                           WHERE (titulo LIKE '%$keyWord%' OR texto LIKE '%$keyWord%')");
        foreach($presentaciones as $item){
            $item->pathFile = json_decode($item->pathFile);
        }
        return $presentaciones;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    static public function creaPresentacion($request)
    {
        $name = Auth::user()->name;
        //dd($request->all(), $name);
        $presentacion = Presentacion::create([
            'titulo'=>$request->titulo,
            'texto'=>$request->texto,
            'pathFile'=>$request->pathFile,
            'usuario'=>$name
        ]);
        return $presentacion;
    }
}
