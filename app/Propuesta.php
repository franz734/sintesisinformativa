<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;
use Mail;
use DB;

class Propuesta extends Model
{
    protected $guarded = [];
    protected $table = 'propuesta';
    protected $connection = 'mysql';
    protected $primaryKey = 'idPropuesta';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel(){
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function tipoPropRel(){
        return $this->belongsTo('App\TipoPropuesta', 'idTipoPropuesta');
    }
    public function incidenciaRel(){
        return $this->belongsTo('App\Incidencia', 'idIncidencia');
    }
    public function notaRel(){
        return $this->belongsTo('App\Nota', 'idNota');
    }
    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /////////////////////
    ///*** Metodos ***///
    ////////////////////
    //crear//
    static public function crearPropuesta($propArrIn){
        //dd($propArrIn);
        $propuesta = Propuesta::create([
            'propuesta'=>$propArrIn['propuesta'],
            'idTipoPropuesta'=>$propArrIn['idTipoPropuesta'],
            'idIncidencia'=>$propArrIn['idIncidencia'],
            'idNota'=>$propArrIn['idNota'],
            'idArea'=>$propArrIn['idArea'],
            'idSubarea'=>$propArrIn['idSubarea'],
            'idEntidad'=>$propArrIn['idEntidad'],
            'idRespuesta'=>$propArrIn['idRespuesta'],
            'statusRev'=>$propArrIn['statusRev']
        ]);
        return $propuesta;
    }
}
