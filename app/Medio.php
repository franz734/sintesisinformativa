<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Medio extends Model
{
    protected $guarded = [];
    protected $table = 'ct_medio';
    protected $connection = 'mysql';
    protected $primaryKey = 'idMedio';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;    
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    // Obtiene los medios y su numero de notas dada una fecha //
    static public function mediosNotasByDate($a, $b){              
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.add = 1
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    // Obtiene los medios y su numero de notas dada una fecha y una referencia //
    static public function mediosNotasByDateAndRef($a, $b, $c){              
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.add = 1
                                    AND nota.idReferencia = $c
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    // Obtiene los medios y su numero de notas dada una fecha //
    static public function mediosNotasByAreaDate($a, $b, $area){              
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idArea = '$area'
                                    AND nota.add = 1
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    // Obtiene los medios y su numero de notas dada una fecha y una referencia //
    static public function mediosNotasByAreaDateAndRef($a, $b, $c, $area){              
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idReferencia = $c
                                    AND nota.idArea = '$area'
                                    AND nota.add = 1
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    // Obtiene los medios y su numero de notas dada una fecha y estados //
    static public function mediosNotasByZonaDateAndRef($a, $b, $c, $idEdos){     
        $idEdos = collect($idEdos)->implode(',');         
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idReferencia = $c
                                    AND nota.idEntidad IN($idEdos)
                                    AND nota.add = 1
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    // Obtiene los medios y su numero de notas dada una fecha //
    static public function mediosNotasByZonaDate($a, $b, $idEdos){ 
        $idEdos = collect($idEdos)->implode(',');             
        $mediosNotas =  DB::select("SELECT ct_medio.idMedio, ct_medio.nomMedio, COUNT(nota.idNota) AS notas
                                    FROM ct_medio LEFT JOIN nota
                                    ON ct_medio.idMedio = nota.idMedio
                                    WHERE nota.fcAlta BETWEEN '$a' AND '$b'
                                    AND nota.fcBorra IS NULL
                                    AND nota.api IS NULL
                                    AND nota.idEntidad IN($idEdos)
                                    AND nota.add = 1
                                    GROUP BY ct_medio.idMedio;");     
    return $mediosNotas;
    }
    
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
}
