<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Origen;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\TipoPersona;
use App\SubTipo;
use App\Municipio;


class DenunciaController extends Controller
{
    public function nuevaDenuncia(){
        //dd('hola denunciante');
        if(session('idArea') != 5){
            return redirect('/subProc/inicio');
        }
        $fuentes = Origen::all();
        $areas = Area::where([['idArea','!=', 4],['idArea','!=', 2],['idArea','!=', 5]])->get();
        $tiposPer = TipoPersona::all();
        $entidades = Entidad::all();
        //dd($fuentes);
        return view('subProc.Jur.denunciaForm',['fuentes'=>$fuentes, 'areas'=>$areas, 'tiposPer'=>$tiposPer,
                                                'entidades'=>$entidades]);
    }
    public function getSubTipos($idA, $idB){        
        $subtipos = SubTipo::getSubTipoByTipoPer($idA,$idB);
        return $subtipos;
    } 
    public function getMunicipios($id){        
        $municipios = Municipio::getMunicipiosByEdo($id);
        return $municipios;
    }
}
