<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Entidad;
use App\Area;
use App\Tipo;


class EntidadController extends Controller
{
    // Mostrar notas segun estado //
    public function entidad($id){
        $entidad = Entidad::entidadById($id);
        $areas = Area::all();
        $tipos = Tipo::all();
        return view('admin.entidad', ['entidad'=>$entidad, 'tipo'=>$tipos, 'areas'=>$areas]);
    }

}
