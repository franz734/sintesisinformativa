<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use App\Exports\generalExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Entidad;
use App\Area;
use App\Medio;
use App\Tipo;
use App\Nota;
use App\NotaEntidad;
use App\Fuente;
use App\Referencia;
use App\Suceso;
use App\Lugar;

class NotaController extends Controller
{
    ////////////////////////
    ///***   NOTAS   ***///
    ///////////////////////

    /// Panel de inicio ///
    public function inicio(){
        $ejeX = [];
        $ejeY = [];
        $entidades = NotaEntidad::entidadesNotas();
        $notasAnio = Nota::notasAnio();
        $entidadMaxNotas = NotaEntidad::entidadMaxNotas();
        $areaMaxNotas= Area::areaMaxNotas();
        foreach($entidades as $entidad){
            array_push($ejeX,$entidad->nomEntidad);
            array_push($ejeY,$entidad->notas);
        }        
        //dd($entidades);
        return view('admin.panel', ['notasAnio'=>$notasAnio, 'entidadMaxNotas'=>$entidadMaxNotas,
                                    'areaMaxNotas'=>$areaMaxNotas, 'entidades'=>$entidades,
                                    'ejeX'=>$ejeX, 'ejeY'=>$ejeY]);
    }
    /// Mostrar todas las notas ///
    public function notas(){
        $tipos = Tipo::all();
        $entidades = Entidad::all();
        $areas = Area::all();
        return view('admin.admstrNotas',['tipo'=>$tipos,'entidades'=>$entidades,'areas'=>$areas]);
    }
    /// Datatable Notas ///
    public function getNotas(){
        $notas = Nota::getAllNotas();
        //$notas = Nota::all();
        //dd($notas);
        return datatables()->of($notas)->make(true);
    }
    /// Datatable notas dado un estado ///
    public function notasByEdo($id){
        $notas = Nota::getNotasByEdo($id);
        return datatables()->of($notas)->make(true);
    }
    /// Datatable notas dado un estado y area ///
    public function notasByEdoArea($idEdo, $idArea){        
        $notas = Nota::getNotasByEdoArea($idEdo, $idArea);
        return datatables()->of($notas)->make(true);
    }
    // Nueva nota //
    public function nuevaNota(){
        $ahora = Carbon::now();
        $ini = Carbon::today()->setTime(9,0,0);
        $tope = Carbon::today()->setTime(9,10,0);  
        //dd($ahora);
        if($ahora->lte($tope) && $ahora->gte($ini)){
            return view('admin.noNueva');
        }
        $medios = Medio::all();
        $areas = Area::all();
        $entidades = Entidad::all();        
        $fuentes = Fuente::all();
        $referencias = Referencia::whereIn('idReferencia',[1,2])->get();
        $sucesos = Suceso::all();
        $lugares = Lugar::all();
        return view('admin.notaForm', ['medios'=>$medios, 'areas'=>$areas, 'entidades'=>$entidades, 
                                       'fuentes'=>$fuentes, 'referencias'=>$referencias,
                                       'sucesos'=>$sucesos, 'lugares'=>$lugares]);
    }
    // Guarda nota //
    public function guardaNota(Request $request){     
        //dd($request->all());
        $request->add = ($request->has('addPdf')) ? (int)$request->addPdf : 0;        
        if(!$request->has('subArea')){
            $request->subArea = NULL;
        }        
        if($request->flagNvaFnte == 1){
            $fuente = Fuente::create([
                'nomFuente'=>$request->fuente,
            ]);
            $request->fuente = $fuente->nomFuente;
        }
        else{
            $fuente = Fuente::where('idFuente',$request->fuente)->first();
            $request->fuente = $fuente->nomFuente;
        }
        if($request->flagNvoSuceso){
            $suceso = Suceso::create([
                'nomSuceso'=>$request->suceso,
                'idEntidad'=>$request->entidadFed
            ]);            
            $request->suceso = $suceso->idSuceso;            
        }
        else{
            $suceso = Suceso::where('idSuceso',$request->suceso)->first();
            $request->suceso = $suceso->idSuceso;
        }
        if($request->flagNvoLugar){
            $lugar = Lugar::create([
                'nomLugar'=>$request->lugar,
                'idEntidad'=>$request->entidadFed
            ]);
            $request->lugar = $lugar->idLugar;            
        }
        else{
            $lugar = Lugar::where('idLugar',$request->lugar)->first();
            $request->lugar = $lugar->idLugar;
        }
        if($request->medio == '2' || $request->medio == '4'){            
            $hoy = Carbon::today()->toDateString();
            $imgField = ($request->medio == '4') ? 'imgRedSoc' : 'imgTv';
            $imgData = base64_decode($request->$imgField);        
            $destinationPath ='public/imgNotas/'.$hoy;
            $filename = time() . '.png' ;
            $imgNota = Storage::put($destinationPath.'/'.$filename, $imgData);                        
            $request->imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename;
        }        
        /* if($request->medio == '1'){
            $ext = ['pdf'];
            if($request->hasFile('documento')){
                $extension = $request->file('documento')->getClientOriginalExtension();
                if(!in_array($extension,$ext)){
                    $msg = 'Formato no valido';
                    return response()->json($msg,401);
                }
                else{
                    $file = $request->file('documento');
                    $hoy = Carbon::today()->toDateString();
                    //$destinationPath = storage_path('app/public/imgNotas/'.$hoy);
                    $destinationPath ='storage/imgNotas/'.$hoy;
                    $filename = time() . '.' . $request->file('documento')->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);  
                    $request->imgNota = $destinationPath.'/'.$filename;                        
                    //$request->imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename;
                }
            }
            else{
                $msg = 'No se agregó anexo';
                return response()->json($msg,401);
            }
        } */ 
        DB::beginTransaction();
        try{
            $nota = Nota::crearNota($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($nota->idNota,200);   
        //dd($request->all());
    }
    // Edita nota //
    public function editaNota($id){
        $nota = Nota::getNotaById($id);
        $medios = Medio::all();
        $areas = Area::all();
        $entidades = Entidad::all();
        //dd($nota);
        return view('admin.editaNota',['nota'=>$nota, 'medios'=>$medios, 'areas'=>$areas,
                                        'entidades'=>$entidades]);
    }
    public function modificaNota(Request $request){
        //dd($request->all());
        $hoy = Carbon::today()->toDateString();
        $idMedio = Nota::where('idNota',$request->idNota)->first();        
        $imgField = ($idMedio->idMedio == 4) ? 'imgRedSoc' : 'imgTv';        
        if(!is_null($request->$imgField)){            
            $imgData = base64_decode($request->$imgField);        
            $destinationPath ='public/imgNotas/'.$hoy;
            $filename = time() . '.png' ;
            $imgNota = Storage::put($destinationPath.'/'.$filename, $imgData);                        
            $request->imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename;
        }
        else{
            $request->imgNota = '';
        }
        DB::beginTransaction();
        try{
            $nota = Nota::updateNota($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($nota->idNota,200);
    }
    // Borra Nota //
    public function borraNota(Request $request){
        //dd($request->idNota);
        DB::beginTransaction();
        try{
            $nota = Nota::deleteNota($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo borrar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'borrada';
        return response()->json($msg,200);
    }
    // Asigna tipo //
    public function asignaTipo(Request $request){        
        $nota = Nota::where('idNota', $request->idNota)->first();        
        $nota->idTipo = $request->idTipo;
        $nota->save();
        $msg = 'modificado';
        return response()->json($msg,200);
    }
    // Envia nota //
    public function enviaNota(Request $request){
        //dd($request->all());
        $nota = Nota::sendMailNota($request);
        $msg = 'enviada';
        return response()->json($msg,200);
    }
    // Agrega nota extemporanea //
    public function notaExtemporanea(){                 
        $medios = Medio::all();
        $areas = Area::all();
        $entidades = Entidad::all();        
        return view('admin.notaExtForm', ['medios'=>$medios, 'areas'=>$areas, 'entidades'=>$entidades]);
    }
    // Notas desde CSV //
    public function notasFromCSV(){
        return view('admin.notasFromCSV');
    }
    public function cargaCSV(Request $request){
        //dd($request->all());
        if(!$request->hasFile('documento')){
            $msg = 'No se Agregó Archivo';
            return response()->json($msg,401);
        }
        else{
            //dd($request->all());
            $ext = ['csv'];
            $extension = $request->file('documento')->getClientOriginalExtension();
            if(!in_array($extension,$ext)){
                $msg = 'Formato no valido';
                return response()->json($msg,401);
            }
            else{
                $file = $request->file('documento');
                $hoy = Carbon::today()->toDateString();
                //$destinationPath = storage_path('app/public/imgNotas/'.$hoy);
                $destinationPath ='storage/csvNotas/'.$hoy;
                $filename = 'notas'.$hoy.'_'.time(). '.' . $request->file('documento')->getClientOriginalExtension();
                $file->move($destinationPath, $filename);                  
            }
            //dd($destinationPath.'/'.$filename);
            $notasArr = [];
            $fileNotas = fopen($destinationPath.'/'.$filename,'r');
            while (($line = fgetcsv($fileNotas)) !== FALSE) {    
                array_push($notasArr,$line);
            }
            array_shift($notasArr);
            //dd($notasArr);
            $notasArrClean = [];
            foreach($notasArr as $key){
                if(!empty($key[0])){
                    $key[0]=utf8_encode($key[0]);
                    $key[1]=utf8_encode($key[1]);
                    $key[2]=utf8_encode($key[2]);
                    $key[4]=utf8_encode($key[4]);
                    array_push($notasArrClean,$key);
                }
            }
            //dd($notasArrClean);
            $notasArrM = [];
            foreach($notasArrClean as $nota){                
                $nota[4] = cadToIdEntidad(trim($nota[4]));
                $nota[1] = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, utf8_decode($nota[1]));   
                $nota[1] = str_replace("\n",'',$nota[1]);           
                $nota[1] = trim(utf8_encode($nota[1]));
                array_push($notasArrM,$nota);
            }
            //dd($notasArrM);
            DB::beginTransaction();
            try{
                $nota = Nota::crearNotaCSV($notasArrM);
            }
            catch(ValidationException $e){
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg,401);
            }
            DB::commit();
            $msg = 'Se cargaron '.$nota.' notas';
            return response()->json($msg,200);
        }
    }
    // Completa Notas //
    public function completaNotasCSV(){
        return view('admin.completaNotasCSV');
    }
     /// Datatable Notas ///
     public function getNotasCVS(){
        $notas = Nota::getAllNotasCSV();        
        return datatables()->of($notas)->make(true);
    }
    // Edita nota //
    public function editaNotaCSV($id){
        $nota = Nota::getNotaById($id);
        $medios = Medio::all();
        $areas = Area::all();
        $referencias = Referencia::whereIn('idReferencia',[1,2])->get();
        $sucesos = Suceso::where('idEntidad',$nota->idEntidad)->orderBy('nomSuceso')->get();
        $lugares = Lugar::where('idEntidad',$nota->idEntidad)->orderBy('nomLugar')->get();
        //$entidades = Entidad::all();
        //dd($nota);
        return view('admin.editaNotaCSV',['nota'=>$nota, 'medios'=>$medios, 'areas'=>$areas,
                                        'referencias'=>$referencias, 'sucesos'=>$sucesos, 'lugares'=>$lugares]);
    }
    public function modificaNotaCSV(Request $request){
        //dd($request->all());
        $request->add = ($request->has('addPdf')) ? (int)$request->addPdf : 0;
        $idEntidad = Nota::where('idNota',$request->idNota)->pluck('idEntidad');        
        if(!$request->has('subArea')){
            $request->subArea = NULL;
        }
        if($request->flagNvoSuceso){
            $suceso = Suceso::create([
                'nomSuceso'=>$request->suceso,
                'idEntidad'=>$idEntidad[0]
            ]);            
            $request->suceso = $suceso->idSuceso;            
        }
        else{
            $suceso = Suceso::where('idSuceso',$request->suceso)->first();
            $request->suceso = $suceso->idSuceso;
        }
        if($request->flagNvoLugar){
            $lugar = Lugar::create([
                'nomLugar'=>$request->lugar,
                'idEntidad'=>$idEntidad[0]
            ]);
            $request->lugar = $lugar->idLugar;            
        }
        else{
            $lugar = Lugar::where('idLugar',$request->lugar)->first();
            $request->lugar = $lugar->idLugar;
        }                                       
        if($request->medio == '2' || $request->medio == '4'){            
            $hoy = Carbon::today()->toDateString();
            $imgField = ($request->medio == '4') ? 'imgRedSoc' : 'imgTv';
            $imgData = base64_decode($request->$imgField);        
            $destinationPath ='public/imgNotas/'.$hoy;
            $filename = time() . '.png' ;
            $imgNota = Storage::put($destinationPath.'/'.$filename, $imgData);                        
            $request->imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename;
        }    
        /* if($request->medio == '1'){
            $ext = ['pdf'];
            if($request->hasFile('documento')){
                $extension = $request->file('documento')->getClientOriginalExtension();
                if(!in_array($extension,$ext)){
                    $msg = 'Formato no valido';
                    return response()->json($msg,401);
                }
                else{
                    $file = $request->file('documento');
                    $hoy = Carbon::today()->toDateString();
                    //$destinationPath = storage_path('app/public/imgNotas/'.$hoy);
                    $destinationPath ='storage/imgNotas/'.$hoy;
                    $filename = time() . '.' . $request->file('documento')->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);  
                    $request->imgNota = $destinationPath.'/'.$filename;                        
                    //$request->imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename;
                }
            }
            else{
                $msg = 'No se agregó anexo';
                return response()->json($msg,401);
            }
        } */ 
        DB::beginTransaction();
        try{
            $nota = Nota::updateNotaCSV($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        //dd($nota);
        return response()->json($nota->idNota,200);
    }
    /// Filtro de Notas ///
    public function filtraNotas(Request $request){
        //dd($request->all());
        $idsArr = [];
        $ids = Nota::query();
        if($request->has('entidadFed')){
            $ids = $ids->where('idEntidad',$request->entidadFed);
        }
        if($request->has('lugar')){
            $ids = $ids->where('idLugar',$request->lugar);
        }
        if($request->has('categoria')){
            $ids = $ids->where('categoria',$request->categoria);
        }
        if($request->has('area')){
            $ids = $ids->where('idArea',$request->area);
        }
        if($request->has('subArea')){
            $ids = $ids->where('idSubArea',$request->subArea);
        }
        if($request->encabezado != null){
            $ids = $ids->where('encabezado', 'like','%'.$request->encabezado.'%');
        }
        if($request->texto != null){
            $ids = $ids->where('texto', 'like','%'.$request->texto.'%');
        }
        if($request->datefilter != null){            
            $fcAlta = explode('-', $request->datefilter);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcAlta',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->get('idNota');
        foreach($ids as $id){
            array_push($idsArr,$id->idNota);
        }
        //dd($idsArr);
        if(empty($idsArr)){
            $msg = 'Sin Resultados';
            return response()->json($msg,401);
        }
        else{
            return response()->json($idsArr,200);
        }
    }
    /// Notas filtradas ///
    public function getFilteredNotas($ids){
        $idsArr = explode(',',$ids);
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('csv',null)->where('api',null)->whereIn('idNota',$idsArr)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }
        return datatables()->of($notas)->make(true);
    }
    /// Excel Notas ///
    public function getExcelNotas($params){
        $params = json_decode($params);    
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = Nota::query();
        if($params->entidad != null || $params->entidad != ''){
            $ids = $ids->where('idEntidad',$params->entidad);
        }
        if($params->lugar != null || $params->lugar != ''){
            $ids = $ids->where('idLugar',$params->lugar);
        }
        if($params->categoria != null || $params->categoria != ''){
            $ids = $ids->where('categoria',$params->categoria);
        }
        if($params->area != null || $params->area != ''){
            $ids = $ids->where('idArea',$params->area);
        }
        if($params->subArea != null || $params->subArea != ''){
            $ids = $ids->where('idSubArea',$params->subArea);
        }
        if($params->encabezado != null || $params->encabezado != ''){
            $ids = $ids->where('encabezado', 'like','%'.$params->encabezado.'%');
        }
        if($params->texto != null || $params->texto != ''){
            $ids = $ids->where('texto', 'like','%'.$params->texto.'%');
        }
        if($params->datefilter != null || $params->datefilter != ''){            
            $fcAlta = explode('-', $params->datefilter);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcAlta',[$fcAlta[0], $fcAlta[1]]);
        }        
        $ids = $ids->get('idNota');
        foreach($ids as $id){
            array_push($idsArr,$id->idNota);
        } 
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel','subAreaRel','sucesoRel','lugarRel'])->where('csv',null)->where('api',null)->whereIn('idNota',$idsArr)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }                
        foreach($notas as $item){
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;
            $lugar = ($item->lugarRel == null) ? '' : $item->lugarRel->nomLugar;
            $suceso = ($item->sucesoRel == null) ? '' : $item->sucesoRel->nomSuceso;
            $link = ($item->link == null) ? '' : $item->link;
            $itemIn['idNota'] = $item->idNota;
            $itemIn['encabezado'] = $item->encabezado;
            $itemIn['texto'] = $item->texto;
            $itemIn['fuente'] = $item->fuente;
            $itemIn['medio'] = $item->medioRel->nomMedio;
            $itemIn['link'] = $link;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);
            $itemIn['lugar'] = $lugar;
            $itemIn['suceso'] = $suceso;
            $itemIn['area'] = $item->areaRel->nomArea;
            $itemIn['subArea'] = $subArea;
            $itemIn['categoria'] = $item->categoria;
            $itemIn['fechaPublicacion'] = Carbon::parse($item->fcPublicacion)->toDateString();
            $itemIn['fechaAlta'] = Carbon::parse($item->fcAlta)->toDateString();
            array_push($itemArr,$itemIn);
        }     
        //dd($itemArr);   
        $export = new generalExport($itemArr);        
        return Excel::download($export, 'Notas.xlsx');
    }
    // Notas desde API //
    public function notasFromAPI(){            
        return view('admin.notasFromAPI');
    }

    public function getNotasApi(){
        $notas = Nota::getAllNotasAPI();    
        /* foreach($notas as $nota){
            if($nota->idMedio == 1){
                //$nota->imgNota = 'http://127.0.0.1:8000/'.$nota->imgNota;
                $nota->imgNota = 'https://10.64.2.42/'.$nota->imgNota;
           }
        } */
        return datatables()->of($notas)->make(true);
    }
    // Edita nota //
    public function editaNotaAPI($id){
        $nota = Nota::getNotaById($id);        
        $areas = Area::all();        
        $entidades = Entidad::all();
        $sucesos = Suceso::all();
        $lugares = Lugar::all();
        //dd($nota);
        return view('admin.editaNotaAPI',['nota'=>$nota, 'areas'=>$areas, 'entidades'=>$entidades,
                                          'sucesos'=>$sucesos, 'lugares'=>$lugares]);
    }
    public function modificaNotaAPI(Request $request){
        //dd($request->all());
        $request->add = ($request->has('addPdf')) ? (int)$request->addPdf : 0;
        if(!$request->has('subArea')){
            $request->subArea = NULL;
        }   
        if($request->flagNvoSuceso){
            $suceso = Suceso::create([
                'nomSuceso'=>$request->suceso,
                'idEntidad'=>$request->entidadFed
            ]);            
            $request->suceso = $suceso->idSuceso;            
        }
        else{
            $suceso = Suceso::where('idSuceso',$request->suceso)->first();
            $request->suceso = $suceso->idSuceso;
        }
        if($request->flagNvoLugar){
            $lugar = Lugar::create([
                'nomLugar'=>$request->lugar,
                'idEntidad'=>$request->entidadFed
            ]);
            $request->lugar = $lugar->idLugar;            
        }
        else{
            $lugar = Lugar::where('idLugar',$request->lugar)->first();
            $request->lugar = $lugar->idLugar;
        }                                            
        DB::beginTransaction();
        try{
            $nota = Nota::updateNotaAPI($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        //dd($nota);
        return response()->json($nota->idNota,200);
    }
    // Borra Nota API //
    public function borraNotaAPI(Request $request){
        //dd($request->idNota);
        DB::beginTransaction();
        try{
            $nota = Nota::deleteNotaAPI($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo borrar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'borrada';
        return response()->json($msg,200);
    }
    public function getNotasFromApi(){
        $hoy = Carbon::today()->toDateString();          
        $notasArr = [];        
        $notaIn = [];                
        $JSONData = file_get_contents("https://www.efinf.com/feeder/feed.php?d=9a2ccad532bdbe7043072366da045c19");
        $dataObject = json_decode($JSONData);
        //dd($dataObject->lastId);
        $lastId = $dataObject->lastId;
        if($lastId == -1){
            return 'Sin Notas para Cargar';
        }
        $temas = $dataObject->temas;
        foreach($temas as $tema){
            $ctl = 0;
            $notas = $tema->notas;
            if($tema->nombre == 'PROFEPA'){
                $referecia = 1;
            }
            if($tema->nombre == 'PROCURADORA'){
                $referecia = 2;
            }
            foreach($notas as $nota){
                $key = encriptar($nota->encabezado);
                $verifNota = Nota::where('unkey',$key)->withTrashed()->count();
                //dd($verifNota);
                if($verifNota == 0){
                    $nota->resumen = str_replace("\n",'',$nota->resumen);
                    if($nota->categoria == 'PRENSA'){
                        $fileUrl = $nota->testigo;
                        /* $filename = rand().'.pdf';
                        $destinationPath ='public/imgNotas/'.$hoy;
                        $pdf = file_get_contents($fileUrl);                                  
                        Storage::put($destinationPath.'/'.$filename,$pdf);                                                            
                        $imgNota = 'storage/imgNotas/'.$hoy.'/'.$filename; */   
                        $imgNota = '';
                        $link = $fileUrl;                                     
                    }   
                    else{
                        $imgNota = '';                       
                        $link = $nota->testigo;
                    }             
                    $notaIn = [
                        'encabezado'=> $nota->encabezado,
                        'texto'=> preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->resumen),
                        'fuente'=> $nota->medio,
                        'imgNota'=> $imgNota,
                        'link'=> $link,
                        'idMedio'=> cadToIdMedio($nota->categoria),                    
                        'ctaVerif'=> 0,
                        'idReferencia'=> $referecia,
                        'fcPublicacion'=>$nota->fecha,     
                        'api'=>1,
                        'unkey'=>$key,               
                    ];
                    array_push($notasArr, $notaIn);
                    $ctl++;
                }                                
            }
        }
        //dd($notasArr, $ctl);
        if($ctl != 0){
            DB::beginTransaction();
            try{
                $nota = Nota::crearNotaAPI($notasArr);
            }
            catch(ValidationException $e){
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg,401);
            }
            DB::commit();
            return $nota;
        }
        else{
            return 'Sin Notas para Cargar';
        }
        
    }

    public function getLugares($idEntidad){
        $lugares = Lugar::where('idEntidad',$idEntidad)->orderBy('nomLugar')->get();
        return response()->json($lugares,200);
    }
    
    public function getSucesos($idEntidad){
        $sucesos = Suceso::where('idEntidad',$idEntidad)->orderBy('nomSuceso')->get();
        return response()->json($sucesos,200);
    }
}
