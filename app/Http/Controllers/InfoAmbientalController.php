<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\InfoAmbiental;
use App\Entidad;
use App\Area;
use App\Medio;
use App\Referencia;

class InfoAmbientalController extends Controller
{
    ///////////////////////////////////////
    ///***   INFORMACIÓN AMBIENTAL   ***///
    //////////////////////////////////////

    // Mostar Boletines //
    public function ambientales(){
        return view('admin.admAmbientales');
    }
    // Datatable ambiental //
    public function getAmbientales(){
        $ambientales = InfoAmbiental::getAllAmbientales();
        return datatables()->of($ambientales)->make(true);
    }
    // Nueva Ambiental //
    public function nuevaAmbiental(){
        $ahora = Carbon::now();
        $ini = Carbon::today()->setTime(9,0,0);
        $tope = Carbon::today()->setTime(9,10,0);       
        if($ahora->lte($tope) && $ahora->gte($ini)){
            return view('admin.noNueva');
        }
        $medios = Medio::all();
        $areas = Area::all();
        $entidades = Entidad::all();
        $referencias = Referencia::whereNotIn('idReferencia',[1,2])->get();        
        return view('admin.infoAmbientalForm', ['medios'=>$medios, 'areas'=>$areas, 'entidades'=>$entidades, 'referencias'=>$referencias]);
        
    }    
    // Guarda Ambiental //
    public function guardaAmbiental(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $ambiental = InfoAmbiental::crearAmbiental($request);          
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'no se pudo guardar la información';
            return response()->json($msg,401);
        }            
        DB::commit();       
        $msg = 'Guardado';
        return response()->json($msg,200);
    }    
    // Edita ambiental //
    public function editaAmbiental($id){
        $ambiental = InfoAmbiental::getAmbientalById($id);
        //dd($ambiental);
        return view('admin.editaAmbiental', ['ambiental'=>$ambiental]);
    }
    // Modifica ambiental //
    public function modificaAmbiental(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $ambiental = InfoAmbiental::updateAmbiental($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($ambiental->idAmbiental,200);
    }
    // Borra ambiental //
    public function borraAmbiental(Request $request){
        DB::beginTransaction();
        try{
            $ambiental = InfoAmbiental::deleteAmbiental($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo borrar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'Borrado';
        return response()->json($msg,200);
    }
    // Información Ambiental desde CSV //
    public function ambientalFromCSV(){
        return view('admin.ambientalFromCSV');
    }
    public function cargaCSV(Request $request){        
        if(!$request->hasFile('documento')){
            $msg = 'No se Agregó Archivo';
            return response()->json($msg,401);
        }
        else{
            //dd($request->all());
            $ext = ['csv'];
            $extension = $request->file('documento')->getClientOriginalExtension();
            if(!in_array($extension,$ext)){
                $msg = 'Formato no valido';
                return response()->json($msg,401);
            }
            else{
                $file = $request->file('documento');
                $hoy = Carbon::today()->toDateString();
                //$destinationPath = storage_path('app/public/imgNotas/'.$hoy);
                $destinationPath ='storage/csvNotas/'.$hoy;
                $filename = 'ambientales'.$hoy.'_'.time(). '.' . $request->file('documento')->getClientOriginalExtension();
                $file->move($destinationPath, $filename);                  
            }
            //dd($destinationPath.'/'.$filename);
            $ambientalArr = [];
            $fileAmbientales = fopen($destinationPath.'/'.$filename,'r');
            while (($line = fgetcsv($fileAmbientales)) !== FALSE) {    
                array_push($ambientalArr,$line);
            }
            array_shift($ambientalArr);  
            $ambientalArrClean = [];
            foreach($ambientalArr as $key){
                if(!empty($key[0])){
                    $key[0]=utf8_encode($key[0]);
                    $key[1]=utf8_encode($key[1]);
                    $key[2]=utf8_encode($key[2]);
                    $key[4]=utf8_encode($key[4]);
                    array_push($ambientalArrClean,$key);
                }
            }          
            $ambientalArrM = [];
            foreach($ambientalArrClean as $ambiental){                                                 
                $ambiental[1] = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $ambiental[1]);   
                $ambiental[1] = str_replace("\n",'',$ambiental[1]);           
                $ambiental[1] = trim($ambiental[1]);
                array_push($ambientalArrM,$ambiental);
            }
            //dd($ambientalArrM);
            DB::beginTransaction();
            try{
                $ambiental= InfoAmbiental::crearAmbientalCSV($ambientalArrM);
            }
            catch(ValidationException $e){
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg,401);
            }
            DB::commit();
            $msg = 'Se cargaron '.$ambiental.' registros';
            return response()->json($msg,200);
        }
    }
    // Completa Ambientales //
    public function completaAmbientalesCSV(){
        return view('admin.completaAmbientalesCSV');
    }
     /// Datatable Ambientales ///
     public function getAmbientalesCSV(){
        $ambientales = InfoAmbiental::where('csv',1)->get();        
        return datatables()->of($ambientales)->make(true);
    }
    // Edita Ambiental //
    public function editaAmbientalCSV($id){
        $ambiental = InfoAmbiental::getAmbientalById($id);        
        $referencias = Referencia::whereNotIn('idReferencia',[1,2])->get();                
        return view('admin.editaAmbientalCSV',['ambiental'=>$ambiental, 'referencias'=>$referencias]);
    }
    // Modifica Ambiental
    public function modificaAmbientalCSV(Request $request){
        //dd($request->all());        
        DB::beginTransaction();
        try {
            $ambiental = InfoAmbiental::updateAmbientalCSV($request);
        } catch (ValidationException $e) {
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg, 401);
        }
        DB::commit();
        //dd($nota);
        return response()->json($ambiental->idInfoAmbiental, 200);
    }
}
