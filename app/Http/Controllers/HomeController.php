<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //return view('home');
        if( Auth::user()->hasRole('SuperAdmin')){
            return redirect('/inicio');
        }
        if( Auth::user()->hasRole('Administrador')){
            return redirect('/inicio');
        }  
        if(Auth::user()->hasRole('Colaborador')){
            return redirect('/incidencia/inicio');
            //return redirect('/incidencia/nuevaIncidencia');            
        }
        if(Auth::user()->hasRole('Delegado')){
            //dd('ingresó delegado');
            return redirect('/delegado/inicioCS');            
        }
        if(Auth::user()->hasRole('Denunciante')){
            //dd('ingresó delegado');
            return redirect('/denunciante/nuevaDenuncia');            
        }
        if(Auth::user()->hasRole('MonitorZona')){
            //dd('ingresó monitor de zona');
            return redirect('/monitorZona/inicio');            
        }
        if(Auth::user()->hasRole('Procurador')){            
            return redirect('/procurador/inicio');            
        }
        if(Auth::user()->hasRole('Consultor')){            
            return redirect('/consultas/inicio');            
        }
        if(Auth::user()->hasRole('SubProcurador')){            
            return redirect('/subProc/inicio');            
        } 
    }
}
