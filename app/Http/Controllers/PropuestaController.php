<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Propuesta;

class PropuestaController extends Controller
{
    // Ver propuesta //
    public function verPropuesta($id){
        //dd($id);
        $propuesta = Propuesta::with(['entidadRel', 'areaRel', 'SubAreaRel', 'tipoPropRel',
                                      'incidenciaRel', 'notaRel'])
                                ->where('idPropuesta', $id)->first();
        //dd($propuesta);
        return view('admin.verPropuesta',['propuesta'=>$propuesta]);
    }

    // Ver propuesta //
    public function verPropuestaMZ($id){
        //dd($id);
        $propuesta = Propuesta::with(['entidadRel', 'areaRel', 'SubAreaRel', 'tipoPropRel',
                                      'incidenciaRel', 'notaRel'])
                                ->where('idPropuesta', $id)->first();
        //dd($propuesta);
        return view('monitorZona.verPropuesta',['propuesta'=>$propuesta]);
    }
}
