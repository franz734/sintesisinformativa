<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class CustomHomeController extends Controller
{
    public function index(){
        $usuarios = User::all()->count();
        if($usuarios == 0){            
            return view('auth.customRegister');
        }
        else{
            return view('auth.customLogin');
        }
    }
    public function indexDelegado()
    {
        return view('auth.customLoginDelegado');
    }
    public function indexSubProc(){
        dd('comenzamos');
    }
}
