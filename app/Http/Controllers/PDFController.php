<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use Carbon\Carbon;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\Medio;
use App\Nota;
use App\NotaEntidad;
use App\OchoColumnas;
use App\Boletin;
use App\InfoAmbiental;
use App\Incidencia;
use App\Respuesta;
use App\Referencia;

class PDFController extends Controller
{
    /////////////////////////////
    /// Sintesis Informativa ///
    ///////////////////////////

    /// Mege Sintesis y Anexos ///
    /// Valida ///
    public function validaPdfAndMerge(){        
        $dt =Carbon::now()->addDay(1); 
        $rango = getRangoVerif($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $filesToAdd = Nota::where('idMedio',1)->whereBetween('fcAlta',[$fcIni, $fcFin])
                            ->orderBy('idMedio')->orderBy('idEntidad')->pluck('imgNota');            
        $file1 = $this->validaPdf();        
        /* if($filesToAdd->count() != 0){
            $pdfMerger = PDFMerger::init();
            $pdfMerger->addPDF($file1, 'all');
            foreach($filesToAdd as $file){
                $file = str_replace('storage/','/var/www/html/sintesisinformativa/storage/app/public/',$file);
                $pdfMerger->addPDF($file, 'all', 'P');   
            }
            $pdfMerger->merge();
            $pdfMerger->save($file1);
        }  */            
        $file1 = str_replace('/var/www/html/sintesisinformativa/storage/app/public/','storage/',$file1);
        return response()->json($file1,200);
        
    }
    /// Reader ///
    public function PdfAndMerge($t){
        //dd('comenzamos');
        /* $dt =Carbon::now(); 
        $rango = getRango($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $filesToAdd = Nota::where('idMedio',1)->whereBetween('fcAlta',[$fcIni, $fcFin])
                            ->orderBy('idMedio')->orderBy('idEntidad')->pluck('imgNota'); */   
        $f = 'ss';                                        
        $file1 = $this->generatePDF($f,$t);        
        /* if($filesToAdd->count() != 0){
            $pdfMerger = PDFMerger::init();
            $pdfMerger->addPDF($file1, 'all');
            foreach($filesToAdd as $file){
                $file = str_replace('storage/','/var/www/html/sintesisinformativa/storage/app/public/',$file);
                $pdfMerger->addPDF($file, 'all', 'P');   
            }
            $pdfMerger->merge();
            $pdfMerger->save($file1);
        } */             
        $file1 = str_replace('/var/www/html/sintesisinformativa/storage/app/public/','storage/',$file1);
        return response()->json($file1,200);
        
    }
    public function PdfAndMergeByArea($t,$area,$cveArea){       
        $f = 'ss';                                        
        $file4 = $this->generatePDFbyArea($f,$t,$area,$cveArea);
        $file4 = str_replace('/var/www/html/sintesisinformativa/storage/app/public/','storage/',$file4);
        return response()->json($file4,200);
    }
    public function PdfAndMergeByZona($t,$z){       
        $f = 'ss';           
        $file5 = $this->generatePDFbyZona($f,$t,$z);
        $file5 = str_replace('/var/www/html/sintesisinformativa/storage/app/public/','storage/',$file5);
        return response()->json($file5,200);
    }

    /// Valida ///
    public function validaPdf(){        
        ///Informacion///        
        $pRoot = $_SERVER['DOCUMENT_ROOT'];     
        $f = '';   
        $hoyEs = Carbon::today()->addDay(1)->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');        
        $mediosData = [];
        $mediosLabel = [];
        $mediosG = [];
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $ejeX = [];
        $ejeY = [];
        $total = 0;
        $dt =Carbon::now()->addDay(1); 
        $rango = getRangoVerif($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];               
        ///notas///        
        $notas = Nota::getNotas($fcIni, $fcFin);
        $referencias = Referencia::referenciasNotasByDate($fcIni, $fcFin);
        $ctlProcuradora = Medio::mediosNotasByDateAndRef($fcIni, $fcFin, 2);
        $ctlProfepa = Medio::mediosNotasByDateAndRef($fcIni, $fcFin, 1);
        //dd($notas);
        //$mediosCtl = Medio::all();        
        ///estados///
        $entidadesNotas = NotaEntidad::entidadesNotasByDate($fcIni, $fcFin); 
        //dd($entidadesNotas);       
        foreach($entidadesNotas as $entidadNotas){
            array_push($ejeX,$entidadNotas->nomEntidad);
            array_push($ejeY,$entidadNotas->notas);
            $total = $total + $entidadNotas->notas;
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafEdos_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);  
        ///medios///              
        $mediosNotas = Medio::mediosNotasByDate($fcIni, $fcFin);
        $ctlImp = 0;
        $ctlTv = 0;
        $ctlWeb = 0;
        $ctlRed = 0;
        //dd($mediosNotas);
        foreach($mediosNotas as $medioNotas){
            array_push($mediosData,$medioNotas->notas);
            array_push($mediosLabel, $medioNotas->nomMedio);
            if($medioNotas->idMedio == 1){
                $ctlImp = $ctlImp + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 2){
                $ctlTv = $ctlTv + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 3){
                $ctlWeb = $ctlWeb + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 4){
                $ctlRed = $ctlRed + $medioNotas->notas;
            }
            
        }
        $mediosG = [
            'labels' => $mediosLabel,
            'data' => $mediosData
        ];      
        //dd($ctlRed);        
        /* $ctlImp = $mediosNotas[0]->notas; //1
        $ctlTv = $mediosNotas[1]->notas; //1
        $ctlWeb = $mediosNotas[2]->notas; //0
        $ctlRed = $mediosNotas[3]->notas;  //1  */        
        $mediosL = implode("','",$mediosG['labels']);
        $mediosD = implode(",",$mediosG['data']);        
        $chartMedios = "{
            type: 'doughnut',
            data: {
                labels:['$mediosL'],
              datasets: [{
                data:[$mediosD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartMedios = str_replace(' ','',$chartMedios);
        $chartMedios = str_replace("\n",'',$chartMedios);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartMedios);        
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafMedios_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasNotas = Area::areasNotasByDate($fcIni, $fcFin);
        //dd($areasNotas);
        foreach($areasNotas as $areaNotas){
            array_push($areasData, $areaNotas->notas);
            array_push($areasLabel, $areaNotas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);
        //dd($linkChart);
        /* $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart); */
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafAreas_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///SubAreas///
        $subAreasNotas = SubArea::getSubAreasNotasByDate($fcIni, $fcFin);
        //dd($subAreasNotas);
        ///ambientales///
        $ambientales = infoAmbiental::getAmbiental($fcIni, $fcFin);
        //dd($ambientales->count());
        ///boletines///
        $boletines = Boletin::getBoletin($fcIni, $fcFin);
        //dd($boletines);
        ///ocho columnas///
        $columnas = OchoColumnas::getColumnas($fcIni, $fcFin);
        //dd($columnas);
        $flg = 'v';
        ///PDF///                 
        $parametros = [$hoyEs, $hoyG, $entidadesNotas, $mediosNotas, $areasNotas,
                        $ctlRed, $ctlTv, $ctlImp, $ctlWeb, $ambientales,
                        $boletines, $columnas, $flg, $total, $notas, $subAreasNotas, 
                        $referencias, $ctlProcuradora, $ctlProfepa, $f,$t='m'];   
        $reporte = pdfSintesis($parametros); 
        return $reporte;
        //dd($reporte);
    }

    /// Reader ///
    public function generatePDF($f, $t){
        ///Informacion///        
        $pRoot = $_SERVER['DOCUMENT_ROOT'];        
        //dd($pRoot);
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        //dd(str_replace('','-',$hoyEs));
        $hoyG = date('Y-m-d');
        //dd($hoy);
        $mediosData = [];
        $mediosLabel = [];
        $mediosG = [];
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $ejeX = [];
        $ejeY = [];
        $total = 0;
        $dt =Carbon::now();        
        if($t == 'm'){
            $rango = getRangoM($dt);
        }
        elseif($t == 'v'){
            $rango = getRangoV($dt);
        }        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];                       
        ///notas///
        $notas = Nota::getNotas($fcIni, $fcFin);
        $ctlVerde = 0;
        $ctlAmarillo = 0;
        $ctlRojo = 0;
        foreach($notas as $nota){
            if($nota->categoria == 'verde'){
                $ctlVerde = $ctlVerde+1;
            }
            if($nota->categoria == 'amarillo'){
                $ctlAmarillo = $ctlAmarillo+1;
            }
            if($nota->categoria == 'rojo'){
                $ctlRojo = $ctlRojo+1;
            }
        }
        $referencias = Referencia::referenciasNotasByDate($fcIni, $fcFin);
        $ctlProcuradora = Medio::mediosNotasByDateAndRef($fcIni, $fcFin, 2);
        $ctlProfepa = Medio::mediosNotasByDateAndRef($fcIni, $fcFin, 1);
        $resumen = Nota::getNotasLugarSuceso($fcIni,$fcFin);
        ///sucesos//
        $totSucesos = Nota::getTotSucesosByDate($fcIni,$fcFin);
        $sucesosEntidad = Nota::gatSucesosEntidad($fcIni,$fcFin);
        //dd($totSucesos, $sucesosEntidad);                
        ///estados///
        $entidadesNotas = NotaEntidad::entidadesNotasByDate($fcIni, $fcFin);        
        foreach($entidadesNotas as $entidadNotas){
            array_push($ejeX,$entidadNotas->nomEntidad);
            array_push($ejeY,$entidadNotas->notas);
            $total = $total + $entidadNotas->notas;
        }          
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdos_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);  
        ///medios///              
        $mediosNotas = Medio::mediosNotasByDate($fcIni, $fcFin);
        $ctlImp = 0;
        $ctlTv = 0;
        $ctlWeb = 0;
        $ctlRed = 0;        
        //dd($mediosNotas);
        foreach($mediosNotas as $medioNotas){
            array_push($mediosData,$medioNotas->notas);
            array_push($mediosLabel, $medioNotas->nomMedio);
            if($medioNotas->idMedio == 1){
                $ctlImp = $ctlImp + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 2){
                $ctlTv = $ctlTv + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 3){
                $ctlWeb = $ctlWeb + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 4){
                $ctlRed = $ctlRed + $medioNotas->notas;
            }
            
        }
        $mediosG = [
            'labels' => $mediosLabel,
            'data' => $mediosData
        ];      
        //dd($ctlRed);                
        $mediosL = implode("','",$mediosG['labels']);
        $mediosD = implode(",",$mediosG['data']);        
        $chartMedios = "{
            type: 'doughnut',
            data: {
                labels:['$mediosL'],
              datasets: [{
                data:[$mediosD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartMedios = str_replace(' ','',$chartMedios);
        $chartMedios = str_replace("\n",'',$chartMedios);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartMedios);        
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafMedios_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasNotas = Area::areasNotasByDate($fcIni, $fcFin);
        //dd($areasNotas);
        foreach($areasNotas as $areaNotas){
            array_push($areasData, $areaNotas->notas);
            array_push($areasLabel, $areaNotas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);
        //dd($linkChart);
        /* $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart); */
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreas_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///SubAreas///
        $subAreasNotas = SubArea::getSubAreasNotasByDate($fcIni, $fcFin);
        ///ambientales///
        $ambientales = infoAmbiental::getAmbiental($fcIni, $fcFin);
        $referenciasAmbiental = Referencia::referenciasAmbientalByDate($fcIni, $fcFin);
        //dd($referenciasAmbiental);
        ///boletines///
        $boletines = Boletin::getBoletin($fcIni, $fcFin);
        ///ocho columnas///
        $columnas = OchoColumnas::getColumnas($fcIni, $fcFin);
        $flg = 's';
        ///PDF///                 
        $parametros = [$hoyEs, $hoyG, $entidadesNotas, $mediosNotas, $areasNotas,
                        $ctlRed, $ctlTv, $ctlImp, $ctlWeb, $ambientales,
                        $boletines, $columnas, $flg, $total, $notas, $subAreasNotas, 
                        $referencias, $ctlProcuradora, $ctlProfepa, $f, $t, $ctlVerde,
                        $ctlAmarillo, $ctlRojo, $resumen, $totSucesos, $sucesosEntidad,
                        $referenciasAmbiental];   
        $reporte = pdfSintesis($parametros);
        return $reporte;
    }
    public function generatePDFbyArea($f,$t,$area,$cveArea){
        $cveArea = str_replace('.','',$cveArea);        
        ///Informacion///        
        $pRoot = $_SERVER['DOCUMENT_ROOT'];        
        //dd($pRoot);
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        //dd(str_replace('','-',$hoyEs));
        $hoyG = date('Y-m-d');
        //dd($hoy);
        $mediosData = [];
        $mediosLabel = [];
        $mediosG = [];
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $ejeX = [];
        $ejeY = [];
        $total = 0;
        $dt =Carbon::now();
        if($t == 'm'){
            $rango = getRangoM($dt);
        }
        elseif($t == 'v'){
            $rango = getRangoV($dt);
        }        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        ///notas///
        $notas = Nota::getNotasByAreaDate($area, $fcIni, $fcFin);
        $ctlVerde = 0;
        $ctlAmarillo = 0;
        $ctlRojo = 0;
        foreach($notas as $nota){
            if($nota->categoria == 'verde'){
                $ctlVerde = $ctlVerde+1;
            }
            if($nota->categoria == 'amarillo'){
                $ctlAmarillo = $ctlAmarillo+1;
            }
            if($nota->categoria == 'rojo'){
                $ctlRojo = $ctlRojo+1;
            }
        }
        $referencias = Referencia::referenciasNotasByAreaDate($fcIni, $fcFin, $area);
        $ctlProcuradora = Medio::mediosNotasByAreaDateAndRef($fcIni, $fcFin, 2, $area);
        $ctlProfepa = Medio::mediosNotasByAreaDateAndRef($fcIni, $fcFin, 1, $area);
        $resumen = Nota::getNotasLugarSucesoByArea($fcIni,$fcFin,$area);
        ///sucesos//
        $totSucesos = Nota::getTotSucesosByDateArea($fcIni,$fcFin,$area);
        $sucesosEntidad = Nota::gatSucesosEntidadArea($fcIni,$fcFin,$area);
        //dd($totSucesos, $sucesosEntidad);
        ///estados///
        $entidadesNotas = NotaEntidad::entidadesNotasByAreaDate($fcIni, $fcFin, $area);        
        foreach($entidadesNotas as $entidadNotas){
            array_push($ejeX,$entidadNotas->nomEntidad);
            array_push($ejeY,$entidadNotas->notas);
            $total = $total + $entidadNotas->notas;
        }     
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdos_'.$cveArea.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///medios///
        $mediosNotas = Medio::mediosNotasByAreaDate($fcIni, $fcFin, $area);
        $ctlImp = 0;
        $ctlTv = 0;
        $ctlWeb = 0;
        $ctlRed = 0;
        //dd($mediosNotas);
        foreach($mediosNotas as $medioNotas){
            array_push($mediosData,$medioNotas->notas);
            array_push($mediosLabel, $medioNotas->nomMedio);
            if($medioNotas->idMedio == 1){
                $ctlImp = $ctlImp + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 2){
                $ctlTv = $ctlTv + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 3){
                $ctlWeb = $ctlWeb + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 4){
                $ctlRed = $ctlRed + $medioNotas->notas;
            }
            
        }
        $mediosG = [
            'labels' => $mediosLabel,
            'data' => $mediosData
        ];      
        //dd($ctlRed);
        $mediosL = implode("','",$mediosG['labels']);
        $mediosD = implode(",",$mediosG['data']);        
        $chartMedios = "{
            type: 'doughnut',
            data: {
                labels:['$mediosL'],
              datasets: [{
                data:[$mediosD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartMedios = str_replace(' ','',$chartMedios);
        $chartMedios = str_replace("\n",'',$chartMedios);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartMedios);        
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafMedios_'.$cveArea.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasNotas = Area::areasNotasByAreaDate($fcIni, $fcFin, $area);
        //dd($areasNotas);
        foreach($areasNotas as $areaNotas){
            array_push($areasData, $areaNotas->notas);
            array_push($areasLabel, $areaNotas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreas_'.$cveArea.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///SubAreas///
        $subAreasNotas = SubArea::getSubAreasNotasByAreaDate($fcIni, $fcFin, $area);
        ///ambientales///
        $ambientales = infoAmbiental::getAmbiental($fcIni, $fcFin);
        $referenciasAmbiental = Referencia::referenciasAmbientalByDate($fcIni, $fcFin);
        //dd($ambientales->count());
        ///boletines///
        $boletines = Boletin::getBoletin($fcIni, $fcFin);
        ///ocho columnas///
        $columnas = OchoColumnas::getColumnas($fcIni, $fcFin);
        $flg = 's';
        ///PDF///                 
        $parametros = [$hoyEs, $hoyG, $entidadesNotas, $mediosNotas, $areasNotas,
                        $ctlRed, $ctlTv, $ctlImp, $ctlWeb, $ambientales,
                        $boletines, $columnas, $flg, $total, $notas, $subAreasNotas, 
                        $referencias, $ctlProcuradora, $ctlProfepa, $f, $t, $cveArea,
                        $ctlVerde, $ctlAmarillo, $ctlRojo, $resumen, $totSucesos, $sucesosEntidad,
                        $referenciasAmbiental];   
        $reporte = pdfSintesisByArea($parametros);
        return $reporte;
    }
    public function generatePDFbyZona($f,$t,$z){
        if($z == 'norte'){
            $idEdos = [1,2,3,8,5,6,10,11,14,18,19,24,25,26,32,28];
        }
        elseif($z == 'sur'){
            $idEdos = [4,7,12,13,15,16,17,20,21,22,27,29,30,31,9,23];
        }                       
        ///Informacion///        
        $pRoot = $_SERVER['DOCUMENT_ROOT'];        
        //dd($pRoot);
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        //dd(str_replace('','-',$hoyEs));
        $hoyG = date('Y-m-d');
        //dd($hoy);
        $mediosData = [];
        $mediosLabel = [];
        $mediosG = [];
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $ejeX = [];
        $ejeY = [];
        $total = 0;
        $dt =Carbon::now();
        if($t == 'm'){
            $rango = getRangoM($dt);
        }
        elseif($t == 'v'){
            $rango = getRangoV($dt);
        }        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        ///notas///
        $notas = Nota::getNotasByZonaDate($idEdos, $fcIni, $fcFin);        
        $ctlVerde = 0;
        $ctlAmarillo = 0;
        $ctlRojo = 0;
        foreach($notas as $nota){
            if($nota->categoria == 'verde'){
                $ctlVerde = $ctlVerde+1;
            }
            if($nota->categoria == 'amarillo'){
                $ctlAmarillo = $ctlAmarillo+1;
            }
            if($nota->categoria == 'rojo'){
                $ctlRojo = $ctlRojo+1;
            }
        }
        $referencias = Referencia::referenciasNotasByZonaDate($fcIni, $fcFin, $idEdos);        
        $ctlProcuradora = Medio::mediosNotasByZonaDateAndRef($fcIni, $fcFin, 2, $idEdos);
        $ctlProfepa = Medio::mediosNotasByZonaDateAndRef($fcIni, $fcFin, 1, $idEdos);    
        $resumen = Nota::getNotasLugarSucesoByZona($fcIni,$fcFin,$idEdos);    
        ///sucesos//
        $totSucesos = Nota::getTotSucesosByDateZona($fcIni,$fcFin,$idEdos);
        $sucesosEntidad = Nota::gatSucesosEntidadZona($fcIni,$fcFin,$idEdos);
        //dd($totSucesos, $sucesosEntidad);
        ///estados///
        $entidadesNotas = NotaEntidad::entidadesNotasByZonaDate($fcIni, $fcFin, $idEdos);                
        foreach($entidadesNotas as $entidadNotas){
            array_push($ejeX,$entidadNotas->nomEntidad);
            array_push($ejeY,$entidadNotas->notas);
            $total = $total + $entidadNotas->notas;
        }     
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdos_'.$z.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///medios///
        $mediosNotas = Medio::mediosNotasByZonaDate($fcIni, $fcFin, $idEdos);        
        $ctlImp = 0;
        $ctlTv = 0;
        $ctlWeb = 0;
        $ctlRed = 0;        
        foreach($mediosNotas as $medioNotas){
            array_push($mediosData,$medioNotas->notas);
            array_push($mediosLabel, $medioNotas->nomMedio);
            if($medioNotas->idMedio == 1){
                $ctlImp = $ctlImp + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 2){
                $ctlTv = $ctlTv + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 3){
                $ctlWeb = $ctlWeb + $medioNotas->notas;
            }
            if($medioNotas->idMedio == 4){
                $ctlRed = $ctlRed + $medioNotas->notas;
            }
            
        }
        $mediosG = [
            'labels' => $mediosLabel,
            'data' => $mediosData
        ];      
        //dd($ctlRed);
        $mediosL = implode("','",$mediosG['labels']);
        $mediosD = implode(",",$mediosG['data']);        
        $chartMedios = "{
            type: 'doughnut',
            data: {
                labels:['$mediosL'],
              datasets: [{
                data:[$mediosD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartMedios = str_replace(' ','',$chartMedios);
        $chartMedios = str_replace("\n",'',$chartMedios);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartMedios);        
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafMedios_'.$z.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasNotas = Area::areasNotasByZonaDate($fcIni, $fcFin, $idEdos);        
        foreach($areasNotas as $areaNotas){
            array_push($areasData, $areaNotas->notas);
            array_push($areasLabel, $areaNotas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);
        //dd($linkChart);
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreas_'.$z.'_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///SubAreas///
        $subAreasNotas = SubArea::getSubAreasNotasByZonaDate($fcIni, $fcFin, $idEdos);
        //dd($subAreasNotas);
        ///ambientales///
        $ambientales = infoAmbiental::getAmbiental($fcIni, $fcFin);
        $referenciasAmbiental = Referencia::referenciasAmbientalByDate($fcIni, $fcFin);
        //dd($ambientales->count());
        ///boletines///
        $boletines = Boletin::getBoletin($fcIni, $fcFin);
        ///ocho columnas///
        $columnas = OchoColumnas::getColumnas($fcIni, $fcFin);
        $flg = 's';
        ///PDF///                 
        $parametros = [$hoyEs, $hoyG, $entidadesNotas, $mediosNotas, $areasNotas,
                        $ctlRed, $ctlTv, $ctlImp, $ctlWeb, $ambientales,
                        $boletines, $columnas, $flg, $total, $notas, $subAreasNotas, 
                        $referencias, $ctlProcuradora, $ctlProfepa, $f, $t, $z,
                        $ctlVerde, $ctlAmarillo, $ctlRojo, $resumen, $totSucesos, $sucesosEntidad,
                        $referenciasAmbiental];   
        $reporte = pdfSintesisByZona($parametros);
        return $reporte;
    }
    
    ////////////////////
    /// Incidencias ///
    //////////////////

    /// valida ///
    public function validaPdfIncidencias(){               
        $hoyEs = Carbon::today()->addDay(1)->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = [];  
        $dt =Carbon::now()->addDay(1); 
        $rango = getRangoVerifInc($dt);
        //dd($rango);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidencias($fcIni, $fcFin);
        $incidenciasEdos = Incidencia::getIncidecniasByDate($fcIni, $fcFin);
        //dd($incidenciasEdos);
        foreach($incidenciasEdos as $incidenciasEdo){
            array_push($ejeX,$incidenciasEdo->nomEntidad);
            array_push($ejeY,$incidenciasEdo->incidencias);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafEdosInc_'.$hoyG.'.png';        
        $flg = 'v';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);          
        ///areas///
        $areasIncidencias = Area::areasIncidenciasByDate($fcIni, $fcFin);        
        foreach($areasIncidencias as $areaIncidencias){
            array_push($areasData, $areaIncidencias->incidencias);
            array_push($areasLabel, $areaIncidencias->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);   
        ///SubAreas///
        $subAreasIncidencias = SubArea::getSubAreasIncidenciasByDate($fcIni, $fcFin);
        //dd($subAreasIncidencias);
        foreach($subAreasIncidencias as $subAreaIncidencias){
            array_push($subAreasData, $subAreaIncidencias->incidencias);
            array_push($subAreasLabel, $subAreaIncidencias->nomSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);            
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafSubAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        $parametros = [$hoyEs, $incidencias, $incidenciasEdos, $hoyG, $flg, $areasIncidencias, $subAreasIncidencias];
        $reporte = pdfIncidencias($parametros);
    }
    /// reader ///
    public function generatePDFIncidencias(){
        //dd('PDF');
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];  
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = []; 
        $dt =Carbon::now(); 
        $rango = getRangoInc($dt);
        //dd($rango);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidencias($fcIni, $fcFin);
        $incidenciasEdos = Incidencia::getIncidecniasByDate($fcIni, $fcFin);
        //dd($incidenciasEdos);
        foreach($incidenciasEdos as $incidenciasEdo){
            array_push($ejeX,$incidenciasEdo->nomEntidad);
            array_push($ejeY,$incidenciasEdo->incidencias);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdosInc_'.$hoyG.'.png';
        $flg = 's';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img); 
        ///areas///
        $areasIncidencias = Area::areasIncidenciasByDate($fcIni, $fcFin);        
        foreach($areasIncidencias as $areaIncidencias){
            array_push($areasData, $areaIncidencias->incidencias);
            array_push($areasLabel, $areaIncidencias->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);   
        ///SubAreas///
        $subAreasIncidencias = SubArea::getSubAreasIncidenciasByDate($fcIni, $fcFin);
        //dd($subAreasIncidencias);
        foreach($subAreasIncidencias as $subAreaIncidencias){
            array_push($subAreasData, $subAreaIncidencias->incidencias);
            array_push($subAreasLabel, $subAreaIncidencias->nomSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);            
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafSubAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);           
        $parametros = [$hoyEs, $incidencias, $incidenciasEdos, $hoyG, $flg, $areasIncidencias, $subAreasIncidencias];
        $reporte = pdfIncidencias($parametros);
    }

    ////////////////////
    /// Respuestas ///
    //////////////////

    ///valida///
    public function validaPDFRespuestas(){
        $hoyEs = Carbon::today()->addDay(1)->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];  
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = []; 
        $dt =Carbon::now(); 
        $rango = getRangoVerifInc($dt);        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];        
        $notasSend = Nota::getNotasEnviadasByDate($fcIni, $fcFin);
        $sinRespuesta = Nota::sinRespuestaByDate($fcIni, $fcFin);        
        $respuestas = Respuesta::getRespuestas($fcIni, $fcFin);
        $respuestasEdos = Respuesta::getRespuestasByDate($fcIni, $fcFin);        
        foreach($respuestasEdos as $respuestasEdo){
            array_push($ejeX,trim($respuestasEdo->nomEntidad));
            array_push($ejeY,$respuestasEdo->respuestas);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartEstados);
        //$linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);                   
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafEdosResp_'.$hoyG.'.png';
        $flg = 'v';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasRespuestas = Area::areasRespuestasByDate($fcIni, $fcFin);                
        foreach($areasRespuestas as $areaRespuestas){
            array_push($areasData, $areaRespuestas->respuestas);
            array_push($areasLabel, $areaRespuestas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);                
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img); 
        ///SubAreas///
        $subAreasRespuestas = SubArea::getSubAreasRespuestasByDate($fcIni, $fcFin);        
        foreach($subAreasRespuestas as $subAreaRespuestas){
            array_push($subAreasData, $subAreaRespuestas->respuestas);
            array_push($subAreasLabel, $subAreaRespuestas->cveSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);          
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafSubAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        $parametros = [$hoyEs, $respuestas, $respuestasEdos, $hoyG, $flg, $areasRespuestas, $subAreasRespuestas,
                        $notasSend, $sinRespuesta];        
        $reporte = pdfRespuestas($parametros);
    }
    ///reader///
    public function generatePDFRespuestas($flg){
        
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];  
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = []; 
        $dt =Carbon::now(); 
        $rango = getRangoResp($dt, $flg);
        //dd($rango);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];       
        $notasSend = Nota::getNotasEnviadasByDate($fcIni, $fcFin); 
        $sinRespuesta = Nota::sinRespuestaByDate($fcIni, $fcFin);
        $respuestas = Respuesta::getRespuestas($fcIni, $fcFin);
        $respuestasEdos = Respuesta::getRespuestasByDate($fcIni, $fcFin);        
        foreach($respuestasEdos as $respuestasEdo){
            array_push($ejeX,trim($respuestasEdo->nomEntidad));
            array_push($ejeY,$respuestasEdo->respuestas);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartEstados);
        //$linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);                   
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdosResp_'.$hoyG.'.png';
        //$flg = 's';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasRespuestas = Area::areasRespuestasByDate($fcIni, $fcFin);                
        foreach($areasRespuestas as $areaRespuestas){
            array_push($areasData, $areaRespuestas->respuestas);
            array_push($areasLabel, $areaRespuestas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);                
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img); 
        ///SubAreas///
        $subAreasRespuestas = SubArea::getSubAreasRespuestasByDate($fcIni, $fcFin);        
        foreach($subAreasRespuestas as $subAreaRespuestas){
            array_push($subAreasData, $subAreaRespuestas->respuestas);
            array_push($subAreasLabel, $subAreaRespuestas->cveSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);          
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafSubAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        $parametros = [$hoyEs, $respuestas, $respuestasEdos, $hoyG, $flg, $areasRespuestas, $subAreasRespuestas,
                        $notasSend, $sinRespuesta];        
        $reporte = pdfRespuestas($parametros);

    }
}

/// Funciones ///

/// PDF Sintesis Informativa ///
function pdfSintesis($parametros){
    $pRoot = $_SERVER['DOCUMENT_ROOT'];
    //dd($parametros);    
    $hoyEs = $parametros[0];
    $hoyG = $parametros[1];
    $entidadesNotas = $parametros[2];
    $mediosNotas = $parametros[3];
    $areasNotas = $parametros[4];
    $ctlRed = $parametros[5];
    $ctlTv = $parametros[6];
    $ctlImp = $parametros[7];
    $ctlWeb = $parametros[8];
    $ambientales = $parametros[9];
    $boletines = $parametros[10];
    $columnas = $parametros[11];
    $flg = $parametros[12];
    $total = $parametros[13];
    $notas = $parametros[14];
    $subAreasNotas = $parametros[15];
    $referencias = $parametros[16];
    $mediosProcuradora = $parametros[17];
    $mediosProfepa = $parametros[18];
    $f = $parametros[19];
    $horario = $parametros[20];
    $ctlVerde = $parametros[21];
    $ctlAmarillo = $parametros[22];
    $ctlRojo = $parametros[23];
    $resumen = $parametros[24];
    $totSucesos = $parametros[25];
    $sucesosEntidad = $parametros[26];
    $referenciasAmbiental = $parametros[27];
    $pdf = new miPDF();             
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);   
    $pdf::AddPage('P', 'LETTER');
    //$img = 'assets/images/reader/portadas/sintesisPort'.$hoyG.'.jpg';
    if($horario == 'm'){
        $img = 'assets/images/reader/portadas/sintesisPort_m'.$hoyG.'.jpg';
        $descanso = 'assets/images/reader/Descanso01.jpg';
    }
    elseif($horario == 'v'){
        $img = 'assets/images/reader/portadas/sintesisPort_v'.$hoyG.'.jpg';
        $descanso = 'assets/images/reader/Descanso02.jpg';
    }
    //dd($img);
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(30);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(150);        
        $pdf::SetTextColor(105,28,50);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $cad = '';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }
    else{
        $img = 'assets/images/reader/sintesisPort.jpg';
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(10);
        $pdf::SetTextColor(35,91,78);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }    
    $pdf::SetAutoPageBreak(true, 0);
    ///en numeros///
    $pdf::AddPage();
    $pdf->Footer();
    //$pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    //$cad = '...EN NÚMEROS';
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(2);
    ///estados// 
    /* $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(10, 5, 'No.', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $cont = 1;
    foreach($entidadesNotas as $entidad){
        $pdf::SetTextColor(0,0,0);        
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(10, 5, $cont, 1, 'C', 1, 0, '', '', true);        
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->notas, 1, 'C', 1, 1, '', '', true);   
        $cont++;                                       
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true); */     
    ///areas// 
    /* $pdf::setXY(100,32);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $ctlA = 1;
    foreach($areasNotas as $area){
        $pdf::SetX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $area->nomArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->notas, 1, 'C', 1, 1, '', '', true);
        $ctlA++;                                   
    }
    $pdf::SetX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);     
    $pos = ($ctlA*5)+50; */   
    ///subAreas///
    /* $pdf::setXY(100,$pos);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);    
    $total2 = 0;
    $ctlSA = 1;
    foreach($subAreasNotas as $subArea){
        $pdf::setX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->notas, 1, 'C', 1, 1, '', '', true); 
        $total2 = $total2 + $subArea->notas;   
        $ctlSA++;        
    }
    $pdf::setX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total2, 1, 'C', 1, 0, '', '', true);
    $pos+= ($ctlSA*5) + 15;*/
    //$pdf::Ln(15);    
    ///en resumen///
    /* $pdf::AddPage();*/
    $pdf->Footer();
    //$pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));    
    /* if($cont > 13){
        $pos = ($cont*5)+40;
        $pdf::SetY($pos);
    }  */
    /* $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10); */
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Notas en Medios';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    $pdf::SetFont('dejavusans', '', 12, '', true);           
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);    
    $ctr = 1;
    foreach($resumen as $item){        
        $comp = ($item->notas > 1) ? ' Notas' : ' Nota';
        $cad = $ctr.'.- '.$item->nomSuceso.' en '.$item->nomLugar.', '.$item->nomEntidad.'.';
        $numberOfLines = $pdf::getNumLines($cad, 175);
        $h = $numberOfLines*5;
        $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);    
        $pdf::SetTextColor(35,91,78);            
        $pdf::MultiCell(40, 5,'Impacto: '. $item->notas.$comp, '', 'L', 0, 1, '', '', true);
        $pdf::Ln(2);
        $ctr++;  
        $pdf::SetTextColor(0,0,0);      
    }
    $ctr = 1;
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Sector Ambiental';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    $pdf::SetFont('dejavusans', '', 12, '', true);           
    $pdf::SetTextColor(0,0,0);
    foreach($referenciasAmbiental as $referencia){
        $cad = strtoupper($referencia->nomReferencia);
        //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(188, 149, 92);
        $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
        //$pdf::Ln(5);
        foreach($ambientales as $item){        
            if($referencia->idReferencia == $item->idReferencia){
                $pdf::SetFont('dejavusans', '', 12, '', true);
                $pdf::SetTextColor(0,0,0);
                $cad = $ctr.'.- '.$item->encabezado/* .' - '.$item->referenciaRel->nomReferencia */;
                $numberOfLines = $pdf::getNumLines($cad, 175);
                $h = $numberOfLines*5;
                $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);
                $pdf::Ln(2);
                $ctr++;  
                $pdf::SetTextColor(0,0,0);
            }
        }
    }
    //dd($ambientales);
    ///notas///
    $pdf::AddPage();
    $ctl = 0;
    $ctl2 = 0;
    $ctl3 = 0;
    $ctl4 = 0;
    $ctl5 = 0;
    $ctlO = 0;
    $ctlR = 0;
    foreach($referencias as $referencia){
        $cad = strtoupper($referencia->nomReferencia);        
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(188,149,92);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $mediosDiff = 0;
        if($referencia->idReferencia == 2){
            $mediosNotas = $mediosProcuradora;            
        }
        elseif($referencia->idReferencia == 1){
            $mediosNotas = $mediosProfepa;
        }
        foreach($mediosNotas as $medio){
            //dd($medio);      
            $mediosDiff = $medio->notas;            
            //dd($mediosDiff);
            $cad = strtoupper($medio->nomMedio);
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }
            $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
            $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = strtoupper(($medio->nomMedio));
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }            
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10); 
            if($medio->idMedio == 4){
                $pdf::Ln(-5);
                $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = 'Click en la imágen para ver más información';
                $pdf::SetX(60);
                $pdf::MultiCell(95, 5, $cad, 0, 'C', 0, 0, '', '', true);
                $pdf::Ln(10);
    
            }
            /* if($medio->idMedio == 1){
                $pdf::Ln(-10);
                $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = 'Para ver la nota completa, consultar la sección "ANEXOS"';
                $pdf::SetX(60);
                $pdf::MultiCell(95, 5, $cad, 0, 'C', 0, 0, '', '', true);
                $pdf::Ln(20);
    
            } */           
            $moveX = 0;
            $moveY = 0;            
            foreach($notas as $nota){
                if($nota->subAreaRel == null){
                    $subArea = '';
                }
                else{
                    $subArea = ' - '.$nota->subAreaRel->cveSubArea;
                }
                $nomArea = $nota->areaRel->cveArea;
                if($medio->idMedio == $nota->idMedio){
                    if($nota->idMedio == 4 && $referencia->idReferencia == $nota->idReferencia){ ///red social//                 
                        $img = $pRoot.'/'.$nota->imgNota;                        
                        $link = $nota->link;                        
                        $x = 25+$moveX;                        
                        $pdf::Image($img, $x, '', 80, 100, '', $link, '', false, 300, '', false, false, 1, false, false, false);
                        /* $pdf::SetFont('dejavusans', '', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        $moveX = $moveX+85;
                        $ctl3 = $ctl3+1;
                        $ctl4 = $ctl4+1;
                        $ctlO = $ctlO+1;
                        if($ctl3 == 2){
                            $ctl3 = 0;
                            $pdf::Ln(110);
                            $moveX = 0;
                        }
                        if($ctl4 == 4 && $ctlO != $mediosDiff /* && $ctlR != 1 */){
                            $ctl4 = 0;
                            $moveX = 0;
                            $pdf::AddPage();
                        }
                    }
                    elseif($nota->idMedio == 2 && $referencia->idReferencia == $nota->idReferencia){ ///radio y television///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+7);

                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(190, 5, $cad, 0, 'L', 0, 0, '', '', true);  
                        $pdf::Ln(10);
                        //$pdf::Ln(12);                        
                        /* $img = $pRoot.'/'.$nota->imgNota;
                        $pdf::Image($img,'','', 50, 30, '', '', '', true, 150, '', false, false, 0, false, false, false);
                        $pdf::Ln(35); */
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+10);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J');
                        $pdf::Ln(10); */ 
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);
                        ///link///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">VER NOTA</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(10);                       
                        $ctl5 = $ctl5+1;
                        if($ctl5%2 == 0 && $ctl5 != $mediosDiff){
                            //$ctl5 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
    
                    }
                    elseif($nota->idMedio == 1 && $referencia->idReferencia == $nota->idReferencia){ ///impresos///                      
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+7);
                        //$pdf::Ln(7);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(190, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        //$pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+10);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        //$pdf::Ln(10);
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);   
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);                        
                        //$html = '<a href="https://10.64.2.42/'.$nota->imgNota.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                    
                        ///fuente///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        //$pdf::SetY($y+5);                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        //$pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 0, $cad, 0, 'R', 0, 0, '','', true);
                        $pdf::Ln(30);                       
                        $ctl = $ctl+1;                          
                        if($ctl%2 == 0 && $ctl != $mediosDiff){
                            //$ctl = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
                    }
                    elseif($nota->idMedio == 3 && $referencia->idReferencia == $nota->idReferencia){ ///web///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92);
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(12);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, false, true, false, 'J');
                        $pdf::Ln(5); */
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5); 
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(30);                       
                        $ctl2 = $ctl2+1;
                        if($ctl2%2 == 0 && $ctl2 != $mediosDiff){
                            //$ctl2 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);                           
                        }
                        
                    }
                    
                }                
            }                                                
            $pdf::AddPage();
        }
        $ctlR++;
        $ctl = 0;
        $ctl2 = 0;
        $ctl3 = 0;
        $ctl4 = 0;
        $ctl5 = 0;
        $ctlO = 0;
        //$pdf::AddPage();
    }    
    ///Descanso1///    
    /* $img = $descanso;
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);  
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();                                 
    $pdf::AddPage(); */
    ///información ambiental///
    if($ambientales->count() != 0){        
        $totAmb = $ambientales->count();
        $ctlAmb = 0;
        //$totLines = 0;
        $comp = 0;
        //dd($ctlAmb);
        $cad = 'SECTOR AMBIENTAL';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'SECTOR AMBIENTAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        foreach ($referenciasAmbiental as $referencia) {
            $cad = strtoupper($referencia->nomReferencia);
            //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
            $pdf::SetFont('dejavusans', 'B', 28, '', true);
            $pdf::SetTextColor(188, 149, 92);
            $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(5);
            foreach($ambientales as $ambiental){    
                if($referencia->idReferencia == $ambiental->idReferencia){
                    ///encabezado///    
                    $cadEncabezado = $ambiental->encabezado;
                    $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
                    $cadTexto = str_replace('&nbsp;',' ',$ambiental->texto); 
                    $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
                    $totLines = $numberOfLinesC + $numberOfLinesT;
                    $y =  $pdf::GetY();
                    /* echo '<pre>';
                    echo $y.' '.$totLines;
                    echo '</pre>'; */
                    if($totLines > 35 || $y>195){   
                        $comp = 12;  
                        $ctlAmb = 0;           
                        //$pdf::AddPage();
                        //$pdf::SetAutoPageBreak(true, 10);
                        $pdf::Ln(5);
                        if($y>195){
                            $pdf::AddPage();
                            $comp = 0;
                        }                
                    }
                    ///         
                    $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                    $pdf::SetTextColor(0,0,0);
                    $cad = $ambiental->encabezado;                        
                    $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                    $numberOfLines = $pdf::getNumLines($cad, 190);
                    $pdf::Ln($numberOfLines*5);           
                    $pdf::SetFont('dejavusans', '', 12, '', true);        
                    $pdf::SetTextColor(0,0,0);            
                    $cad = str_replace('&nbsp;',' ',$ambiental->texto);               
                    $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
                    $pdf::Ln(5);
                    $numberOfLines = $pdf::getNumLines($cad, 190);                        
                    $y = $pdf::GetY() + $numberOfLines*5;                                      
                    $pdf::SetY($y+$comp);
                    if(!is_null($ambiental->link)){
                        /* $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);*/
                        $html = '<a href="'.$ambiental->link.'" target="_blank">CONTINUAR LEYENDO</a>'; 
                        /*$y = $pdf::getY();*/
                        $pdf::writeHTMLCell(80, '', '', '', $html, 0, 0, 0, true, 'J', true);
                    }
                    $numberOfLines = $pdf::getNumLines($cad, 190);            
                    $y = $pdf::GetY() /* + $numberOfLines*5 */;              
                    $pdf::SetY($y+15);
                    //$pdf::SetAutoPageBreak(false, 10);
                    $ctlAmb = $ctlAmb +1;
                    //$totLines = 0;
                    $comp = 0;
                    if($ctlAmb == 3 && $ctlAmb < $totAmb){
                        $ctlAmb = 0;
                        $pdf::AddPage();
                        $pdf::Ln(5);                           
                    }
                }
            }
            //$pdf::AddPage();
        }        
    }
    ///Descanso2///
    /* $pdf::AddPage();
    $img = 'assets/images/reader/Descanso02.jpg';
    $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0); */
    ///boletines///
    if($boletines->count() != 0){
        $cad = 'BOLETINES';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = 'BOLETINES';
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10);
        foreach($boletines as $boletin){
            ///encabezado///                        
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $boletin->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln(10);
            ///texto///
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $boletin->textoHtml;
            $pdf::writeHTML($html, true, false, true, false, 'J'); */
            $cad = str_replace('&nbsp;',' ',$boletin->texto);               
            $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $pdf::Ln(5);
            $numberOfLines = $pdf::getNumLines($cad, 190);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y);
        }
        //$pdf::AddPage();
    }
    if($columnas->count() !== 0){
        $pdf::AddPage();
        ///ocho columnas///
        $cad = 'OCHO COLUMNAS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'OCHO COLUMNAS';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
        foreach($columnas as $columna){
            $img = '';
            if($columna->fuente == 'El Universal'){
                $img = 'assets/images/reader/logoElUniversal.png';
            }
            elseif ($columna->fuente == 'Reforma') {
                $img = 'assets/images/reader/logoReforma.png';
            }
            elseif ($columna->fuente == 'Milenio') {
                $img = 'assets/images/reader/logoMilenio.png';
            }
            elseif ($columna->fuente == 'Excelsior') {
                $img = 'assets/images/reader/logoExcelsior.png';
            }
            elseif ($columna->fuente == 'La Jornada') {
                $img = 'assets/images/reader/logoJornada.png';
            }
            //$pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
            ///encabezado///   
            ///////
            $cadEncabezado = $columna->encabezado;
            $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
            $cadTexto = str_replace('&nbsp;',' ',$columna->texto); 
            $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
            $totLines = $numberOfLinesC + $numberOfLinesT;
            $y =  $pdf::GetY();
            if(/* $totLines > 8 &&  */$y>200){   
                /* $comp = 12;  
                $ctlAmb = 0; */           
                $pdf::AddPage();
                //$pdf::SetAutoPageBreak(true, 10);
                $pdf::Ln(5);
                $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
                /* if($y>200){
                    $comp = 0;
                } */                
            }
            //////
            $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);   
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+10); 
            //$pdf::SetX(60); 
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $columna->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln();
            ///texto///            
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $columna->textoHtml;
            $y = $pdf::getY();
            $pdf::writeHTMLCell(130, '', 60, $y+7, $html, 0, 1, 0, true, 'J', true); */            
            $cad = $columna->texto;
            $cad = str_replace('&nbsp;',' ',$columna->texto);
            $numberOfLines = $pdf::getNumLines($cad, 190);              
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+5);         
            $pdf::MultiCell(130, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y+15);
            //$pdf::Ln(5);
            //$moveY = $moveY+45;
        }     
    }
    ///Contraportada///
    $pdf::AddPage();
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $img = 'assets/images/reader/portadas/contraportada.jpg';
    $pdf::Image($img, 0, 0, 217, 280, '', '', '', false, 300, '', false, false, 0);
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();
    /* if($ctlImp != 0){
        $pdf::AddPage();
        ///anexos///
        $cad = 'ANEXOS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'ANEXOS';
        $pdf::SetY(100);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
    } */          
    /* $pdf::addTOCPage();        
    $pdf::SetFont('times', 'B', 28);
    $pdf::MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
    $pdf::Ln();

    $pdf::SetFont('dejavusans', '', 12);
    $pdf::addTOC(1, 'courier', '.', 'CONTENIDO', 'B', array(128,0,0));
    $pdf::endTOCPage(); */
    //ob_clean();
    if($flg == 's'){
        if($horario == 'm'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina '.$hoyEs.'.pdf', 'F');
        }
        elseif($horario == 'v'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina '.$hoyEs.'.pdf', 'F');
        }
        if($f == 'b'){
            $pdf::Output();
        }
        elseif($f == 'ss'){
            return $filePath;
        }
        
        //$pdf::Output();
    }
    elseif($flg == 'v'){
        /* $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf';
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf', 'F');
        return $filePath; */
        $pdf::Output();
    }
}

/// PDF Sintesis Informativa dada un area ///
function pdfSintesisByArea($parametros){
    $pRoot = $_SERVER['DOCUMENT_ROOT'];
    //dd($parametros);    
    $hoyEs = $parametros[0];
    $hoyG = $parametros[1];
    $entidadesNotas = $parametros[2];
    $mediosNotas = $parametros[3];
    $areasNotas = $parametros[4];
    $ctlRed = $parametros[5];
    $ctlTv = $parametros[6];
    $ctlImp = $parametros[7];
    $ctlWeb = $parametros[8];
    $ambientales = $parametros[9];
    $boletines = $parametros[10];
    $columnas = $parametros[11];
    $flg = $parametros[12];
    $total = $parametros[13];
    $notas = $parametros[14];
    $subAreasNotas = $parametros[15];
    $referencias = $parametros[16];
    $mediosProcuradora = $parametros[17];
    $mediosProfepa = $parametros[18];
    $f = $parametros[19];
    $horario = $parametros[20];
    $cveArea = $parametros[21];
    $ctlVerde = $parametros[22];
    $ctlAmarillo = $parametros[23];
    $ctlRojo = $parametros[24];
    $resumen = $parametros[25];
    $referenciasAmbiental = $parametros[28];
    $pdf = new miPDF();    
    $pdf::reset();         
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);   
    $pdf::AddPage('P', 'LETTER');
    //$img = 'assets/images/reader/portadas/sintesisPort'.$hoyG.'.jpg';
    if($horario == 'm'){
        $img = 'assets/images/reader/portadas/sintesisPort_m'.$hoyG.'.jpg';
        $descanso = 'assets/images/reader/Descanso01.jpg';
    }
    elseif($horario == 'v'){
        $img = 'assets/images/reader/portadas/sintesisPort_v'.$hoyG.'.jpg';    
        $descanso = 'assets/images/reader/Descanso02.jpg';        
    }
    //dd($img);
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(30);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(150);        
        $pdf::SetTextColor(105,28,50);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        /* if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $cad = '';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0); */
    }
    else{
        $img = 'assets/images/reader/sintesisPort.jpg';
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(10);
        $pdf::SetTextColor(35,91,78);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }
    //$pdf::ln(5);
    $cad = $areasNotas[0]->nomArea;
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    //$pdf->Footer();
    ///portada///
    /* $pdf::Ln(45);
    $pdf::SetFont('dejavusans', 'B', 20, '', true); */        
    /*$pdf::SetTextColor(137,31,54);
    $cad = 'SÍNTESIS';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $cad = 'INFORMATIVA';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(5);
    $pdf::SetFont('dejavusans', '', 14, '', true);        
    $pdf::SetTextColor(132,125,116);
    $cad = 'COORDINACIÓN DE COMUNICACIÓN SOCIAL';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0); */
   /*  $pdf::Ln(10);
    $pdf::SetTextColor(0,0,0);
    $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0); */
    $pdf::SetAutoPageBreak(true, 0);
    ///en numeros///
    $pdf::AddPage();
    $pdf->Footer();
    //$pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    //$cad = '...EN NÚMEROS';
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(2);
    ///estados// 
    /* $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(10, 5, 'No.', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $cont = 1;
    foreach($entidadesNotas as $entidad){
        $pdf::SetTextColor(0,0,0);        
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(10, 5, $cont, 1, 'C', 1, 0, '', '', true);        
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->notas, 1, 'C', 1, 1, '', '', true);   
        $cont++;                                       
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true); */    
    ///areas// 
    /* $pdf::setXY(100,32);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $ctlA = 1;
    foreach($areasNotas as $area){
        $pdf::SetX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $area->nomArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->notas, 1, 'C', 1, 1, '', '', true);
        $ctlA++;                                   
    }
    $pdf::SetX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);     
    $pos = ($ctlA*5)+50; */
    ///subAreas///
    /* $pdf::setXY(100,$pos);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);    
    $total2 = 0;
    $ctlSA = 1;
    foreach($subAreasNotas as $subArea){
        $pdf::setX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->notas, 1, 'C', 1, 1, '', '', true); 
        $total2 = $total2 + $subArea->notas;   
        $ctlSA++;        
    }
    $pdf::setX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total2, 1, 'C', 1, 0, '', '', true);
    $pos+= ($ctlSA*5) + 15; */
    //$pdf::Ln(15);
    ///en resumen///
    /* $pdf::AddPage();*/
    $pdf->Footer();
    //$pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));    
    /* if($cont > 13){
        $pos = ($cont*5)+45;
        $pdf::SetY($pos);
    } */ 
    /* $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10); */
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Notas en Medios';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);    
    $ctr = 1;
    foreach($resumen as $item){        
        $comp = ($item->notas > 1) ? ' Notas' : ' Nota';
        $cad = $ctr.'.- '.$item->nomSuceso.' en '.$item->nomLugar.', '.$item->nomEntidad.'.';
        $numberOfLines = $pdf::getNumLines($cad, 175);
        $h = $numberOfLines*5;
        $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);    
        $pdf::SetTextColor(35,91,78);            
        $pdf::MultiCell(40, 5,'Impacto: '. $item->notas.$comp, '', 'L', 0, 1, '', '', true);
        $pdf::Ln(2);
        $ctr++;  
        $pdf::SetTextColor(0,0,0);      
    }
    $ctr = 1;
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Sector Ambiental';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    $pdf::SetFont('dejavusans', '', 12, '', true);           
    $pdf::SetTextColor(0,0,0);
    foreach($referenciasAmbiental as $referencia){
        $cad = strtoupper($referencia->nomReferencia);
        //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(188, 149, 92);
        $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
        //$pdf::Ln(5);
        foreach($ambientales as $item){        
            if($referencia->idReferencia == $item->idReferencia){
                $pdf::SetFont('dejavusans', '', 12, '', true);
                $pdf::SetTextColor(0,0,0);
                $cad = $ctr.'.- '.$item->encabezado/* .' - '.$item->referenciaRel->nomReferencia */;
                $numberOfLines = $pdf::getNumLines($cad, 175);
                $h = $numberOfLines*5;
                $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);
                $pdf::Ln(2);
                $ctr++;  
                $pdf::SetTextColor(0,0,0);
            }
        }
    }
    ///notas///
    $pdf::AddPage();
    $ctl = 0;
    $ctl2 = 0;
    $ctl3 = 0;
    $ctl4 = 0;
    $ctl5 = 0;
    $ctlO = 0;
    $ctlR = 0;
    foreach($referencias as $referencia){
        $cad = strtoupper($referencia->nomReferencia);        
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(188,149,92);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $mediosDiff = 0;
        if($referencia->idReferencia == 2){
            $mediosNotas = $mediosProcuradora;            
        }
        elseif($referencia->idReferencia == 1){
            $mediosNotas = $mediosProfepa;
        }
        foreach($mediosNotas as $medio){
            //dd($medio);      
            $mediosDiff = $medio->notas;            
            //dd($mediosDiff);
            $cad = strtoupper($medio->nomMedio);
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }
            $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
            $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = strtoupper(($medio->nomMedio));
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }            
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10); 
            if($medio->idMedio == 4){
                $pdf::Ln(-5);
                $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = 'Click en la imágen para ver más información';
                $pdf::SetX(60);
                $pdf::MultiCell(95, 5, $cad, 0, 'C', 0, 0, '', '', true);
                $pdf::Ln(10);
    
            }
            /* if($medio->idMedio == 1){
                $pdf::Ln(-10);
                $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = 'Para ver la nota completa, consultar la sección "ANEXOS"';
                $pdf::SetX(60);
                $pdf::MultiCell(95, 5, $cad, 0, 'C', 0, 0, '', '', true);
                $pdf::Ln(20);
    
            } */           
            $moveX = 0;
            $moveY = 0;            
            foreach($notas as $nota){
                if($nota->subAreaRel == null){
                    $subArea = '';
                }
                else{
                    $subArea = ' - '.$nota->subAreaRel->cveSubArea;
                }
                $nomArea = $nota->areaRel->cveArea;
                if($medio->idMedio == $nota->idMedio){
                    if($nota->idMedio == 4 && $referencia->idReferencia == $nota->idReferencia){ ///red social//                 
                        $img = $pRoot.'/'.$nota->imgNota;                        
                        $link = $nota->link;                        
                        $x = 25+$moveX;                        
                        $pdf::Image($img, $x, '', 80, 100, '', $link, '', false, 300, '', false, false, 1, false, false, false);
                        /* $pdf::SetFont('dejavusans', '', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        $moveX = $moveX+85;
                        $ctl3 = $ctl3+1;
                        $ctl4 = $ctl4+1;
                        $ctlO = $ctlO+1;
                        if($ctl3 == 2){
                            $ctl3 = 0;
                            $pdf::Ln(110);
                            $moveX = 0;
                        }
                        if($ctl4 == 4 && $ctlO != $mediosDiff /* && $ctlR != 1 */){
                            $ctl4 = 0;
                            $moveX = 0;
                            $pdf::AddPage();
                        }
                    }
                    elseif($nota->idMedio == 2 && $referencia->idReferencia == $nota->idReferencia){ ///radio y television///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(12);                        
                        /* $img = $pRoot.'/'.$nota->imgNota;
                        $pdf::Image($img,'','', 50, 30, '', '', '', true, 150, '', false, false, 0, false, false, false);
                        $pdf::Ln(35); */
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J');
                        $pdf::Ln(10); */ 
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);
                        ///link///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">VER NOTA</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(10);                       
                        $ctl5 = $ctl5+1;
                        if($ctl5%2 == 0 && $ctl5 != $mediosDiff){
                            //$ctl5 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
    
                    }
                    elseif($nota->idMedio == 1 && $referencia->idReferencia == $nota->idReferencia){ ///impresos///                      
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+7);
                        //$pdf::Ln(7);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(190, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        //$pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+10);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        //$pdf::Ln(10);
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);   
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);                        
                        //$html = '<a href="https://10.64.2.42/'.$nota->imgNota.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                    
                        ///fuente///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        //$pdf::SetY($y+5);                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        //$pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 0, $cad, 0, 'R', 0, 0, '','', true);
                        $pdf::Ln(30);                       
                        $ctl = $ctl+1;                          
                        if($ctl%2 == 0 && $ctl != $mediosDiff){
                            //$ctl = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
                    }
                    elseif($nota->idMedio == 3 && $referencia->idReferencia == $nota->idReferencia){ ///web///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92);
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(12);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, false, true, false, 'J');
                        $pdf::Ln(5); */
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5); 
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(30);                       
                        $ctl2 = $ctl2+1;
                        if($ctl2%2 == 0 && $ctl2 != $mediosDiff){
                            //$ctl2 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);                           
                        }
                        
                    }
                    
                }                
            }                                               
            $pdf::AddPage();
        }
        $ctlR++;
        $ctl = 0;
        $ctl2 = 0;
        $ctl3 = 0;
        $ctl4 = 0;
        $ctl5 = 0;
        $ctlO = 0;
        //$pdf::AddPage();
    }   
    ///Descanso1///
    /* $img = $descanso;
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);  
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();                                 
    $pdf::AddPage(); */
    ///información ambiental///    
    if($ambientales->count() != 0){        
        $totAmb = $ambientales->count();
        $ctlAmb = 0;
        //$totLines = 0;
        $comp = 0;
        //dd($ctlAmb);
        $cad = 'SECTOR AMBIENTAL';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'SECTOR AMBIENTAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        foreach ($referenciasAmbiental as $referencia) {
            $cad = strtoupper($referencia->nomReferencia);
            //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
            $pdf::SetFont('dejavusans', 'B', 28, '', true);
            $pdf::SetTextColor(188, 149, 92);
            $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(5);
            foreach($ambientales as $ambiental){    
                if($referencia->idReferencia == $ambiental->idReferencia){
                    ///encabezado///    
                    $cadEncabezado = $ambiental->encabezado;
                    $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
                    $cadTexto = str_replace('&nbsp;',' ',$ambiental->texto); 
                    $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
                    $totLines = $numberOfLinesC + $numberOfLinesT;
                    $y =  $pdf::GetY();
                    /* echo '<pre>';
                    echo $y.' '.$totLines;
                    echo '</pre>'; */
                    if($totLines > 35 || $y>195){   
                        $comp = 12;  
                        $ctlAmb = 0;           
                        //$pdf::AddPage();
                        //$pdf::SetAutoPageBreak(true, 10);
                        $pdf::Ln(5);
                        if($y>195){
                            $pdf::AddPage();
                            $comp = 0;
                        }                
                    }
                    ///         
                    $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                    $pdf::SetTextColor(0,0,0);
                    $cad = $ambiental->encabezado;                        
                    $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                    $numberOfLines = $pdf::getNumLines($cad, 190);
                    $pdf::Ln($numberOfLines*5);           
                    $pdf::SetFont('dejavusans', '', 12, '', true);        
                    $pdf::SetTextColor(0,0,0);            
                    $cad = str_replace('&nbsp;',' ',$ambiental->texto);               
                    $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
                    $pdf::Ln(5);
                    $numberOfLines = $pdf::getNumLines($cad, 190);                        
                    $y = $pdf::GetY() + $numberOfLines*5;                                      
                    $pdf::SetY($y+$comp);
                    if(!is_null($ambiental->link)){
                        /* $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);*/
                        $html = '<a href="'.$ambiental->link.'" target="_blank">CONTINUAR LEYENDO</a>'; 
                        /*$y = $pdf::getY();*/
                        $pdf::writeHTMLCell(80, '', '', '', $html, 0, 0, 0, true, 'J', true);
                    }
                    $numberOfLines = $pdf::getNumLines($cad, 190);            
                    $y = $pdf::GetY() /* + $numberOfLines*5 */;              
                    $pdf::SetY($y+15);
                    //$pdf::SetAutoPageBreak(false, 10);
                    $ctlAmb = $ctlAmb +1;
                    //$totLines = 0;
                    $comp = 0;
                    if($ctlAmb == 3 && $ctlAmb < $totAmb){
                        $ctlAmb = 0;
                        $pdf::AddPage();
                        $pdf::Ln(5);                           
                    }
                }
            }
            //$pdf::AddPage();
        }        
    }
    ///Descanso2///
    /* $pdf::AddPage();
    $img = 'assets/images/reader/Descanso02.jpg';
    $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0); */
    ///boletines///
    if($boletines->count() != 0){
        $cad = 'BOLETINES';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = 'BOLETINES';
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10);
        foreach($boletines as $boletin){
            ///encabezado///                        
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $boletin->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln(10);
            ///texto///
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $boletin->textoHtml;
            $pdf::writeHTML($html, true, false, true, false, 'J'); */
            $cad = str_replace('&nbsp;',' ',$boletin->texto);               
            $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $pdf::Ln(5);
            $numberOfLines = $pdf::getNumLines($cad, 190);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y);
        }
        //$pdf::AddPage();
    }
    if($columnas->count() !== 0){
        $pdf::AddPage();
        ///ocho columnas///
        $cad = 'OCHO COLUMNAS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'OCHO COLUMNAS';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
        foreach($columnas as $columna){
            $img = '';
            if($columna->fuente == 'El Universal'){
                $img = 'assets/images/reader/logoElUniversal.png';
            }
            elseif ($columna->fuente == 'Reforma') {
                $img = 'assets/images/reader/logoReforma.png';
            }
            elseif ($columna->fuente == 'Milenio') {
                $img = 'assets/images/reader/logoMilenio.png';
            }
            elseif ($columna->fuente == 'Excelsior') {
                $img = 'assets/images/reader/logoExcelsior.png';
            }
            elseif ($columna->fuente == 'La Jornada') {
                $img = 'assets/images/reader/logoJornada.png';
            }
            //$pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
            ///encabezado///   
            ///////
            $cadEncabezado = $columna->encabezado;
            $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
            $cadTexto = str_replace('&nbsp;',' ',$columna->texto); 
            $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
            $totLines = $numberOfLinesC + $numberOfLinesT;
            $y =  $pdf::GetY();
            if(/* $totLines > 8 &&  */$y>200){   
                /* $comp = 12;  
                $ctlAmb = 0; */           
                $pdf::AddPage();
                //$pdf::SetAutoPageBreak(true, 10);
                $pdf::Ln(5);
                $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
                /* if($y>200){
                    $comp = 0;
                } */                
            }
            //////
            $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);   
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+10); 
            //$pdf::SetX(60); 
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $columna->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln();
            ///texto///            
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $columna->textoHtml;
            $y = $pdf::getY();
            $pdf::writeHTMLCell(130, '', 60, $y+7, $html, 0, 1, 0, true, 'J', true); */            
            $cad = $columna->texto;
            $cad = str_replace('&nbsp;',' ',$columna->texto);
            $numberOfLines = $pdf::getNumLines($cad, 190);              
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+5);         
            $pdf::MultiCell(130, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y+15);
            //$pdf::Ln(5);
            //$moveY = $moveY+45;
        }     
    }
    ///Contraportada///
    $pdf::AddPage();
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $img = 'assets/images/reader/portadas/contraportada.jpg';
    $pdf::Image($img, 0, 0, 217, 280, '', '', '', false, 300, '', false, false, 0);
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();
    /* if($ctlImp != 0){
        $pdf::AddPage();
        ///anexos///
        $cad = 'ANEXOS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'ANEXOS';
        $pdf::SetY(100);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
    } */          
    /* $pdf::addTOCPage();        
    $pdf::SetFont('times', 'B', 28);
    $pdf::MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
    $pdf::Ln();

    $pdf::SetFont('dejavusans', '', 12);
    $pdf::addTOC(1, 'courier', '.', 'CONTENIDO', 'B', array(128,0,0));
    $pdf::endTOCPage(); */
    //ob_clean();
    if($flg == 's'){
        if($horario == 'm'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$cveArea.' '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$cveArea.' '.$hoyEs.'.pdf', 'F');
        }
        elseif($horario == 'v'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$cveArea.' '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$cveArea.' '.$hoyEs.'.pdf', 'F');
        }
        if($f == 'b'){
            $pdf::Output();
        }
        elseif($f == 'ss'){
            return $filePath;
        }
        
        //$pdf::Output();
    }
    elseif($flg == 'v'){
        /* $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf';
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf', 'F');
        return $filePath; */
        $pdf::Output();
    }
}

/// PDF Sintesis Informativa dada una zona ///
function pdfSintesisByZona($parametros){
    $pRoot = $_SERVER['DOCUMENT_ROOT'];
    //dd($parametros);    
    $hoyEs = $parametros[0];
    $hoyG = $parametros[1];
    $entidadesNotas = $parametros[2];
    $mediosNotas = $parametros[3];
    $areasNotas = $parametros[4];
    $ctlRed = $parametros[5];
    $ctlTv = $parametros[6];
    $ctlImp = $parametros[7];
    $ctlWeb = $parametros[8];
    $ambientales = $parametros[9];
    $boletines = $parametros[10];
    $columnas = $parametros[11];
    $flg = $parametros[12];
    $total = $parametros[13];
    $notas = $parametros[14];
    $subAreasNotas = $parametros[15];
    $referencias = $parametros[16];
    $mediosProcuradora = $parametros[17];
    $mediosProfepa = $parametros[18];
    $f = $parametros[19];
    $horario = $parametros[20];
    $zona = $parametros[21];
    $ctlVerde = $parametros[22];
    $ctlAmarillo = $parametros[23];
    $ctlRojo = $parametros[24];
    $resumen = $parametros[25];
    $referenciasAmbiental = $parametros[28];
    $pdf = new miPDF();    
    $pdf::reset();         
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);   
    $pdf::AddPage('P', 'LETTER');
    //$img = 'assets/images/reader/portadas/sintesisPort'.$hoyG.'.jpg';
    if($horario == 'm'){
        $img = 'assets/images/reader/portadas/sintesisPort_m'.$hoyG.'.jpg';
        $descanso = 'assets/images/reader/Descanso01.jpg';
    }
    elseif($horario == 'v'){
        $img = 'assets/images/reader/portadas/sintesisPort_v'.$hoyG.'.jpg';    
        $descanso = 'assets/images/reader/Descanso02.jpg';        
    }
    //dd($img);
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(30);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(150);        
        $pdf::SetTextColor(105,28,50);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        /* if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $cad = '';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0); */
    }
    else{
        $img = 'assets/images/reader/sintesisPort.jpg';
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(10);
        $pdf::SetTextColor(35,91,78);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        if($horario == 'm'){
            $cad = 'Edición Matutina';
        }
        elseif($horario == 'v'){
            $cad = 'Edición Vespertina';            
        }
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }
    //$pdf::ln(5);
    $cad = ucfirst($zona);
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);    
    $pdf::SetAutoPageBreak(true, 0);
    ///en numeros///
    $pdf::AddPage();
    $pdf->Footer();
    //$pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    //$cad = '...EN NÚMEROS';
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(2);
    ///estados// 
    /* $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(10, 5, 'No.', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $cont = 1;
    foreach($entidadesNotas as $entidad){
        $pdf::SetTextColor(0,0,0);        
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(10, 5, $cont, 1, 'C', 1, 0, '', '', true);        
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->notas, 1, 'C', 1, 1, '', '', true);   
        $cont++;                                       
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true); */    
    ///areas// 
    /* $pdf::setXY(100,32);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);
    $ctlA = 1;
    foreach($areasNotas as $area){
        $pdf::SetX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $area->nomArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->notas, 1, 'C', 1, 1, '', '', true);
        $ctlA++;                                   
    }
    $pdf::SetX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);     
    $pos = ($ctlA*5)+50; */
    ///subAreas///
    /* $pdf::setXY(100,$pos);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'NOTAS', 1, 'C', 1, 1, '', '', true);    
    $total2 = 0;
    $ctlSA = 1;
    foreach($subAreasNotas as $subArea){
        $pdf::setX(100);
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFillColor(221, 201, 163);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 1, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->notas, 1, 'C', 1, 1, '', '', true); 
        $total2 = $total2 + $subArea->notas;   
        $ctlSA++;        
    }
    $pdf::setX(100);
    $pdf::SetTextColor(259,259,259);
    $pdf::SetFillColor(35,91,78);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total2, 1, 'C', 1, 0, '', '', true);
    $pos+= ($ctlSA*5) + 15; */
    //$pdf::Ln(15);
    ///en resumen///
    /* $pdf::AddPage();*/
    $pdf->Footer();
    //$pdf::Bookmark('...EN RESUMEN', 0, 0, '', 'B', array(0,64,128));    
    /* if($cont > 13){
        $pos = ($cont*5)+45;
        $pdf::SetY($pos);
    } */ 
    /* $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN RESUMEN';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10); */
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);    
    $ctr = 1;
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Notas en Medios';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    foreach($resumen as $item){        
        $comp = ($item->notas > 1) ? ' Notas' : ' Nota';
        $cad = $ctr.'.- '.$item->nomSuceso.' en '.$item->nomLugar.', '.$item->nomEntidad.'.';
        $numberOfLines = $pdf::getNumLines($cad, 175);
        $h = $numberOfLines*5;
        $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);    
        $pdf::SetTextColor(35,91,78);            
        $pdf::MultiCell(40, 5,'Impacto: '. $item->notas.$comp, '', 'L', 0, 1, '', '', true);
        $pdf::Ln(2);
        $ctr++;  
        $pdf::SetTextColor(0,0,0);      
    }
    $ctr = 1;
    $pdf::SetFont('dejavusans', 'B', 12, '', true);
    $pdf::SetTextColor(35,91,78); 
    $cad = 'Sector Ambiental';               
    $pdf::MultiCell(0, 5, $cad, 0, 'C', 0, 1, '', '', true);
    $pdf::SetFont('dejavusans', '', 12, '', true);           
    $pdf::SetTextColor(0,0,0);
    foreach($referenciasAmbiental as $referencia){
        $cad = strtoupper($referencia->nomReferencia);
        //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(188, 149, 92);
        $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
        //$pdf::Ln(5);
        foreach($ambientales as $item){        
            if($referencia->idReferencia == $item->idReferencia){
                $pdf::SetFont('dejavusans', '', 12, '', true);
                $pdf::SetTextColor(0,0,0);
                $cad = $ctr.'.- '.$item->encabezado/* .' - '.$item->referenciaRel->nomReferencia */;
                $numberOfLines = $pdf::getNumLines($cad, 175);
                $h = $numberOfLines*5;
                $pdf::MultiCell(175, $h, $cad."\n", 0, 'J', 0, 1, '', '', true);
                $pdf::Ln(2);
                $ctr++;  
                $pdf::SetTextColor(0,0,0);
            }
        }
    }   
    ///notas///
    $pdf::AddPage();
    $ctl = 0;
    $ctl2 = 0;
    $ctl3 = 0;
    $ctl4 = 0;
    $ctl5 = 0;
    $ctlO = 0;
    $ctlR = 0;
    foreach($referencias as $referencia){
        $cad = strtoupper($referencia->nomReferencia);        
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(188,149,92);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $mediosDiff = 0;
        if($referencia->idReferencia == 2){
            $mediosNotas = $mediosProcuradora;            
        }
        elseif($referencia->idReferencia == 1){
            $mediosNotas = $mediosProfepa;
        }
        foreach($mediosNotas as $medio){
            //dd($medio);      
            $mediosDiff = $medio->notas;            
            //dd($mediosDiff);
            $cad = strtoupper($medio->nomMedio);
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }
            $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
            $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = strtoupper(($medio->nomMedio));
            if($medio->idMedio == 2){
                $cad = 'RADIO Y TELEVISIÓN';
            }            
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10); 
            if($medio->idMedio == 4){
                $pdf::Ln(-5);
                $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = 'Click en la imágen para ver más información';
                $pdf::SetX(60);
                $pdf::MultiCell(95, 5, $cad, 0, 'C', 0, 0, '', '', true);
                $pdf::Ln(10);
    
            }                     
            $moveX = 0;
            $moveY = 0;            
            foreach($notas as $nota){
                if($nota->subAreaRel == null){
                    $subArea = '';
                }
                else{
                    $subArea = ' - '.$nota->subAreaRel->cveSubArea;
                }
                $nomArea = $nota->areaRel->cveArea;
                if($medio->idMedio == $nota->idMedio){
                    if($nota->idMedio == 4 && $referencia->idReferencia == $nota->idReferencia){ ///red social//                 
                        $img = $pRoot.'/'.$nota->imgNota;                        
                        $link = $nota->link;                        
                        $x = 25+$moveX;                        
                        $pdf::Image($img, $x, '', 80, 100, '', $link, '', false, 300, '', false, false, 1, false, false, false);
                        /* $pdf::SetFont('dejavusans', '', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        $moveX = $moveX+85;
                        $ctl3 = $ctl3+1;
                        $ctl4 = $ctl4+1;
                        $ctlO = $ctlO+1;
                        if($ctl3 == 2){
                            $ctl3 = 0;
                            $pdf::Ln(110);
                            $moveX = 0;
                        }
                        if($ctl4 == 4 && $ctlO != $mediosDiff /* && $ctlR != 1 */){
                            $ctl4 = 0;
                            $moveX = 0;
                            $pdf::AddPage();
                        }
                    }
                    elseif($nota->idMedio == 2 && $referencia->idReferencia == $nota->idReferencia){ ///radio y television///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(12);                        
                        /* $img = $pRoot.'/'.$nota->imgNota;
                        $pdf::Image($img,'','', 50, 30, '', '', '', true, 150, '', false, false, 0, false, false, false);
                        $pdf::Ln(35); */
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J');
                        $pdf::Ln(10); */ 
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);
                        ///link///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">VER NOTA</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(10);                       
                        $ctl5 = $ctl5+1;
                        if($ctl5%2 == 0 && $ctl5 != $mediosDiff){
                            //$ctl5 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
    
                    }
                    elseif($nota->idMedio == 1 && $referencia->idReferencia == $nota->idReferencia){ ///impresos///                      
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92); 
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+7);
                        //$pdf::Ln(7);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(190, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        //$pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+10);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, 0, true, true, 'J'); */
                        //$pdf::Ln(10);
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5);   
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);                        
                        //$html = '<a href="https://10.64.2.42/'.$nota->imgNota.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                    
                        ///fuente///
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        //$pdf::SetY($y+5);                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        //$pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 0, $cad, 0, 'R', 0, 0, '','', true);
                        $pdf::Ln(30);                       
                        $ctl = $ctl+1;                          
                        if($ctl%2 == 0 && $ctl != $mediosDiff){
                            //$ctl = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);
                        }
                    }
                    elseif($nota->idMedio == 3 && $referencia->idReferencia == $nota->idReferencia){ ///web///
                        ///encabezado///        
                        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                        $pdf::SetTextColor(188,149,92);
                        $cad = $nota->entidadRel->nomEntidad;               
                        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::SetTextColor(35,91,78); 
                        $cad = $nomArea.$subArea;               
                        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(12);
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->encabezado;                        
                        $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                        $pdf::Ln(5);
                        ///contenido///
                        $y = $pdf::GetY();                        
                        $pdf::SetY($y+5);
                        $pdf::SetFont('dejavusans', '', 12, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        /* $html = $nota->textoHtml;
                        $pdf::writeHTML($html, true, false, true, false, 'J');
                        $pdf::Ln(5); */
                        $cad = str_replace('&nbsp;',' ',$nota->texto);               
                        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
                        $numberOfLines = $pdf::getNumLines($cad, 190);                        
                        $y = $pdf::GetY() /* + $numberOfLines*5 */;                          
                        $pdf::SetY($y+5); 
                        ///link///
                        /* $y = $pdf::GetY();                        
                        $pdf::SetY($y); */
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $html = '<a href="'.$nota->link.'" target="_blank">CONTINUAR LEYENDO</a>';  
                        $y = $pdf::getY();
                        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);                          
                        ///fuente///                        
                        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                        $pdf::SetTextColor(0,0,0);
                        $cad = $nota->fuente;                        
                        $pdf::MultiCell(95, 5, $cad, 0, 'R', 0, 0, '', '', true);
                        $pdf::Ln(30);                       
                        $ctl2 = $ctl2+1;
                        if($ctl2%2 == 0 && $ctl2 != $mediosDiff){
                            //$ctl2 = 0;
                            $pdf::AddPage();
                            $pdf::Ln(15);                           
                        }
                        
                    }
                    
                }                
            }                                               
            $pdf::AddPage();
        }
        $ctlR++;
        $ctl = 0;
        $ctl2 = 0;
        $ctl3 = 0;
        $ctl4 = 0;
        $ctl5 = 0;
        $ctlO = 0;
        //$pdf::AddPage();
    }   
    ///Descanso1///
    /* $img = $descanso;
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);  
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();                                 
    $pdf::AddPage(); */
   ///información ambiental///
   if($ambientales->count() != 0){        
    $totAmb = $ambientales->count();
    $ctlAmb = 0;
    //$totLines = 0;
    $comp = 0;
    //dd($ctlAmb);
    $cad = 'SECTOR AMBIENTAL';
    $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = 'SECTOR AMBIENTAL';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(5);
    foreach ($referenciasAmbiental as $referencia) {
        $cad = strtoupper($referencia->nomReferencia);
        //$pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);
        $pdf::SetTextColor(188, 149, 92);
        $pdf::Cell(0, 0, $cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        foreach($ambientales as $ambiental){    
            if($referencia->idReferencia == $ambiental->idReferencia){
                ///encabezado///    
                $cadEncabezado = $ambiental->encabezado;
                $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
                $cadTexto = str_replace('&nbsp;',' ',$ambiental->texto); 
                $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
                $totLines = $numberOfLinesC + $numberOfLinesT;
                $y =  $pdf::GetY();
                /* echo '<pre>';
                echo $y.' '.$totLines;
                echo '</pre>'; */
                if($totLines > 35 || $y>195){   
                    $comp = 12;  
                    $ctlAmb = 0;           
                    //$pdf::AddPage();
                    //$pdf::SetAutoPageBreak(true, 10);
                    $pdf::Ln(5);
                    if($y>195){
                        $pdf::AddPage();
                        $comp = 0;
                    }                
                }
                ///         
                $pdf::SetFont('dejavusans', 'B', 12, '', true);        
                $pdf::SetTextColor(0,0,0);
                $cad = $ambiental->encabezado;                        
                $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
                $numberOfLines = $pdf::getNumLines($cad, 190);
                $pdf::Ln($numberOfLines*5);           
                $pdf::SetFont('dejavusans', '', 12, '', true);        
                $pdf::SetTextColor(0,0,0);            
                $cad = str_replace('&nbsp;',' ',$ambiental->texto);               
                $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
                $pdf::Ln(5);
                $numberOfLines = $pdf::getNumLines($cad, 190);                        
                $y = $pdf::GetY() + $numberOfLines*5;                                      
                $pdf::SetY($y+$comp);
                if(!is_null($ambiental->link)){
                    /* $pdf::SetFont('dejavusans', 'B', 10, '', true);        
                    $pdf::SetTextColor(0,0,0);*/
                    $html = '<a href="'.$ambiental->link.'" target="_blank">CONTINUAR LEYENDO</a>'; 
                    /*$y = $pdf::getY();*/
                    $pdf::writeHTMLCell(80, '', '', '', $html, 0, 0, 0, true, 'J', true);
                }
                $numberOfLines = $pdf::getNumLines($cad, 190);            
                $y = $pdf::GetY() /* + $numberOfLines*5 */;              
                $pdf::SetY($y+15);
                //$pdf::SetAutoPageBreak(false, 10);
                $ctlAmb = $ctlAmb +1;
                //$totLines = 0;
                $comp = 0;
                if($ctlAmb == 3 && $ctlAmb < $totAmb){
                    $ctlAmb = 0;
                    $pdf::AddPage();
                    $pdf::Ln(5);                           
                }
            }
        }
        //$pdf::AddPage();
    }        
}
    ///boletines///
    if($boletines->count() != 0){
        $cad = 'BOLETINES';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
            $pdf::SetTextColor(137,31,54);
            $cad = 'BOLETINES';
            $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
            $pdf::Ln(10);
        foreach($boletines as $boletin){
            ///encabezado///                        
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $boletin->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln(10);
            ///texto///
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $boletin->textoHtml;
            $pdf::writeHTML($html, true, false, true, false, 'J'); */
            $cad = str_replace('&nbsp;',' ',$boletin->texto);               
            $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $pdf::Ln(5);
            $numberOfLines = $pdf::getNumLines($cad, 190);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y);
        }
        //$pdf::AddPage();
    }
    if($columnas->count() !== 0){
        $pdf::AddPage();
        ///ocho columnas///
        $cad = 'OCHO COLUMNAS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'OCHO COLUMNAS';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
        foreach($columnas as $columna){
            $img = '';
            if($columna->fuente == 'El Universal'){
                $img = 'assets/images/reader/logoElUniversal.png';
            }
            elseif ($columna->fuente == 'Reforma') {
                $img = 'assets/images/reader/logoReforma.png';
            }
            elseif ($columna->fuente == 'Milenio') {
                $img = 'assets/images/reader/logoMilenio.png';
            }
            elseif ($columna->fuente == 'Excelsior') {
                $img = 'assets/images/reader/logoExcelsior.png';
            }
            elseif ($columna->fuente == 'La Jornada') {
                $img = 'assets/images/reader/logoJornada.png';
            }
            //$pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
            ///encabezado///   
            ///////
            $cadEncabezado = $columna->encabezado;
            $numberOfLinesC = $pdf::getNumLines($cadEncabezado, 190);
            $cadTexto = str_replace('&nbsp;',' ',$columna->texto); 
            $numberOfLinesT = $pdf::getNumLines($cadTexto, 190); 
            $totLines = $numberOfLinesC + $numberOfLinesT;
            $y =  $pdf::GetY();
            if(/* $totLines > 8 &&  */$y>200){   
                /* $comp = 12;  
                $ctlAmb = 0; */           
                $pdf::AddPage();
                //$pdf::SetAutoPageBreak(true, 10);
                $pdf::Ln(5);
                $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);
                /* if($y>200){
                    $comp = 0;
                } */                
            }
            //////
            $pdf::Image($img, '', '', 45, 35, '', '', '', false, 300, '', false, false, 1, false, false, false);   
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+10); 
            //$pdf::SetX(60); 
            $pdf::SetFont('dejavusans', 'B', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            $cad = $columna->encabezado;                        
            $pdf::MultiCell(0, 5, $cad, 0, 'L', 0, 0, '', '', true);
            $pdf::Ln();
            ///texto///            
            $pdf::SetFont('dejavusans', '', 12, '', true);        
            $pdf::SetTextColor(0,0,0);
            /* $html = $columna->textoHtml;
            $y = $pdf::getY();
            $pdf::writeHTMLCell(130, '', 60, $y+7, $html, 0, 1, 0, true, 'J', true); */            
            $cad = $columna->texto;
            $cad = str_replace('&nbsp;',' ',$columna->texto);
            $numberOfLines = $pdf::getNumLines($cad, 190);              
            $y = $pdf::GetY();
            $pdf::SetXY(60,$y+5);         
            $pdf::MultiCell(130, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
            $y = $pdf::GetY() + $numberOfLines*5;                          
            $pdf::SetY($y+15);
            //$pdf::Ln(5);
            //$moveY = $moveY+45;
        }     
    }
    ///Contraportada///
    $pdf::AddPage();
    $bMargin = $pdf::getBreakMargin();
    $auto_page_break = $pdf::getAutoPageBreak();
    $pdf::SetAutoPageBreak(false, 0);
    $img = 'assets/images/reader/portadas/contraportada.jpg';
    $pdf::Image($img, 0, 0, 217, 280, '', '', '', false, 300, '', false, false, 0);
    $pdf::SetAutoPageBreak($auto_page_break, $bMargin);
    $pdf::setPageMark();
    /* if($ctlImp != 0){
        $pdf::AddPage();
        ///anexos///
        $cad = 'ANEXOS';
        $pdf::Bookmark($cad, 0, 0, '', 'B', array(0,64,128));
        $pdf::SetFont('dejavusans', 'B', 28, '', true);        
        $pdf::SetTextColor(137,31,54);
        $cad = 'ANEXOS';
        $pdf::SetY(100);
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(10);
    } */          
    /* $pdf::addTOCPage();        
    $pdf::SetFont('times', 'B', 28);
    $pdf::MultiCell(0, 0, 'CONTENIDO', 0, 'C', 0, 1, '', '', true, 0);
    $pdf::Ln();

    $pdf::SetFont('dejavusans', '', 12);
    $pdf::addTOC(1, 'courier', '.', 'CONTENIDO', 'B', array(128,0,0));
    $pdf::endTOCPage(); */
    //ob_clean();
    if($flg == 's'){
        if($horario == 'm'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$zona.' '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$zona.' '.$hoyEs.'.pdf', 'F');
        }
        elseif($horario == 'v'){
            $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$zona.' '.$hoyEs.'.pdf';
            $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$zona.' '.$hoyEs.'.pdf', 'F');
        }
        if($f == 'b'){
            $pdf::Output();
        }
        elseif($f == 'ss'){
            return $filePath;
        }
        
        //$pdf::Output();
    }
    elseif($flg == 'v'){
        /* $filePath = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf';
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaValidate '.$hoyEs.'.pdf', 'F');
        return $filePath; */
        $pdf::Output();
    }
}

/// PDF incidencias ///
function pdfIncidencias($parametros){
    //dd($parametros);
    $pRoot = $_SERVER['DOCUMENT_ROOT']; 
    $pDir = $_SERVER['SERVER_NAME'];
    //dd($pDir);
    $hoyEs = $parametros[0];
    $incidencias = $parametros[1];
    $entidadesIncidencias = $parametros[2];
    $hoyG = $parametros[3];
    $flg = $parametros[4];
    $areasIncidencias = $parametros[5];
    $subAreasIncidencias = $parametros[6];
    $total = 0; 
    $pdf = new miPDF();    
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::AddPage('P', 'LETTER');
    $numInc = count($incidencias);
    //dd($numInc);
    $img = 'assets/images/reader/portadas/incidenciasPort'.$hoyG.'.jpg';
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(175);
        $pdf::SetTextColor(221,201,163);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }
    else{
        $pdf::Ln(125);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(137,31,54);
        $cad = 'REPORTE DE';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $cad = 'INCIDENCIAS';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 14, '', true);        
        $pdf::SetTextColor(132,125,116);
        $cad = 'COORDINACIÓN DE COMUNICACIÓN SOCIAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }    
    //Grafica//
    ///estados//     
    $pdf::AddPage('P', 'LETTER');
    $pdf->Footer();
    $pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN NÚMEROS';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10);
    $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);
    foreach($entidadesIncidencias as $entidad){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->incidencias, 1, 'C', 0, 1, '', '', true);  
        $total = $total + $entidad->incidencias;          
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);    
    if($flg == 'v') {
        $grafE = $pRoot.'/storage/imgGraf/valida/grafEdosInc_'.$hoyG.'.png';    
    }
    else{
        $grafE = $pRoot.'/storage/imgGraf/grafEdosInc_'.$hoyG.'.png';   
    }    
    $pdf::Image($grafE, 95, 31, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::Ln(25);
    ///areas// 
    $pdf::setY(115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(50, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);;
    foreach($areasIncidencias as $area){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(50, 5, $area->nomArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->incidencias, 1, 'C', 0, 1, '', '', true);            
    }
    if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafAreasInc_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafAreasInc_'.$hoyG.'.png';   
    }
    $pdf::Image($grafM, 95, 105, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///SubAreas// 
    $pdf::setY(185);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);;
    foreach($subAreasIncidencias as $subArea){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->incidencias, 1, 'C', 0, 1, '', '', true);            
    }
    /* if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafSubAreasInc_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafSubAreasInc_'.$hoyG.'.png';   
    }
    $pdf::Image($grafM, 45, 185, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false); */
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    //Incidencias//
    $pdf::AddPage('P', 'LETTER');  
    $pdf::Bookmark('INCIDENCIAS COMUNICACIÓN SOCIAL', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 15, '', true);
    $pdf::SetTextColor(137,31,54);
    $pdf::SetFont('dejavusans', 'B', 20, '', true);
    $cad = 'INCIDENCIAS';
    $pdf::Cell(0, 5,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(3);
    $pdf::SetTextColor(0,0,0);
    /* $pdf::SetFont('dejavusans', '', 15, '', true);
    $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0); */  
    $pdf::Ln(5);
    ///contenido///
    $totY = 0; 
    $ctlInc = 0;
    $c = 0;
    foreach($incidencias as $incidencia){
        if($incidencia->subAreaRel == null){
            $subArea = '';
        }
        else{
            $subArea = ' - '.$incidencia->subAreaRel->cveSubArea;
        }
        $nomArea = $incidencia->areaRel->cveArea;
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(188,149,92); 
        $cad = $incidencia->entidadRel->nomEntidad;               
        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
        $pdf::SetTextColor(35,91,78); 
        $cad = $nomArea.$subArea;               
        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
        $pdf::Ln(12);
        ///tema///
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = $incidencia->tema;                        
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
        ///resumen///
        $numberOfLines = $pdf::getNumLines($cad, 190);
        $ln  =$numberOfLines*5;
        $pdf::Ln($ln);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$incidencia->resumen);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $numberOfLines = $pdf::getNumLines($cad, 190);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5); 
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);
        $html = '<a href="https://189.254.22.36/incidencias/verIncidencia/'.$incidencia->idIncidencia.'" target="_blank">Ver Incidencia</a>';  
        $y = $pdf::getY();
        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);
        /* $fecha = Carbon::parse($incidencia->fcCrea)->isoFormat('D/M/YY hh:mm a');
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);                                
        $pdf::MultiCell(95, 5, $fecha, 0, 'R', 0, 0, '', '', true); */
        $pdf::Ln(30);
        $ctlInc++;   
        $c++;
        $y = $pdf::GetY();
        $totY += $y;  
        if(($ctlInc == 2 && $c<$total) || $totY > 405){
            $pdf::AddPage('P', 'LETTER'); 
            $ctlInc = 0;
            $totY = 0;
            
        }
        
    }
    /* $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);    
    $pdf::MultiCell(50, 5, 'FECHA DE RECIBIDO', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(75, 5, 'TEMA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(70, 5, 'RESUMEN', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(45, 5, 'ESTADO', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(20, 5, 'VER', 1, 'C', 1, 1, '', '', true); */   
   /*  $totY = 0; 
    $ctlInc = 0;
    $c = 0;
    foreach($incidencias as $incidencia){
        $h = $pdf::getNumLines($incidencia->resumen."\n", 70, '', false, '', 5);
        $p = $h*0.333;
        $h = ($h*5)+$p;        
        $fecha = Carbon::parse($incidencia->fcCrea)->isoFormat('D/M/YY hh:mm a');        
        $pdf::SetFont('dejavusans', '',12, '', true);        
        $pdf::MultiCell(50, $h, $fecha, 1, 'C', 0, 0, '', '', true);
        $pdf::MultiCell(75, $h, $incidencia->tema."\n", 1, 'J', 0, 0, '', '', true);
        $pdf::MultiCell(70, $h, $incidencia->resumen."\n", 1, 'J', 0, 0, '', '', true);
        $pdf::MultiCell(45, $h, $incidencia->entidadRel->nomEntidad, 1, 'L', 0, 0, '', '', true);         
        $html = '<a href="https://189.254.22.36/incidencias/verIncidencia/'.$incidencia->idIncidencia.'" target="_blank">Ver</a>';                 
        $pdf::writeHTMLCell(20, $h, '', '', $html, 1, 1, 0, true, 'C', true);            
        $ctlInc++;   
        $c++;
        $y = $pdf::GetY();
        $totY += $y;        
        if($ctlInc == 2 || $totY > 105 && $c<$total){
            $pdf::AddPage('L', 'LETTER'); 
            $ctlInc = 0;
            $totY = 0;
            $pdf::SetFont('dejavusans', '', 12, '', true);               
            $pdf::SetTextColor(0,0,0);
            $pdf::SetFillColor(221, 201, 163);            
            $pdf::MultiCell(50, 5, 'FECHA DE RECIBIDO', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(75, 5, 'TEMA', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(70, 5, 'RESUMEN', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(45, 5, 'ESTADO', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(20, 5, 'VER', 1, 'C', 1, 1, '', '', true);
        }            
    } */    
    if($flg == 's'){
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/incidenciasPdf/incidencias '.$hoyEs.'.pdf', 'F');        
        $pdf::Output();
    }
    elseif($flg == 'v'){
        $pdf::Output();
    }
}

/// PDF respuestas ///
function pdfRespuestas($parametros){
    //dd($parametros);
    $pRoot = $_SERVER['DOCUMENT_ROOT']; 
    $pDir = $_SERVER['SERVER_NAME'];
    //dd($pDir);
    $hoyEs = $parametros[0];
    $respuestas = $parametros[1];
    $entidadesRespuestas = $parametros[2];
    $hoyG = $parametros[3];
    $flg = $parametros[4];
    $areasRespuestas = $parametros[5];
    $subAreasRespuestas = $parametros[6];
    $notasSend = $parametros[7];
    $sinRespuesta = $parametros[8];
    $totNotasSend = $notasSend->count();
    $total = 0; 
    $pdf = new miPDF();
    $pdf::reset();    
    $pdf::setPrintHeader(false);
    //$pdf::SetAutoPageBreak(false, 0);
    $pdf::AddPage('P', 'LETTER');
    $numResp = count($respuestas);
    //dd($numInc);
    $img = 'assets/images/reader/portadas/respuestasPort'.$hoyG.'.jpg';
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(40);
        $pdf::SetTextColor(0,0,0);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }
    else{
        $pdf::Ln(125);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(137,31,54);
        $cad = 'REPORTE DE';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $cad = 'RESPUESTAS A SOLICITUDES DE INFORMACIÓN';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 14, '', true);        
        $pdf::SetTextColor(132,125,116);
        $cad = 'COORDINACIÓN DE COMUNICACIÓN SOCIAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(35,91,78);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }    
    //Grafica//
    ///estados//     
    $pdf::AddPage('P', 'LETTER');
    $pdf->Footer();
    $pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN NÚMEROS';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10);
    $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);
    foreach($entidadesRespuestas as $entidad){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->respuestas, 1, 'C', 0, 1, '', '', true);  
        $total = $total + $entidad->respuestas;          
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);    
    if($flg == 'v') {
        $grafE = $pRoot.'/storage/imgGraf/valida/grafEdosResp_'.$hoyG.'.png';    
    }
    else{
        $grafE = $pRoot.'/storage/imgGraf/grafEdosResp_'.$hoyG.'.png';   
    }    
    $pdf::Image($grafE, 95, 31, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::Ln(25);
    ///areas// 
    $pdf::setY(115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(50, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);;
    foreach($areasRespuestas as $area){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(50, 5, $area->nomArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->respuestas, 1, 'C', 0, 1, '', '', true);            
    }
    /* if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafAreasResp_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafAreasResp_'.$hoyG.'.png';   
    } */
    /* $pdf::Image($grafM, 95, 105, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false); */
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///SubAreas// 
    $pdf::setXY(100,115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);;
    foreach($subAreasRespuestas as $subArea){
        $pdf::setX(100);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->respuestas, 1, 'C', 0, 1, '', '', true);            
    }    
    $pdf::setX(100);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///Sin Respuesta///
    $pdf::setXY(50,165);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(100, 5, 'SOLICITUDES SIN RESPUESTA', 1, 'C', 1, 1, '', '', true);
    $pdf::setXY(50,170);
    $pdf::MultiCell(65, 5, 'ENTIDAD', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(35, 5, 'SOLICITUDES', 1, 'C', 1, 1, '', '', true);    
    $total2 = 0;
    foreach($sinRespuesta as $key){
        $pdf::setX(50);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $key->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(35, 5, $key->solicitudes, 1, 'C', 0, 1, '', '', true); 
        $total2 = $total2 + $key->solicitudes;           
    }
    $pdf::setX(50);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(35, 5, $total2, 1, 'C', 1, 0, '', '', true);
    //Respuestas//
    $pdf::AddPage('P', 'LETTER');  
    $pdf::Bookmark('RESPUESTAS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 15, '', true);
    $pdf::SetTextColor(137,31,54);
    $pdf::SetFont('dejavusans', 'B', 20, '', true);
    $cad = 'RESPUESTA A SOLICITUDES DE INFORMACIÓN';
    $pdf::Cell(0, 5,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(3);
    $pdf::SetTextColor(0,0,0);     
    $pdf::Ln(5);
    ///contenido///
    $totY = 0; 
    $ctlResp = 0;
    $c = 0;
    foreach($respuestas as $respuesta){
        if($respuesta->subAreaRel == null){
            $subArea = '';
        }
        else{
            $subArea = ' - '.$respuesta->subAreaRel->cveSubArea;
        }
        $nomArea = $respuesta->areaRel->cveArea;
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(188,149,92); 
        $cad = $respuesta->entidadRel->nomEntidad;               
        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
        $pdf::SetTextColor(35,91,78); 
        $cad = $nomArea.$subArea;               
        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
        $pdf::Ln(8);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, 'Antecedente', 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        ///encabezado///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = $respuesta->notaRel->encabezado;                        
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
        ///antecedente///
        $numberOfLines = $pdf::getNumLines($cad, 190);
        $e1 = $numberOfLines;        
        $ln  =$numberOfLines*5;
        $pdf::Ln($ln);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$respuesta->notaRel->texto);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $numberOfLines = $pdf::getNumLines($cad, 190);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5);
        $e2 = $numberOfLines+$e1;
        //dd($numberOfLines+$e1);
        ///actuación///        
        $cadEnc = 'Actuación de la PROFEPA';
        $numberOfLinesE = $pdf::getNumLines($cadEnc, 190);
        $cadAct = str_replace('&nbsp;',' ',$respuesta->actuacion);
        $numberOfLinesA = $pdf::getNumLines($cadAct, 190);
        $totalLines = $numberOfLinesE +$numberOfLinesA+$e2; // antecedente y actuación
        //echo $totalLines.' | ';
        //dd($totalLines);
        if($totalLines < 40){
            //$pdf::AddPage('P', 'LETTER');
            //dd($totalLines);
            //$pdf::SetAutoPageBreak(true, 10);
            //$totalLines = 0;
            //$pdf::SetAutoPageBreak(false, 0);
        }
        ///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, $cadEnc, 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);                       
        $pdf::MultiCell(190, 5, $cadAct."\n", 0, 'J', 0, 2, '', '', true);                       
        $numberOfLines = $pdf::getNumLines($cad, 190);
        //$pdf::SetAutoPageBreak(false, 0);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5);
        /* if($totalLines >= 40){
            $pdf::SetAutoPageBreak(true, 10);
            $pdf::AddPage('P', 'LETTER');
            $totalLines = 0;
        } */
        ///acción///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, 'Acciones a Realizar', 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$respuesta->acciones);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $ctlResp++;
        $c++;
        if($ctlResp == 1 && $c<$total){
            $pdf::AddPage('P', 'LETTER'); 
            $ctlResp = 0;
        }
    }
    if($flg == 's'){
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/respuestasPdf/respuestas '.$hoyEs.'.pdf', 'F');
        return true;
        //$pdf::Output();
    }
    elseif($flg == 'v' || $flg == 'b'){
        $pdf::Output();
    }
}

/// Clases ///
class miPDF extends PDF
{
    public function Footer(){
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-15);
            $pdf->SetFont('helvetica','I',8);
            $pdf->Cell(0,10,$pdf->getAliasNumPage().' de '.$pdf->getAliasNbPages(),0,false,'R',0,'',0,false,'T','M');

        });
    }   
}
