<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;
use Mail;
use Carbon\Carbon;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\Medio;
use App\Nota;
use App\NotaEntidad;
use App\OchoColumnas;
use App\Boletin;
use App\InfoAmbiental;
use App\Incidencia;
use App\Respuesta;
use App\Referencia;
use App\SolicitudInfo;
use App\TemaRelevante;

class HelperController extends Controller
{
    /////////////////////////////
    /// Sintesis Informativa ///
    ///////////////////////////
    
    ////////////////////
    /// Incidencias ///
    //////////////////

    /// valida ///
    public function validaPdfIncidencias(){               
        $hoyEs = Carbon::today()->addDay(1)->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = [];  
        $dt =Carbon::now()->addDay(1); 
        $rango = getRangoVerifInc($dt);
        //dd($rango);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidencias($fcIni, $fcFin);
        $incidenciasEdos = Incidencia::getIncidecniasByDate($fcIni, $fcFin);
        //dd($incidenciasEdos);
        foreach($incidenciasEdos as $incidenciasEdo){
            array_push($ejeX,$incidenciasEdo->nomEntidad);
            array_push($ejeY,$incidenciasEdo->incidencias);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafEdosInc_'.$hoyG.'.png';        
        $flg = 'v';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);          
        ///areas///
        $areasIncidencias = Area::areasIncidenciasByDate($fcIni, $fcFin);        
        foreach($areasIncidencias as $areaIncidencias){
            array_push($areasData, $areaIncidencias->incidencias);
            array_push($areasLabel, $areaIncidencias->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);   
        ///SubAreas///
        $subAreasIncidencias = SubArea::getSubAreasIncidenciasByDate($fcIni, $fcFin);
        //dd($subAreasIncidencias);
        foreach($subAreasIncidencias as $subAreaIncidencias){
            array_push($subAreasData, $subAreaIncidencias->incidencias);
            array_push($subAreasLabel, $subAreaIncidencias->nomSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);            
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/valida';
        $filename = 'grafSubAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        $parametros = [$hoyEs, $incidencias, $incidenciasEdos, $hoyG, $flg, $areasIncidencias, $subAreasIncidencias];
        $reporte = pdfIncidenciasH($parametros);
    }
    /// reader ///
    public function generatePDFIncidencias(){
        //dd('PDF');
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];  
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = []; 
        $dt =Carbon::now(); 
        $rango = getRangoInc($dt);
        //dd($rango);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidencias($fcIni, $fcFin);
        $incidenciasEdos = Incidencia::getIncidecniasByDate($fcIni, $fcFin);
        //dd($incidenciasEdos);
        foreach($incidenciasEdos as $incidenciasEdo){
            array_push($ejeX,$incidenciasEdo->nomEntidad);
            array_push($ejeY,$incidenciasEdo->incidencias);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.$chartEstados;
        $linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdosInc_'.$hoyG.'.png';
        $flg = 's';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img); 
        ///areas///
        $areasIncidencias = Area::areasIncidenciasByDate($fcIni, $fcFin);        
        foreach($areasIncidencias as $areaIncidencias){
            array_push($areasData, $areaIncidencias->incidencias);
            array_push($areasLabel, $areaIncidencias->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);        
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);   
        ///SubAreas///
        $subAreasIncidencias = SubArea::getSubAreasIncidenciasByDate($fcIni, $fcFin);
        //dd($subAreasIncidencias);
        foreach($subAreasIncidencias as $subAreaIncidencias){
            array_push($subAreasData, $subAreaIncidencias->incidencias);
            array_push($subAreasLabel, $subAreaIncidencias->nomSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        $chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);            
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafSubAreasInc_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);           
        $parametros = [$hoyEs, $incidencias, $incidenciasEdos, $hoyG, $flg, $areasIncidencias, $subAreasIncidencias];
        $reporte = pdfIncidenciasH($parametros);
    }

    ////////////////////
    /// Respuestas ///
    //////////////////

    /// reader ///
    public function generatePDFRespuestas(){
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        $hoyG = date('Y-m-d');       
        $ejeX = [];
        $ejeY = [];       
        $areasData = [];
        $areasLabel = [];
        $areasG = [];  
        $subAreasData = [];
        $subAreasLabel = [];
        $subAreasG = []; 
        $dt =Carbon::now(); 
        $rango = getRangoInc($dt);        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];       
        $notasSend = Nota::getNotasEnviadasByDate($fcIni, $fcFin); 
        $sinRespuesta = Nota::sinRespuestaByDate($fcIni, $fcFin);
        $respuestas = Respuesta::getRespuestas($fcIni, $fcFin);
        $respuestasEdos = Respuesta::getRespuestasByDate($fcIni, $fcFin);        
        foreach($respuestasEdos as $respuestasEdo){
            array_push($ejeX,trim($respuestasEdo->nomEntidad));
            array_push($ejeY,$respuestasEdo->respuestas);            
        }  
        $toX = implode("','",$ejeX);
        $toY = implode(",",$ejeY);
        $chartEstados = "{type:'bar',data:{labels:['$toX'],datasets:[{data:[$toY],backgroundColor: [
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)',
            'rgba(159, 34, 65,0.7)',
            'rgba(35, 91, 78,0.7)',
            'rgba(221, 201, 163,0.7)',
            'rgba(152, 152, 154,0.7)',
            'rgba(105, 28, 50,0.7)',
            'rgba(16, 49, 43,0.7)',
            'rgba(188, 149, 92,0.7)',
            'rgba(111, 114, 113,0.7)'            
        ],}]},options:{legend:{display: false,
        },scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    stepSize: 1
                },
                scaleLabel: {
                    display: true,
                    labelString: 'Notas'
                }
            }]
        },},}";
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartEstados);
        //$linkChart = str_replace(' ','',$linkChart);
        $linkChart = str_replace("\n",'',$linkChart);                   
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafEdosResp_'.$hoyG.'.png';
        $flg = 's';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        ///areas///
        $areasRespuestas = Area::areasRespuestasByDate($fcIni, $fcFin);                
        foreach($areasRespuestas as $areaRespuestas){
            array_push($areasData, $areaRespuestas->respuestas);
            array_push($areasLabel, $areaRespuestas->nomArea);
        }
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        $areasL = implode("','",$areasG['labels']);
        $areasD = implode(",",$areasG['data']);
        $chartAreas = "{
            type: 'doughnut',
            data: {
                labels:['$areasL'],
              datasets: [{
                data:[$areasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartAreas = str_replace(' ','',$chartAreas);
        $chartAreas = str_replace("\n",'',$chartAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartAreas);                
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img); 
        ///SubAreas///
        $subAreasRespuestas = SubArea::getSubAreasRespuestasByDate($fcIni, $fcFin);        
        foreach($subAreasRespuestas as $subAreaRespuestas){
            array_push($subAreasData, $subAreaRespuestas->respuestas);
            array_push($subAreasLabel, $subAreaRespuestas->cveSubArea);
        }
        $subAreasG = [
            'labels' => $subAreasLabel,
            'data' => $subAreasData
        ];
        $subAreasL = implode("','",$subAreasG['labels']);
        $subAreasD = implode(",",$subAreasG['data']);
        $chartSubAreas = "{
            type: 'doughnut',
            data: {
                labels:['$subAreasL'],
              datasets: [{
                data:[$subAreasD],
                backgroundColor: [
                    'rgba(159, 34, 65, 0.7)',
                    'rgba(35, 91, 78, 0.7)',
                    'rgba(221, 201, 163, 0.7)',
                    'rgba(152, 152, 154, 0.7)',
                    'rgba(105, 28, 50, 0.7)'
                ],
              }]
            },
            options: {
              plugins: {
                datalabels: {
                  display: true,
                  backgroundColor: '#fff',
                  borderRadius: 3,
                  font: {  
                    size:20,                  
                    weight: 'bold',
                  }
                },
              }
            }
          }";
        //$chartSubAreas = str_replace(' ','',$chartSubAreas);
        $chartSubAreas = str_replace("\n",'',$chartSubAreas);
        $linkChart = 'https://quickchart.io/chart?c='.urlencode($chartSubAreas);          
        $img = file_get_contents($linkChart);
        $destinationPath ='public/imgGraf/';
        $filename = 'grafSubAreasResp_'.$hoyG.'.png';
        $imgGraf = Storage::put($destinationPath.'/'.$filename, $img);
        $parametros = [$hoyEs, $respuestas, $respuestasEdos, $hoyG, $flg, $areasRespuestas, $subAreasRespuestas,
                        $notasSend, $sinRespuesta];
        $reporte = pdfRespuestasH($parametros);
    }

    ///////////////////////////////////
    /// Solicitudes de Información ///
    /////////////////////////////////
    public function enviaSolInfo(){
        //$mail = Auth::user()->email;
        //dd('hola');
        $msg = '';
        $hoy = Carbon::today();
        $inicio = $hoy->setTime(0, 0, 0)->toDateTimeString();
        $fin = $hoy->setTime(23, 59, 0)->toDateTimeString();
        $copyTo = 'miguel.dorantes@profepa.gob.mx';
        $subject = 'Solicitud de Información';
        $solicitudes = SolicitudInfo::where('status',null)->whereBetween('fcSolicita',[$inicio,$fin])->get();        
        if(!$solicitudes->isEmpty()){
            foreach($solicitudes as $item){
                if($item->idTemaRel != null){
                    $origen = TemaRelevante::select('temaRel','idEntidad')->where('idTemaRel',$item->idTemaRel)->first();
                    $item->origen = $origen->temaRel;
                    $item->idEntidad = $origen->idEntidad;
                }
                elseif($item->idNota != null){
                    $origen = Nota::where('idNota',$item->idNota)->first();                    
                    $item->origen = $origen->encabezado;
                    $item->idEntidad = $origen->idEntidad;
                    $origen->idTipo = 1;
                    $origen->statusEnvio = 1;                    
                    $origen->save();
                }
                if($origen->idEntidad == null){
                    $item->destino = ['lady.esquivel@profepa.gob.mx', 'fernando.reyna@profepa.gob.mx'];
                }
                else{
                    $item->destino = getMailEntidad($origen->idEntidad);
                }
                $mailTo = $item->destino;                 
                //envio de correo//                                
                Mail::send('admin.correoSolInfo',['solicitud'=> $item], function($message) use($mailTo, $subject, $copyTo) {            
                    $message->to($mailTo)->subject($subject)->cc($copyTo);                     
                });                    
                if (Mail::failures()) {
                    $msg.= 'Error de envio: '.$mailTo."\n";
                }
                else{                          
                    $solicitudIn = SolicitudInfo::where('idSolicitudInfo',$item->idSolicitudInfo)->first();                    
                    $solicitudIn->status = 1;
                    $solicitudIn->save();
                    $msg.= 'Hecho - ';
                }            
            }                      
        }
        else{
            $msg = 'Nada por enviar';            
        }        
        return $msg;   
    }

}

/// Funciones ///

/// PDF incidencias ///
function pdfIncidenciasH($parametros){
    //dd($parametros);
    $pRoot = $_SERVER['DOCUMENT_ROOT']; 
    $pDir = $_SERVER['SERVER_NAME'];
    //dd($pDir);
    $hoyEs = $parametros[0];
    $incidencias = $parametros[1];
    $entidadesIncidencias = $parametros[2];
    $hoyG = $parametros[3];
    $flg = $parametros[4];
    $areasIncidencias = $parametros[5];
    $subAreasIncidencias = $parametros[6];
    $total = 0; 
    $pdf = new miPDFH();    
    $pdf::reset();
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::AddPage('P', 'LETTER');
    $numInc = count($incidencias);
    //dd($numInc);
    $img = 'assets/images/reader/portadas/incidenciasPort'.$hoyG.'.jpg';
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(175);
        $pdf::SetTextColor(221,201,163);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }
    else{
        $pdf::Ln(125);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(137,31,54);
        $cad = 'REPORTE DE';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $cad = 'INCIDENCIAS';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 14, '', true);        
        $pdf::SetTextColor(132,125,116);
        $cad = 'COORDINACIÓN DE COMUNICACIÓN SOCIAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    }    
    //Grafica//
    ///estados//     
    $pdf::AddPage('P', 'LETTER');
    $pdf->Footer();
    $pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN NÚMEROS';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10);
    $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);
    foreach($entidadesIncidencias as $entidad){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->incidencias, 1, 'C', 0, 1, '', '', true);  
        $total = $total + $entidad->incidencias;          
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);    
    if($flg == 'v') {
        $grafE = $pRoot.'/storage/imgGraf/valida/grafEdosInc_'.$hoyG.'.png';    
    }
    else{
        $grafE = $pRoot.'/storage/imgGraf/grafEdosInc_'.$hoyG.'.png';   
    }    
    $pdf::Image($grafE, 95, 31, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::Ln(25);
    ///areas// 
    $pdf::setY(115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(50, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);;
    foreach($areasIncidencias as $area){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(50, 5, $area->nomArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->incidencias, 1, 'C', 0, 1, '', '', true);            
    }
    if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafAreasInc_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafAreasInc_'.$hoyG.'.png';   
    }
    $pdf::Image($grafM, 95, 105, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///SubAreas// 
    $pdf::setY(185);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'INCIDENCIAS', 1, 'C', 1, 1, '', '', true);;
    foreach($subAreasIncidencias as $subArea){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->incidencias, 1, 'C', 0, 1, '', '', true);            
    }
    /* if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafSubAreasInc_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafSubAreasInc_'.$hoyG.'.png';   
    }
    $pdf::Image($grafM, 45, 185, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false); */
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    //Incidencias//
    $pdf::AddPage('P', 'LETTER');  
    $pdf::Bookmark('INCIDENCIAS COMUNICACIÓN SOCIAL', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 15, '', true);
    $pdf::SetTextColor(137,31,54);
    $pdf::SetFont('dejavusans', 'B', 20, '', true);
    $cad = 'INCIDENCIAS';
    $pdf::Cell(0, 5,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(3);
    $pdf::SetTextColor(0,0,0);
    /* $pdf::SetFont('dejavusans', '', 15, '', true);
    $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0); */  
    $pdf::Ln(5);
    ///contenido///
    $totY = 0; 
    $ctlInc = 0;
    $c = 0;
    foreach($incidencias as $incidencia){
        if($incidencia->subAreaRel == null){
            $subArea = '';
        }
        else{
            $subArea = ' - '.$incidencia->subAreaRel->cveSubArea;
        }
        $nomArea = $incidencia->areaRel->cveArea;
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(188,149,92); 
        $cad = $incidencia->entidadRel->nomEntidad;               
        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
        $pdf::SetTextColor(35,91,78); 
        $cad = $nomArea.$subArea;               
        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
        $pdf::Ln(12);
        ///tema///
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = $incidencia->tema;                        
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
        ///resumen///
        $numberOfLines = $pdf::getNumLines($cad, 190);
        $ln  =$numberOfLines*5;
        $pdf::Ln($ln);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$incidencia->resumen);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $numberOfLines = $pdf::getNumLines($cad, 190);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5); 
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);
        $html = '<a href="https://189.254.22.36/incidencias/verIncidencia/'.$incidencia->idIncidencia.'" target="_blank">Ver Incidencia</a>';  
        $y = $pdf::getY();
        $pdf::writeHTMLCell(80, '', '', $y, $html, 0, 0, 0, true, 'J', true);
        /* $fecha = Carbon::parse($incidencia->fcCrea)->isoFormat('D/M/YY hh:mm a');
        $pdf::SetFont('dejavusans', 'B', 10, '', true);        
        $pdf::SetTextColor(0,0,0);                                
        $pdf::MultiCell(95, 5, $fecha, 0, 'R', 0, 0, '', '', true); */
        $pdf::Ln(30);
        $ctlInc++;   
        $c++;
        $y = $pdf::GetY();
        $totY += $y;  
        if(($ctlInc == 2 && $c<$total) || $totY > 405){
            $pdf::AddPage('P', 'LETTER'); 
            $ctlInc = 0;
            $totY = 0;
            
        }
        
    }
    /* $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);    
    $pdf::MultiCell(50, 5, 'FECHA DE RECIBIDO', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(75, 5, 'TEMA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(70, 5, 'RESUMEN', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(45, 5, 'ESTADO', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(20, 5, 'VER', 1, 'C', 1, 1, '', '', true); */   
   /*  $totY = 0; 
    $ctlInc = 0;
    $c = 0;
    foreach($incidencias as $incidencia){
        $h = $pdf::getNumLines($incidencia->resumen."\n", 70, '', false, '', 5);
        $p = $h*0.333;
        $h = ($h*5)+$p;        
        $fecha = Carbon::parse($incidencia->fcCrea)->isoFormat('D/M/YY hh:mm a');        
        $pdf::SetFont('dejavusans', '',12, '', true);        
        $pdf::MultiCell(50, $h, $fecha, 1, 'C', 0, 0, '', '', true);
        $pdf::MultiCell(75, $h, $incidencia->tema."\n", 1, 'J', 0, 0, '', '', true);
        $pdf::MultiCell(70, $h, $incidencia->resumen."\n", 1, 'J', 0, 0, '', '', true);
        $pdf::MultiCell(45, $h, $incidencia->entidadRel->nomEntidad, 1, 'L', 0, 0, '', '', true);         
        $html = '<a href="https://189.254.22.36/incidencias/verIncidencia/'.$incidencia->idIncidencia.'" target="_blank">Ver</a>';                 
        $pdf::writeHTMLCell(20, $h, '', '', $html, 1, 1, 0, true, 'C', true);            
        $ctlInc++;   
        $c++;
        $y = $pdf::GetY();
        $totY += $y;        
        if($ctlInc == 2 || $totY > 105 && $c<$total){
            $pdf::AddPage('L', 'LETTER'); 
            $ctlInc = 0;
            $totY = 0;
            $pdf::SetFont('dejavusans', '', 12, '', true);               
            $pdf::SetTextColor(0,0,0);
            $pdf::SetFillColor(221, 201, 163);            
            $pdf::MultiCell(50, 5, 'FECHA DE RECIBIDO', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(75, 5, 'TEMA', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(70, 5, 'RESUMEN', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(45, 5, 'ESTADO', 1, 'C', 1, 0, '', '', true);
            $pdf::MultiCell(20, 5, 'VER', 1, 'C', 1, 1, '', '', true);
        }            
    } */    
    if($flg == 's'){
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/incidenciasPdf/incidencias '.$hoyEs.'.pdf', 'F');        
        return true;
    }
    elseif($flg == 'v'){
        $pdf::Output();
    }
}

/// PDF respuestas ///
function pdfRespuestasH($parametros){
    //dd($parametros);
    $pRoot = $_SERVER['DOCUMENT_ROOT']; 
    $pDir = $_SERVER['SERVER_NAME'];
    //dd($pDir);
    $hoyEs = $parametros[0];
    $respuestas = $parametros[1];
    $entidadesRespuestas = $parametros[2];
    $hoyG = $parametros[3];
    $flg = $parametros[4];
    $areasRespuestas = $parametros[5];
    $subAreasRespuestas = $parametros[6];
    $notasSend = $parametros[7];
    $sinRespuesta = $parametros[8];
    $totNotasSend = $notasSend->count();
    $total = 0; 
    $pdf = new miPDFH();   
    $pdf::reset(); 
    $pdf::setPrintHeader(false);
    $pdf::SetAutoPageBreak(false, 0);
    $pdf::AddPage('P', 'LETTER');
    $numResp = count($respuestas);
    //dd($numInc);
    $img = 'assets/images/reader/portadas/respuestasPort'.$hoyG.'.jpg';
    if(file_exists($img)){
        $pdf::Image($img, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        $pdf::Ln(45);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::Ln(40);
        $pdf::SetTextColor(0,0,0);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }
    else{
        $pdf::Ln(125);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(137,31,54);
        $cad = 'REPORTE DE';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $cad = 'RESPUESTAS A SOLICITUDES DE INFORMACIÓN';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 14, '', true);        
        $pdf::SetTextColor(132,125,116);
        $cad = 'COORDINACIÓN DE COMUNICACIÓN SOCIAL';
        $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', 'B', 20, '', true);
        $pdf::SetTextColor(35,91,78);
        $pdf::Cell(0, 0,$hoyEs, 0, 1, 'C', 0, '', 0);
    }    
    //Grafica//
    ///estados//     
    $pdf::AddPage('P', 'LETTER');
    $pdf->Footer();
    $pdf::Bookmark('...EN NÚMEROS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 28, '', true);        
    $pdf::SetTextColor(137,31,54);
    $cad = '...EN NÚMEROS';
    $pdf::Cell(0, 0,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(10);
    $pdf::SetFont('dejavusans', '', 12, '', true);               
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'ESTADOS', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);
    foreach($entidadesRespuestas as $entidad){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(40, 5, $entidad->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $entidad->respuestas, 1, 'C', 0, 1, '', '', true);  
        $total = $total + $entidad->respuestas;          
    }
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(40, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);    
    if($flg == 'v') {
        $grafE = $pRoot.'/storage/imgGraf/valida/grafEdosResp_'.$hoyG.'.png';    
    }
    else{
        $grafE = $pRoot.'/storage/imgGraf/grafEdosResp_'.$hoyG.'.png';   
    }    
    $pdf::Image($grafE, 95, 31, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf::Ln(25);
    ///areas// 
    $pdf::setY(115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(50, 5, 'AREA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);;
    foreach($areasRespuestas as $area){
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(50, 5, $area->nomArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $area->respuestas, 1, 'C', 0, 1, '', '', true);            
    }
    /* if($flg == 'v'){
        $grafM = $pRoot.'/storage/imgGraf/valida/grafAreasResp_'.$hoyG.'.png';
    }
    else{
        $grafM = $pRoot.'/storage/imgGraf/grafAreasResp_'.$hoyG.'.png';   
    }
    $pdf::Image($grafM, 95, 105, 105, 70, '', '', '', true, 150, '', false, false, 0, false, false, false); */
    $pdf::MultiCell(50, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///SubAreas// 
    $pdf::setXY(100,115);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(65, 5, 'MATERIA', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, 'RESPUESTAS', 1, 'C', 1, 1, '', '', true);;
    foreach($subAreasRespuestas as $subArea){
        $pdf::setX(100);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $subArea->cveSubArea, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(30, 5, $subArea->respuestas, 1, 'C', 0, 1, '', '', true);            
    }    
    $pdf::setX(100);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(30, 5, $total, 1, 'C', 1, 0, '', '', true);
    ///Sin Respuesta///
    $pdf::setXY(50,165);
    $pdf::SetFont('dejavusans', '', 12, '', true);        
    $pdf::SetTextColor(0,0,0);
    $pdf::SetFillColor(221, 201, 163);
    $pdf::MultiCell(100, 5, 'SOLICITUDES SIN RESPUESTA', 1, 'C', 1, 1, '', '', true);
    $pdf::setXY(50,170);
    $pdf::MultiCell(65, 5, 'ENTIDAD', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(35, 5, 'SOLICITUDES', 1, 'C', 1, 1, '', '', true);    
    $total2 = 0;
    foreach($sinRespuesta as $key){
        $pdf::setX(50);
        $pdf::SetFont('dejavusans', '',10, '', true);
        $pdf::MultiCell(65, 5, $key->nomEntidad, 1, 'L', 0, 0, '', '', true);
        $pdf::MultiCell(35, 5, $key->solicitudes, 1, 'C', 0, 1, '', '', true); 
        $total2 = $total2 + $key->solicitudes;           
    }
    $pdf::setX(50);
    $pdf::MultiCell(65, 5, 'TOTAL', 1, 'C', 1, 0, '', '', true);
    $pdf::MultiCell(35, 5, $total2, 1, 'C', 1, 0, '', '', true);
    //Respuestas//
    $pdf::AddPage('P', 'LETTER');  
    $pdf::Bookmark('RESPUESTAS', 0, 0, '', 'B', array(0,64,128));
    $pdf::SetFont('dejavusans', 'B', 15, '', true);
    $pdf::SetTextColor(137,31,54);
    $pdf::SetFont('dejavusans', 'B', 20, '', true);
    $cad = 'RESPUESTA A SOLICITUDES DE INFORMACIÓN';
    $pdf::Cell(0, 5,$cad, 0, 1, 'C', 0, '', 0);
    $pdf::Ln(3);
    $pdf::SetTextColor(0,0,0);     
    $pdf::Ln(5);
    ///contenido///
    $totY = 0; 
    $ctlResp = 0;
    $c = 0;
    foreach($respuestas as $respuesta){
        if($respuesta->subAreaRel == null){
            $subArea = '';
        }
        else{
            $subArea = ' - '.$respuesta->subAreaRel->cveSubArea;
        }
        $nomArea = $respuesta->areaRel->cveArea;
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(188,149,92); 
        $cad = $respuesta->entidadRel->nomEntidad;               
        $pdf::MultiCell(50, 5, $cad, 0, 'L', 0, 0, '', '', true);
        $pdf::SetTextColor(35,91,78); 
        $cad = $nomArea.$subArea;               
        $pdf::MultiCell(140, 5, $cad, 0, 'R', 0, 0, '', '', true);
        $pdf::Ln(8);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, 'Antecedente', 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        ///encabezado///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = $respuesta->notaRel->encabezado;                        
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 0, '', '', true);
        ///antecedente///
        $numberOfLines = $pdf::getNumLines($cad, 190);
        $ln  =$numberOfLines*5;
        $pdf::Ln($ln);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$respuesta->notaRel->texto);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $numberOfLines = $pdf::getNumLines($cad, 190);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5); 
        ///actuación///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, 'Actuación de la PROFEPA', 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$respuesta->actuacion);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $numberOfLines = $pdf::getNumLines($cad, 190);                        
        $y = $pdf::GetY();                          
        $pdf::SetY($y+5);
        ///acción///
        $pdf::SetFont('dejavusans', 'B', 12, '', true);
        $pdf::SetTextColor(137,31,54);
        $pdf::MultiCell(140, 5, 'Acciones a Realizar', 0, 'L', 0, 0, '', '', true);
        $pdf::Ln(5);
        $pdf::SetFont('dejavusans', '', 12, '', true);        
        $pdf::SetTextColor(0,0,0);
        $cad = str_replace('&nbsp;',' ',$respuesta->acciones);               
        $pdf::MultiCell(190, 5, $cad."\n", 0, 'J', 0, 2, '', '', true);
        $ctlResp++;
        $c++;
        if($ctlResp == 1 && $c<$total){
            $pdf::AddPage('P', 'LETTER'); 
            $ctlResp = 0;
        }
    }
    if($flg == 's'){
        $pdf::OutPut('/var/www/html/sintesisinformativa/storage/app/public/respuestasPdf/respuestas '.$hoyEs.'.pdf', 'F');        
        return true;
    }
    elseif($flg == 'v'){
        $pdf::Output();
    }
}

/// Clases ///
class miPDFH extends PDF
{
    public function Footer(){
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-15);
            $pdf->SetFont('helvetica','I',8);
            $pdf->Cell(0,10,$pdf->getAliasNumPage().' de '.$pdf->getAliasNbPages(),0,false,'R',0,'',0,false,'T','M');

        });
    }   
}

