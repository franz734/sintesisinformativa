<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DataTables;
use App\OchoColumnas;

class OchoColumnasController extends Controller
{
    ///////////////////////////////
    ///***   OCHO COLUMNAS   ***///
    //////////////////////////////

    // Mostrar ocho columnas //
    public function ochoColumnas(){
        return view('admin.admOchoColumas');
    }
    // Datatable ocho columnas //
    public function getOchoColumnas(){
        $ochoColumnas = OchoColumnas::getAllColumnas();
        return datatables()->of($ochoColumnas)->make(true);
    }
    // Nuevas ocho columnas //
    public function nuevasColumnas(){
        $ahora = Carbon::now();
        $ini = Carbon::today()->setTime(9,0,0);
        $tope = Carbon::today()->setTime(9,10,0);        
        if($ahora->lte($tope) && $ahora->gte($ini)){            
            return view('admin.noNueva');
        }
        return view('admin.ochoForm');        
    }
    /// Guarda Columnas ///
    public function guardaColumnas(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $columna = OchoColumnas::crearColumnas($request);          
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'no se pudo guardar la información';
            return response()->json($msg,401);
        }            
        DB::commit();       
        $msg = 'Guardado';
        return response()->json($msg,200);
    }
    /// Edita Columnas ///
    public function editaOchoColumnas($id){        
        $columna = OchoColumnas::getColumnaById($id);        
        return view('admin.editaOcho', ['columna'=>$columna]);
    }
    /// Modifica Columnas ///
    public function modificaColumnas(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $columna = OchoColumnas::updateColumna($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($columna->idOchoColumna,200);
    }
    /// Borra Columnas ///
    public function borraColumnas(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $columna = OchoColumnas::deleteColumna($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'Borrado';
        return response()->json($msg,200);
    }
}
