<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Presentacion;
use Auth;

class PresentacionController extends Controller
{
    public function inicio()
    {
        return view('shared.presentacionForm');
    }

    public function guardaPresentacion(Request $request)
    {
        $hoy = Carbon::today()->toDateString();
        $ext = ['ppt', 'pptx', 'PPT', 'PPTX'];        
        if ($request->hasFile('archivo')) {
            $pres = $request->archivo;
            $extension = $pres->getClientOriginalExtension();
            if (!in_array($extension, $ext)) {
                $data = 'Formato de archivo no valido';
                $code = 401;
            } else {
                $file = $pres;
                $destinationPath = storage_path('app/public/presentaciones/' . $hoy);
                $filename = rand() . '.' . $pres->getClientOriginalExtension();
                $file->move($destinationPath, $filename);
                $cad = 'storage/presentaciones/' . $hoy . '/' . $filename;
                $request->request->add([
                    'pathFile' => json_encode($cad),
                ]);
            }            
            DB::beginTransaction();
            try{
                $presentacion = Presentacion::creaPresentacion($request);
                $data = 'Guardado';
                $code = 200;
            }
            catch(ValidationException $e){
                DB::rollback();
                $data = 'No se pudo guardar la información';
                $code = 401;
            }
            DB::commit();
        } else {
            $data = 'Es Obligatorio Cargar un Archivo';
            $code = 401;
        }
        return response()->json($data, $code);
    }

    public function presentaciones()
    {
        return view('shared.admPresentaciones');
    }

    public function getPresentaciones()
    {
        $presentaciones = Presentacion::all();
        foreach($presentaciones as $item){
            $item->pathFile = json_decode($item->pathFile);
        }
        return datatables()->of($presentaciones)->make(true);
    }
}
