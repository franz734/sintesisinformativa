<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use App\Directorio;
use App\Nota;
use App\Incidencia;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\TipoPropuesta;
use App\Respuesta;
use App\Materia;
use App\TemaRelevante;
use App\RespuestaTemaRelevante;
use Auth;

class ProcuradorController extends Controller
{
    /// panel inicio ///
    public function inicio(){        
        return view('procurador.panel');
    }
    /// Mostrar todas las notas ///
    public function notas(){        
        return view('procurador.admNotas');
    }
    /// Datatable Notas ///
    public function getNotas(){
        $notas = Nota::getAllNotas();
        //$notas = Nota::all();
        //dd($notas);
        return datatables()->of($notas)->make(true);
    }
    /// Mostrar todas las incidencias ///
    public function incidencias(){        
        return view('procurador.admIncidencias');
    }
    /// Dtatable Incidencias ///
    public function getIncidencias(){
        $incidencias = Incidencia::getAllIncidencias();
        return datatables()->of($incidencias)->make(true);
    }
    /// ver Incidencia ///
    public function verIncidencia($id){
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia',$id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia->evidencia);
        return view('procurador.verIncidencia', ['incidencia'=>$incidencia]);
    }
    ///Panel Admin Respuestas ///
    public function respuestas(){
        $entidades = Entidad::all();
        return view('procurador.admRespuestas', ['entidades'=>$entidades]);
    }
    /// Dtatable Respuestas ///
    public function getRespuestas(){
        $respuestas = Respuesta::getAllRespuestas();
        return datatables()->of($respuestas)->make(true);
    }
    /// ver Respuesta ///
    public function verRespuesta($id){
        $respuesta = Respuesta::with(['entidadRel','areaRel','subAreaRel', 'notaRel'])->where('idRespuesta',$id)->first();
        $fcSuceso = Carbon::parse($respuesta->fcSuceso)->locale('es')->isoFormat('LL');;
        $respuesta->fcSuceso = $fcSuceso;
        $respuesta->evidencia = json_decode($respuesta->evidencia);
        //dd($Respuesta->evidencia);
        return view('procurador.verRespuesta', ['respuesta'=>$respuesta]);
    }
    /// Temas relevantes ///
    public function temasRelevantes(){
        return view('procurador.temasRel');
    }
    /// dtatatable temas relevantes ///
    public function getTemasRel(){        
        $temas = TemaRelevante::join('ct_entidad','ct_temaRel.idEntidad','=','ct_entidad.idEntidad')->orderBy('nomEntidad','ASC')->get();        
        foreach($temas as $tema){
            $tema->temaRel = trim($tema->temaRel);
            $tema->nomEntidad = trim($tema->nomEntidad);
        }
        return datatables()->of($temas)->make(true);
    }

    /// Ver Respuesta a tema relevante ///
    public function verRespuestaTemaRel($idTemaRel){        
        $respuestas = RespuestaTemaRelevante::with(['temaRelevanteRel','entidadRel'])->where('idTemaRel',$idTemaRel)->get();        
        foreach($respuestas as $respuesta){               
            $fcCrea = Carbon::parse($respuesta->fcCrea)->locale('es')->isoFormat('LL'); 
            $respuesta->fecha = $fcCrea;
        }
        return view('procurador.verRespuestaTemaRel',['respuestas'=>$respuestas]);       
    }
}
