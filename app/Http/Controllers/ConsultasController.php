<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Nota;
use App\Incidencia;
use App\TemaRelevante;
use App\RespuestaTemaRelevante;
use App\Respuesta;
use App\Directorio;
use App\NotaInformativa;
use App\Norma;
use App\Presentacion;

class ConsultasController extends Controller
{
    /// panel inicio ///
    public function inicio()
    {
        return view('consultas.inicio');
    }
    /// realiza la busqueda ///
    public function getResultados($keyWord)
    {
        $normas = Norma::getNormasByKeyWord($keyWord);
        $keyWord = trim($keyWord);
        $notas = Nota::getNotasByKeyWord($keyWord);
        $incidencias = Incidencia::getIncidenciasByKeyWord($keyWord);
        $temasRel = TemaRelevante::getTemasRelByKeyWord($keyWord);
        $notasInfo = NotaInformativa::getNotasInfoByKeyWord($keyWord);
        $presentaciones = Presentacion::getPresentacionesByKeyWord($keyWord);
        $countNotas = count($notas);
        $countIncidencias = count($incidencias);
        $countTemasRel = count($temasRel);
        $countNotasInfo = count($notasInfo);
        $countNormas = count($normas);
        $countPresentaciones = count($presentaciones);        
        $numeros = [
            'countNotas' => $countNotas,
            'countIncidencias' => $countIncidencias,
            'countTemasRel' => $countTemasRel,
            'countNotasInfo' => $countNotasInfo,
            'countNormas' => $countNormas,
            'countPresentaciones' => $countPresentaciones
        ];
        return response()->json($numeros, 200);
    }

    /// notas ///
    public function notas($keyWord)
    {
        $keyWord = trim($keyWord);
        $notas = Nota::getNotasByKeyWord($keyWord);
        $notas = collect($notas);
        $notas = datatables()->of($notas)->make(true);
        $notas = json_decode($notas->content());
        //dd($notas);
        return view('consultas.admNotas', ['notas' => $notas, 'keyWord' => $keyWord]);
    }

    public function verNota($idNota)
    {
        $nota = Nota::getNotaById($idNota);
        $respuesta = Respuesta::where('idNota', $idNota)->get()->last();
        $nota->texto = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->texto);
        $nota->texto = str_replace("\n", '', $nota->texto);
        $nota->texto = str_replace('&nbsp;', ' ', $nota->texto);
        $cel = Directorio::where('idEntidad', $nota->idEntidad)->pluck('cel')->first();
        //dd($cel, $nota);
        return view('consultas.verNota', ['nota' => $nota, 'respuesta' => $respuesta, 'cel' => $cel]);
    }

    public function verRespuesta($idRespuesta)
    {
        //dd($idRespuesta);
        $respuesta = Respuesta::with(['entidadRel', 'areaRel', 'subAreaRel', 'notaRel'])->where('idRespuesta', $idRespuesta)->first();
        $fcAlta = Carbon::parse($respuesta->fcAlta)->locale('es')->isoFormat('LL');
        $respuesta->fcAlta = $fcAlta;
        $respuesta->evidencia = json_decode($respuesta->evidencia);
        $cel = Directorio::where('idEntidad', $respuesta->idEntidad)->pluck('cel')->first();
        //dd($Respuesta->evidencia);
        return view('consultas.verRespuesta', ['respuesta' => $respuesta, 'cel' => $cel]);
    }

    /// incidencias ///
    public function incidencias($keyWord)
    {
        $keyWord = trim($keyWord);
        $incidencias = Incidencia::getIncidenciasByKeyWord($keyWord);
        $incidencias = collect($incidencias);
        $incidencias = datatables()->of($incidencias)->make(true);
        $incidencias = json_decode($incidencias->content());
        //dd($notas);
        return view('consultas.admIncidencias', ['incidencias' => $incidencias, 'keyWord' => $keyWord]);
    }

    public function verIncidencia($id)
    {
        $incidencia = Incidencia::with(['entidadRel', 'areaRel', 'subAreaRel'])->where('idIncidencia', $id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        $cel = Directorio::where('idEntidad', $incidencia->idEntidad)->pluck('cel')->first();
        //dd($incidencia);
        return view('consultas.verIncidencia', ['incidencia' => $incidencia, 'cel' => $cel]);
    }

    /// temas relevantes ///
    public function temasRel($keyWord)
    {
        $temasRel = TemaRelevante::getTemasRelByKeyWord($keyWord);
        $temasRel = collect($temasRel);
        $temasRel = datatables()->of($temasRel)->make(true);
        $temasRel = json_decode($temasRel->content());
        //dd($temasRel);
        return view('consultas.admTemasRel', ['temasRel' => $temasRel, 'keyWord' => $keyWord]);
    }

    public function verTemaRel($id)
    {
        $respuestas = RespuestaTemaRelevante::with(['temaRelevanteRel', 'entidadRel'])->where('idTemaRel', $id)->get();
        $temaRel = TemaRelevante::where('idTemaRel', $id)->first();
        foreach ($respuestas as $respuesta) {
            $fcCrea = Carbon::parse($respuesta->fcCrea)->locale('es')->isoFormat('LL');
            $respuesta->fecha = $fcCrea;
        }
        $cel = Directorio::where('idEntidad', $temaRel->idEntidad)->pluck('cel')->first();
        //dd($temaRel);
        return view('consultas.verRespuestaTemaRel', ['tema' => $temaRel, 'respuestas' => $respuestas, 'cel' => $cel]);
    }

    /// Notas Informativas ///
    public function notasInformativas($keyWord)
    {
        $keyWord = trim($keyWord);
        $notas = NotaInformativa::getNotasInfoByKeyWord($keyWord);
        $notas = collect($notas);
        $notas = datatables()->of($notas)->make(true);
        $notas = json_decode($notas->content());
        //dd($notas);
        return view('consultas.admNotasInfo', ['notas' => $notas, 'keyWord' => $keyWord]);
    }

    public function verNotaInformativa($id)
    {
        $nota = NotaInformativa::getNotaInfoById($id);
        $nota->texto = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->texto);
        $nota->texto = str_replace("\n", '', $nota->texto);
        $nota->texto = str_replace('&nbsp;', ' ', $nota->texto);
        $nota->imgNota = json_decode($nota->imgNota);
        $fcSuceso = Carbon::parse($nota->fcSuceso)->locale('es')->isoFormat('LL');
        $nota->fcSuceso = $fcSuceso;
        $cel = Directorio::where('idEntidad', $nota->idEntidad)->pluck('cel')->first();
        //dd($cel, $nota);
        return view('consultas.verNotaInfo', ['nota' => $nota, 'cel' => $cel]);
    }

    /// normas ///
    public function normas($keyWord)
    {
        //$keyWord = trim($keyWord);
        $keyWord = json_decode($keyWord);
        //dd(json_decode($keyWord));
        $flg = 1;
        $normas = Norma::getNormasByKeyWord($keyWord, $flg);
        $ctlNormas = count($normas);
        if ($ctlNormas == 0) {
            $normas = Norma::getAllNormas();
            foreach ($normas as $norma) {
                $norma->idLey = cadLeyToId($norma->ley);
                $dataNormas = Norma::getNormasByKeyWordIdLey($norma->idLey);
                $data = collect($dataNormas);
                $data = datatables()->of($data)->make(true);
                $norma->data = json_decode($data->content());
            }
        } else {
            foreach ($normas as $norma) {
                $norma->idLey = cadLeyToId($norma->ley);
                $dataNormas = Norma::getNormasByKeyWordIdLey($norma->idLey, $keyWord);
                $data = collect($dataNormas);
                $data = datatables()->of($data)->make(true);
                $norma->data = json_decode($data->content());
            }
        }
        //dd($normas);
        return view('consultas.admNormas', ['normas' => $normas, 'keyWord' => $keyWord]);
    }

    /// Presentaciones ///
    public function presentaciones($keyWord)
    {
        $keyWord = trim($keyWord);
        $presentaciones = Presentacion::getPresentacionesByKeyWord($keyWord);
        $presentaciones = collect($presentaciones);
        $presentaciones = datatables()->of($presentaciones)->make(true);
        $presentaciones = json_decode($presentaciones->content());
        return view('consultas.admPresentaciones', ['presentaciones' => $presentaciones, 'keyWord' => $keyWord]);
    }

    /// Compartir información ///
    public function comparteInfo(Request $request)
    {
        //dd($request->all());
        if ($request->task == 'respTemaRel') {
            $request->idRespuesta = $request->id;
            $respSend = RespuestaTemaRelevante::sendRespuesta($request);
            $resp = $respSend->idRespuestaTemaRel;
        } elseif ($request->task == 'respSolInfo') {
            $respSend = Respuesta::sendRespuesta($request);
            $resp = $respSend->idRespuesta;
        } elseif ($request->task == 'nota') {
            $respSend = Nota::shareNota($request);
            $resp = $respSend->idNota;
        } elseif ($request->task == 'incidencia') {
            $respSend = Incidencia::shareIncidencia($request);
            $resp = $respSend->idIncidencia;
        } elseif ($request->task == 'notaInfo') { 
            $respSend = NotaInformativa::shareNotaInfo($request);
            $resp = $respSend->idNotaInformativa;
        }
        return response()->json($resp, 200);
    }
}
