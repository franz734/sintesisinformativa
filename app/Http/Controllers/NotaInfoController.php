<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Exports\generalExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use DataTables;
use App\Entidad;
use App\Area;
use App\NotaInformativa;

class NotaInfoController extends Controller
{
    public function notasInformativas()
    {
        $entidades = Entidad::all();
        $areas = Area::all();
        return view('shared.admNotasInfo', ['entidades' => $entidades, 'areas' => $areas]);
    }

    /// Datatable Notas Informativas ///
    public function getNotasInformativas()
    {
        $notas = NotaInformativa::getAllNotasInfo();
        return datatables()->of($notas)->make(true);
    }

    public function verNotaInformativa($idNotaInfo)
    {
        $nota = NotaInformativa::getNotaInfoById($idNotaInfo);
        $nota->imgNota = json_decode($nota->imgNota);
        return view('shared.verNotaInfo', ['nota' => $nota]);
    }

    public function nuevaNotaInfo()
    {
        $entidades = Entidad::all();
        $areas = Area::all();
        return view('shared.notaInfoForm', ['entidades' => $entidades, 'areas' => $areas]);
    }

    public function guardaNotaInfo(Request $request)
    {
        $hoy = Carbon::today()->toDateString();
        $nomEntidad = str_replace(' ', '', getNomEdo($request->entidadFed));
        $ext = ['png', 'jpg', 'jpeg'];
        $ctlImg = 0;
        $imgArr = [];
        if ($request->hasFile('imgNota')) {
            $countImg = count($request->imgNota);
            foreach ($request->imgNota as $img) {
                $extension = $img->getClientOriginalExtension();
                if (!in_array($extension, $ext)) {
                    $msg = 'Imagen no valida';
                    return response()->json($msg, 401);
                } else {
                    $file = $img;
                    $destinationPath = storage_path('app/public/imgNotasInfo/' . $nomEntidad . '/' . $hoy);
                    //$filename = str_replace(' ','',$img->getClientOriginalName());
                    $filename = rand() . '.' . $img->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);
                    $cad = 'storage/imgNotasInfo/' . $nomEntidad . '/' . $hoy . '/' . $filename;
                    $imgArr[$ctlImg] = $cad;
                    $ctlImg = $ctlImg + 1;
                }
            }
            $request->imgNota = json_encode($imgArr);
        } else {
            $request->imgNota = "";
        }
        DB::beginTransaction();
        try {
            $nota = NotaInformativa::crearNotaInformativa($request);
        } catch (ValidationException $e) {
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg, 401);
        }
        DB::commit();
        return response()->json($nota->idNotaInformativa, 200);
    }

    public function filtraNotasInfo(Request $request)
    {
        //dd($request->all());
        $idsArr = [];
        $ids = NotaInformativa::query();
        if ($request->has('entidadFed')) {
            $ids = $ids->where('idEntidad', $request->entidadFed);
        }
        if ($request->has('area')) {
            $ids = $ids->where('idArea', $request->area);
        }
        if ($request->has('subArea')) {
            $ids = $ids->where('idSubArea', $request->subArea);
        }
        if ($request->encabezado != null) {
            $ids = $ids->where('encabezado', 'like', '%' . $request->encabezado . '%');
        }
        if ($request->texto != null) {
            $ids = $ids->where('texto', 'like', '%' . $request->texto . '%');
        }
        if ($request->datefilter != null) {
            $fcCrea = explode('-', $request->datefilter);
            $ctl = 0;
            foreach ($fcCrea as $item) {
                $item = trim($item);
                $fcCrea[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcCrea[$ctl] = Carbon::createFromFormat('d/m/Y', $fcCrea[$ctl])->format('Y-m-d ' . $time);
                $ctl++;
            }
            $ids = $ids->whereBetween('fcCrea', [$fcCrea[0], $fcCrea[1]]);
        }
        $ids = $ids->get('idNotaInformativa');
        foreach ($ids as $id) {
            array_push($idsArr, $id->idNotaInformativa);
        }
        //dd($idsArr);
        if (empty($idsArr)) {
            $msg = 'Sin Resultados';
            return response()->json($msg, 401);
        } else {
            return response()->json($idsArr, 200);
        }
    }

    public function getFilteredNotasInfo($ids)
    {
        $idsArr = explode(',', $ids);
        $notas = NotaInformativa::with(['entidadRel', 'areaRel'])->whereIn('idNotaInformativa', $idsArr)->orderBy('idNotaInformativa', 'desc')->get();
        foreach ($notas as $nota) {
            $nota->texto = str_replace('&nbsp;', ' ', $nota->texto);
        }
        return datatables()->of($notas)->make(true);
    }

    public function getExcelNotasInfo($params)
    {
        $params = json_decode($params);
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = NotaInformativa::query();
        if ($params->entidad != null || $params->entidad != '') {
            $ids = $ids->where('idEntidad', $params->entidad);
        }
        if ($params->area != null || $params->area != '') {
            $ids = $ids->where('idArea', $params->area);
        }
        if ($params->subArea != null || $params->subArea != '') {
            $ids = $ids->where('idSubArea', $params->subArea);
        }
        if ($params->encabezado != null || $params->encabezado != '') {
            $ids = $ids->where('encabezado', 'like', '%' . $params->encabezado . '%');
        }
        if ($params->texto != null || $params->texto != '') {
            $ids = $ids->where('texto', 'like', '%' . $params->texto . '%');
        }
        if ($params->datefilter != null || $params->datefilter != '') {
            $fcCrea = explode('-', $params->datefilter);
            $ctl = 0;
            foreach ($fcCrea as $item) {
                $item = trim($item);
                $item = str_replace('.', '/', $item);
                $fcCrea[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcCrea[$ctl] = Carbon::createFromFormat('d/m/Y', $fcCrea[$ctl])->format('Y-m-d ' . $time);
                $ctl++;
            }
            $ids = $ids->whereBetween('fcCrea', [$fcCrea[0], $fcCrea[1]]);
        }
        $ids = $ids->get('idNotaInformativa');
        foreach ($ids as $id) {
            array_push($idsArr, $id->idNotaInformativa);
        }
        $notas = NotaInformativa::with(['entidadRel', 'areaRel', 'subAreaRel'])->whereIn('idNotaInformativa', $idsArr)->orderBy('idNotaInformativa', 'desc')->get();
        foreach ($notas as $nota) {
            $nota->texto = str_replace('&nbsp;', ' ', $nota->texto);
        }
        foreach ($notas as $item) {
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;
            $itemIn['idNotaInformativa'] = $item->idNotaInformativa;
            $itemIn['encabezado'] = $item->encabezado;
            $itemIn['texto'] = $item->texto;
            $itemIn['resumen'] = $item->resumen;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);
            $itemIn['area'] = $item->areaRel->nomArea;
            $itemIn['subArea'] = $subArea;
            $itemIn['fechaSuceso'] = Carbon::parse($item->fcSuceso)->toDateString();
            $itemIn['fechaAlta'] = Carbon::parse($item->fcAlta)->toDateString();
            array_push($itemArr, $itemIn);
        }
        //dd($itemArr);   
        $export = new generalExport($itemArr);
        return Excel::download($export, 'NotasInformativas.xlsx');
    }
}
