<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Auth;
use App\Entidad;
use App\Area;
use App\Medio;
use App\Tipo;
use App\Nota;
use App\NotaEntidad;
use App\TemaRelevante;
use App\RespuestaTemaRelevante;
use App\Respuesta;
use App\Incidencia;
use App\Materia;

class MonitorZonaController extends Controller
{
    /// panel inicio ///
    public function inicio(){
        $mail = Auth::user()->email;
        //dd($mail);
        if($mail == 'lady.esquivel@profepa.gob.mx'){
            $zona = 'norte';
            $idEdos = [1,2,3,8,5,6,10,11,14,18,19,24,25,26,32,28];            
        }
        elseif($mail == 'fernando.reyna@profepa.gob.mx'){
            $zona = 'sur';
            $idEdos = [4,7,12,13,15,16,17,20,21,22,27,29,30,31,9,23];
        }
        elseif($mail == 'oficinaCentral@profepa.gob.mx' || $mail =='nestor.guerrero@profepa.gob.mx'){
            $zona = 'territorio nacional';
            $idEdos = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33];
        }
        session(['estados'=>$idEdos, 'zona'=>$zona]);
        return view('monitorZona.panel');
    }

    /// Mostrar todas las notas ///
    public function notas(){
        $tipos = Tipo::all();
        $entidades = Entidad::all();
        $areas = Area::all();
        return view('monitorZona.admNotas',['tipo'=>$tipos,'entidades'=>$entidades,'areas'=>$areas]);
    }
    /// Datatable Notas ///
    public function getNotas(){
        $idEdos = session('estados');
        $notas = Nota::getAllNotas()->whereIn('idEntidad',$idEdos);
        //$notas = Nota::all();
        //dd($notas->whereIn('idEntidad',$idEdos));
        return datatables()->of($notas)->make(true);
    }

    /// Temas relevantes ///
    public function temasRelevantes(){
        return view('monitorZona.temasRel');
    }
    public function getTemasRel(){
        $idEdos = session('estados');
        //dd($idEdos);
        $temas = TemaRelevante::join('ct_entidad','ct_temaRel.idEntidad','=','ct_entidad.idEntidad')->whereIn('ct_temaRel.idEntidad',$idEdos)
                                ->orderBy('ct_entidad.nomEntidad','ASC')->get();
        $temasCent = TemaRelevante::where('idEntidad',null)->get();
        foreach($temasCent as $tema){
            $tema->temaRel = trim($tema->temaRel);
            $tema->nomEntidad = 'Oficina Central';
            $tema->cveEntidad = 'OC';
        }        
        foreach($temas as $tema){
            $tema->temaRel = trim($tema->temaRel);
            $tema->nomEntidad = trim($tema->nomEntidad);
        }
        $temas = $temas->merge($temasCent);
        return datatables()->of($temas)->make(true);
    }

    /// Ver Respuesta a tema relevante ///
    public function verRespuesta($idTemaRel){
        //$respuesta = TemaRelevante::with(['respuestaRel','entidadRel'])->where('idTemaRel',$idTemaRel)->first();
        $respuestas = RespuestaTemaRelevante::with(['temaRelevanteRel','entidadRel'])->where('idTemaRel',$idTemaRel)->get();        
        foreach($respuestas as $respuesta){               
            $fcCrea = Carbon::parse($respuesta->fcCrea)->locale('es')->isoFormat('LL'); 
            $respuesta->fecha = $fcCrea;
        }
        return view('monitorZona.verRespuestaTemaRel',['respuestas'=>$respuestas]);       
    }

    /// Responder tema relevante ///
    public function responderTemaRelevante($idTema){
        $tema = TemaRelevante::where('idTemaRel',$idTema)->first();        
        return view('monitorZona.respondeTemaRel',['tema'=>$tema]);
    }

    /// guarda respuesta temas relevantes///
    public function guardaRespuestaTemaRel(Request $request){   
        $mail = Auth::user()->email;        
        $idEntidad = null;
        $request->idEntidad = $idEntidad;     
        DB::beginTransaction();
        try{
            $respuestaTemaRel = RespuestaTemaRelevante::crearRespuesta($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();        
        return response()->json($respuestaTemaRel->idRespuestaTemaRel,200);
    }

    /// Respuestas a solicitude de información ///
    public function respuestas(){
        $entidades = Entidad::all();
        return view('monitorZona.admRespuestas',['entidades'=>$entidades]);
    }
    /// Dtatable Respuestas ///
    public function getRespuestas(){
        $idEdos = session('estados');
        $respuestas = Respuesta::getAllRespuestas()->whereIn('idEntidad',$idEdos);        
        return datatables()->of($respuestas)->make(true);
    }
    /// ver Respuesta ///
    public function verRespuestaSolInfo($id){
        $respuesta = Respuesta::with(['entidadRel','areaRel','subAreaRel', 'notaRel'])->where('idRespuesta',$id)->first();
        $fcSuceso = Carbon::parse($respuesta->fcSuceso)->locale('es')->isoFormat('LL');;
        $respuesta->fcSuceso = $fcSuceso;
        $respuesta->evidencia = json_decode($respuesta->evidencia);        
        return view('shared.verRespuesta', ['respuesta'=>$respuesta]);
    }

    /// Incidencias ///
    public function incidencias(){
        $entidades = Entidad::all();
        $areas = Area::where('idArea','!=', 4)->get();
        $materias = Materia::all();
        return view('monitorZona.admIncidencias',['entidades'=>$entidades, 'areas'=>$areas, 'materias'=>$materias]);
    }
    /// Dtatable Incidencias ///
    public function getIncidencias(){
        $idEdos = session('estados');
        $incidencias = Incidencia::getAllIncidencias()->whereIn('idEntidad',$idEdos);
        return datatables()->of($incidencias)->make(true);
    }
    /// ver Incidencia ///
    public function verIncidencia($id){
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia',$id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia->evidencia);
        return view('monitorZona.verIncidencia', ['incidencia'=>$incidencia]);
    }
}
