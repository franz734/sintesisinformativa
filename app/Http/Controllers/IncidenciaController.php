<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use App\Exports\generalExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Incidencia;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\TipoPropuesta;
use App\Materia;

class IncidenciaController extends Controller
{
    /// Inicio Colaborador ///
    public function inicio(){
        return view('colaborador.panel');
    }
    /// Panel Incidencias ///
    public function panelIncidencias(){
        $incidenciasDesp = [];
        $idEdos = [];
        $ejeX = [];
        $ejeY = [];
        $dt =Carbon::now();
        $rango = getRangoInc($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];        
        $incidencias = Incidencia::getIncidecniasByDate($fcIni, $fcFin);        
        foreach($incidencias as $incidencia){
            if($incidencia->incidencias != 0){
                array_push($incidenciasDesp, $incidencia);
                $edo = [
                    'idEdo'=>$incidencia->cveEntidad,
                    'incidencias'=>$incidencia->incidencias
                ];
                array_push($idEdos,$edo);
                $edo = [];
                array_push($ejeX,$incidencia->nomEntidad);
                array_push($ejeY,$incidencia->incidencias);
            }
        }
        //dd($rango);
        return view('reader.incidencias', ['entidades'=>$incidenciasDesp,'idEdos'=>$idEdos, 'ejeX'=>$ejeX, 'ejeY'=>$ejeY]);
    }
    /// Incidencias por Entidad //
    public function entidad($id){
        $idEntidad = $id;
        $dt =Carbon::now();
        $rango = getRangoInc($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidenciasByEdoByDate($idEntidad, $fcIni, $fcFin);
        $entidad = Entidad::entidadById($id);
        //dd($incidencias);
        return view('reader.incidenciasEntidad',['incidencias'=>$incidencias, 'entidad'=>$entidad]);
    }
    /// Datatable Incidencias por entidad ///
    public function getIncidenciasByEdo($id){
        $dt =Carbon::now();
        $rango = getRangoInc($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $incidencias = Incidencia::getIncidenciasByEdoByDate($id, $fcIni, $fcFin);
        return datatables()->of($incidencias)->make(true);
    }
    /// Panel Admin Incidencias ///
    public function incidencias(){
        $entidades = Entidad::all();
        $areas = Area::where('idArea','!=', 4)->get();
        $materias = Materia::all();
        return view('admin.admIncidencias',['entidades'=>$entidades,'areas'=>$areas,'materias'=>$materias]);
    }
    /// Dtatable Incidencias ///
    public function getIncidencias(){
        $incidencias = Incidencia::getAllIncidencias();
        return datatables()->of($incidencias)->make(true);
    }
    /// ver Incidencia ///
    public function verIncidencia($id){
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia',$id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia->evidencia);
        return view('admin.verIncidencia', ['incidencia'=>$incidencia]);
    }
    /// ver Incidencia Reader ///
    public function verIncidenciaReader($id){
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia',$id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia);
        return view('reader.verIncidencia', ['incidencia'=>$incidencia]);
    }
    /// Nueva Incidencia ///
    public function nuevaIncidencia(){
        $entidades = Entidad::all();
        $areas = Area::where('idArea','!=', 4)->get();
        $tiposProp = TipoPropuesta::all();
        $materias = Materia::all();
        //dd($tiposProp);
        return view('reader.incidenciaForm',['entidades'=>$entidades, 'areas'=>$areas, 'tiposProp'=>$tiposProp, 'materias'=>$materias]);
    }
    public function getSubareas($id){
        //dd($id);
        $subareas = SubArea::getSubAreaByArea($id);
        return $subareas;
    }
    /// Guarda incidencia ///
    public function guardaIncidencia(Request $request){
        //dd($request->all());
        $hoy = Carbon::today()->toDateString();
        $nomEntidad = str_replace(' ','',getNomEdo($request->entidadFed));        
        $ext = ['png', 'jpg', 'jpeg'];
        $ctlImg = 0;
        $imgArr = [];
        if($request->area == null || $request->area == '' || $request->subArea == null || $request->subArea == ''){
            $msg = 'Intente de Nuevo';
            return response()->json($msg,401);
        }
        if($request->hasFile('evidencia')){
            $countImg = count($request->evidencia);
            foreach($request->evidencia as $img){
                $extension = $img->getClientOriginalExtension();
                if(!in_array($extension,$ext)){
                    $msg = 'Imagen no valida';
                    return response()->json($msg,401);
                }
                else{            
                    $file = $img;        
                    $destinationPath = storage_path('app/public/imgIncidencias/'.$nomEntidad.'/'.$hoy);
                    //$filename = str_replace(' ','',$img->getClientOriginalName());
                    $filename = rand().'.'.$img->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);  
                    $cad = 'storage/imgIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;            
                    $imgArr[$ctlImg] = $cad;
                    $ctlImg = $ctlImg + 1;            
                }   
            }
            $request->evidencia = json_encode($imgArr);            
        }
        else{
            $request->evidencia = "";
        }
        if($request->hasFile('documento')){
            $extension = $request->documento->getClientOriginalExtension();
            $extDoc = ['pdf', 'docx', 'DOCX'];
            if(!in_array($extension,$extDoc)){
                $msg = 'Archivo no valido';
                return response()->json($msg,401);
            }
            else{            
                $file = $request->documento;        
                $destinationPath = storage_path('app/public/docsIncidencias/'.$nomEntidad.'/'.$hoy);
                //$filename = str_replace(' ','',$request->documento->getClientOriginalName());
                $filename = rand().'.'.$request->documento->getClientOriginalExtension();
                $file->move($destinationPath, $filename);  
                $cad = 'storage/docsIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;                                       
            } 
            $request->documento = $cad;  
        }
        $request->accionVS = ($request->accionVS) ? $request->accionVS : null;
        //dd($request->accionVS);
        DB::beginTransaction();
        try{
            $nota = Incidencia::crearIncidencia($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($nota->idNota,200);
    }
    /// Borra Incidencia ///
    public function borraIncidencia(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $incidencia = Incidencia::deleteIncidencia($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo borrar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'borrada';
        return response()->json($msg,200);
    }
    /// Filtro de Incidencias ///
    public function filtraIncidencias(Request $request){
        //dd($request->all());
        $idsArr = [];
        $ids = Incidencia::query();
        if($request->has('entidadFed')){
            $ids = $ids->where('idEntidad',$request->entidadFed);
        }
        if($request->has('materia')){
            $ids = $ids->where('materia',$request->materia);
        }
        if($request->has('area')){
            $ids = $ids->where('idArea',$request->area);
        }
        if($request->has('subArea')){
            $ids = $ids->where('idSubArea',$request->subArea);
        }
        if($request->has('accionVS')){
            $ids = $ids->where('accionVS',$request->accionVS);
        }
        if($request->tema != null){
            $ids = $ids->where('tema', 'like','%'.$request->tema.'%');
        }
        if($request->problematica != null){
            $ids = $ids->where('problematica', 'like','%'.$request->problematica.'%');
        }
        if($request->datefilter != null){            
            $fcAlta = explode('-', $request->datefilter);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->get('idIncidencia');
        foreach($ids as $id){
            array_push($idsArr,$id->idIncidencia);
        }     
        if(empty($idsArr)){
            $msg = 'Sin Resultados';
            return response()->json($msg,401);
        }
        else{
            return response()->json($idsArr,200);
        }
    }
    /// Incidencias filtradas ///
    public function getFilteredIncidencias($ids){
        $idsArr = explode(',',$ids);
        $incidencias = Incidencia::with(['entidadRel', 'areaRel', 'SubAreaRel','propuestaRel','materiaRel'])->whereIn('idIncidencia',$idsArr)->orderBy('idIncidencia','DESC')->get();        
        return datatables()->of($incidencias)->make(true);
    }
    /// Excel Incidencias ///
    public function getExcelIncidencias($params){
        $params = json_decode($params);
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = Incidencia::query();
        if($params->entidad != null || $params->entidad != ''){
            $ids = $ids->where('idEntidad',$params->entidad);
        }
        if($params->materia != null || $params->materia != ''){
            $ids = $ids->where('materia',$params->materia);
        }
        if($params->area != null || $params->area != ''){
            $ids = $ids->where('idArea',$params->area);
        }
        if($params->subArea != null || $params->subArea != ''){
            $ids = $ids->where('idSubArea',$params->subArea);
        }
        if($params->accionVS != null || $params->accionVS != ''){
            $ids = $ids->where('accionVS',$params->accionVS);
        }
        if($params->tema != null || $params->tema != ''){
            $ids = $ids->where('tema', 'like','%'.$params->tema.'%');
        }
        if($params->problematica != null || $params->problematica != ''){
            $ids = $ids->where('problematica', 'like','%'.$params->problematica.'%');
        }
        if($params->datefilter != null || $params->datefilter != ''){            
            $fcAlta = explode('-', $params->datefilter);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->get('idIncidencia');
        foreach($ids as $id){
            array_push($idsArr,$id->idIncidencia);
        } 
        $incidencias = Incidencia::with(['entidadRel', 'areaRel', 'SubAreaRel','propuestaRel','materiaRel'])->whereIn('idIncidencia',$idsArr)->orderBy('idIncidencia','DESC')->get();
        foreach($incidencias as $item){            
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;
            $materia = ($item->materiaRel == null) ? '' : $item->materiaRel->nomMateria;
            $area = ($item->areaRel == null) ? '' : $item->areaRel->nomArea;
            $itemIn['idIncidencia'] = $item->idIncidencia;
            $itemIn['tema'] = $item->tema;
            $itemIn['materia'] = $materia;
            $itemIn['problematica'] = $item->problematica;
            $itemIn['actuacion'] = $item->actuacion;
            $itemIn['acciones'] = $item->acciones;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);
            $itemIn['area'] = $area;
            $itemIn['subArea'] = $subArea;
            $itemIn['fechaSuceso'] = Carbon::parse($item->fcSuceso)->toDateString();
            $itemIn['fechaAlta'] = $item->fcCrea->toDateString();
            array_push($itemArr,$itemIn);
        }
        //$data= json_decode( json_encode($itemArr), true);
        /* Excel::create('Incidencias', function($excel) use($data) {
            $excel->sheet('incidencias', function($sheet) use($data) {
                
            });                            
        })->export('xls'); */
        //dd($data);
        $export = new generalExport($itemArr);        
        return Excel::download($export, 'Incidencias.xlsx');
    }
    
}
