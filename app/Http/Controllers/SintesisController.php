<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Entidad;
use App\Area;
use App\Medio;
use App\Nota;
use App\NotaEntidad;
use App\OchoColumnas;
use App\Boletin;
use App\InfoAmbiental;
use App\Directorio;
use Mail;


class SintesisController extends Controller
{
    // Panel Síntesis informativa //
    public function panelSintesis(){
        $mediosData = [];
        $mediosLabel = [];
        $mediosG = [];
        $areasData = [];
        $areasLabel = [];
        $areasG = [];
        $ejeX = [];
        $ejeY = [];
        $dt =Carbon::now();
        $rango = getRango($dt);        
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        /* echo $fcIni.' a ';
        echo $fcFin;
        die; */
        $entidadesNotas = NotaEntidad::entidadesNotasByDate($fcIni, $fcFin);
        //dd($entidadesNotas);
        foreach($entidadesNotas as $entidadNotas){
            array_push($ejeX,$entidadNotas->nomEntidad);
            array_push($ejeY,$entidadNotas->notas);
        }        
        $mediosNotas = Medio::mediosNotasByDate($fcIni, $fcFin);
        foreach($mediosNotas as $medioNotas){
            array_push($mediosData,$medioNotas->notas);
            array_push($mediosLabel, $medioNotas->nomMedio);
        }
        //dd($mediosNotas);
        $mediosG = [
            'labels' => $mediosLabel,
            'data' => $mediosData
        ];
        $areasNotas = Area::areasNotasByDate($fcIni, $fcFin);
        foreach($areasNotas as $areaNotas){
            array_push($areasData, $areaNotas->notas);
            array_push($areasLabel, $areaNotas->nomArea);
        }
        //dd($areasNotas);
        $areasG = [
            'labels' => $areasLabel,
            'data' => $areasData
        ];
        //dd($entidadesNotas);
        $idEdos = [];
        foreach($entidadesNotas as $entidad){
            $edo = [
                'idEdo'=>$entidad->cveEntidad,
                'notas'=>$entidad->notas
            ];
            array_push($idEdos,$edo);
            $edo = [];
        }
        //dd((object)$idEdos);
        return view('reader.sintesis', ['entidades'=>$entidadesNotas, 'fecha'=>$dt, 'mediosNotas'=>$mediosNotas,
                                        'mediosG'=>$mediosG, 'areasG'=>$areasG, 'ejeX'=>$ejeX, 'ejeY'=>$ejeY, 
                                        'idEdos'=>$idEdos]);
    }
    // Mostrar notas segun estado //
    public function entidad($id){        
        $dt =Carbon::now();
        $rango = getRango($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $entidad = Entidad::entidadById($id);
        $mediosID = mediosControl($id, $fcIni, $fcFin);
        //dd($mediosID);
        $notas = Nota::getNotasEntidadByDate($id, $fcIni, $fcFin, $mediosID);
        //dd($notas);
        return view('reader.sintesisEntidad', ['entidad'=>$entidad, 'notas'=>$notas]);
    }
    /// Mostrar Ocho Columnas ///
    public function ochoColumnas(){        
        $dt =Carbon::now();
        $rango = getRango($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $columnas = OchoColumnas::getColumnas($fcIni, $fcFin);        
        return view('reader.ochoColumnas', ['columnas'=>$columnas]);
    }
    /// Mostar Boletines //
    public function boletin(){        
        $dt =Carbon::now();
        $rango = getRango($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $boletines = Boletin::getBoletin($fcIni, $fcFin);        
        return view('reader.boletin', ['boletines'=>$boletines]);
    }
    /// Mostrar Información Ambiental ///
    public function infoAmbiental(){        
        $dt =Carbon::now();
        $rango = getRango($dt);
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $ambientales = infoAmbiental::getAmbiental($fcIni, $fcFin);        
        return view('reader.infoAmbiental', ['ambientales'=>$ambientales]);
    }
    /// Envia Sintesis ///
    public function sendMailSintesis($t, $f1, $f2){
        //dd($t,$f1,$f2);
        //app('App\Http\Controllers\HelperController')->generatePDF();
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL'); 
        $filesArr = [];  
        $cad = '';        

        app('App\Http\Controllers\PDFController')->PdfAndMerge($t);
        if($t == 'm'){
            $file = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina '.$hoyEs.'.pdf';
            array_push($filesArr,$file);
            $comp = 'matutina';
        }
        elseif($t == 'v'){
            $file = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina '.$hoyEs.'.pdf';
            array_push($filesArr,$file);
            $comp = 'vespertina';
        }
        if($f1 == 'inc'){
            app('App\Http\Controllers\HelperController')->generatePDFIncidencias();
            $file2 = '/var/www/html/sintesisinformativa/storage/app/public/incidenciasPdf/incidencias '.$hoyEs.'.pdf';
            array_push($filesArr,$file2);
            $cad .= ', así como el reporte de incidencias recibidas el día de ayer';
        }
        if($f2 == 'resp'){
            $t = 's';
            app('App\Http\Controllers\PDFController')->generatePDFRespuestas($t);
            $file3 = '/var/www/html/sintesisinformativa/storage/app/public/respuestasPdf/respuestas '.$hoyEs.'.pdf';
            array_push($filesArr,$file3);
            $cad .= ' y el reporte de respuestas a solicitudes de información';
        }   
        //dd($filesArr, $cad);
        $destinatarios = Directorio::all()->pluck('correoElectronico');        
        $mailTo = ['comunicacion.social@profepa.gob.mx',
                    //'silvia.rodriguez@profepa.gob.mx',                    
                    //'enrique.castaneda@profepa.gob.mx',
                    //'Rafael.Coello@profepa.gob.mx'
                    ];
        $copyTo1 = [];
        foreach($destinatarios as $destinatario){
            array_push($copyTo1,$destinatario);
        }        
        $copyTo2 = ['francisco.sancheze@profepa.gob.mx',
                    'sefrancisco734@gmail.com',                    
                    'Raquel.Soto@profepa.gob.mx',
                    'maria.romero@profepa.gob.mx',
                    'miguel.urbina@profepa.gob.mx',
                    'magdalena.quiroz@profepa.gob.mx',                    
                    'raul.garcia@profepa.gob.mx',
                    'jesus.mejia@profepa.gob.mx',
                    'karol.silva@profepa.gob.mx',
                    'fernando.miranda@profepa.gob.mx',
                    'jonathan.caballero@profepa.gob.mx',
                    'rosario.peyrot@profepa.gob.mx',
                    'porfirio.ugalde@profepa.gob.mx',
                    'maria.vicencio@profepa.gob.mx',
                    'irene.xicotencatl@profepa.gob.mx',
                    'richard.dominguez@profepa.gob.mx',
                    'violeta.gomez@profepa.gob.mx',
                    'edgar.rivera@profepa.gob.mx',
                    'ruben.jimenez@profepa.gob.mx',
                    'Aurora.Anzures@profepa.gob.mx',
                    'monica.nunez@profepa.gob.mx',
                    'cuauhtemoc.pacheco@profepa.gob.mx',
                    'veronica.valdes@profepa.gob.mx',
                    'alejandra.rodriguez@profepa.gob.mx',                    
                    'aurora.anzures@profepa.gob.mx',                    
                    'jose.cahuich@profepa.gob.mx',
                    'alfredo.ledesma@profepa.gob.mx',
                    'jose.morales@profepa.gob.mx',
                    'francisco.navarrete@profepa.gob.mx',
                    'felipe.olmedo@profepa.gob.mx',
                    'enrique.ortiz@profepa.gob.mx',
                    'uriel.ortiz@profepa.gob.mx',
                    'arturo.romero@profepa.gob.mx',                    
                    'rafael.serrano@profepa.gob.mx',
                    'raul.tamayo@profepa.gob.mx',
                    'angel.tapia@profepa.gob.mx',                    
                    'lucio.garcia@profepa.gob.mx',
                    'maria.vicencio@profepa.gob.mx',
                    'rosario.peyrot@profepa.gob.mx',
                    'tamez.montoya@profepa.gob.mx',                    
                    'rodrigo.munoz@profepa.gob.mx',
                    'jose.galindo@profepa.gob.mx',
                    'isaac.quiroz@profepa.gob.mx',                    
                    'saulo.martinez@profepa.gob.mx',
                    'ana.nunez@profepa.gob.mx',
                    'raul.avila@profepa.gob.mx',
                    'alejandro.reyesr@profepa.gob.mx',
                    'rogelio.esquivias@profepa.gob.mx',                    
                    'marisol.vega@profepa.gob.mx',
                    'jonathan.caballero@profepa.gob.mx',                    
                    'rosendo.gonzalez@profepa.gob.mx',
                    'edgar.torres@profepa.gob.mx',
                    'luis.serrano@profepa.gob.mx',                    
                    'pedro.gutierrez@profepa.gob.mx',
                    'lidio.ruiz@profepa.gob.mx',
                    'jesus.montoya@profepa.gob.mx',                    
                    'lady.esquivel@profepa.gob.mx',
                    'elvira.yanez@profepa.gob.mx',
                    'alejandra.valadez@profepa.gob.mx',
                    'sandra.olmedo@profepa.gob.mx',
                    'jorge.martinezp@profepa.gob.mx',
                    'patricia.resendiz@profepa.gob.mx',
                    'fernando.reyna@profepa.gob.mx',
                    'gabriel.zanatta@profepa.gob.mx',
                    'claudia.castellanos@profepa.gob.mx',
                    'nestor.guerrero@profepa.gob.mx',
                    'antonio.avalos@profepa.gob.mx'
                ];  
        $copyTo = array_merge($copyTo1,$copyTo2);        
        $mailToProc = ['blanca.mendoza@profepa.gob.mx'];
        $copyToProc = ['alma.garciam@profepa.gob.mx', 
                       'miguel.dorantes@profepa.gob.mx',
                       'florisel.santiago@profepa.gob.mx',                        
                        'lady.esquivel@profepa.gob.mx',
                        'claudia.palacios@profepa.gob.mx',
                        'yeudiel.maldonado@profepa.gob.mx'
                    ];
        /* $mailToSubProc = [
                        'silvia.rodriguez@profepa.gob.mx',                    
                        'enrique.castaneda@profepa.gob.mx',
                        'Rafael.Coello@profepa.gob.mx',
                        'patricio.vilchis@profepa.gob.mx',
                        'margarita.balcazar@profepa.gob.mx'
                        ]; */
        //dd($copyToProc);        
        $subject = 'SÍNTESIS INFORMATIVA '.$hoyEs;  
        //todas las delegaciones, solo sintesis //
        Mail::send('admin.correoSintesis',['hoy'=>$hoyEs, 'comp'=>$comp], function($message) use($mailTo, $subject, $filesArr, $copyTo) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject)->cc($copyTo);              
            $message->attach($filesArr[0]);
        });
        /* //Subprocuradores tres archivos // //En desuso
        Mail::send('admin.correoSintesisSubProc',['hoy'=>$hoyEs, 'cad'=>$cad], function($message) use($mailToSubProc, $subject, $filesArr) {
            // Set the receiver and subject of the mail.
            $message->to($mailToSubProc)->subject($subject);    
            foreach($filesArr as $file){
                $message->attach($file);
            }           
        }); */
        //Procuradora tres archivos
        Mail::send('admin.correoSintesisProc',['hoy'=>$hoyEs, 'cad'=>$cad,'comp'=>$comp], function($message) use($mailToProc, $subject, $filesArr, $copyToProc) {
            // Set the receiver and subject of the mail.
            $message->to($mailToProc)->subject($subject)->cc($copyToProc);    
            foreach($filesArr as $file){
                $message->attach($file);
            }            
        });        
        //Subprocuradores tres archivos y sintesis por area //
        $dt =Carbon::now(); 
        if($t == 'm'){
            $rango = getRangoM($dt);
        }
        elseif($t == 'v'){
            $rango = getRangoV($dt);
        }
        $fcIni = $rango['inicio'];
        $fcFin = $rango['fin'];
        $areas = Area::where('idArea','!=',4)->get(['idArea','correoResponsable','cveArea']);
        $fileTmp = $filesArr;
        $cadTmp = $cad;
        foreach($areas as $area){            
            $area->file = '';
            $cadC = '';
            $filesArr = $fileTmp;
            $cad = $cadTmp;
            $notas = Nota::getNotasByAreaDate($area->idArea,$fcIni,$fcFin);
            $area->totNotas = $notas->count();
            if($area->idArea == 1){
                $area->correoResponsable = [$area->correoResponsable,'margarita.balcazar@profepa.gob.mx'];
            }    
            $mailToSubProc = $area->correoResponsable;
            if($area->totNotas != 0){                
                $cadC= '. '."\n".'Se anexa también la síntesis con las notas correspondientes a su área.';
                $cad.= $cadC;
                app('App\Http\Controllers\PDFController')->PdfAndMergeByArea($t,$area->idArea,$area->cveArea);
                $cveArea = str_replace('.','',$area->cveArea);
                if($t == 'm'){
                    $file4 = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$cveArea.' '.$hoyEs.'.pdf';                    
                }
                elseif($t == 'v'){
                    $file4 = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$cveArea.' '.$hoyEs.'.pdf';                    
                } 
                array_push($filesArr,$file4);                    
            }                                       
            Mail::send('admin.correoSintesisSubProc',['hoy'=>$hoyEs, 'cad'=>$cad, 'comp'=>$comp], function($message) use($mailToSubProc, $subject, $filesArr) {
                // Set the receiver and subject of the mail.
                $message->to($mailToSubProc)->subject($subject);    
                foreach($filesArr as $file){
                    $message->attach($file);
                }                               
            });            
        }          
        //Sintesis por zona 1 archivo//
        $z = ['norte','sur'];
        foreach($z as $item){
            app('App\Http\Controllers\PDFController')->PdfAndMergeByZona($t,$item);
            if($item == 'norte'){
                $mailToZona = 'lady.esquivel@profepa.gob.mx';
            }
            elseif($item == 'sur'){
                $mailToZona = 'fernando.reyna@profepa.gob.mx';
            }
            if($t == 'm'){
                $file5 = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaMatutina_'.$item.' '.$hoyEs.'.pdf';                    
            }
            elseif($t == 'v'){
                $file5 = '/var/www/html/sintesisinformativa/storage/app/public/sintesisPdf/sintesisInformativaVespertina_'.$item.' '.$hoyEs.'.pdf';                    
            } 
            /* Mail::send('admin.correoSintesis',['hoy'=>$hoyEs, 'comp'=>$comp], function($message) use($mailToZona, $subject, $file5) {
                // Set the receiver and subject of the mail.
                $message->to($mailToZona)->subject($subject);              
                $message->attach($file5);
            }); */ 
        }              
        app('App\Http\Controllers\RevistaController')->sendRevista($t);  
        $msg = 'enviado';
        return $msg;        
    }
    /// revista ///
    public function revista(){
        return view('reader.revista');
    }
}
