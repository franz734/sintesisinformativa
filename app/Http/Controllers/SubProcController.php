<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;
use DataTables;
use App\Exports\generalExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Nota;
use App\NotaEntidad;
use App\Incidencia;
use App\Tipo;
use App\Entidad;
use App\Materia;

class SubProcController extends Controller
{
    /// panel inicio ///
    public function inicio(){        
        $mail = Auth::user()->email;
        $userArea = getArea($mail);
        session(['idArea'=>$userArea->idArea, 'nomArea'=>$userArea->nomArea, 'correoArea'=>$userArea->correoResponsable]);
        $idArea = session('idArea');
        $notasAnio = Nota::notasAnioByArea($idArea);
        $totalIncidencias = Incidencia::incidenciasAnioByArea($idArea);
        //dd($notasAnio, $totalIncidencias);        
        return view('subProc.panel', ['notasAnio'=>$notasAnio, 'totalIncidencias'=>$totalIncidencias]);
    }
    /// Mostrar todas las notas ///
    public function notas(){   
        $tipos = Tipo::all();
        $entidades = Entidad::all();             
        return view('subProc.admNotas', ['tipo'=>$tipos,'entidades'=>$entidades]);
    }
    /// Datatable Notas ///
    public function getNotas(){
        $notas = Nota::getAllNotasByArea(session('idArea'));        
        return datatables()->of($notas)->make(true);
    }
    /// Filtro de Notas ///
    public function filtraNotas(Request $request){
        //dd($request->all());
        $idsArr = [];
        $ids = Nota::query();
        if($request->has('entidadFed')){
            $ids = $ids->where('idEntidad',$request->entidadFed);
        }
        if($request->has('categoria')){
            $ids = $ids->where('categoria',$request->categoria);
        }
        /* if($request->has('area')){
            $ids = $ids->where('idArea',$request->area);
        } */
        if($request->has('subArea')){
            $ids = $ids->where('idSubArea',$request->subArea);
        }
        if($request->encabezado != null){
            $ids = $ids->where('encabezado', 'like','%'.$request->encabezado.'%');
        }
        if($request->texto != null){
            $ids = $ids->where('texto', 'like','%'.$request->texto.'%');
        }
        if($request->datefilter != null){            
            $fcAlta = explode('-', $request->datefilter);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcAlta',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->where('idArea',session('idArea'))->get('idNota');
        foreach($ids as $id){
            array_push($idsArr,$id->idNota);
        }     
        if(empty($idsArr)){
            $msg = 'Sin Resultados';
            return response()->json($msg,401);
        }
        else{
            return response()->json($idsArr,200);
        }
    }
    /// Notas filtradas ///
    public function getFilteredNotas($ids){
        $idsArr = explode(',',$ids);
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('csv',null)->where('api',null)->whereIn('idNota',$idsArr)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }
        return datatables()->of($notas)->make(true);
    }
    /// Excel Notas ///
    public function getExcelNotas($params){
        $params = json_decode($params);    
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = Nota::query();
        if($params->entidad != null || $params->entidad != ''){
            $ids = $ids->where('idEntidad',$params->entidad);
        }
        if($params->categoria != null || $params->categoria != ''){
            $ids = $ids->where('categoria',$params->categoria);
        }
        /* if($params->area != null || $params->area != ''){
            $ids = $ids->where('idArea',$params->area);
        }
        if($params->subArea != null || $params->subArea != ''){
            $ids = $ids->where('idSubArea',$params->subArea);
        } */
        if($params->encabezado != null || $params->encabezado != ''){
            $ids = $ids->where('encabezado', 'like','%'.$params->encabezado.'%');
        }
        if($params->texto != null || $params->texto != ''){
            $ids = $ids->where('texto', 'like','%'.$params->texto.'%');
        }
        if($params->datefilter != null || $params->datefilter != ''){            
            $fcAlta = explode('-', $params->datefilter);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcAlta',[$fcAlta[0], $fcAlta[1]]);
        }        
        $ids = $ids->where('idArea',session('idArea'))->get('idNota');
        foreach($ids as $id){
            array_push($idsArr,$id->idNota);
        } 
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel','subAreaRel','sucesoRel','lugarRel'])->where('csv',null)->where('api',null)->whereIn('idNota',$idsArr)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }                
        foreach($notas as $item){
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;
            $lugar = ($item->lugarRel == null) ? '' : $item->lugarRel->nomLugar;
            $suceso = ($item->sucesoRel == null) ? '' : $item->sucesoRel->nomSuceso;
            $link = ($item->link == null) ? '' : $item->link;
            $itemIn['idNota'] = $item->idNota;
            $itemIn['encabezado'] = $item->encabezado;
            $itemIn['texto'] = $item->texto;
            $itemIn['fuente'] = $item->fuente;
            $itemIn['medio'] = $item->medioRel->nomMedio;
            $itemIn['link'] = $link;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);
            $itemIn['lugar'] = $lugar;
            $itemIn['suceso'] = $suceso;
            $itemIn['area'] = $item->areaRel->nomArea;
            $itemIn['subArea'] = $subArea;
            $itemIn['categoria'] = $item->categoria;
            $itemIn['fechaPublicacion'] = Carbon::parse($item->fcPublicacion)->toDateString();
            $itemIn['fechaAlta'] = Carbon::parse($item->fcAlta)->toDateString();
            array_push($itemArr,$itemIn);
        }     
        //dd($itemArr);   
        $export = new generalExport($itemArr);        
        return Excel::download($export, 'Notas.xlsx');
    }

    /// Mostrar todas las incidencias ///
    public function incidencias(){
        $entidades = Entidad::all();        
        $materias = Materia::all();        
        return view('subProc.admIncidencias', ['entidades'=>$entidades,'materias'=>$materias]);
    }
    /// Dtatable Incidencias ///
    public function getIncidencias(){
        $incidencias = Incidencia::getAllIncidenciasByArea(session('idArea'));
        return datatables()->of($incidencias)->make(true);
    }
    /// ver Incidencia ///
    public function verIncidencia($id){
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia',$id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia->evidencia);
        return view('subProc.verIncidencia', ['incidencia'=>$incidencia]);
    }
    /// Filtro de Incidencias ///
    public function filtraIncidencias(Request $request){
        //dd($request->all());
        $idsArr = [];
        $ids = Incidencia::query();
        if($request->has('entidadFed')){
            $ids = $ids->where('idEntidad',$request->entidadFed);
        }
        if($request->has('materia')){
            $ids = $ids->where('materia',$request->materia);
        }
        /* if($request->has('area')){
            $ids = $ids->where('idArea',$request->area);
        }
        if($request->has('subArea')){
            $ids = $ids->where('idSubArea',$request->subArea);
        } */
        if($request->has('accionVS')){
            $ids = $ids->where('accionVS',$request->accionVS);
        }
        if($request->tema != null){
            $ids = $ids->where('tema', 'like','%'.$request->tema.'%');
        }
        if($request->problematica != null){
            $ids = $ids->where('problematica', 'like','%'.$request->problematica.'%');
        }
        if($request->datefilter != null){            
            $fcAlta = explode('-', $request->datefilter);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->where('idArea',session('idArea'))->get('idIncidencia');
        foreach($ids as $id){
            array_push($idsArr,$id->idIncidencia);
        }     
        if(empty($idsArr)){
            $msg = 'Sin Resultados';
            return response()->json($msg,401);
        }
        else{
            return response()->json($idsArr,200);
        }
    }
    /// Incidencias filtradas ///
    public function getFilteredIncidencias($ids){
        $idsArr = explode(',',$ids);
        $incidencias = Incidencia::with(['entidadRel', 'areaRel', 'SubAreaRel','propuestaRel','materiaRel'])->whereIn('idIncidencia',$idsArr)->orderBy('idIncidencia','DESC')->get();        
        return datatables()->of($incidencias)->make(true);
    }
    /// Excel Incidencias ///
    public function getExcelIncidencias($params){
        $params = json_decode($params);
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = Incidencia::query();
        if($params->entidad != null || $params->entidad != ''){
            $ids = $ids->where('idEntidad',$params->entidad);
        }
        if($params->materia != null || $params->materia != ''){
            $ids = $ids->where('materia',$params->materia);
        }
        /* if($params->area != null || $params->area != ''){
            $ids = $ids->where('idArea',$params->area);
        }
        if($params->subArea != null || $params->subArea != ''){
            $ids = $ids->where('idSubArea',$params->subArea);
        } */
        /* if($params->accionVS != null || $params->accionVS != ''){
            $ids = $ids->where('accionVS',$params->accionVS);
        } */
        if($params->tema != null || $params->tema != ''){
            $ids = $ids->where('tema', 'like','%'.$params->tema.'%');
        }
        if($params->problematica != null || $params->problematica != ''){
            $ids = $ids->where('problematica', 'like','%'.$params->problematica.'%');
        }
        if($params->datefilter != null || $params->datefilter != ''){            
            $fcAlta = explode('-', $params->datefilter);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->where('idArea',session('idArea'))->get('idIncidencia');
        foreach($ids as $id){
            array_push($idsArr,$id->idIncidencia);
        } 
        $incidencias = Incidencia::with(['entidadRel', 'areaRel', 'SubAreaRel','propuestaRel','materiaRel'])->whereIn('idIncidencia',$idsArr)->orderBy('idIncidencia','DESC')->get();
        foreach($incidencias as $item){            
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;
            $materia = ($item->materiaRel == null) ? '' : $item->materiaRel->nomMateria;
            $area = ($item->areaRel == null) ? '' : $item->areaRel->nomArea;
            $itemIn['idIncidencia'] = $item->idIncidencia;
            $itemIn['tema'] = $item->tema;
            $itemIn['materia'] = $materia;
            $itemIn['problematica'] = $item->problematica;
            $itemIn['actuacion'] = $item->actuacion;
            $itemIn['acciones'] = $item->acciones;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);
            $itemIn['area'] = $area;
            $itemIn['subArea'] = $subArea;
            $itemIn['fechaSuceso'] = Carbon::parse($item->fcSuceso)->toDateString();
            $itemIn['fechaAlta'] = $item->fcCrea->toDateString();
            array_push($itemArr,$itemIn);
        }
        //$data= json_decode( json_encode($itemArr), true);
        /* Excel::create('Incidencias', function($excel) use($data) {
            $excel->sheet('incidencias', function($sheet) use($data) {
                
            });                            
        })->export('xls'); */
        //dd($data);
        $export = new generalExport($itemArr);        
        return Excel::download($export, 'Incidencias.xlsx');
    }
}
