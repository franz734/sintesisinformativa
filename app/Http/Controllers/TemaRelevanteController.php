<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\TemaRelevante;
use DB;
use App\Entidad;
use App\RespuestaTemaRelevante;
use App\SolicitudInfo;

class TemaRelevanteController extends Controller
{
    public function temasRelevantes(){
        return view('admin.temasRel');
    }

    /// dtatatable temas relevantes ///
    public function getTemasRel(){        
        $temas = TemaRelevante::join('ct_entidad','ct_temaRel.idEntidad','=','ct_entidad.idEntidad')->orderBy('nomEntidad','ASC')->get();
        $temasCent = TemaRelevante::where('idEntidad',null)->get();
        foreach($temasCent as $tema){
            $tema->temaRel = trim($tema->temaRel);
            $tema->nomEntidad = 'Oficina Central';
            $tema->cveEntidad = 'OC';
        }
        foreach($temas as $tema){
            $tema->temaRel = trim($tema->temaRel);
            $tema->nomEntidad = trim($tema->nomEntidad);
        }
        $temas = $temas->merge($temasCent);
        return datatables()->of($temas)->make(true);
    }

    /// Cerrar tema relevante ///
    public function cierraTemaRel(Request $request){        
        DB::beginTransaction();
        try{
            $tema = TemaRelevante::cerrarTemaRel($request->idTema);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();        
        return response()->json($tema->idTemaRel,200);
    }

    /// Abrir tema relevates ///
    public function abreTemaRel(Request $request){        
        DB::beginTransaction();
        try{
            $tema = TemaRelevante::abrirTemaRel($request->idTema);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();        
        return response()->json($tema->idTemaRel,200);
    }

    /// Nuevo tema relevante ///
    public function nuevoTemaRel(){
        $estados = Entidad::where('idEntidad', '!=',33)->get();
        foreach($estados as $estado){
            $estado->nomEntidad = trim($estado->nomEntidad);
        }
        return view('admin.temaRelForm',['entidades'=>$estados]);        
    }
    public function guardaTemaRel(Request $request){
        //dd($request->all());        
        DB::beginTransaction();
        try{
            $temaRel = TemaRelevante::crearTemaRel($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($temaRel->idTemaRel,200);
    }
    public function enviaRespuestaTemaRel(Request $request){
        //dd($request->all());
        $respSend = RespuestaTemaRelevante::sendRespuesta($request);
        return response()->json($respSend->idRespuestaTemaRel,200);
    }

    //Solicitud de información//
    public function masInfoTemaRel(Request $request){
        //dd($request->all());
        $request->fecha = Carbon::createFromFormat('d/m/Y',$request->fecha)->format('Y-m-d H:i:s');        
        if($request->fecha == null){
            $msg = 'Debe ingresar una fecha';
            return response()->json($msg,401);
        }
        DB::beginTransaction();
        try{
            $solicitud = SolicitudInfo::crearSolicitud($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo enviar la solicitud';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($solicitud->idSolicitudInfo,200);
    }

}
