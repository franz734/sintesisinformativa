<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\generalExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use Carbon\Carbon;
use App\Respuesta;
use App\Entidad;
use App\TemaRelevante;
use App\RespuestaTemaRelevante;

class RespuestaController extends Controller
{
    ///Panel Admin Respuestas ///
    public function respuestas(){
        $entidades = Entidad::all();
        return view('admin.admRespuestas', ['entidades'=>$entidades]);
    }
    /// Dtatable Respuestas ///
    public function getRespuestas(){
        $respuestas = Respuesta::getAllRespuestas();
        return datatables()->of($respuestas)->make(true);
    }
    /// ver Respuesta ///
    public function verRespuesta($id){
        $respuesta = Respuesta::with(['entidadRel','areaRel','subAreaRel', 'notaRel'])->where('idRespuesta',$id)->first();
        $fcSuceso = Carbon::parse($respuesta->fcSuceso)->locale('es')->isoFormat('LL');
        $respuesta->fcSuceso = $fcSuceso;
        $respuesta->evidencia = json_decode($respuesta->evidencia);
        //dd($Respuesta->evidencia);
        return view('shared.verRespuesta', ['respuesta'=>$respuesta]);
    }
    /// Filtro de Respuestas ///
    public function filtraRespuestas(Request $request){
        //dd($request->all());
        $idsArr = [];
        $ids = Respuesta::query();
        $ids = $ids->join('nota','respuesta.idNota','=','nota.idNota');
        if($request->has('entidadFed')){
            $ids = $ids->where('respuesta.idEntidad',$request->entidadFed);
        }
        if($request->encabezado != null){
            $ids = $ids->where('nota.encabezado', 'like','%'.$request->encabezado.'%');
        }
        if($request->texto != null){
            $ids = $ids->where('nota.texto', 'like','%'.$request->texto.'%');
        }
        if($request->datefilter != null){            
            $fcAlta = explode('-', $request->datefilter);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('respuesta.fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        if($request->datefilter2 != null){            
            $fcAlta = explode('-', $request->datefilter2);
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('nota.fcEnvio',[$fcAlta[0], $fcAlta[1]]);
        }
        $ids = $ids->get();        
        foreach($ids as $id){
            array_push($idsArr,$id->idRespuesta);
        }     
        if(empty($idsArr)){
            $msg = 'Sin Resultados';
            return response()->json($msg,401);
        }
        else{            
            return response()->json($idsArr,200);
        }
    }

    public function getFilteredRespuestas($ids){
        $idsArr = explode(',',$ids);
        $respuestas = Respuesta::with(['entidadRel','areaRel','subAreaRel', 'notaRel', 'propuestaRel'])->whereIn('idRespuesta',$idsArr)->orderBy('idRespuesta', 'desc')->get();          
        return datatables()->of($respuestas)->make(true);
    }
    public function getExcelRespuestas($params){
        $params = json_decode($params);    
        //dd($params);
        $idsArr = [];
        $itemIn = [];
        $itemArr = [];
        $ids = Respuesta::query();
        $ids = $ids->join('nota','respuesta.idNota','=','nota.idNota');
        if($params->entidad != null || $params->entidad != ''){
            $ids = $ids->where('respuesta.idEntidad',$params->entidad);
        }        
        if($params->encabezado != null || $params->encabezado != ''){
            $ids = $ids->where('nota.encabezado', 'like','%'.$params->encabezado.'%');
        }
        if($params->texto != null || $params->texto != ''){
            $ids = $ids->where('nota.texto', 'like','%'.$params->texto.'%');
        }
        if($params->datefilter != null || $params->datefilter != ''){            
            $fcAlta = explode('-', $params->datefilter);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('respuesta.fcCrea',[$fcAlta[0], $fcAlta[1]]);
        }
        if($params->datefilter2 != null || $params->datefilter2 != ''){            
            $fcAlta = explode('-', $params->datefilter2);            
            $ctl = 0;
            foreach($fcAlta as $item){
                $item = trim($item);  
                $item = str_replace('.','/',$item);
                $fcAlta[$ctl] = $item;
                $time = ($ctl == 0) ? '00:00:00' : '23:59:59';
                $fcAlta[$ctl] = Carbon::createFromFormat('d/m/Y',$fcAlta[$ctl])->format('Y-m-d '.$time);
                $ctl++;              
            }
            $ids = $ids->whereBetween('nota.fcEnvio',[$fcAlta[0], $fcAlta[1]]);
        }        
        $ids = $ids->get();
        foreach($ids as $id){
            array_push($idsArr,$id->idRespuesta);
        }         
        $respuestas = Respuesta::with(['entidadRel','areaRel','subAreaRel', 'notaRel', 'propuestaRel'])->whereIn('idRespuesta',$idsArr)->orderBy('idRespuesta', 'desc')->get();
        //dd($respuestas);
        foreach($respuestas as $item){
            $subArea = ($item->subAreaRel == null) ? '' : $item->subAreaRel->cveSubArea;            
            $itemIn['idRespuesta'] = $item->idRespuesta;
            $itemIn['tema'] = $item->notaRel->encabezado;
            $itemIn['antecedente'] = $item->notaRel->texto; 
            $itemIn['actuacion'] = $item->actuacion;
            $itemIn['acciones'] = $item->acciones;
            $itemIn['entidad'] = trim($item->entidadRel->nomEntidad);            
            $itemIn['area'] = $item->areaRel->nomArea;
            $itemIn['subArea'] = $subArea;            ;
            $itemIn['fechaSuceso'] = Carbon::parse($item->fcSuceso)->toDateString();
            $itemIn['fechaNotificacion'] = Carbon::parse($item->notaRel->fcEnvio)->toDateString();
            $itemIn['fechaRespuesta'] = Carbon::parse($item->fcCrea)->toDateString();
            array_push($itemArr,$itemIn);
        }     
        //dd($itemArr);   
        $export = new generalExport($itemArr);        
        return Excel::download($export, 'Respuestas.xlsx');
    }

    /////////////////////////////////////
    /** RESPUESTAS A TEMAS RELEVANTES **/
    /////////////////////////////////////

    /// Ver Respuesta a tema relevante ///
    public function verRespuestaTemaRel($idTemaRel){
        //$respuesta = TemaRelevante::with(['respuestaRel','entidadRel'])->where('idTemaRel',$idTemaRel)->first();
        $respuestas = RespuestaTemaRelevante::with(['temaRelevanteRel','entidadRel','areaRel','subAreaRel'])->where('idTemaRel',$idTemaRel)->get();        
        foreach($respuestas as $respuesta){               
            $fcCrea = Carbon::parse($respuesta->fcCrea)->locale('es')->isoFormat('LL'); 
            $respuesta->fecha = $fcCrea;
        }
        return view('admin.verRespuestaTemaRel',['respuestas'=>$respuestas]);       
    }
}
