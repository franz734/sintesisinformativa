<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use App\Directorio;
use App\Nota;
use App\Incidencia;
use App\Entidad;
use App\Area;
use App\SubArea;
use App\TipoPropuesta;
use App\Respuesta;
use App\Materia;
use App\TemaRelevante;
use App\RespuestaTemaRelevante;
use App\NotaInformativa;
use Auth;

class DelegadoController extends Controller
{
    /// panel inicio ///
    public function inicio()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $entidad = trim($delegacion->entidadRel->nomEntidad);
        $entidad = ($entidad == 'Ciudad de México') ? 'ZMVM' : $entidad;
        return view('delegado.panel', ['entidad'=>$entidad]);
    }
    /// panel incio comunicación social ///
    public function inicioCS()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $entidad = trim($delegacion->entidadRel->nomEntidad);
        $entidad = ($entidad == 'Ciudad de México') ? 'ZMVM' : $entidad;
        $notasAnio = Nota::notasAnioByEntidad($delegacion->idEntidad);
        $totalIncidencias = Incidencia::incidenciasAnioByEdo($delegacion->idEntidad);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $solicitudes = Nota::getSolicitudes($idEntidad);
        $totalSolicitudes = $solicitudes->count();
        $totalTemasRel = TemaRelevante::totalTemasByEdo($idEntidad)->count();
        $totalNotasInfo = NotaInformativa::totalNotasInfoByEdo($idEntidad)->count();
        //dd($totalNotasInfo);
        return view('delegado.panelCS', ['entidad'=>$entidad, 'notasAnio'=>$notasAnio, 'totalIncidencias'=>$totalIncidencias,
                                        'totalSolicitudes'=>$totalSolicitudes, 'totalTemasRel'=>$totalTemasRel,
                                        'totalNotasInfo'=>$totalNotasInfo]);
    }
    /// Mostrar todas las notas ///
    public function notas()
    {
        return view('delegado.admNotas');
    }
    /// dtatable notas dado un estado ///
    public function getNotas()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $notas = Nota::getNotasByEdo($idEntidad);
        return datatables()->of($notas)->make(true);
    }
    /// Mostrar todas las incidencias ///
    public function incidencias()
    {
        return view('delegado.admIncidencias');
    }
    /// datatable incidencias dado un estado //
    public function getIncidencias()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $incidencias = Incidencia::getIncidenciasByEdo($idEntidad);
        return datatables()->of($incidencias)->make(true);
    }
    /// formulario nueva incidencia ///
    public function nuevaIncidencia()
    {
        $mail = Auth::user()->email;
        $entidad = getEntidad($mail);
        $areas = Area::where('idArea', '!=', 4)->get();
        $tiposProp = TipoPropuesta::all();
        $materias = Materia::all();
        return view('delegado.incidenciaForm', ['entidad'=>$entidad, 'areas'=>$areas, 'tiposProp'=>$tiposProp,'materias'=>$materias]);
    }
    /// ver Incidencia ///
    public function verIncidencia($id)
    {
        $incidencia = Incidencia::with(['entidadRel','areaRel','subAreaRel'])->where('idIncidencia', $id)->first();
        $fcSuceso = Carbon::parse($incidencia->fcSuceso)->locale('es')->isoFormat('LL');
        ;
        $incidencia->fcSuceso = $fcSuceso;
        $incidencia->evidencia = json_decode($incidencia->evidencia);
        //dd($incidencia->evidencia);
        return view('delegado.verIncidencia', ['incidencia'=>$incidencia]);
    }
    /// mostrar todas las solicitudes ///
    public function solicitudes()
    {
        return view('delegado.admSolicitudes');
    }
    /// datatable de solicitudes dado un estado ///
    public function getSolicitudes()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $solicitudes = Nota::getSolicitudes($idEntidad);
        return datatables()->of($solicitudes)->make(true);
    }
    /// formulario para responder solicitudes ///
    public function responderSolicitud($id)
    {
        $mail = Auth::user()->email;
        $entidad = getEntidad($mail);
        $areas = Area::where('idArea', '!=', 4)->get();
        $tiposProp = TipoPropuesta::all();
        $nota = Nota::getNotaById($id);
        return view('delegado.respSolicitud', ['entidad'=>$entidad, 'nota'=>$nota, 'tiposProp'=>$tiposProp, 'areas'=>$areas]);
    }
    /// guarda respuesta ///
    public function guardaRespuesta(Request $request)
    {
        //dd($request->all());
        if ($request->subArea == null || $request->subArea == '') {
            $msg = 'Recargue la página e Intente de Nuevo';
            return response()->json($msg, 401);
        }
        $hoy = Carbon::today()->toDateString();
        $nomEntidad = str_replace(' ', '', getNomEdo($request->entidadFed));
        $ext = ['png', 'jpg', 'jpeg'];
        $ctlImg = 0;
        $imgArr = [];
        if ($request->hasFile('evidencia')) {
            $countImg = count($request->evidencia);
            foreach ($request->evidencia as $img) {
                $extension = $img->getClientOriginalExtension();
                if (!in_array($extension, $ext)) {
                    $msg = 'Imagen no valida';
                    return response()->json($msg, 401);
                } else {
                    $file = $img;
                    $destinationPath = storage_path('app/public/imgIncidencias/'.$nomEntidad.'/'.$hoy);
                    //$filename = str_replace(' ','',$img->getClientOriginalName());
                    $filename = rand().'.'.$img->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);
                    $cad = 'storage/imgIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;
                    $imgArr[$ctlImg] = $cad;
                    $ctlImg = $ctlImg + 1;
                }
            }
            $request->evidencia = json_encode($imgArr);
        } else {
            $request->evidencia = "";
        }
        if ($request->hasFile('documento')) {
            $extension = $request->documento->getClientOriginalExtension();
            $extDoc = ['pdf', 'docx'];
            if (!in_array($extension, $extDoc)) {
                $msg = 'Archivo no valido';
                return response()->json($msg, 401);
            } else {
                $file = $request->documento;
                $destinationPath = storage_path('app/public/docsIncidencias/'.$nomEntidad.'/'.$hoy);
                //$filename = str_replace(' ','',$request->documento->getClientOriginalName());
                $filename = rand().'.'.$request->documento->getClientOriginalExtension();
                $file->move($destinationPath, $filename);
                $cad = 'storage/docsIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;
            }
            $request->documento = $cad;
        }
        DB::beginTransaction();
        try {
            $respuesta = Respuesta::crearRespuesta($request);
        } catch (ValidationException $e) {
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg, 401);
        }
        DB::commit();
        return response()->json($respuesta->idrespuesta, 200);
    }
    /// Temas relevantes ///
    public function temasRelevantes()
    {
        return view('delegado.temasRel');
    }
    public function getTemasRel()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $temas = TemaRelevante::where('idEntidad', $idEntidad)->get();
        foreach ($temas as $tema) {
            $tema->temaRel = trim($tema->temaRel);
        }
        return datatables()->of($temas)->make(true);
    }
    /// Responder tema relevante ///
    public function responderTemaRelevante($idTema)
    {
        $tema = TemaRelevante::with(['entidadRel','areaRel','subAreaRel'])->where('idTemaRel', $idTema)->first();
        $mail = Auth::user()->email;
        $entidad = getEntidad($mail);
        $areas = Area::where('idArea', '!=', 4)->get();
        $tiposProp = TipoPropuesta::all();
        return view('delegado.respondeTemaRel', ['tema'=>$tema,'entidad'=>$entidad, 'tiposProp'=>$tiposProp, 'areas'=>$areas]);
    }
    /// guarda respuesta temas relevantes///
    public function guardaRespuestaTemaRel(Request $request)
    { //Eliminar para versión 2
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $request->idEntidad = $idEntidad;
        DB::beginTransaction();
        try {
            $respuestaTemaRel = RespuestaTemaRelevante::crearRespuesta($request);
        } catch (ValidationException $e) {
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg, 401);
        }
        DB::commit();
        return response()->json($respuestaTemaRel->idRespuestaTemaRel, 200);
    }
    /* public function guardaRespuestaTemaRel(Request $request){  //para versión 2
        if($request->subArea == null || $request->subArea == '') {
            $msg = 'Recargue la página e Intente de Nuevo';
            return response()->json($msg,401);
        }
        $hoy = Carbon::today()->toDateString();
        $nomEntidad = str_replace(' ','',getNomEdo($request->entidadFed));
        $ext = ['png', 'jpg', 'jpeg'];
        $ctlImg = 0;
        $imgArr = [];
        if($request->hasFile('evidencia')){
            $countImg = count($request->evidencia);
            foreach($request->evidencia as $img){
                $extension = $img->getClientOriginalExtension();
                if(!in_array($extension,$ext)){
                    $msg = 'Imagen no valida';
                    return response()->json($msg,401);
                }
                else{
                    $file = $img;
                    $destinationPath = storage_path('app/public/imgIncidencias/'.$nomEntidad.'/'.$hoy);
                    //$filename = str_replace(' ','',$img->getClientOriginalName());
                    $filename = rand().'.'.$img->getClientOriginalExtension();
                    $file->move($destinationPath, $filename);
                    $cad = 'storage/imgIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;
                    $imgArr[$ctlImg] = $cad;
                    $ctlImg = $ctlImg + 1;
                }
            }
            $request->evidencia = json_encode($imgArr);
        }
        else{
            $request->evidencia = "";
        }
        if($request->hasFile('documento')){
            $extension = $request->documento->getClientOriginalExtension();
            $extDoc = ['pdf', 'docx'];
            if(!in_array($extension,$extDoc)){
                $msg = 'Archivo no valido';
                return response()->json($msg,401);
            }
            else{
                $file = $request->documento;
                $destinationPath = storage_path('app/public/docsIncidencias/'.$nomEntidad.'/'.$hoy);
                //$filename = str_replace(' ','',$request->documento->getClientOriginalName());
                $filename = rand().'.'.$request->documento->getClientOriginalExtension();
                $file->move($destinationPath, $filename);
                $cad = 'storage/docsIncidencias/'.$nomEntidad.'/'.$hoy.'/'.$filename;
            }
            $request->documento = $cad;
        }
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->entidadRel->idEntidad;
        $request->idEntidad = $idEntidad;
        DB::beginTransaction();
        try{
            $respuestaTemaRel = RespuestaTemaRelevante::crearRespuesta($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($respuestaTemaRel->idRespuestaTemaRel,200);
    } */

    /// Ver respuestas a tema relevante ///
    public function verRespuestaTemaRel($idTemaRel)
    {
        $respuestas = RespuestaTemaRelevante::with(['temaRelevanteRel','entidadRel'])->where('idTemaRel', $idTemaRel)->get();
        //dd($respuestas);
        foreach ($respuestas as $respuesta) {
            //dd($respuesta);
            $fcCrea = Carbon::parse($respuesta->fcCrea)->locale('es')->isoFormat('LL');
            $respuesta->fecha = $fcCrea;
        }
        return view('delegado.verRespuestaTemaRel', ['respuestas'=>$respuestas]);
    }

    /// Cerrar tema relevante ///
    public function cierraTemaRel(Request $request)
    {
        DB::beginTransaction();
        try {
            $tema = TemaRelevante::cerrarTemaRel($request->idTema);
        } catch (ValidationException $e) {
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg, 401);
        }
        DB::commit();
        return response()->json($tema->idTemaRel, 200);
    }

    /// Notas Informativas ///
    public function getNotasInformativas()
    {
        $mail = Auth::user()->email;
        $delegacion = getEntidad($mail);
        $idEntidad = $delegacion->idEntidad;
        $notas = NotaInformativa::with(['entidadRel', 'areaRel', 'subAreaRel'])->where('idEntidad', $idEntidad)->orderBy('idNotaInformativa', 'desc')->get();
        return datatables()->of($notas)->make(true);
    }
}
