<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Boletin;
use App\Entidad;
use App\Area;
use App\Medio;

class BoletinController extends Controller
{
    /////////////////////////
    ///***   BOLETÍN   ***///
    /////////////////////////

    // Mostar Boletines //
    public function boletines(){
        return view('admin.admBoletines');
    }
    // Datatable boletines //
    public function getBoletines(){
        $boletines = Boletin::getAllBoletines();
        return datatables()->of($boletines)->make(true);
    }
    // Nuevo Boletín //
    public function nuevoBoletin(){
        $ahora = Carbon::now();
        $ini = Carbon::today()->setTime(9,0,0);
        $tope = Carbon::today()->setTime(9,10,0);   
        if($ahora->lte($tope) && $ahora->gte($ini)){
            return view('admin.noNueva');
        }
        $medios = Medio::all();
        $areas = Area::all();
        $entidades = Entidad::all();
        return view('admin.boletinForm', ['medios'=>$medios, 'areas'=>$areas, 'entidades'=>$entidades]);
        
    }
    // Guarda Boletín //
    public function guardaBoletin(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $boletin = Boletin::crearBoletin($request);          
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'no se pudo guardar la información';
            return response()->json($msg,401);
        }            
        DB::commit();       
        $msg = 'Guardado';
        return response()->json($msg,200);
    }
    // Edita boletin //
    public function editaBoletin($id){
       $boletin = Boletin::getBoletinById($id);
       return view('admin.editaBoletin',['boletin'=>$boletin]);
    }
    /// Modifica Boletin ///
    public function modificaBoletin(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        try{
            $boletin = Boletin::updateBoletin($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo guardar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        return response()->json($boletin->idBoletin,200);
    }
    // Borra Boletin //
    public function borraBoletin(Request $request){
        DB::beginTransaction();
        try{
            $boletin = Boletin::deleteBoletin($request);
        }
        catch(ValidationException $e){
            DB::rollback();
            $msg = 'No se pudo borrar la información';
            return response()->json($msg,401);
        }
        DB::commit();
        $msg = 'Borrado';
        return response()->json($msg,200);
    }
}
