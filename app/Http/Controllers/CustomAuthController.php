<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Directorio;

class CustomAuthController extends Controller
{
    public function login(Request $request){
        //dd($request->all());
        if($request->usuario == '' || $request->password == ''){
            return redirect('/delegado')->with('notification', 'Debe Ingresar Usuario y Contraseña');
        }
        $info = Directorio::where('usuario',$request->usuario)->where('pwd', $request->password)->first(); 
        //dd($info);
        $com  =$request->usuario.'@profepa.gob.mx';
        $verif = User::where('email',$com)->first();
        //dd($verif);        
        if(empty($info)){             
             return redirect('/delegado')->with('notification', 'Usuario y/o Contraseña Incorrectos');
        }
        elseif(!empty($verif)){                        
            $usr = $com;            
            $credential['email'] = $usr;
            $pwd = $info->pwd;        
            $credential['password'] = $pwd;            
            $login = User::where('email',$credential['email'])->first();            
            $login->password = Hash::make($credential['password']);
            $login->save();            
            if(Auth::attempt($credential)){                
                return redirect('home');
            }                    
        }
        else{            
            $user = User::create([
                'name' => $info->delegado,
                'email' => $com,
                'password' => Hash::make($info->pwd),                
                ]);            
            $user->assignRole('Delegado');
            $usr = $com;            
            $credential['email'] = $usr;
            $pwd = $info->pwd;        
            $credential['password'] = $pwd;            
            $login = User::where('email',$credential['email'])->first();            
            $login->password = Hash::make($credential['password']);
            $login->save();
            if(Auth::attempt($credential)){                
                return redirect('home');
            }           
        }
        
    }
}
