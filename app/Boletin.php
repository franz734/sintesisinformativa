<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;

class Boletin extends Model
{
    protected $guarded = [];
    protected $table = 'boletin';
    protected $connection = 'mysql';
    protected $primaryKey = 'idBoletin';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    // Otiene los boletines de un dia dado //
    static public function getBoletin($a, $b){
        $boletines = Boletin::whereBetween('fcAlta', [$a, $b])->get();
        return $boletines;
    }
    /// Obtiene todas los boletines para datatable ///
    static public function getAllBoletines(){
        $boletines = Boletin::orderBy('idBoletin', 'desc')->get();
        return $boletines;
    }
    // Obtiene un boletin dado el id //
    static public function getBoletinById($id){
        $boletin = Boletin::where('idBoletin', $id)->first();
        return $boletin;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    // Crear ///
    static public function crearBoletin($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $idMedio = ($request->medio) ? $request->medio : NULL;
        $idEntidad = ($request->entidadFed) ? $request->entidadFed : NULL;
        $idArea = ($request->subProc) ? $request->subProc : NULL;
        //dd($idMedio);
        $request->fcAlta = Carbon::createFromFormat('d/m/Y',$request->fechaAlta)->format('Y-m-d H:i:s');
        $boletin = Boletin::create([
            'encabezado'=>$request->encabezado,
            'texto'=>$request->texto,
            'textoHtml'=>$request->textoHtml,
            'link'=>$request->link,
            'idArea'=>$idArea,
            'idMedio'=>$idMedio,
            'idEntidad'=>$idEntidad,
            'usuario'=>$name,
            'fcAlta'=>$request->fcAlta
        ]);
        return $boletin;
    }
    // Actualizar //
    static public function updateBoletin($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $boletinIn = Boletin::where('idBoletin', $request->idBoletin)->first();
        //dd($boletinIn);
        $boletinIn->encabezado = $request->encabezado;
        $boletinIn->texto = $request->texto;
        $boletinIn->textoHtml = $request->textoHtml;
        $boletinIn->link = $request->link;
        $boletinIn->usuario = $name;
        $boletinIn->save();
        return $boletinIn;
    }
    // Borrar //
    static public function deleteBoletin($request){
        //dd($request->all());
        $boletin = Boletin::where('idBoletin', $request->idBoletin)->first();
        //dd($boletin);
        $boletin->delete();
        return $boletin;
    }
}
