<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;

class OchoColumnas extends Model
{
    protected $guarded = [];
    protected $table = 'ochoColumna';
    protected $connection = 'mysql';
    protected $primaryKey = 'idOchoColumna';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /// Obtiene las columnas de un dá dado ///
    static public function getColumnas($a, $b){
        $columnas = OchoColumnas::whereBetween('fcAlta', [$a, $b])->get();
        return $columnas;
    }
    /// Obtiene todas las columnas para datatable ///
    static public function getAllColumnas(){
        $columnas = OchoColumnas::orderBy('idOchoColumna', 'desc')->get();
        //dd($columnas);
        return $columnas;
    }
    /// Obtinen la columna dado el id ///
    static public function getColumnaById($id){
        $columna = OchoColumnas::where('idOchoColumna', $id)->first();
        return $columna;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    /// Crear ///
    static public function crearColumnas($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $columnas = json_decode($request->columnas);        
        //dd($columnas);
        foreach($columnas as $columna){            
            if(!empty($columna) || !is_null($columna)){
                $columna->fcAlta = Carbon::createFromFormat('d/m/Y',$columna->fcAlta)->format('Y-m-d H:i:s');
                $columnaIn = OchoColumnas::create([
                    'encabezado'=>$columna->encabezado,
                    'texto'=>$columna->texto,
                    'textoHtml'=>$columna->textoHtml,
                    'fuente'=>$columna->fuente,
                    'fcAlta'=>$columna->fcAlta,
                    'usuario'=>$name

                ]);
            }
        }
        return $columnaIn;
    }

    /// Modificar ///
    static public function updateColumna($request){
        //dd($request);
        $name = Auth::user()->name;
        $columnaIn = OchoColumnas::where('idOchoColumna',$request->idOchoColumna)->first();        
        //dd($columnaIn);
        $columnaIn->encabezado = $request->encabezado;
        $columnaIn->texto = $request->texto;
        $columnaIn->textoHtml = $request->textoHtml;
        $columnaIn->fuente = $request->fuenteA;
        $columnaIn->usuario = $name;
        $columnaIn->save();
        
        return $columnaIn;
    }

    /// Borrar ///
    static public function deleteColumna($request){
        //dd($request->all());
        $columna = OchoColumnas::where('idOchoColumna',$request->idColumna)->first();
        //dd($columna);
        $columna->delete();                        
        return $columna;
    }
}
