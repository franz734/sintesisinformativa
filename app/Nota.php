<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\NotaEntidad;
use App\Area;
use App\Directorio;
use Auth;
use Mail;
use DB;
//use App\Medio;

class Nota extends Model
{
    protected $guarded = [];
    protected $table = 'nota';
    protected $connection = 'mysql';
    protected $primaryKey = 'idNota';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel(){
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function medioRel(){
        return $this->belongsTo('App\Medio', 'idMedio');
    }
    public function tipoRel(){
        return $this->belongsTo('App\Tipo', 'idTipo');
    }
    public function sucesoRel(){
        return $this->belongsTo('App\Suceso', 'idSuceso');
    }
    public function lugarRel(){
        return $this->belongsTo('App\Lugar', 'idLugar');
    }
    
    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    // total de notas anual //
    static public function notasAnio(){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $notasAnio = Nota::whereBetween('fcAlta',[$from, $to])->count();
        return $notasAnio;
    }
    // total de notas anual de un estado dado //
    static public function notasAnioByEntidad($idEntidad){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $notasAnio = Nota::where('idEntidad',$idEntidad)->whereBetween('fcAlta',[$from, $to])->count();        
        return $notasAnio;
    }
     // total de notas anual de una area dado //
     static public function notasAnioByArea($idArea){
        $from = date('Y').'-01'.'-01';
        $to = date('Y').'-12'.'-31';
        $notasAnio = Nota::where('idArea',$idArea)->whereBetween('fcAlta',[$from, $to])->count();
        return $notasAnio;
    }
    // Obtiene todas las notas dado un estado y una fecha //
    static public function getNotasEntidadByDate($idEntidad, $a, $b, $c){
        //dd($idEntidad);
        $notasC = [];
        $control = $c;
        //dd($control);
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel'])->where('idEntidad',$idEntidad)
                                ->whereBetween('fcAlta', [$a, $b])->get();
        //dd($notas[0]->medioRel->idMedio);
        if($notas->isEmpty()){
            $flag = 'noNotas';
            return $flag;
        }
        else{
            foreach($control as $key){
                $gpoArr = [];
                foreach($notas as $nota){
                    if($key->idMedio == $nota->medioRel->idMedio){
                        array_push($gpoArr,$nota);
                        $label = $key->nomMedio;
                    }
                }
                $notasC[$label] = $gpoArr;
            }            
            return $notasC;
        }        
    }
    /// Obtiene las notas por fecha ///
    static public function getNotas($a, $b){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'subAreaRel'])->where('csv',null)->where('api',null)->whereBetween('fcAlta',[$a, $b])
                        ->where('add',1)->orderBy('idMedio')->orderBy('idEntidad')->get();
        //dd($notas);
        foreach($notas as $nota){
            $nota->texto = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->texto);
            $nota->texto = str_replace("\n",'',$nota->texto);
            if($nota->categoria == null){
                $nota->categoria = 'verde';
            }
        }
        return $notas;
    } 
    /// Obtiene todas las notas ///
    static public function getAllNotas(){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('csv',null)->where('api',null)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas;
    }
    static public function getNotaById($id){
        $nota = Nota::with(['entidadRel', 'areaRel', 'subAreaRel', 'medioRel'])->where('idNota',$id)->first();
        return $nota;
    }
    /// Obtiene las notas de un estado dado ///
    static public function getNotasByEdo($id){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('idEntidad',$id)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas; 
    }
    /// Obtiene las notas de un estado dado y una area///
    static public function getNotasByEdoArea($idEntidad, $idArea){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('idEntidad',$idEntidad)->
                        where('idArea',$idArea)->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas; 
    }
    /// Obtiene las solicitudes de información de un estado dado //
    static public function getSolicitudes($idEntidad){
        $notas = Nota::with(['entidadRel', 'areaRel', 'subAreaRel', 'medioRel', 'tipoRel'])->where('idEntidad',$idEntidad)->
                        where('statusEnvio',1)->where('idTipo',1)->where('idArea','!=',4)->orderBy('idNota', 'desc')->get();
        return $notas;
    }     
    /// Obtiene las solicitudes enviadas dada una fecha ///
    static public function getNotasEnviadasByDate($a, $b){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel'])->whereBetween('fcEnvio', [$a, $b])->get();
        return $notas;
    }
    /// Obtiene las solicitudes sin respuesta dada una fecha //
    static public function sinRespuestaByDate($a,$b){
        $sinRespuesta = DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, count(nota.idNota) AS solicitudes
                                    FROM ct_entidad JOIN nota
                                    ON ct_entidad.idEntidad = nota.idEntidad 
                                    AND nota.fcBorra  IS NULL
                                    AND nota.fcEnvio BETWEEN '$a' AND '$b'
                                    AND nota.statusEnvio = 1
                                    GROUP BY ct_entidad.idEntidad ORDER BY solicitudes DESC;");
        return $sinRespuesta;
    }
    // Obtiene las notas cargadas por csv //
    static public function getAllNotasCSV(){
        $notas = Nota::with(['entidadRel'])->where('csv',1)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas;
    }
    /// Obtiene las notas dada un area y fecha ///
    static public function getNotasByAreaDate($idArea,$a,$b){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'subAreaRel'])->where('csv',null)->where('api',null)->where('idArea',$idArea)->whereBetween('fcAlta',[$a, $b])
                        ->where('add',1)->orderBy('idMedio')->orderBy('idEntidad')->get();
        //dd($notas);
        foreach($notas as $nota){
            $nota->texto = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->texto);
            $nota->texto = str_replace("\n",'',$nota->texto);
        }        
        return $notas;
    }
    /// Obtiene las notas dado un conjunto de estados y fecha ///
    static public function getNotasByZonaDate($idEdos,$a,$b){
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'subAreaRel'])->where('csv',null)->where('api',null)->whereIn('idEntidad',$idEdos)->whereBetween('fcAlta',[$a, $b])
                        ->where('add',1)->orderBy('idMedio')->orderBy('idEntidad')->get();
        //dd($notas);
        foreach($notas as $nota){
            $nota->texto = preg_replace("/[\r\n|\n|\r]+/", PHP_EOL, $nota->texto);
            $nota->texto = str_replace("\n",'',$nota->texto);
        }        
        return $notas;
    }
    // Obtiene las notas cargadas via API //
    static public function getAllNotasAPI(){
        $notas = Nota::with(['medioRel'])->where('api',1)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas;
    }
    /// Obtiene las notas agrupadas por lugar y suceso ///
    static public function getNotasLugarSuceso($a, $b){
        $resumen = DB::select("SELECT nota.idLugar, nota.idSuceso, ct_lugar.nomLugar,ct_suceso.nomSuceso, ct_suceso.idEntidad,
                               ct_entidad.nomEntidad, COUNT(idNota) AS notas
                               FROM  nota JOIN ct_lugar
                               JOIN ct_suceso
                               JOIN ct_entidad
                               WHERE ct_lugar.idLugar = nota.idLugar 
                               AND ct_suceso.idSuceso = nota.idSuceso 
                               AND ct_suceso.idEntidad = ct_entidad.idEntidad
                               AND fcAlta BETWEEN '$a' AND '$b'
                               AND nota.fcBorra IS NULL
                               AND api IS NULL 
                               AND csv IS NULL
                               AND nota.add = 1                             
                               GROUP BY idSuceso,idLugar
                               ORDER BY notas DESC, ct_entidad.nomEntidad");
        foreach($resumen as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $resumen;
    }
    /// Obtiene las notas agrupadas por lugar y suceso, dada un area ///
    static public function getNotasLugarSucesoByArea($a, $b, $area){
        $resumen = DB::select("SELECT nota.idLugar, nota.idSuceso, ct_lugar.nomLugar,ct_suceso.nomSuceso, ct_suceso.idEntidad,
                               ct_entidad.nomEntidad, COUNT(idNota) AS notas
                               FROM  nota JOIN ct_lugar
                               JOIN ct_suceso
                               JOIN ct_entidad
                               WHERE ct_lugar.idLugar = nota.idLugar 
                               AND ct_suceso.idSuceso = nota.idSuceso 
                               AND ct_suceso.idEntidad = ct_entidad.idEntidad
                               AND fcAlta BETWEEN '$a' AND '$b'
                               AND nota.fcBorra IS NULL
                               AND api IS NULL 
                               AND csv IS NULL
                               AND nota.idArea = $area
                               GROUP BY idSuceso,idLugar
                               ORDER BY notas DESC, ct_entidad.nomEntidad");
        foreach($resumen as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $resumen;
    }
    /// Obtiene las notas agrupadas por lugar y suceso, dada un zona ///
    static public function getNotasLugarSucesoByZona($a, $b, $idEdos){
        $idEdos = collect($idEdos)->implode(',');
        $resumen = DB::select("SELECT nota.idLugar, nota.idSuceso, ct_lugar.nomLugar,ct_suceso.nomSuceso, ct_suceso.idEntidad,
                               ct_entidad.nomEntidad, COUNT(idNota) AS notas
                               FROM  nota JOIN ct_lugar
                               JOIN ct_suceso
                               JOIN ct_entidad
                               WHERE ct_lugar.idLugar = nota.idLugar 
                               AND ct_suceso.idSuceso = nota.idSuceso 
                               AND ct_suceso.idEntidad = ct_entidad.idEntidad
                               AND fcAlta BETWEEN '$a' AND '$b'
                               AND nota.fcBorra IS NULL
                               AND api IS NULL 
                               AND csv IS NULL
                               AND nota.idEntidad IN($idEdos)
                               GROUP BY idSuceso,idLugar
                               ORDER BY notas DESC, ct_entidad.nomEntidad");
        foreach($resumen as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $resumen;
    }

    /// Obtiene el total de sucesos dada una fecha ///
    static public function getTotSucesosByDate($a,$b){
        $totSucesos = DB::select("SELECT COUNT( DISTINCT idSuceso) as sucesos FROM nota 
                                  WHERE fcAlta BETWEEN '$a' AND '$b'
                                  AND api IS NULL 
                                  AND csv IS NULL
                                  AND fcBorra IS NULL");
        return $totSucesos[0];
    }
    /// Obtiene el totat de sucesos por estado ///
    static public function gatSucesosEntidad($a,$b){
        $sucesosEntidad = DB::select("SELECT  nota.idEntidad, ct_entidad.nomEntidad, count( DISTINCT nota.idSuceso) as sucesos 
                                     FROM  nota JOIN ct_entidad
                                     WHERE nota.idEntidad = ct_entidad.idEntidad
                                     AND nota.fcAlta BETWEEN '$a' AND '$b'
                                     AND nota.api IS NULL 
                                     AND nota.csv IS NULL 
                                     AND nota.fcBorra IS NULL
                                     GROUP BY idEntidad
                                     ORDER BY sucesos DESC, ct_entidad.nomEntidad");
        foreach($sucesosEntidad as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $sucesosEntidad;
    }

    /// Obtiene el total de sucesos dada una fecha y area ///
    static public function getTotSucesosByDateArea($a,$b,$area){
        $totSucesos = DB::select("SELECT COUNT( DISTINCT idSuceso) as sucesos FROM nota 
                                  WHERE fcAlta BETWEEN '$a' AND '$b'
                                  AND api IS NULL 
                                  AND csv IS NULL
                                  AND idArea = $area
                                  AND fcBorra IS NULL");
        return $totSucesos[0];
    }
    /// Obtiene el totat de sucesos por estado y area ///
    static public function gatSucesosEntidadArea($a,$b,$area){
        $sucesosEntidad = DB::select("SELECT  nota.idEntidad, ct_entidad.nomEntidad, count( DISTINCT nota.idSuceso) as sucesos 
                                     FROM  nota JOIN ct_entidad
                                     WHERE nota.idEntidad = ct_entidad.idEntidad
                                     AND nota.fcAlta BETWEEN '$a' AND '$b'
                                     AND nota.api IS NULL 
                                     AND nota.csv IS NULL 
                                     AND nota.fcBorra IS NULL
                                     AND nota.idArea = $area
                                     GROUP BY idEntidad
                                     ORDER BY sucesos DESC, ct_entidad.nomEntidad");
        foreach($sucesosEntidad as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $sucesosEntidad;
    }

    /// Obtiene el total de sucesos dada una fecha y zona ///
    static public function getTotSucesosByDateZona($a,$b,$idEdos){
        $idEdos = collect($idEdos)->implode(',');
        $totSucesos = DB::select("SELECT COUNT( DISTINCT idSuceso) as sucesos FROM nota 
                                  WHERE fcAlta BETWEEN '$a' AND '$b'
                                  AND api IS NULL 
                                  AND csv IS NULL
                                  AND idEntidad IN($idEdos)
                                  AND fcBorra IS NULL");
        return $totSucesos[0];
    }
    /// Obtiene el totat de sucesos por estado y zona ///
    static public function gatSucesosEntidadZona($a,$b,$idEdos){
        $idEdos = collect($idEdos)->implode(',');
        $sucesosEntidad = DB::select("SELECT  nota.idEntidad, ct_entidad.nomEntidad, count( DISTINCT nota.idSuceso) as sucesos 
                                     FROM  nota JOIN ct_entidad
                                     WHERE nota.idEntidad = ct_entidad.idEntidad
                                     AND nota.fcAlta BETWEEN '$a' AND '$b'
                                     AND nota.api IS NULL 
                                     AND nota.csv IS NULL 
                                     AND nota.fcBorra IS NULL
                                     AND nota.idEntidad IN($idEdos)
                                     GROUP BY idEntidad
                                     ORDER BY sucesos DESC, ct_entidad.nomEntidad");
        foreach($sucesosEntidad as $item){
            $item->nomEntidad = trim($item->nomEntidad);
        }
        return $sucesosEntidad;
    }
    /// Obtiene todas las notas dada un area ///
    static public function getAllNotasByArea($idArea){        
        $notas = Nota::with(['entidadRel', 'areaRel', 'medioRel', 'tipoRel'])->where('idArea',$idArea)->where('csv',null)->where('api',null)->orderBy('idNota', 'desc')->get();  
        foreach($notas as $nota){
            $nota->texto = str_replace('&nbsp;',' ',$nota->texto);
        }      
        return $notas;
    }

    /* MODULO CONSULTAS */
    static public function getNotasByKeyWord($keyWord){
        $idEntidad = cadToIdEntidad($keyWord);        
        $comp = ($idEntidad == 33) ? "AND (nota.encabezado LIKE '%$keyWord%' OR nota.texto LIKE '%$keyWord%')" : "AND nota.idEntidad = $idEntidad";        
        $notas = DB::select("SELECT nota.idNota, nota.encabezado, nota.texto, nota.link, nota.fcPublicacion, ct_entidad.nomEntidad
                             FROM nota JOIN ct_entidad
                             WHERE nota.idEntidad = ct_entidad.idEntidad
                             $comp
                             AND nota.api IS NULL 
                             AND nota.csv IS NULL
                             AND nota.fcBorra IS NULL
                             ORDER BY nota.fcPublicacion DESC,ct_entidad.nomEntidad,nota.idNota");                             
        foreach($notas as $nota){
            $nota->nomEntidad = trim($nota->nomEntidad);
            $nota->fcPublicacion = Carbon::parse($nota->fcPublicacion)->locale('es')->isoFormat('LL');;
        }        
        return $notas;                             
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    // Crear //
    static public function crearNota($request){        
        //dd($request->all());
        $name = Auth::user()->name;        
        if($request->extemp == 1){
            //dd($request->all());
            $request->fechaAlta = '01/01/2020';
            $request->fechaAlta = Carbon::createFromFormat('d/m/Y',$request->fechaAlta)->format('Y-m-d H:i:s');
            $request->fechaPub = Carbon::createFromFormat('d/m/Y',$request->fechaPub)->format('Y-m-d H:i:s');
            //dd($request->fechaAlta);
            $nota = Nota::create([
                'encabezado'=>$request->encabezado,
                'texto'=>$request->texto,
                'textoHtml'=>$request->textoHtml,
                'fuente'=>$request->fuente,
                'imgNota'=>$request->imgNota,
                'link'=>$request->link,
                'idEntidad'=>(int)$request->entidadFed,
                'idArea'=>(int)$request->subProc,
                'idSubArea'=>$request->subArea,
                'idMedio'=>(int)$request->medio,
                'ctaVerif'=>(int)$request->ctaVer,
                'categoria'=>$request->categoria,
                'usuario'=>$name,
                'idSuceso'=>$request->suceso,
                'idLugar'=>$request->lugar,
                'fcAlta'=>$request->fechaAlta,
                'fcPublicacion'=>$request->fechaPub
            ]);
            $notaEntidad = NotaEntidad::create([
                'idNota'=>$nota->idNota,
                'idEntidad'=>(int)$request->entidadFed,
                'fcAlta'=>$request->fechaAlta
            ]);
            return $nota;
        }
        else{
            $request->fechaAlta = Carbon::createFromFormat('d/m/Y',$request->fechaAlta)->format('Y-m-d H:i:s');
            $request->fechaPub = Carbon::createFromFormat('d/m/Y',$request->fechaPub)->format('Y-m-d H:i:s');
            //dd($request->fechaAlta);
            $nota = Nota::create([
                'encabezado'=>$request->encabezado,
                'texto'=>$request->texto,
                'textoHtml'=>$request->textoHtml,
                'fuente'=>$request->fuente,
                'imgNota'=>$request->imgNota,
                'link'=>$request->link,
                'idEntidad'=>(int)$request->entidadFed,
                'idArea'=>(int)$request->subProc,
                'idSubArea'=>$request->subArea,
                'idMedio'=>(int)$request->medio,
                'ctaVerif'=>(int)$request->ctaVer,
                'idReferencia'=>$request->referencia,
                'categoria'=>$request->categoria,
                'usuario'=>$name,
                'idSuceso'=>$request->suceso,
                'idLugar'=>$request->lugar,
                'fcAlta'=>$request->fechaAlta,
                'fcPublicacion'=>$request->fechaPub,
                'add'=>$request->add
            ]);
            $notaEntidad = NotaEntidad::create([
                'idNota'=>$nota->idNota,
                'idEntidad'=>(int)$request->entidadFed,
                'fcAlta'=>$request->fechaAlta,
                'add'=>$request->add
            ]);
            //dd($nota);
            if($nota->categoria == 'rojo'){
                $nota->idTipo = 1;
                $nota->statusEnvio = 1;
                $nota->save();
                $send = self::sendRojo($nota);                
            }
            return $nota;
        }
    }
    /// Crear Nota desde CSV ///
    static public function crearNotaCSV($a){
        $name = Auth::user()->name;
        $dt = Carbon::now()->toDateTimeString();
        //dd($dt);
        $cnt = 0;
        foreach($a as $key){
            $nota = Nota::create([
                'encabezado'=>$key[0],
                'texto'=>$key[1],    
                'textoHtml'=>'<p>'.$key[1].'</p>',            
                'fuente'=>$key[2],                
                'link'=>$key[3],
                'idEntidad'=>(int)$key[4],
                'ctaVerif'=>0,                
                'usuario'=>$name,
                'csv'=>1,
                'fcAlta'=>$dt,
                'fcPublicacion'=>$key[6]
            ]);
            $notaEntidad = NotaEntidad::create([
                'idNota'=>$nota->idNota,
                'idEntidad'=>(int)$key[4],
                'fcAlta'=>$dt
            ]);
            $cnt++;
        }
        return $cnt;
    }
    /// Crear notas desde api ///
    static public function crearNotaAPI($a){
        //$name = Auth::user()->name;
        $name = 'wwwdata';
        $dt = Carbon::now()->toDateTimeString();
        //dd($dt);
        $cnt = 0;
        foreach($a as $key){
            $nota = Nota::create([
                'encabezado'=>$key['encabezado'],
                'texto'=>$key['texto'],
                'textoHtml'=>'<p>'.$key['texto'].'</p>',                
                'fuente'=>$key['fuente'],   
                'imgNota'=>$key['imgNota'],
                'link'=>$key['link'],
                'idMedio'=>$key['idMedio'],
                'ctaVerif'=>0,                
                'usuario'=>$name,
                'idReferencia'=>$key['idReferencia'],
                'api'=>1,
                'fcAlta'=>$dt,
                'fcPublicacion'=>$key['fcPublicacion'],
                'unkey'=>$key['unkey']
            ]);            
            $cnt++;
        }
        return $cnt;
    }
    /// Actualizar ///
    static public function updateNota($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $notaIn = Nota::where('idNota',$request->idNota)->first();  
        $request->imgNota = ($request->imgNota == '') ? $notaIn->imgNota : $request->imgNota;
        $notaIn->encabezado = $request->encabezado;
        $notaIn->texto = $request->texto;
        $notaIn->textoHtml = $request->textoHtml;
        $notaIn->fuente = $request->fuente;
        $notaIn->imgNota = $request->imgNota;
        $notaIn->link = $request->link;
        $notaIn->usuario = $name;
        $notaIn->save();
        return $notaIn;
    }
    /// Actualizar desde CSV///
    static public function updateNotaCSV($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $notaIn = Nota::where('idNota',$request->idNota)->first();          
        $request->imgNota = ($request->imgNota == '') ? $notaIn->imgNota : $request->imgNota;
        //$notaIn->encabezado = $request->encabezado;
        $notaIn->texto = $request->texto;
        $notaIn->textoHtml = '<p>'.$request->texto.'</p>';
        $notaIn->fuente = $request->fuente;
        $notaIn->imgNota = $request->imgNota;
        $notaIn->idArea = $request->subProc;
        $notaIn->idSubArea = $request->subArea;
        $notaIn->idMedio = $request->medio;
        $notaIn->idReferencia = $request->referencia;
        $notaIn->csv = null;
        $notaIn->categoria = $request->categoria;
        $notaIn->usuario = $name;
        $notaIn->idSuceso = $request->suceso;
        $notaIn->idLugar = $request->lugar;
        $notaIn->add = $request->add;
        $notaIn->save();
        if($request->add == 1){
            $notaEntidad = NotaEntidad::where('idNota',$notaIn->idNota)->first();
            $notaEntidad->add = $request->add;
            $notaEntidad->save();
        }
        if($notaIn->categoria == 'rojo'){
            $notaIn->idTipo = 1;
            $notaIn->statusEnvio = 1;
            $notaIn->save();
            $send = self::sendRojo($notaIn);
        }
        //dd($notaIn);
        return $notaIn;
    }
    /// Actualizar desde API ///
    static public function updateNotaAPI($request){
        //dd($request->imgNota);
        $dt = Carbon::now()->toDateTimeString();
        $name = Auth::user()->name;
        $notaIn = Nota::where('idNota',$request->idNota)->first();          
        $request->imgNota = ($request->imgNota == '') ? $notaIn->imgNota : $request->imgNota;
        $notaIn->encabezado = $request->encabezado;
        $notaIn->texto = $request->texto;
        $notaIn->textoHtml = '<p>'.$request->texto.'</p>';
        $notaIn->fuente = $request->fuente;
        $notaIn->imgNota = $request->imgNota;
        $notaIn->idEntidad = $request->entidadFed;
        $notaIn->idArea = $request->subProc;
        $notaIn->idSubArea = $request->subArea;
        //$notaIn->idMedio = $request->medio;
        //$notaIn->idReferencia = $request->referencia;
        $notaIn->api = null;
        $notaIn->categoria = $request->categoria;
        $notaIn->usuario = $name;
        $notaIn->idSuceso = $request->suceso;
        $notaIn->idLugar = $request->lugar;
        $notaIn->fcAlta = $dt;
        $notaIn->add = $request->add;
        $notaIn->save();
        $notaEntidad = NotaEntidad::create([
            'idNota'=>$notaIn->idNota,
            'idEntidad'=>(int)$request->entidadFed,
            //'fcAlta'=>$notaIn->fcAlta
            'fcAlta'=>$dt,
            'add'=>$request->add
        ]);
        if($notaIn->categoria == 'rojo'){
            $notaIn->idTipo = 1;
            $notaIn->statusEnvio = 1;
            $notaIn->save();
            $send = self::sendRojo($notaIn);
        }
        //dd($notaIn);
        return $notaIn;
    }
    /// Borrar ///
    static public function deleteNota($request){
        //dd($request->idNota);
        $nota = Nota::where('idNota',$request->idNota)->first();
        $nota->delete();        
        $notaEntidad = NotaEntidad::where('idNota',$request->idNota)->first();
        $notaEntidad->delete();
        //dd($notaEntidad);
        return $nota;
    }
    static public function deleteNotaAPI($request){
        //dd($request->idNota);
        $nota = Nota::where('idNota',$request->idNota)->first();
        $nota->delete();        
        /* $notaEntidad = NotaEntidad::where('idNota',$request->idNota)->first();
        $notaEntidad->delete(); */
        //dd($notaEntidad);
        return $nota;
    }
    /// Borrar desde API ///

    /// Send Mail ///
    static public function sendMailNota($request){
        $hoyEs = Carbon::today()->locale('es')->isoFormat('LL');
        $dt = Carbon::now();
        $notaSend = Nota::with(['entidadRel', 'areaRel', 'medioRel'])->where('idNota',$request->idNota)->first();
        //dd($dt);
        $destinatario = Directorio::where('idEntidad',$notaSend->idEntidad)->first();
        $mailTo = $destinatario->correoElectronico;
        //dd($mailTo);
        //$mailTo = 'francisco_sanchez@dgire.unam.mx';
        if($notaSend->idTipo == 1){
            $subject = 'SOLICITUD DE NOTA INFORMATIVA '.$hoyEs;
        }
        elseif($notaSend->idTipo == 2){
            $subject = 'SOLICITUD DE NOTA DE SEGUIMIENTO '.$hoyEs;
        }
        elseif($notaSend->idTipo == 3){
            $subject = 'NOTA DE CONOCIMIENTO '.$hoyEs;
        } 
        $copyTo = $notaSend->areaRel->correoResponsable;  
        $copyTo = [$copyTo,
                    'miguel.dorantes@profepa.gob.mx',
                    'raquel.soto@profepa.gob.mx',
                    'lady.esquivel@profepa.gob.mx',
                    'fernando.reyna@profepa.gob.mx',
                    'nestor.guerrero@profepa.gob.mx'];    
        if($notaSend->idArea == 4){            
            $mailTo = $notaSend->areaRel->correoResponsable;
            $copyTo = ['miguel.dorantes@profepa.gob.mx',
                        'raquel.soto@profepa.gob.mx',
                        'lady.esquivel@profepa.gob.mx',
                        'fernando.reyna@profepa.gob.mx',
                        'nestor.guerrero@profepa.gob.mx'];
            array_push($copyTo,'florisel.santiago@profepa.gob.mx');
        }    
        //dd($copyTo);
        //$copyTo = ['sefrancisco734@gmail.com', 'francisco.sancheze@profepa.gob.mx'];
        Mail::send('admin.correo',['nota'=> $notaSend], function($message) use($mailTo, $subject, $copyTo) {            
            $message->to($mailTo)->subject($subject)->cc($copyTo);                     
        });
        $notaSend->statusEnvio = 1;
        $notaSend->fcEnvio = $dt;
        $notaSend->save();
        return $notaSend;
    }    
    ///Envia nota categoria rojo///
    static public function sendRojo($nota){
        $notaSend = Nota::with(['entidadRel','areaRel'])->where('idNota',$nota->idNota)->first();        
        $destinatario = Directorio::where('idEntidad',$notaSend->idEntidad)->first();
        if($notaSend->idMedio == 1){
            $dirDoc = '/var/www/html/sintesisinformativa/public/'.$notaSend->imgNota;
            $dirDoc = '';
        }
        else{
            $dirDoc = '';
        }
        $subject = 'INFORMACIÓN IMPORTANTE';
        if($notaSend->areaRel->idArea == 4){
            $mailTo = [
                $destinatario->correoElectronico,
                'florisel.santiago@profepa.gob.mx',
                'lady.esquivel@profepa.gob.mx',
                'fernando.reyna@profepa.gob.mx',
                'monica.jasso@profepa.gob.mx',
                'nestor.guerrero@profepa.gob.mx'
            ];
        }
        else{
            $mailTo = [
                $destinatario->correoElectronico,
                $notaSend->areaRel->correoResponsable,
                'florisel.santiago@profepa.gob.mx',
                'lady.esquivel@profepa.gob.mx',
                'fernando.reyna@profepa.gob.mx',
                'monica.jasso@profepa.gob.mx',
                'nestor.guerrero@profepa.gob.mx'
            ];
        }
        $copyTo = [
            'miguel.dorantes@profepa.gob.mx',
            'claudia.palacios@profepa.gob.mx',
            'ana.carta@profepa.gob.mx',
            'maria.vicencio@profepa.gob.mx'

        ];
        //dd($mailTo);
        Mail::send('admin.correoNotaRojo',['nota'=> $notaSend], function($message) use($mailTo, $subject, $copyTo, $dirDoc) {            
            $message->to($mailTo)->subject($subject)->cc($copyTo);  
            if($dirDoc != ''){
                $message->attach($dirDoc);
            }                   
        });
        return true;
    }

    // Compartir //
    static public function shareNota($request){
        //dd($request);                
        $notaSend = Nota::with(['entidadRel','areaRel'])->where('idNota',$request->id)->first();
        $mailTo = $request->mail;
        $subject = 'Nota publicada en medios';
        $userMail = Auth::user()->email;
        $copyTo = ['miguel.dorantes@profepa.gob.mx',$userMail];       
        try{            
            Mail::send('admin.correoComparteNota',['nota'=> $notaSend], function($message) use($mailTo, $subject, $copyTo) {            
                $message->to($mailTo)->subject($subject)->cc($copyTo);                                    
            });
        }
        catch(\Exception $e){            
            return response()->json('Error al enviar',401);
        }
        return $notaSend;
    }
    

}
