<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;
use Mail;
use DB;
use App\TemaRelevante;

class RespuestaTemaRelevante extends Model
{
    protected $guarded = [];
    protected $table = 'respuestaTemaRel';
    protected $connection = 'mysql';
    protected $primaryKey = 'idRespuestaTemaRel';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes; 

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function temaRelevanteRel(){
        return $this->belongsTo('App\TemaRelevante', 'idTemaRel');
    }
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel(){
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function propuestaRel(){
        return $this->hasOne('App\Propuesta','idIncidencia', 'idIncidencia');
    }
    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    // Crear //
    static public function crearRespuesta($request){  //Eliminar para versión 2
        //dd($request->idEntidad);
        $respuesta = RespuestaTemaRelevante::create([        
            'idTemaRel'=>$request->idTema,     
            'idEntidad'=>$request->idEntidad,
            'respuesta'=>$request->respuesta
        ]);
        $temaRel = TemaRelevante::where('idTemaRel',$request->idTema)->first();
        $temaRel->status = 1;
        $temaRel->save();
        $respuestaSend = RespuestaTemaRelevante::with(['entidadRel','temaRelevanteRel'])->where('idTemaRel',$respuesta->idTemaRel)->first();        
        if($request->idEntidad != null){
            $mailZona = getMailZona($request->idEntidad);
            $mailEntidad = getMailEntidad($request->idEntidad);
            $mailTo = [$mailZona, $mailEntidad, 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }
        else{
            $mailTo = ['lady.esquivel@profepa.gob.mx', 'fernando.reyna@profepa.gob.mx', 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }                
        $subject = 'Respuesta a Tema Relevante';
        Mail::send('delegado.correoRespuestaTemaRel',['respuesta'=> $respuestaSend], function($message) use($mailTo, $subject, $copyTo) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject)->cc($copyTo);                     
        });
        return $respuesta;
    }
    /* static public function crearRespuesta($request){ //para versión 2
        //dd($request);
        $respuesta = RespuestaTemaRelevante::create([        
            'idTemaRel'=>$request->idTema,     
            'idEntidad'=>$request->idEntidad,
            'idArea'=>$request->area,
            'idSubArea'=>$request->subArea,            
            'respuesta'=>$request->resumen,
            'actuacion'=>$request->texto,
            'actuacionHtml'=>$request->textoHtml,
            'acciones'=>$request->acciones,
            'evidencia'=>$request->evidencia,
            'docRespuesta'=>$request->documento,
            'flgPropuesta'=>$request->flgPropuesta

        ]);
        if($respuesta->flgPropuesta == 1){
            DB::beginTransaction();
            try{
                $propArrIn = [
                    'propuesta'=>json_encode($objProp),
                    'idTipoPropuesta'=>$request->propuesta,
                    'idIncidencia'=>null,
                    'idNota'=>null,
                    'idRespuesta'=>null,
                    'idRespuestaTemaRel'=>$respuesta->idRespuestaTemaRel,
                    'idArea'=>$request->area,
                    'idSubarea'=>$request->subArea,
                    'idEntidad'=>$request->entidadFed,
                    'statusRev'=>null
                ];
                $propuesta = Propuesta::crearPropuesta($propArrIn);
            }
            catch(ValidationException $e){
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg,401);
            }
            DB::commit();
            $propuestaSend = Propuesta::where('idPropuesta',$propuesta->idPropuesta)->first();
        }
        $temaRel = TemaRelevante::where('idTemaRel',$request->idTema)->first();
        $temaRel->status = 1;
        $temaRel->save();
        $respuestaSend = RespuestaTemaRelevante::with(['entidadRel','temaRelevanteRel'])->where('idTemaRel',$respuesta->idTemaRel)->first();        
        if($request->idEntidad != null){
            $mailZona = getMailZona($request->idEntidad);
            $mailEntidad = getMailEntidad($request->idEntidad);
            $mailTo = [$mailZona, $mailEntidad, 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }
        else{
            $mailTo = ['lady.esquivel@profepa.gob.mx', 'fernando.reyna@profepa.gob.mx', 'nestor.guerrero@profepa.gob.mx'];
            $copyTo = ['miguel.dorantes@profepa.gob.mx'];
        }                
        $subject = 'Respuesta a Tema Relevante';
        Mail::send('delegado.correoRespuestaTemaRel',['respuesta'=> $respuestaSend], function($message) use($mailTo, $subject, $copyTo) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject)->cc($copyTo);                     
        });
        return $respuesta;
    } */

    // Enviar //
    static public function sendRespuesta($request){
        //dd($request);                
        $respuestaSend = RespuestaTemaRelevante::with(['entidadRel','temaRelevanteRel'])->where('idRespuestaTemaRel',$request->idRespuesta)->first();
        $mailTo = $request->mail;
        $subject = 'Respuesta a tema relevante';
        $userMail = Auth::user()->email;
        $copyTo = ['miguel.dorantes@profepa.gob.mx',$userMail];
        try{
            Mail::send('admin.correoComparteRespTemaRel',['respuesta'=> $respuestaSend], function($message) use($mailTo, $subject, $copyTo) {            
                $message->to($mailTo)->subject($subject)->cc($copyTo);                                    
            });
        }
        catch(\Exception $e){            
            return response()->json('Error al enviar',401);
        }
        return $respuestaSend;
    }
}
