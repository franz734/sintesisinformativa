<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;
use Mail;
use DB;
use App\Propuesta;
use App\Nota;

class Respuesta extends Model
{
    protected $guarded = [];
    protected $table = 'respuesta';
    protected $connection = 'mysql';
    protected $primaryKey = 'idRespuesta';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;    
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function entidadRel(){
        return $this->belongsTo('App\Entidad', 'idEntidad');
    }
    public function notaRel(){
        return $this->belongsTo('App\Nota', 'idNota');
    }
    public function areaRel(){
        return $this->belongsTo('App\Area', 'idArea');
    }
    public function subAreaRel(){
        return $this->belongsTo('App\SubArea', 'idSubArea');
    }
    public function propuestaRel(){
        return $this->hasOne('App\Propuesta','idIncidencia', 'idIncidencia');
    }
    ///////////////////////
    ///*** Funciones ***///
    //////////////////////
    static public function getAllRespuestas(){
        $respuestas = Respuesta::with(['entidadRel','notaRel','propuestaRel'])->orderBy('idRespuesta', 'desc')->get();
        return $respuestas;
    }
    static public function getRespuestas($a, $b){
        $respuestas = Respuesta::with(['entidadRel', 'areaRel', 'SubAreaRel', 'notaRel'])->whereBetween('fcCrea',[$a, $b])->get();        
        return $respuestas;
    } 
    static public function getRespuestasByDate($a, $b){
        //$today = Carbon::now()->toDateString();
        $respuestas = DB::select("SELECT ct_entidad.idEntidad, ct_entidad.cveEntidad, ct_entidad.nomEntidad, count(respuesta.idRespuesta) AS respuestas
                                    FROM ct_entidad JOIN respuesta
                                    ON ct_entidad.idEntidad = respuesta.idEntidad 
                                    AND respuesta.fcBorra  IS NULL
                                    AND respuesta.fcCrea BETWEEN '$a' AND '$b'
                                    GROUP BY ct_entidad.idEntidad ORDER BY respuestas DESC;");
        return $respuestas;
    }
    /////////////////////
    ///*** Metodos ***///
    ////////////////////
    // Crear //
    static public function crearRespuesta($request){
        $objProp = (object)[];      
        $propuestaSend = null;  
        if($request->flgPropuesta == 1){
            $tipoProp = $request->propuesta;
            switch ($tipoProp) {
                case 1:
                    $objProp->texto = $request->txtTweet;
                    break;
                case 2:
                    $objProp->titulo = $request->titBoletin;
                    $objProp->textHtml = $request->textoHtmlBoletin;
                    $objProp->texto = $request->textoBoletin;
                    break;                
            }            
        }
        //dd($objProp);
        //$request->fechaSuc = Carbon::createFromFormat('d/m/Y',$request->fechaSuc)->format('Y-m-d H:i:s');
        $respuesta = Respuesta::create([        
            'idNota'=>$request->idNota,     
            'idEntidad'=>$request->entidadFed,
            'idArea'=>$request->area,
            'idSubArea'=>$request->subArea,       
            'actuacion'=>$request->texto,
            'actuacionHtml'=>$request->textoHtml,
            'acciones'=>$request->acciones,
            'resumen'=>$request->resumen,
            'evidencia'=>$request->evidencia,
            'docRespuesta'=>$request->documento,
            'fcSuceso'=>$request->fechaSuc,            
            'flgPropuesta'=>$request->flgPropuesta
        ]);        
        if($respuesta->flgPropuesta == 1){
            DB::beginTransaction();
            try{
                $propArrIn = [
                    'propuesta'=>json_encode($objProp),
                    'idTipoPropuesta'=>$request->propuesta,
                    'idIncidencia'=>null,
                    'idNota'=>null,
                    'idRespuesta'=>$respuesta->idRespuesta,
                    'idRespuestaTemaRel'=>null,
                    'idArea'=>$request->area,
                    'idSubarea'=>$request->subArea,
                    'idEntidad'=>$request->entidadFed,
                    'statusRev'=>null
                ];
                $propuesta = Propuesta::crearPropuesta($propArrIn);
            }
            catch(ValidationException $e){
                DB::rollback();
                $msg = 'No se pudo guardar la información';
                return response()->json($msg,401);
            }
            DB::commit();
            $propuestaSend = Propuesta::where('idPropuesta',$propuesta->idPropuesta)->first();
        }
        $nota = Nota::where('idNota',$request->idNota)->first();
        $nota->statusEnvio = 2;
        $nota->save();
        $respuestaSend = Respuesta::with(['entidadRel'])->where('idRespuesta',$respuesta->idRespuesta)->first();  
        $mailEntidad = getMailEntidad($request->entidadFed);        
        $mailTo = ['comunicacion.social@profepa.gob.mx'];        
        $copyTo = ['miguel.dorantes@profepa.gob.mx', 'comunicacion.social@profepa.gob.mx', $mailEntidad, 'nestor.guerrero@profepa.gob.mx'];        
        $subject = 'Respuesta a Solicitud de información';
        Mail::send('delegado.correoRespuesta',['respuesta'=> $respuestaSend, 'propuesta'=>$propuestaSend], function($message) use($mailTo, $subject, $copyTo) {
            // Set the receiver and subject of the mail.
            $message->to($mailTo)->subject($subject)->cc($copyTo);                     
        });
        return $respuesta;
    }

     // Enviar //
     static public function sendRespuesta($request){
        //dd($request);                
        $respuestaSend = Respuesta::with(['entidadRel','notaRel'])->where('idRespuesta',$request->id)->first();
        $mailTo = $request->mail;
        $subject = 'Respuesta a nota publicada en medios';
        $userMail = Auth::user()->email;
        $copyTo = ['miguel.dorantes@profepa.gob.mx',$userMail];
        //dd($respuestaSend);
        try{
            Mail::send('admin.correoComparteRespuesta',['respuesta'=> $respuestaSend], function($message) use($mailTo, $subject, $copyTo) {            
                $message->to($mailTo)->subject($subject)->cc($copyTo);                                    
            });
        }
        catch(\Exception $e){            
            return response()->json('Error al enviar',401);
        }
        return $respuestaSend;
    }
}
