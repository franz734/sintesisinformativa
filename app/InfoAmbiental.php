<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Auth;

class InfoAmbiental extends Model
{
    protected $guarded = [];
    protected $table = 'infoAmbiental';
    protected $connection = 'mysql';
    protected $primaryKey = 'idInfoAmbiental';    
    const CREATED_AT = 'fcCrea';
    const UPDATED_AT = 'fcActualiza';
    const DELETED_AT = 'fcBorra';
    use SoftDeletes;
    /* use SoftDeletes;
    protected $date; */

    ////////////////////////
    ///*** Relaciones ***///
    ///////////////////////
    public function referenciaRel(){
        return $this->belongsTo('App\Referencia', 'idReferencia');
    }

    ///////////////////////
    ///*** Funciones ***///
    //////////////////////

    // Otiene las ambientales de un dia dado //
    static public function getAmbiental($a, $b){
        $ambientales = InfoAmbiental::with(['referenciaRel'])->whereBetween('fcAlta', [$a, $b])->where('csv',null)->get();
        return $ambientales;
    }
    // obtiene todas las ambientales para datatable //
    static public function getAllAmbientales(){
        $ambientales = InfoAmbiental::where('csv',null)->orderBy('idInfoAmbiental', 'desc')->get();
        return $ambientales;
    }
    // Obtine infoAmbiental dado un id //
    static public function getAmbientalById($id){
        $ambiental = InfoAmbiental::where('idInfoAmbiental', $id)->first();
        return $ambiental;
    }

    /////////////////////
    ///*** Metodos ***///
    ////////////////////

    // Crear ///
    static public function crearAmbiental($request){
        //dd($request->all());
        //$now = Carbon::now();
        $name = Auth::user()->name;
        $idMedio = ($request->medio) ? $request->medio : NULL;
        $idEntidad = ($request->entidadFed) ? $request->entidadFed : NULL;
        $idArea = ($request->subProc) ? $request->subProc : NULL;
        $request->fcAlta = Carbon::createFromFormat('d/m/Y',$request->fechaAlta)->format('Y-m-d H:i:s');
        //dd($request->fcAlta);
        $ambiental = InfoAmbiental::create([
            'encabezado'=>$request->encabezado,
            'texto'=>$request->texto,
            'textoHtml'=>$request->textoHtml,
            'link'=>$request->link,
            'idReferencia'=>$request->referencia,
            'idArea'=>$idArea,
            'idMedio'=>$idMedio,
            'idEntidad'=>$idEntidad,
            'usuario'=>$name,
            'fcAlta'=>$request->fcAlta
        ]);
        return $ambiental;
    }
     /// Crear Ambiental desde CSV ///
     static public function crearAmbientalCSV($a){
        $name = Auth::user()->name;
        $dt = Carbon::now()->toDateTimeString();
        //dd($dt);
        $cnt = 0;
        foreach($a as $key){
            $ambiental = InfoAmbiental::create([
                'encabezado'=>$key[0],
                'texto'=>$key[1],                                                
                'link'=>$key[3],                                
                'usuario'=>$name,
                'csv'=>1,             
                'fcAlta'=>$dt                
            ]);            
            $cnt++;
        }
        return $cnt;
    }
    // Actualizar //
    static public function updateAmbiental($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $ambientalIn = InfoAmbiental::where('idInfoAmbiental', $request->idAmbiental)->first();
        $ambientalIn->encabezado = $request->encabezado;
        $ambientalIn->textoHtml = $request->textoHtml;
        $ambientalIn->texto = $request->texto;
        $ambientalIn->link = $request->link;
        $ambientalIn->usuario = $name;
        $ambientalIn->save();
        return $ambientalIn;
    }
    /// Actualizar desde CSV///
    static public function updateAmbientalCSV($request){
        //dd($request->all());
        $name = Auth::user()->name;
        $ambientalIn = InfoAmbiental::where('idInfoAmbiental',$request->idInfoAmbiental)->first();                  
        $ambientalIn->encabezado = $request->encabezado;
        $ambientalIn->texto = $request->texto;
        $ambientalIn->textoHtml = '<p>'.$request->texto.'</p>';        
        $ambientalIn->idReferencia = $request->referencia;
        $ambientalIn->csv = null;        
        $ambientalIn->save();        
        //dd($ambientalIn);
        return $ambientalIn;
    }
    // Borrar //
    static public function deleteAmbiental($request){
        $ambiental = InfoAmbiental::where('idInfoAmbiental', $request->idAmbiental)->first();
        //dd($ambiental);
        $ambiental->delete();
        return $ambiental;
    }
}
