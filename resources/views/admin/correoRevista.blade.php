<p>Estimada/o:</p>
<p>Por este medio compartimos el <b>SÉPTIMO</b> número de la revista <b>MI PROFEPA</b>, publicación periódica que tiene el objetivo de
    difundir las acciones de esta Procuraduría, motivar el interés colectivo en la justicia ambiental e informar de
    manera actualizada lo que todos aportamos diariamente con nuestro trabajo a esta institución.</p>
<p>Sin más, esperando que este nuevo producto sea de su agrado, reciba cordiales saludos.</p>