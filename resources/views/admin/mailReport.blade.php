<div class="campo">
    {{-- <div class="container-fluid" style="background-color:#efefef"> --}}
    <br>
    <br>
    <div class="container-fluid">
        <h1 style="text-align:center">Reporte de error "Sintesís Informativa"</h1>
    </div>
    <br>
    <table class="emailExceptionTable" style="text-align: left;" border="0" cellspacing="0" cellpadding="3">
        <tr>
            <td>
                <strong>Entorno:</strong>
            </td>
            <td>
                {{ env('APP_ENV', 'Desarrollo') }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Url:</strong>
            </td>
            <td>
                {!! Request::fullUrl() !!}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Clase:</strong>
            </td>
            <td>
                {{ get_class($error['exception']) }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Mensaje:</strong>
            </td>
            <td>
                {{ $error['exception']->getMessage() }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Codigo:</strong>
            </td>
            <td>
                {{ $error['exception']->getCode() }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>Usuario:</strong>
            </td>
            <td>
                {{ $error['name'] }}
            </td>
        </tr>
        <tr>
            <td>
                <strong>IP usuario:</strong>
            </td>
            <td>
                {{ $error['ip'] }}
            </td>
        </tr>
    </table>
    <br>
    <table align="center" style="text-align: center;" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                Error en <strong>{{ $error['exception']->getFile() }}</strong> en la linea
                <strong>{{ $error['exception']->getLine() }}</strong>
            </td>
        </tr>
    </table>
    <br>
    <table align="center" style="text-align: center;" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <strong>Stack Trace:</strong>
            </td>
        </tr>
        <tr>
            <td align="left" style="text-align: left;">
                {!! nl2br($error['exception']->getTraceAsString()) !!}
            </td>
        </tr>
    </table>
    {{-- </div> --}}
</div>
