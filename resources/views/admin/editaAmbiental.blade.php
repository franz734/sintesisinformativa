@extends('admin.master')
@section('css')
<link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
<div class="col s12 m12 l12">
<div class="card">
    <div class="card-content">
        <form id="editaAmbientalForm" action="">
            <div class="row">
                <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                <div class="col s12 m4 l8">
                    <input hidden id="idAmbiental" class="validar" name="idAmbiental" value="{{$ambiental->idInfoAmbiental}}" required> 
                    <div class="input-field">                        
                        <label for="encabezado">Encabezado</label>
                        <input id="encabezado" class="validar" name="encabezado" type="text" value="{{$ambiental->encabezado}}" required>
                    </div>
                    <div class="input-field">                            
                            <label for="texto">Texto</label>                                    
                            {{-- <br>
                            <br>                                
                            <div id="infoAmbientalTexto" class="mi_editor-container">{!!$ambiental->textoHtml!!}</div> 
                            <input hidden id="textoHtml" class="validar" name="textoHtml" required> --}}
                            <textarea name="texto" id="texto" class="materialize-textarea validar" cols="30" rows="10">{{$ambiental->texto}}</textarea>
                            {{-- <input hidden id="texto" class="validar" name="texto" required> --}}                            
                    </div>
                    <div class="input-field">
                        <label for="link">Link</label>
                        <input id="link" name="link" data-parsley-type="url" type="text" value="{{$ambiental->link}}">
                    </div>                                                                                                                                              
                </div>
                <div class="col s12 m4 l2"></div><!--ofsset derecho-->
            </div>
            <div class="row right-align">
                <a id="btnModifAmbiental" class="waves-effect waves-light btn indigo miA">Guardar Información Ambiental</a>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
<script>
    // Initialize QUill editor
    var quill = new Quill('#infoAmbientalTexto', {
      modules: {
        toolbar: [
          [{ header: [1, 2, 3, 4, 5, 6,  false] }],
          ['bold', 'italic', 'underline','strike'],
          [{ 'color': [] }, { 'background': [] }],
          ['link'],
          [{ 'script': 'sub'}, { 'script': 'super' }],
          [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                   
        ]
      },      
      theme: 'snow'  // or 'bubble'
    });      
    $('.ql-snow select').addClass('browser-default');  
    quill.on('text-change', function(delta, source) {
        updateHtmlOutput_msj()
    })        
    $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
        function getQuillHtml_msj() { return quill.root.innerHTML; }
        function updateHtmlOutput_msj()
        {
            let html = getQuillHtml_msj();
            //console.log ( html );            
            $('#textoHtml').val(html);
            var contenido = $('#textoHtml').val();
            var texto = contenido.replace(/<[^>]*>?/g, '');
            $('#texto').val(texto);  
        }
    updateHtmlOutput_msj();
</script> 
@endsection