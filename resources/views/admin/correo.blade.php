@php
    if($nota->idMedio == 4){
        $cad = 'publicación en redes sociales';
    }
    else{
        $cad = 'nota periodística';
    }
@endphp
<p>Estimada/o:</p>
@if ($nota->idTipo == 1)
    <p>Esperando se encuentre bien, por este medio me permito solicitar, de la manera más atenta, tenga a bien compartirnos una <b>nota informativa</b> del siguiente tema obtenido del monitoreo de medios de comunicación y redes sociales.</p>    
@elseif( $nota->idTipo == 2)
    <p>Esperando se encuentre bien, por este medio me permito solicitar, de la manera más atenta, tenga a bien compartirnos una <b>nota de seguimiento</b> del siguiente tema obtenido del monitoreo de medios de comunicación y redes sociales.</p>    
@elseif( $nota->idTipo == 3)
    <p>Esperando se encuentre bien, por este medio me permito compartir, para su conocimiento, la siguiente publicación obtenida del monitoreo de medios de comunicación y redes sociales. </p>    
@endif  
<p>Sin más por el momento, le enviamos cordiales saludos.</p>
<br>
<p>{{$nota->entidadRel->nomEntidad}}</p>
<p>{{$nota->areaRel->nomArea}}</p>
@if ($nota->idMedio == 1)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <p><b>{{$nota->fuente}}</b></p>
@elseif($nota->idMedio == 2)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">VER NOTA</a>
@elseif($nota->idMedio == 3)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">CONTINUAR LEYENDO</a>
@elseif($nota->idMedio == 4)
    <a href="{{$nota->link}}" title="{{$nota->link}}" target="_blank">                        
        <img class="notaCont" style="margin-right: 10px" src="{{URL::asset($nota->imgNota)}}" width="310" height="400">
    </a>
@endif