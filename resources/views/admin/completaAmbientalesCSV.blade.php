@extends('admin.master')
@section('css')
    
@endsection
@section('contenido')
<h1>Información Ambiental Cargada Vía CSV</h1>
<style>
    td.details-control {
      background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
      cursor: pointer;
    }
      tr.shown td.details-control {
      background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
    }
</style>
<input hidden id="idNota" type="text">
<div class="row no-m-t no-m-b">
    <div class="card">
        <div class="card-content">
            <table id="tableAmbietalesCSV" class="display responsive-table datatable-example">
                <thead>
                    <tr>
                        <th>&nbsp&nbsp&nbsp</th>
                        <th>ID</th>
                        <th scope="col">Encabezado</th>                                                                       
                        <th scope="col">Link</th>                        
                        <th scope="col">Fecha de Alta</th>                        
                        <th scope="col">Acción</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
    
@endsection
@section('scripts')
<script src="{{URL::asset('assets/js/customNotasCSV.js')}}?{{rand()}}"></script>
@endsection