@extends('admin.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('contenido')
    <h1>Incidencias</h1>
    <style>
        td.details-control {
            background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }

    </style>
    <div class="row no-m-t no-m-b">
        <!--Filtro de busqueda-->
        <div class="col s12 m4 l2"></div>
        <!--ofsset izquierdo-->
        <div class="col s12 m4 l8">
            <form id="filterFormInc" action="">
                <div class="card stats-card">
                    <div class="card-content">
                        <span class="card-title">Busqueda</span>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectEdo" name="entidadFed" class="js-states browser-default" tabindex="-1"
                                        style="width: 100%">
                                        <option disabled selected value="def">Entidad Federativa</option>
                                        @foreach ($entidades as $entidad)
                                            <option value={{ $entidad->idEntidad }}>{{ $entidad->nomEntidad }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectMateria" name="materia" class="js-states browser-default"
                                        tabindex="-1" style="width: 100%">
                                        <option disabled selected value="def">Materia</option>
                                        @foreach ($materias as $materia)
                                            <option value={{ $materia->idMateria }}>{{ $materia->nomMateria }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectArea" name="area" class="js-states browser-default" tabindex="-1"
                                        style="width: 100%">
                                        <option disabled selected value="def">Área</option>
                                        @foreach ($areas as $area)
                                            <option value={{ $area->idArea }}>{{ $area->nomArea }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectSubArea" name="subArea" class="js-states browser-default"
                                        tabindex="-1" style="width: 100%">
                                        <option disabled selected value="def">Sub-Área</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div id="contSelAccVS" class="input-field" style="display: none">
                                    <select name="accionVS" id="selectAccioVS" class="js-states browser-default validar"
                                        tabindex="-1" style="width: 100%">
                                        <option disabled selected value="def">Acción</option>
                                        <option value="aseguramiento">Aseguramiento Precautorio</option>
                                        <option value="decomision">Decomiso</option>
                                        <option value="liberacion">Liberación</option>
                                        <option value="deposito">Deposito</option>
                                        <option value="devolucion">Devolución</option>
                                        <option value="muerte">Muerte de Ejemplar</option>
                                        <option value="reunion">Reunión</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="tema">Tema</label>
                                    <input id="tema" name="tema" type="text" class="validar">
                                </div>
                            </div>
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="problematica">Problematica</label>
                                    <input id="problematica" name="problematica" type="text" class="validar">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="datefilter">Fecha de Alta</label>
                                    <br>
                                    <input id="datefilter" type="text" name="datefilter" />
                                </div>
                            </div>
                        </div>
                        <div class="right-align">
                            <a id="excelInc" class="waves-effect waves-light btn green" style="display: none"><i
                                    class="material-icons right">description</i>Excel</a>
                            <a id="limpiaFilter" class="waves-effect waves-light btn"
                                style="background-color: #235b4e">Limpiar</a>
                            <a id="filterIncidencias" class="waves-effect waves-light btn"
                                style="background-color: #9f2241">Buscar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col s12 m4 l2"></div>
        <!--ofsset derecho-->
    </div>
    <!--Fin del filtro-->
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableIncidencias" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Tema</th>
                            <th scope="col">Entidad</th>
                            <th scope="col">Fecha de Suceso</th>
                            <th scope="col">Fecha de Alta</th>
                            <th scope="col">Propuesta</th>
                            <th scope="col">Documento</th>
                            <th scope="col">Más</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(function() {

            $('input[name="datefilter"]').daterangepicker({
                opens: 'center',
                drops: 'up',
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format(
                    'DD/MM/YYYY'));
            });

            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });

    </script>
@endsection
