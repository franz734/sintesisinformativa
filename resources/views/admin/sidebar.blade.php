@php
    //dd(url());
@endphp
<header class="mn-header navbar-fixed">
    <nav class="miNavHeader">
        <div class="nav-wrapper row">
            <section class="material-design-hamburger navigation-toggle">
                <a href="javascript:void(0)" data-activates="slide-out" class="button-collapse show-on-large material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <div class="header-title col s3 m3">      
                {{-- <span class="chapter-title">SIGEMINC</span> --}}
                <img src="{{URL::asset('assets/images/Logo_gob.png')}}" height="45px">
            </div>            
        </div>
    </nav>
</header>
<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">                
                <img src="{{URL::asset('assets/images/user.png')}}" class="circle" alt="">
            </div>
            <div class="sidebar-profile-info">
                {{-- <a href="javascript:void(0);" class="account-settings-link"> --}}
                    <p>{{ Auth::user()->name }}</p>
                    {{-- <span>david@gmail.com<i class="material-icons right">arrow_drop_down</i></span> --}}
                {{-- </a> --}}
            </div>
        </div>
        {{-- <div class="sidebar-account-settings">
            <ul>
                <li class="no-padding">
                    <a class="waves-effect waves-grey"><i class="material-icons">mail_outline</i>Inbox</a>
                </li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey"><i class="material-icons">star_border</i>Starred<span class="new badge">18</span></a>
                </li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey"><i class="material-icons">done</i>Sent Mail</a>
                </li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey"><i class="material-icons">history</i>History<span class="new grey lighten-1 badge">3 new</span></a>
                </li>
                <li class="divider"></li>
                <li class="no-padding">
                    <a class="waves-effect waves-grey"><i class="material-icons">exit_to_app</i>Sign Out</a>
                </li>
            </ul>
        </div> --}}
    <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
        @role ('Colaborador')
        <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/incidencia/inicio')}}"><i class="material-icons">settings_input_svideo</i>Inicio</a>
        </li>
        @endrole
        @hasanyrole ('SuperAdmin|Administrador')
        <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/inicio')}}"><i class="material-icons">settings_input_svideo</i>Inicio</a>
        </li>
        <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">add_circle_outline</i>Agregar<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="{{url('/nuevaNota')}}">Nota</a></li>
                    <li><a href="{{url('/notasCSV')}}">Nota Desde CSV</a></li>
                    <li><a href="{{url('/notasAPI')}}">Notas Desde FEEDER</a></li>
                    <li><a href="{{url('/nuevaNotaInformativa')}}">Nota Informativa</a></li>
                    <li><a href="{{url('/nuevasOchoColumnas')}}">Ocho Columnas</a></li>
                    <li><a href="{{url('/nuvoBoletin')}}">Boletín</a></li>
                    <li><a href="{{url('/nuevaInfoAmbiental')}}">Información Ambiental</a></li>
                    <li><a href="{{url('/infoAmbientalCSV')}}">Información Ambiental <br> Desde CSV</a></li>
                    @role ('Administrador')
                    <li><a href="{{url('/cargaPresentacion')}}">Carga Presentación</a></li>
                    @endrole                    
                </ul>
            </div>
        </li>
        @endrole                    
        <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">visibility</i>Ver<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    @hasanyrole ('SuperAdmin|Administrador')
                    <li><a href="{{url('/notas')}}">Notas</a></li>
                    <li><a href="{{url('/notasInformativas')}}">Notas Informativas</a></li>
                    <li><a href="{{url('/ochoColumnas')}}">Ocho Columnas</a></li>
                    <li><a href="{{url('/boletines')}}">Boletín</a></li>
                    <li><a href="{{url('/ambiental')}}">Información Ambiental</a></li> 
                    @endrole 
                    <li><a href="{{'/incidencias'}}">Incidencias</a></li>
                    @role ('SuperAdmin')
                    {{-- <li><a href="{{'/incidencias'}}">Incidencias</a></li> --}}
                    <li><a href="{{'/respuestas'}}">Respuestas</a></li>  
                    <li><a href="{{'/temasRelevantes'}}">Temas Relevantes</a></li>
                    @endrole
                    @role ('Administrador')
                    <li><a href="{{url('/presentaciones')}}">Presentaciones</a></li>
                    @endrole            
                </ul>
            </div>
        </li>
        {{-- @hasanyrole ('SuperAdmin|Administrador') --}}
        <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">check_circle_outline</i>Validar<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    @hasanyrole ('SuperAdmin|Administrador')
                    <li><a {{-- id="validaPdf" class="miAside" --}} href="{{url('/validaPdf')}}" target="_blank">Síntesis Informativa</a></li> 
                    @endrole
                    @role ('SuperAdmin')                   
                    <li><a href="{{url('/validaPdfIncidencias')}}" target="_blank">Reporte de Incidencias</a></li>
                    <li><a href="{{url('/validaPdfRespuestas')}}" target="_blank">Respuestas a <br> Solicudes de <br> Información</a></li>
                    @endrole
                    @role ('Colaborador')
                    <li><a href="{{url('/validaPdfIncidencias')}}" target="_blank">Reporte de Incidencias</a></li>
                    @endrole
                </ul>
            </div>
        </li>
        {{-- @endrole --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey" href="{{url('/validaPdf')}}" target="_blanck"><i class="material-icons">check_circle_outline</i>Valida PDF</a>            
        </li> --}}
        {{-- <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/notaExtemporanea')}}" style="color: red"><i class="material-icons" style="color: red">query_builder</i>Notas Extemporaneas</a>
        </li> --}}
        <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/sintesisInformativa/inicio')}}" target="_blank"><i class="material-icons" >article</i>Ir a la Síntesis</a>
        </li>       
        <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/incidencias/inicio')}}" target="_blank"><i class="material-icons" >article</i>Ir a las Incidencias</a>
        </li> 
        @role ('SuperAdmin')        
        {{-- <li class="no-padding">
            <a class="waves-effect waves-grey" href="{{url('/incidencias/inicio')}}" target="_blank"><i class="material-icons" >article</i>Ir a las Incidencias</a>
        </li> --}}
        <li class="no-padding">
            <a class="waves-effect waves-grey" id="sendSintesis" href="#" style="color: green"><i class="material-icons" style="color: green">forward_to_inbox</i>Enviar Síntesis <br> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mantutina</a>
        </li>
        <li class="no-padding">
            <a class="waves-effect waves-grey" id="sendSintesisVesp" href="#" style="color: rgb(255, 123, 0)"><i class="material-icons" style="color:rgb(255, 123, 0)">forward_to_inbox</i>Enviar Síntesis <br> 
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vespertina</a>
        </li>
        {{-- <li class="no-padding">
            <a class="waves-effect waves-grey" id="sendRevista" href="#" style="color: blue"><i class="material-icons" style="color: blue">forward_to_inbox</i>Enviar Revista</a>
        </li> --}}
        @endrole
        
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">star_border</i>Plugins<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="miscellaneous-sweetalert.html">Sweet Alert</a>
                    <li><a href="miscellaneous-code-editor.html">Code Editor</a>
                    <li><a href="miscellaneous-nestable.html">Nestable List</a>
                    <li><a href="miscellaneous-masonry.html">Masonry</a>
                    <li><a href="miscellaneous-idle-timer.html">Idle Timer</a>
                </ul>
            </div>
        </li> --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">desktop_windows</i>Layouts<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="layout-blank.html">Blank Page</a></li>
                    <li><a href="layout-boxed.html">Boxed Layout</a></li>
                    <li><a href="layout-hidden-sidebar.html">Hidden Sidebar</a></li>
                    <li><a href="layout-right-sidebar.html">Right Sidebar</a></li>
                </ul>
            </div>
        </li> --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">mode_edit</i>Forms<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="form-elements.html">Form Elements</a></li>
                    <li><a href="form-wizard.html">Form Wizard</a></li>
                    <li><a href="form-upload.html">File Upload</a></li>
                    <li><a href="form-image-crop.html">Image Crop</a></li>
                    <li><a href="form-image-zoom.html">Image Zoom</a></li>
                    <li><a href="form-input-mask.html">Input Mask</a></li>
                    <li><a href="form-select2.html">Select2</a></li>
                </ul>
            </div>
        </li> --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">grid_on</i>Tables<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="table-static.html">Static Tables</a></li>
                    <li><a href="table-responsive.html">Responsive Tables</a></li>
                    <li><a href="table-comparison.html">Comparison Table</a></li>
                    <li><a href="table-data.html">Data Tables</a></li>
                </ul>
            </div>
        </li> --}}
        {{-- <li class="no-padding"><a class="waves-effect waves-grey" href="charts.html"><i class="material-icons">trending_up</i>Charts</a></li> --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">my_location</i>Maps<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="maps-google.html">Google Maps</a></li>
                    <li><a href="maps-vector.html">Vector Maps</a></li>
                </ul>
            </div>
        </li> --}}
        {{-- <li class="no-padding">
            <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">tag_faces</i>Extra Pages<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
            <div class="collapsible-body">
                <ul>
                    <li><a href="404.html">404 Page</a></li>
                    <li><a href="500.html">500 Page</a></li>
                    <li><a href="invoice.html">Invoice</a></li>
                    <li><a href="faq.html">FAQ</a></li>
                    <li><a href="sign-in.html">Sign In</a></li>
                    <li><a href="sign-up.html">Sign Up</a></li>
                    <li><a href="lock-screen.html">Lock Screen</a></li>
                    <li><a href="pattern-lock-screen.html">Pattern Lock Screen</a></li>
                    <li><a href="forgot.html">Forgot Password</a></li>
                    <li><a href="pricing-tables.html">Pricing Tables</a></li>
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="timeline.html">Timeline</a></li>
                    <li><a href="calendar.html">Calendar</a></li>
                    <li><a href="coming-soon.html">Coming Soon</a></li>
                </ul>
            </div>
        </li> --}}
        <li class="divider"></li>
        <li class="no-padding">
            {{-- <a class="waves-effect waves-grey" href="{{ route('logout') }}"><i class="material-icons">exit_to_app</i>Salir</a> --}}
            <a class="waves-effect waves-grey" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                <i class="material-icons">exit_to_app</i>Salir{{-- </a> --}}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>   
    </div>
</aside>