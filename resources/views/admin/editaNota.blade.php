@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    @if ($nota->idMedio == 2 || $nota->idMedio == 4)
        <div class="materialert info">
            <div class="material-icons">info_outline</div>
            <span>No es necesario agragar de nuevo la imagen de la nota, a no ser que desee cambiarla</span>
            <button type="button" class="close-alert">×</button>
        </div>
    @endif
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <form id="editNotasForm" action="">
                    <div>
                        {{-- <!--Datos Generales-->
                        <h3>General</h3>
                        <section>
                            <div class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">                                            
                                            <select id="selectMedio" name="medio" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option selected value="{{$nota->medioRel->idMedio}}">{{$nota->medioRel->nomMedio}}</option>
                                                @foreach ($medios as $medio)
                                                    <option value={{$medio->idMedio}}>{{$medio->nomMedio}}</option>
                                                @endforeach
                                            </select>
                                        </div>                                    
                                        <div class="input-field">                                            
                                            <select id="selectEdo" name="entidadFed" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option selected value="{{$nota->entidadRel->idEntidad}}">{{$nota->entidadRel->nomEntidad}}</option>                                                
                                                @foreach ($entidades as $entidad)
                                                    <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectSubProc" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option selected value="{{$nota->areaRel->idArea}}">{{$nota->areaRel->nomArea}}</option>
                                                @foreach ($areas as $area)
                                                    <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                                @endforeach
                                            </select>
                                        </div>                                        
                                    </div>
                                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->                               
                                </div>
                            </div>
                        </section>
                        <!----> --}}
                        <!--La nota en si-->                        
                        <section>
                            <div id="contNota">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">
                                            <input hidden id="idNota" class="validar" name="idNota" value="{{$nota->idNota}}" required>
                                            <label for="encabezado">Encabezado</label>
                                            <input id="encabezado" class="validar" name="encabezado" type="text" value="{{$nota->encabezado}}" required>
                                        </div>
                                        <div class="input-field">
                                            {{-- <label for="texto">Texto</label>
                                            <textarea id="texto" name="texto" class="materialize-textarea validar" maxlength="1000" length="1000" required></textarea> --}}
                                            {{-- <div class="col"> --}}
                                                <label for="texto">Texto</label>                                    
                                                <br>
                                                <br>
                                                {{-- <textarea class="miInput miTxtArea" id="msjEnc" name="msjEnc" rows="5" cols="50" required></textarea> --}}
                                                <div id="notaTexto" class="mi_editor-container">{!!$nota->textoHtml!!}</div> 
                                                <input hidden id="textoHtml" class="validar" name="textoHtml" value="{{$nota->textoHtml}}" required>
                                                <input hidden id="texto" class="validar" name="texto" value="{{$nota->texto}}" required>
                                            {{-- </div> --}}
                                        </div>
                                        <div class="input-field">
                                            <label for="fuente">Fuente</label>
                                            <input id="fuente" class="validar" name="fuente" type="text" value="{{$nota->fuente}}" required>
                                        </div>                                       
                                        @if ($nota->idMedio == 2)
                                            <div id="contTv"> <!--2:Televisión-->
                                                <label for="imgTv">Imagen</label>                                            
                                                <div class="input-file">
                                                    <div id="contImgTv" class="span4 target"></div> 
                                                    <input hidden id="imgTv" type="text" name="imgTv">                                               
                                                </div>
                                                <br>
                                                <div class="input-field">                            
                                                    <label for="linkTv">Link de la nota</label>
                                                    <input id="linkTv" name="link" type="text" data-parsley-type="url" value="{{$nota->link}}" required>
                                                </div>
                                            </div> <!--Fin Televisión-->
                                        @elseif ($nota->idMedio == 3)
                                            <div id="contWeb"> <!--3:Web-->
                                                <div class="input-field">
                                                    <label for="linkWeb">Link de la nota</label>
                                                    <input id="linkWeb" name="link" type="text" data-parsley-type="url" value="{{$nota->link}}" required>
                                                </div>
                                            </div><!--Fin Web-->  
                                        @elseif($nota->idMedio == 4)
                                            <div id="contRedSoc"> <!--4:Red Social-->
                                                <div class="input-field">
                                                    <label for="linkRedSoc">Link de la nota</label>
                                                    <input id="linkRedSoc" name="link" type="text" data-parsley-type="url" value="{{$nota->link}}" required>
                                                </div>
                                                <label for="imgRedSoc">Imagen</label>
                                                {{-- <div class="input-field">                                                                            
                                                    <input id="imgRedSoc" name="imgRedSoc" class="dropify" type="file" multiple required>
                                                </div> --}}
                                                <div class="input-file">
                                                    <div id="contImgRedSoc" class="span4 target"></div> 
                                                    <input hidden id="imgRedSoc" type="text" name="imgRedSoc">                                               
                                                </div>                                                
                                            </div><!--Fin Red Social-->
                                        @elseif ($nota->idMedio == 1)
                                            <div id="contImp"> <!--1:Impresos-->
                                                <div class="input-field">
                                                    <label for="linkImp">Link de la nota</label>
                                                    <input id="linkImp" name="link" type="text" data-parsley-type="url" value="{{$nota->link}}" required>
                                                </div>
                                            </div><!--Fin Impresos-->                                          
                                        @endif                                                                                                                        
                                    </div>                                    
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                    </div>
                    <div class="row right-align">
                        <a id="btnModifNotas" class="waves-effect waves-light btn indigo miA">Guardar Cambios</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
    <script>
        var quillNota = new Quill('#notaTexto', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quillNota.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quillNota.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtml').val(html);
                var contenido = $('#textoHtml').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#texto').val(texto);  
            }
        updateHtmlOutput_msj();
        // jquery.paste_image_reader.js
        (function($) {
        	var defaults;
        	$.event.fix = (function(originalFix) {
        		return function(event) {
        			event = originalFix.apply(this, arguments);
        			if (event.type.indexOf("copy") === 0 || event.type.indexOf("paste") === 0) {
        				event.clipboardData = event.originalEvent.clipboardData;
        			}
        			return event;
        		};
        	})($.event.fix);
        	defaults = {
        		callback: $.noop,
        		matchType: /image.*/
        	};
        	return ($.fn.pasteImageReader = function(options) {
        		if (typeof options === "function") {
        			options = {
        				callback: options
        			};
        		}
        		options = $.extend({}, defaults, options);
        		return this.each(function() {
        			var $this, element;
        			element = this;
        			$this = $(this);
        			return $this.bind("paste", function(event) {
        				var clipboardData, found;
        				found = false;
        				clipboardData = event.clipboardData;
        				return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
        					var file, reader;
        					if (found) {
        						return;
        					}
        					if (
        						type.match(options.matchType) ||
        						clipboardData.items[i].type.match(options.matchType)
        					) {
        						file = clipboardData.items[i].getAsFile();
        						reader = new FileReader();
        						reader.onload = function(evt) {
        							return options.callback.call(element, {
        								dataURL: evt.target.result,
        								event: evt,
        								file: file,
        								name: file.name
        							});
        						};
        						reader.readAsDataURL(file);                                
        						snapshoot();
        						return (found = true);
        					}
        				});
        			});
        		});
        	});
        })(jQuery);
        var dataURL, filename;
        $("html").pasteImageReader(function(results) {            
        	filename = results.filename, dataURL = results.dataURL;
            var imgText = dataURL.replace(/^data\:image\/\w+\;base64\,/, '');
            $('#imgTv').val(imgText);
            $('#imgRedSoc').val(imgText);
        	$data.text(dataURL);
        	$size.val(results.file.size);
        	$type.val(results.file.type);
        	var img = document.createElement("img");
        	img.src = dataURL;
        	var w = img.width;
        	var h = img.height;
        	$width.val(w);
        	$height.val(h);
        	return $(".activeImg")
        		.css({
        			backgroundImage: "url(" + dataURL + ")"
        		})
        		.data({ width: w, height: h });
        });
        
        var $data, $size, $type, $width, $height;
        $(function() {
        	$data = $(".data");
        	$size = $(".size");
        	$type = $(".type");
        	$width = $("#width");
        	$height = $("#height");
        	$(".target").on("click", function() {
        		var $this = $(this);
        		var bi = $this.css("background-image");
        		if (bi != "none") {
        			$data.text(bi.substr(4, bi.length - 6));
        		}
            
        		$(".activeImg").removeClass("activeImg");
        		$this.addClass("activeImg");
            
        		$this.toggleClass("contain");
            
        		$width.val($this.data("width"));
        		$height.val($this.data("height"));
        		if ($this.hasClass("contain")) {
        			$this.css({
        				width: $this.data("width"),
        				height: $this.data("height"),
        				"z-index": "10"
        			});
        		} else {
        			$this.css({ width: "", height: "", "z-index": "" });
        		}
        	});
        });
    </script>   
@endsection