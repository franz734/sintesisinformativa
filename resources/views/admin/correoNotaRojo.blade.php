@php    
    $cad = ($nota->idArea == 4) ? '.' : ', solicitando de la manera más atenta se comparta una nota informativa al respecto.';
@endphp
<p>Estimada/o:</p>
<p>Se hace de su conocimiento la siguiente información{{$cad}}</p>
<p>Sin más por el momento, le enviamos cordiales saludos.</p>
<br>
<p>{{$nota->entidadRel->nomEntidad}}</p>
<p>{{$nota->areaRel->nomArea}}</p>
@if ($nota->idMedio == 1)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    {{-- <p><b>{{$nota->fuente}}</b></p> --}}
    <a href="{{$nota->link}}" target="_blank">VER NOTA</a>
@elseif($nota->idMedio == 2)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">VER NOTA</a>
@elseif($nota->idMedio == 3)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">CONTINUAR LEYENDO</a>
@elseif($nota->idMedio == 4)
    <a href="{{$nota->link}}" title="{{$nota->link}}" target="_blank">                        
        <img class="notaCont" style="margin-right: 10px" src="{{URL::asset($nota->imgNota)}}" width="310" height="400">
    </a>
@endif