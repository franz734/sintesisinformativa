@extends('admin.master')
@section('css')
    <link href="{{ URL::asset('assets/plugins/dropify/css/dropify.css') }}" rel="stylesheet" />
@endsection
@section('contenido')
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <form id="editAmbientalCSVForm" action="">
                    <div>
                        <section>
                            <div id="contNota">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">
                                            <input hidden id="idInfoAmbiental" class="validar" name="idInfoAmbiental"
                                                value="{{ $ambiental->idInfoAmbiental }}" required>
                                            <label for="encabezado">Encabezado</label>
                                            <input id="encabezado" class="validar" name="encabezado" type="text"
                                                value="{{ $ambiental->encabezado }}" required>
                                        </div>
                                        <div class="input-field">
                                            <label for="texto">Texto</label>
                                            <textarea id="texto" name="texto" class="materialize-textarea validar"
                                                required>{{ $ambiental->texto }}</textarea>
                                        </div>
                                        <div class="input-field">
                                            <select id="selectRef" name="referencia"
                                                class="js-states browser-default validar" tabindex="-1" style="width: 100%"
                                                required>
                                                <option disabled selected value="def">Referencia</option>
                                                @foreach ($referencias as $referencia)
                                                    <option value={{ $referencia->idReferencia }}>
                                                        {{ $referencia->nomReferencia }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                    </div>
                    <div class="row right-align">
                        <a id="btnModifAmbientalCSV" class="waves-effect waves-light btn indigo miA">Guardar Cambios</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('assets/js/customNotasCSV.js') }}?{{ rand() }}"></script>    
@endsection
