<p>
    Se hace de su conocimiento una incidencia registrada por el estado de
    <b>{{ $incidencia->entidadRel->nomEntidad }}</b>
 </p>
 <h4>Tema</h4>
 <p>{{$incidencia->tema}}</p>
 <h4>Resumen</h4>
 <p>{{$incidencia->resumen}}</p>
 <h4>Antecedentes y/o Problematica</h4>
 <p>{{$incidencia->problematica}}</p>
 <h4>Actuación de la Delegación PROFEPA</h4>
 <p>{{$incidencia->actuacion}}</p>
 <h4>Acciones a Realizar</h4>
 <p>{{$incidencia->acciones}}</p>
