@php
//dd($respuestas);
if ($respuestas[0]->entidadRel == null) {
    $cad = 'Oficina Central';
} else {
    $cad = $respuestas[0]->entidadRel->nomEntidad;
}
@endphp
@extends('admin.master')
@section('css')
    <link href="{{ URL::asset('assets/plugins/datepicker/datepicker.css') }}" rel="stylesheet" />
@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5 style="color: #bc955c">{{ $cad }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $respuestas[0]->temaRelevanteRel->temaRel }}</blockquote>
                        <div class="right-align" style="padding-right: 10px">
                            <a class="waves-effect waves-light btn" style="background-color: #9f2241"
                                title="Solicitar más información"
                                onclick="masInfoTema({{ $respuestas[0]->temaRelevanteRel->idTemaRel }},'temaRel')">
                                <i class="material-icons">info</i>
                            </a>
                        </div>
                        <br>
                    </div>
                </div>
                @foreach ($respuestas as $respuesta)
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Respuesta</b></h5>
                            <input hidden id="respuestaTemaRel{{ $respuesta->idRespuestaTemaRel }}" type="text"
                                value="{{ $respuesta->idRespuestaTemaRel }}">
                            <p>{{ $respuesta->fecha }}</p>
                            <blockquote>{{ $respuesta->respuesta }}</blockquote>
                            <br>
                            <div class="right-align" style="padding-right: 10px">
                                <a class="waves-effect waves-light btn" style="background-color: #235b4e"
                                    title="Enviar por Correo"
                                    onclick="enviaRespuesta({{ $respuesta->idRespuestaTemaRel }})">
                                    <i class="material-icons">email</i>
                                </a>
                            </div>
                            <br>                            
                        </div>
                    </div>
                @endforeach
                {{-- <ul class="collapsible" data-collapsible="accordion">
                    @foreach ($respuestas as $respuesta)
                        <li>
                            <div class="collapsible-header">                                
                                <h5 style="color: #10312b"><u><b>Respuesta</b></u></h5>
                                <p>{{ $respuesta->fecha }}</p>
                            </div>
                            <div class="collapsible-body">
                                <div class="card notaCont">
                                    <div class="card-coontent cont">   
                                        <br>                      
                                        <h5><b>Área</b></h5>       
                                        <blockquote>{{$respuesta->areaRel->nomArea.'/'.$respuesta->subAreaRel->nomSubArea}}</blockquote>
                                        <br>
                                    </div>
                                </div>
                                <div class="card notaCont">
                                    <div class="card-coontent cont">   
                                        <br>                      
                                        <h5><b>Respuesta</b></h5>       
                                        <blockquote>{{$respuesta->respuesta}}</blockquote>
                                        <div class="right-align" style="padding-right: 10px">
                                            <a class="waves-effect waves-light btn" style="background-color: #235b4e"
                                                title="Enviar por Correo"
                                                onclick="enviaRespuesta({{ $respuesta->idRespuestaTemaRel }})">
                                                <i class="material-icons">email</i>
                                            </a>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="card notaCont">
                                    <div class="card-coontent cont">   
                                        <br>                      
                                        <h5><b>Actuación de la Delegación PROFEPA</b></h5>       
                                        <blockquote>{!!$respuesta->actuacionHtml!!}</blockquote>
                                        <br>
                                    </div>
                                </div>
                                <div class="card notaCont">
                                    <div class="card-coontent cont">   
                                        <br>                      
                                        <h5><b>Acciones a Realizar</b></h5>       
                                        <blockquote>{!!$respuesta->acciones!!}</blockquote>
                                        <br>
                                    </div>
                                </div>
                                @if (!is_null($respuesta->evidencia))
                                    <div class="card notaCont">
                                        <div class="card-coontent cont">   
                                            <br>                      
                                            <h5><b>Multimedia</b></h5>     
                                            @php
                                                $ctl = 0;
                                                $respuesta->evidencia = json_decode($respuesta->evidencia);
                                            @endphp  
                                            @foreach ($respuesta->evidencia as $img)
                                                @php
                                                    $img = str_replace('Queru00e9taro','Querétaro',$img);
                                                    $img = str_replace('Michoacu00e1n','Michoacán',$img);
                                                    $img = str_replace('NuevoLeu00f3n','NuevoLeón',$img);
                                                    $img = str_replace('Mu00e9xico','México',$img);
                                                    $img = str_replace('Yucatu00e1n','Yucatán',$img);
                                                    if($ctl%2 == 0){
                                                        $inicio = '<div class="row center-align">';
                                                        $fin = '';
                                                    }
                                                    else{
                                                        $inicio = '';
                                                        $fin = '</div><br>';
                                                    }
                                                    $ctl++;
                                                @endphp
                                                {!!$inicio!!}                                                                                       
                                                    <a href="{{URL::asset($img)}}" target="_blank">                                     
                                                        <img class="imgEvi" src="{{URL::asset($img)}}" width="300" height="300"> 
                                                    </a>                                                                             
                                                {!!$fin!!}
                                            @endforeach
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </li>
                    @endforeach
                </ul> --}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('assets/plugins/datepicker/datepicker.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datepicker/datepicker.es-ES.js') }}"></script>

@endsection
