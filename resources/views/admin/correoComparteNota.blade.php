
<p>
    Se hace de su conocimiento la siguiente nota publicada en medios, del estado de
    <b>{{ $nota->entidadRel->nomEntidad }}</b>
 </p>
 @if ($nota->idMedio == 1)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <p><b>{{$nota->fuente}}</b></p>
@elseif($nota->idMedio == 2)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">VER NOTA</a>
@elseif($nota->idMedio == 3)
    <p><b>{{$nota->encabezado}}</b></p>
    <p>{{str_replace('&nbsp;',' ',$nota->texto)}}</p>
    <a href="{{$nota->link}}" target="_blank">CONTINUAR LEYENDO</a>
@elseif($nota->idMedio == 4)
    <a href="{{$nota->link}}" title="{{$nota->link}}" target="_blank">                        
        <img class="notaCont" style="margin-right: 10px" src="{{URL::asset($nota->imgNota)}}" width="310" height="400">
    </a>
@endif   
