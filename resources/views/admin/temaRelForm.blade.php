@extends('admin.master')
@section('css')
    
@endsection
@section('contenido')
    <h1>Agregar Nuevo Tema Relevante</h1>
    <div class="row no-m-t no-m-b">
        <!--Filtro de busqueda-->
        <div class="col s12 m4 l2"></div>
        <!--ofsset izquierdo-->
        <div class="col s12 m4 l8">
            <form id="temaRelForm" action="">
                <div class="card stats-card">
                    <div class="card-content">                        
                        <div class="row">
                            {{-- <div class="col s12 m12 l12"> --}}
                                <div class="input-field">
                                    <label for="temaRel">Tema</label>
                                    <input id="temaRel" name="temaRel" type="text" class="validar" required>
                                </div>
                                <div class="input-field">
                                    <select id="selectEdo" name="entidadFed" class="js-states browser-default validar" tabindex="-1"
                                        style="width: 100%" data-parsley-errors-container=".errorBlock" required>
                                        <option disabled selected value="def">Entidad Federativa</option>
                                        @foreach ($entidades as $entidad)
                                            <option value={{ $entidad->idEntidad }}>{{ $entidad->nomEntidad }}</option>
                                        @endforeach
                                            <option value='null'>Oficina Central</option>
                                    </select>
                                </div>
                                <div class="errorBlock"></div>
                            {{-- </div> --}}                            
                        </div>                                                                        
                        <div class="right-align">                            
                            <a id="guardaTemaRel" class="waves-effect waves-light btn"
                                style="background-color: #9f2241">Guardar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col s12 m4 l2"></div>
        <!--ofsset derecho-->
    </div>
@endsection
@section('scripts')
    
@endsection