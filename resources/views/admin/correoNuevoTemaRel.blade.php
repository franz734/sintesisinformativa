@php
    if($temaRel->entidadRel == null){
        $comp = ' correspondiente a ';
        $cad = 'Oficina Central';
    }
    else{
        $comp = ' correspondiente al estado de ';
        $cad = $temaRel->entidadRel->nomEntidad;
    }
@endphp
<p>Se ha creado un nuevo tema, <b>{{$temaRel->temaRel}}</b> {{$comp}} <b>{{$cad}}</b> </p>
