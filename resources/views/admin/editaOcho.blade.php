@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
{{-- <h3>Ocho Columnas</h3> --}}
@php
    if($columna->fuente == 'El Universal'){
        $cad = '1';
    }
    if($columna->fuente == 'Reforma'){
        $cad = '2';
    }
    if($columna->fuente == 'Milenio'){
        $cad = '3';
    }
    if($columna->fuente == 'La Jornada'){
        $cad = '4';
    }
    if($columna->fuente == 'Excelsior'){
        $cad = '5';
    }
@endphp
<div class="col s12 m12 l12">
    <div class="card">
        <div class="card-content">
            <form id="editColumnasForm" action="">
                <div class="row">
                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                    <div class="col s12 m4 l8">                       
                        <div class="input-field"> 
                            <input hidden id="idOchoColumna" class="validar" name="idOchoColumna" value="{{$columna->idOchoColumna}}" required>                                           
                            <select id="selectFuente" name="fuente" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                <option selected value="{{$cad}}">{{$columna->fuente}}</option>
                                <option value="1">El Universal</option>
                                <option value="2">Reforma</option>
                                <option value="3">Milenio</option>
                                <option value="4">La Jornada</option>
                                <option value="5">Excelsior</option>                                
                            </select>
                        </div>                                                                                                                           
                    </div>
                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->                               
                </div>
                <div class="row">
                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                    <div class="col s12 m4 l8">
                        <div class="input-field">
                            <label for="encabezado">Encabezado</label>
                            <input id="encabezado" class="validar" name="encabezado" type="text" value="{{$columna->encabezado}}" required>
                        </div>
                        <div class="input-field">                            
                                <label for="texto">Texto</label>                                    
                                <br>
                                <br>                                
                                <div id="columnaTexto" class="mi_editor-container">{!!$columna->textoHtml!!}</div> 
                                <input hidden id="textoHtml" class="validar" name="textoHtml" required>
                                <input hidden id="texto" class="validar" name="texto" required>                            
                        </div>
                       {{--  <div class="input-field">
                            <label for="fechaAlta">Fecha de Alta</label>
                            <input id="fechaAlta" name="fechaAlta" type="text" class="datepicker" disabled value="{{date('d/m/Y')}}">
                        </div> --}}
                    </div>
                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->
                </div>
                {{-- <div class="row right-align">
                    <a id="btnAgColumna" class="waves-effect waves-light btn  miA">Agregar Columna</a>
                </div>
                <br> --}}                
                <div class="row right-align">
                    <a id="btnModifColumnas" class="waves-effect waves-light btn indigo miA">Guardar Cambios</a>
                </div>
            </form>
        </div>
    </div>
</div>   
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
    <script>
        // Initialize QUill editor
        var quill = new Quill('#columnaTexto', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quill.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quill.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtml').val(html);
                var contenido = $('#textoHtml').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#texto').val(texto);  
            }
        updateHtmlOutput_msj();
    </script>
@endsection