@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    {{-- <h3>Obtener Notas Desde FEEDER</h3>
    <div class="row no-m-t no-m-b">
        <div class="col s12 m12 l4">
            <div class="card">
                <div class="card-content">            
                    <a id="btnFeeder"  class="waves-effect waves-light btn" style="background-color: #9f2241"><i class="material-icons right">save_alt</i>Obtener Notas</a>           
                </div>
            </div>
        </div>
    </div> --}}
    <h3>Notas Desde FEEDER</h3>
    <style>
        td.details-control {
          background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
          cursor: pointer;
        }
          tr.shown td.details-control {
          background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }
    </style>
    <input hidden id="idNota" type="text">
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableNotasAPI" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Encabezado</th>
                            <th scope="col">Medio</th>                                                                           
                            <th scope="col">Link</th>                            
                            <th scope="col">Fecha de Publicación</th>
                            <th scope="col">Fecha de Alta</th>                        
                            <th scope="col">Acción</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    
@endsection
@section('scripts')
<script src="{{URL::asset('assets/js/customNotasAPI.js')}}?{{rand()}}"></script>
@endsection