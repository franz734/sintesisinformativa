<p>Estimadas:</p>
<p>Se envía la síntesis informativa {{$comp}} del {{$hoy}}.</p>
{{-- <a href="{{'https://10.64.2.42/sintesisInformativa/inicio'}}">Ver Síntesis Red PROFEPA</a>
<br>
<a href="{{'https://189.254.22.36/sintesisInformativa/inicio'}}">Ver Síntesis Red Externa</a> --}}
<p>Al mismo tiempo, se les extiende una cordial invitación para que sigan a la PROFEPA en redes sociales:</p>
<ul>
    <li><a href="https://twitter.com/PROFEPA_Mx" target="_blank">twitter</a></li>
    <li><a href="https://www.facebook.com/ProfepaOficial" target="_blank">facebook</a></li>
    <li><a href="https://www.instagram.com/profepa_mx" target="_blank">instagram</a></li>
</ul>
<p>Si alguna de sus colaboradoras requiere la síntesis informativa, favor de hacerlo saber por este medio.</p>
<p>Saludos cordiales</p>