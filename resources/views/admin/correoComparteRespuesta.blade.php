<p>
    Se hace de su conocimiento la respuesta dada a la solicitud de información de la nota publicada en medios: <b>{{ $respuesta->notaRel->encabezado }}</b> del estado de
    <b>{{ $respuesta->entidadRel->nomEntidad }}</b>
 </p>
 <h4>Resumen</h4>
 <p>{{$respuesta->resumen}}</p>
 <h4>Antecedentes y/o Problematica</h4>
 <p>{{$respuesta->notaRel->texto}}</p>
 <h4>Actuación de la Delegación PROFEPA</h4>
 <p>{{$respuesta->actuacion}}</p>
 <h4>Acciones a Realizar</h4>
 <p>{{$respuesta->acciones}}</p>
