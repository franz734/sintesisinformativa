@extends('admin.master')
@section('css')
    
@endsection
@section('contenido')
    <style>
        td.details-control {
          background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
          cursor: pointer;
        }
          tr.shown td.details-control {
          background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }
    </style>
    <h1 style="color: #bc955c">{{$entidad->nomEntidad}}</h1>
    <input hidden id="idEntidad" type="text" value="{{$entidad->idEntidad}}">
    <input hidden id="idNota" type="text">
    <div class="row no-m-t no-m-b"> <!--Filtro de busqueda-->
        <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
        <div class="col s12 m4 l8">
            <form id="filterForm" action="">
                <div class="card stats-card">
                    <div class="card-content">                    
                        <div class="input-field">                                            
                            <select id="selectSubProc" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                <option disabled selected value="def">SubProcuraduría</option>                                
                                @foreach ($areas as $area)
                                    <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>                                        
                </div>
            </form>
        </div>
        <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
    </div> <!--Fin del filtro-->
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableNotasEdo" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Encabezado</th>
                            <th scope="col">Medio</th>
                            <th scope="col">Entidad</th>
                            <th scope="col">SubProcuraduría</th>
                            <th scope="col">Fecha de Publicación</th> 
                            <th scope="col">Link</th> 
                            <th scope="col">Tipo</th>
                            <th scope="col">Más</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Structure -->
  <div id="modalSelect" class="modal">
    <div class="modal-content">
        <div class="input-field">                                            
            <select id="selectTipo" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                <option disabled selected value="def">Tipo</option>                                
                @foreach ($tipo as $tipo)
                    <option value={{$tipo->idTipo}}>{{$tipo->nomTipo}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" id="btnAsigTipo" class=" waves-effect waves-green btn-flat">Asignar</a>
    </div>
  </div>
@endsection
@section('scripts')
    
@endsection