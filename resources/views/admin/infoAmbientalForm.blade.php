@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Información Ambiental</h3>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <form id="ambientalForm" action="">
                    <div class="row">
                        <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                        <div class="col s12 m4 l8">
                            {{-- <div class="input-field">                                            
                                <select id="selectMedio" name="medio" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Medio de publicación</option>                                    
                                    @foreach ($medios as $medio)
                                        <option value={{$medio->idMedio}}>{{$medio->nomMedio}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            {{-- <div class="input-field">                                            
                                <select id="selectEdo" name="entidadFed" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Entidad Federativa</option>                                                
                                    @foreach ($entidades as $entidad)
                                        <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            {{-- <div class="input-field">                                            
                                <select id="selectSubProc" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">SubProcuraduría</option>                                    
                                    @foreach ($areas as $area)
                                        <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            <div class="input-field">
                                <label for="encabezado">Encabezado</label>
                                <input id="encabezado" class="validar" name="encabezado" type="text" required>
                            </div>
                            <div class="input-field">                            
                                    <label for="texto">Texto</label>                                    
                                    <br>
                                    <br>                                
                                    <div id="infoAmbientalTexto" class="mi_editor-container"></div> 
                                    <input hidden id="textoHtml" class="validar" name="textoHtml" required>
                                    <input hidden id="texto" class="validar" name="texto" required>                            
                            </div>
                            <div class="input-field">
                                <select id="selectRef" name="referencia" class="js-states browser-default"
                                    tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Referencia</option>
                                    @foreach ($referencias as $referencia)
                                        <option value={{ $referencia->idReferencia }}>
                                            {{ $referencia->nomReferencia }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field">
                                <label for="link">Link</label>
                                <input id="link" name="link" data-parsley-type="url" type="text">
                            </div>
                            <div class="input-field">
                                <label for="fechaAlta">Fecha de Alta</label>
                                <input id="fechaAlta" name="fechaAlta" type="text" class="datepicker" disabled value="{{date('d/m/Y')}}">
                            </div>                                                                                                                                                      
                        </div>
                        <div class="col s12 m4 l2"></div><!--ofsset derecho-->
                    </div>
                    <div class="row right-align">
                        <a id="guardaAmbiental" class="waves-effect waves-light btn indigo miA">Guardar Información Ambiental</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
    <script>
        // Initialize QUill editor
        var quill = new Quill('#infoAmbientalTexto', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 

            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quill.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quill.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtml').val(html);
                var contenido = $('#textoHtml').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#texto').val(texto);  
            }
        updateHtmlOutput_msj();
    </script>
@endsection