@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
<div class="materialert warning">
    <div class="material-icons">warning</div>
    <span>Esta sección es para agregar notas pasadas. No apareceran en la síntesis del día de mañana</span>
    <button type="button" class="close-alert">×</button>
</div>
    <h3>Notas Extemporaneas</h3>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <form id="notasForm" action="">
                    <div>
                        <!--Datos Generales-->
                        <h3>General</h3>
                        <section>
                            <div class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <input hidden type="text" id="extemp" name="extemp" value="1">
                                        <div class="input-field">                                            
                                            <select id="selectMedio" name="medio" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Medio de publicación</option>
                                                {{-- <option value="1">Impreso</option>
                                                <option value="2">Televisión</option>
                                                <option value="3">Web</option>
                                                <option value="4">Redes Sociales</option> --}}
                                                @foreach ($medios as $medio)
                                                    <option value={{$medio->idMedio}}>{{$medio->nomMedio}}</option>
                                                @endforeach
                                            </select>
                                        </div>                                    
                                        <div class="input-field">                                            
                                            <select id="selectEdo" name="entidadFed" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Entidad Federativa</option>                                                
                                                @foreach ($entidades as $entidad)
                                                    <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectSubProc" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">SubProcuraduría</option>
                                                {{-- <option value="1">Recursos Naturales</option>
                                                <option value="2">Auditoria Ambiental</option>
                                                <option value="3">Inspeccion Industrial</option> --}}
                                                @foreach ($areas as $area)
                                                    <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                                @endforeach
                                            </select>
                                        </div>                                        
                                    </div>
                                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->                               
                                </div>
                            </div>
                        </section>
                        <!---->
                        <!--La nota en si-->
                        <h3>Nota</h3>
                        <section>
                            <div id="contNota" class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">
                                            <label for="encabezado">Encabezado</label>
                                            <input id="encabezado" class="validar" name="encabezado" type="text" required>
                                        </div>
                                        <div class="input-field">
                                            {{-- <label for="texto">Texto</label>
                                            <textarea id="texto" name="texto" class="materialize-textarea validar" maxlength="1000" length="1000" required></textarea> --}}
                                            {{-- <div class=""> --}}
                                                <label for="texto">Texto</label>                                    
                                                <br>
                                                <br>
                                                {{-- <textarea class="miInput miTxtArea" id="msjEnc" name="msjEnc" rows="5" cols="50" required></textarea> --}}
                                                <div id="notaTexto" class="mi_editor-container"></div> 
                                                <input hidden id="textoHtml" class="validar" name="textoHtml" required>
                                                <input hidden id="texto" class="validar" name="texto" required>
                                            {{-- </div> --}}
                                        </div>
                                        <div class="input-field">
                                            <label for="fuente">Fuente</label>
                                            <input id="fuente" class="validar" name="fuente" type="text" required>
                                        </div>
                                        <div class="input-field">
                                            <label for="fechaPub">Fecha de publicación</label>
                                            <input id="fechaPub"  name="fechaPub" type="text" class="datepicker validar" required>
                                        </div>
                                        <div class="input-field">
                                            <label for="fechaAlta">Fecha de Alta</label>
                                            <input id="fechaAlta" name="fechaAlta" type="text" class="datepicker" disabled value="{{date('d/m/Y')}}">
                                        </div>
                                        <div id="contTv" style="display:none"> <!--2:Televisión-->
                                            <label for="imgTv">Imagen</label>
                                            {{-- <div class="input-field">                                                                            
                                                <input id="imgTv" name="imgTv" class="dropify" type="file" multiple required>
                                            </div> --}}
                                            <div class="input-file">
                                                <div id="contImgTv" class="span4 target"></div> 
                                                <input hidden id="imgTv" type="text" name="imgTv" >                                               
                                            </div>
                                            <div class="input-field">                            
                                                <label for="linkTv">Link de la nota</label>
                                                <input id="linkTv" {{-- name="linkTv" --}} type="text" data-parsley-type="url" required>
                                            </div>
                                        </div> <!--Fin Televisión-->
                                        <div id="contWeb" style="display:none"> <!--3:Web-->
                                            <div class="input-field">
                                                <label for="linkWeb">Link de la nota</label>
                                                <input id="linkWeb" {{-- name="linkWeb" --}} type="text" data-parsley-type="url" required>
                                            </div>
                                        </div><!--Fin Web-->
                                        <div id="contRedSoc" style="display:none"> <!--4:Red Social-->
                                            <div class="input-field">
                                                <label for="linkRedSoc">Link de la nota</label>
                                                <input id="linkRedSoc" {{-- name="linkWeb" --}} type="text" data-parsley-type="url" required>
                                            </div>
                                            <label for="imgRedSoc">Imagen</label>
                                            {{-- <div class="input-field">                                                                            
                                                <input id="imgRedSoc" name="imgRedSoc" class="dropify" type="file" multiple required>
                                            </div> --}}
                                            <div class="input-file">
                                                <div id="contImgRedSoc" class="span4 target"></div> 
                                                <input hidden id="imgRedSoc" type="text" name="imgRedSoc">                                               
                                            </div>
                                            <div class="input-field">
                                                <div class="switch m-b-md">
                                                    <label>
                                                        No verificada
                                                        <input id="ctaVerif" type="checkbox" >
                                                        <span class="lever"></span>
                                                        Verificada
                                                    </label>
                                                </div>
                                                <br>
                                                <input hidden id="ctaVer" {{-- name="ctaVer" --}} type="text" value="0">
                                            </div>
                                        </div><!--Fin Red Social-->
                                    </div>                                    
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                        <!--Finalizar-->
                        <h3>Terminar</h3>
                        <section>
                            <div class="wizard-content">
                                Verifiqué que todo sea correcto y presione finalizar.
                            </div>
                        </section>
                        <!---->
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
<script>
    // Formulario nueva nota //
    var form = $("#notasForm");
    /* var validator = $("#notasForm").validate({
        errorPlacement: function errorPlacement(error, element) { element.after(error); },
        rules: {
            confirm: {
                equalTo: "#password"
            }
        }
    }); */
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        //transitionEffect: "fade",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if(currentIndex < newIndex){
                // Step 1 form validation
                if(currentIndex === 0){
                    var medio = $('#selectMedio').parsley();
                    var estado = $('#selectEdo').parsley();
                    var subProc = $('#selectSubProc').parsley();
                    //return true;
                    if(medio.isValid() && estado.isValid() && subProc.isValid()){
                        return true;
                    }
                    else{
                        medio.validate();
                        estado.validate();
                        subProc.validate();
                    }
                }
                // Step 2 form validation
                if(currentIndex === 1){
                    if($('#textoHtml').val()==='<p><br></p>'){
                            $('#textoHtml').val('');
                        }                                                          
                    var campos = $('#contNota').find('.validar').parsley();
                    console.log(campos);
                    var ctl = 0;
                    for(const i in campos){
                        if(campos[i].isValid()){
                            ctl++
                        }
                        else{
                            campos[i].validate();
                        }
                        if(ctl === campos.length){                            
                            return true;
                        }
                    }
                }
            }
            else{
                return true;
            }
        },
        /* onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        }, */
        onFinished: function (event, currentIndex)
        {
            $('#fechaAlta').prop("disabled", false);
            var formdata = new FormData($("#notasForm")[0]);            
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
                url: "guardaNota",
                type: 'POST',
                dataType: 'json',
                data: formdata,                
                cache:false,
                processData: false,
                contentType: false,
            })
            .done(function(result) {
               console.log(result)               
                Swal.fire(
                  'Guardado',
                  'Los datos han sido registrados',
                  'success'
                )
                var delayInMilliseconds = 2000; //1 second
                setTimeout(function() {
                    //location.reload();
                    $("#notasForm").trigger('reset');
                    $('#selectMedio').val('def').trigger('change');
                    $('#selectEdo').val('def').trigger('change');
                    $('#selectSubProc').val('def').trigger('change');
                    $('#fechaAlta').prop("disabled", true);
                    quillNota.setContents([{ insert: '' }]);                    
                    $('.dropify-clear').click();
                    location.reload();
                    /* $( "#notasForm" ).steps('reset'); */
                    //var form = $("#notasForm").steps();
                }, delayInMilliseconds);
            })
            .fail(function(result) {
                console.log(result);
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: result.responseJSON,
                })                
            })
            .always(function() {
                console.log('complete');
            });
        }
    });
    
    $(".wizard .actions ul li a").addClass("waves-effect waves-blue btn-flat");
    $(".wizard .steps ul").addClass("tabs z-depth-1");
    $(".wizard .steps ul li").addClass("tab");
    $('ul.tabs').tabs();
    $('select').material_select();
    $('.select-wrapper.initialized').prev( "ul" ).remove();
    $('.select-wrapper.initialized').prev( "input" ).remove();
    $('.select-wrapper.initialized').prev( "span" ).remove();
    /* $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15 // Creates a dropdown of 15 years to control year
    }); */
    /// Datepicker //
    $(function(){
	    $('.datepicker').pickadate({
            format: 'dd/mm/yyyy',        
	    	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	    	monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
	    	weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	    	weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
	    	selectMonths: true,
	    	selectYears: 15, // Puedes cambiarlo para mostrar más o menos años
	    	today: 'Hoy',
	    	clear: 'Limpiar',
	    	close: 'Ok',
	    	labelMonthNext: 'Siguiente mes',
	    	labelMonthPrev: 'Mes anterior',
	    	labelMonthSelect: 'Selecciona un mes',
	    	labelYearSelect: 'Selecciona un año',
	    });
    });
    /// upload image file ///
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastra y suelta un archivo aquí o haz click',
            'replace': 'Arrastra y suelta o haz click para reemplazar',
            'remove':  'Eliminar',
            'error':   'Ooops, Algo salió mal.'
        }
    });
    // Initialize QUill editor
    var quillNota = new Quill('#notaTexto', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quillNota.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quillNota.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtml').val(html);
                var contenido = $('#textoHtml').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#texto').val(texto);  
            }
        updateHtmlOutput_msj();
        // jquery.paste_image_reader.js
        (function($) {
        	var defaults;
        	$.event.fix = (function(originalFix) {
        		return function(event) {
        			event = originalFix.apply(this, arguments);
        			if (event.type.indexOf("copy") === 0 || event.type.indexOf("paste") === 0) {
        				event.clipboardData = event.originalEvent.clipboardData;
        			}
        			return event;
        		};
        	})($.event.fix);
        	defaults = {
        		callback: $.noop,
        		matchType: /image.*/
        	};
        	return ($.fn.pasteImageReader = function(options) {
        		if (typeof options === "function") {
        			options = {
        				callback: options
        			};
        		}
        		options = $.extend({}, defaults, options);
        		return this.each(function() {
        			var $this, element;
        			element = this;
        			$this = $(this);
        			return $this.bind("paste", function(event) {
        				var clipboardData, found;
        				found = false;
        				clipboardData = event.clipboardData;
        				return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
        					var file, reader;
        					if (found) {
        						return;
        					}
        					if (
        						type.match(options.matchType) ||
        						clipboardData.items[i].type.match(options.matchType)
        					) {
        						file = clipboardData.items[i].getAsFile();
        						reader = new FileReader();
        						reader.onload = function(evt) {
        							return options.callback.call(element, {
        								dataURL: evt.target.result,
        								event: evt,
        								file: file,
        								name: file.name
        							});
        						};
        						reader.readAsDataURL(file);                                
        						snapshoot();
        						return (found = true);
        					}
        				});
        			});
        		});
        	});
        })(jQuery);
        
        var dataURL, filename;
        $("html").pasteImageReader(function(results) {            
        	filename = results.filename, dataURL = results.dataURL;
            var imgText = dataURL.replace(/^data\:image\/\w+\;base64\,/, '');
            $('#imgTv').val(imgText);
            $('#imgRedSoc').val(imgText);
        	$data.text(dataURL);
        	$size.val(results.file.size);
        	$type.val(results.file.type);
        	var img = document.createElement("img");
        	img.src = dataURL;
        	var w = img.width;
        	var h = img.height;
        	$width.val(w);
        	$height.val(h);
        	return $(".activeImg")
        		.css({
        			backgroundImage: "url(" + dataURL + ")"
        		})
        		.data({ width: w, height: h });
        });
        
        var $data, $size, $type, $width, $height;
        $(function() {
        	$data = $(".data");
        	$size = $(".size");
        	$type = $(".type");
        	$width = $("#width");
        	$height = $("#height");
        	$(".target").on("click", function() {
        		var $this = $(this);
        		var bi = $this.css("background-image");
        		if (bi != "none") {
        			$data.text(bi.substr(4, bi.length - 6));
        		}
            
        		$(".activeImg").removeClass("activeImg");
        		$this.addClass("activeImg");
            
        		$this.toggleClass("contain");
            
        		$width.val($this.data("width"));
        		$height.val($this.data("height"));
        		if ($this.hasClass("contain")) {
        			$this.css({
        				width: $this.data("width"),
        				height: $this.data("height"),
        				"z-index": "10"
        			});
        		} else {
        			$this.css({ width: "", height: "", "z-index": "" });
        		}
        	});
        });
</script> 
@endsection