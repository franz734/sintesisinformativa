@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
<div class="row no-m-t no-m-b">
    <div class="card">
        <div class="card-content">
            <form id="editNotasCSVForm" action="">
                <div>                                     
                    <section>
                        <div id="contNota">
                            <div class="row">
                                <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                <div class="col s12 m4 l8">
                                    <div class="input-field">
                                        <input hidden id="idNota" class="validar" name="idNota" value="{{$nota->idNota}}" required>
                                        <label for="encabezado">Encabezado</label>
                                        <input id="encabezado" class="validar" name="encabezado" type="text" value="{{$nota->encabezado}}" required>
                                    </div>
                                    <div class="input-field">
                                        <label for="texto">Texto</label>
                                        <textarea id="texto" name="texto" class="materialize-textarea validar"  required>{{$nota->texto}}</textarea>                                        
                                    </div>
                                    <div class="input-field">
                                        <div id="contSelLugar">
                                            <select id="selectLugar" name="lugar"
                                                class="js-states browser-default validar sel2" tabindex="-1"
                                                style="width: 100%" required>
                                                <option disabled selected value="def">Lugar Específico</option>
                                                <option value="otro">Otro Lugar</option>
                                                @foreach ($lugares as $lugar)
                                                    <option value={{ $lugar->idLugar }}>{{ $lugar->nomLugar }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="contOtroLugar"></div>
                                    </div>
                                    <div class="input-field">
                                        <div id="contSelSuceso">
                                            <select id="selectSuceso" name="suceso"
                                                class="js-states browser-default validar sel2" tabindex="-1"
                                                style="width: 100%" required>
                                                <option disabled selected value="def">Suceso</option>
                                                <option value="otro">Otro Suceso</option>
                                                @foreach ($sucesos as $suceso)
                                                    <option value={{ $suceso->idSuceso }}>{{ $suceso->nomSuceso }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="contOtroSuceso"></div>
                                    </div>
                                    <div class="input-field">
                                        <label for="fuente">Fuente</label>
                                        <input id="fuente" class="validar" name="fuente" type="text" value="{{$nota->fuente}}" required>
                                    </div>                                       
                                    <div class="input-field">                                            
                                        <select id="selectRef" name="referencia" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>
                                            <option disabled selected value="def">Referencia</option>
                                            @foreach ($referencias as $referencia)
                                                <option value={{$referencia->idReferencia}}>{{$referencia->nomReferencia}}</option>
                                            @endforeach                                                                                                
                                        </select>
                                    </div>
                                    <div class="input-field">                                            
                                        <select id="selectSubProc" name="subProc" class="js-states browser-default anid validar" tabindex="-1" style="width: 100%" required>
                                            <option disabled selected value="def">SubProcuraduría</option>                                                
                                            @foreach ($areas as $area)
                                                <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                            @endforeach
                                        </select>
                                    </div>                 
                                    <div class="input-field contSA">                                        
                                    </div> 
                                    <select id="selectMedioCSV" name="medio" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>
                                        <option disabled selected value="def">Medio de publicación</option>                                                
                                        @foreach ($medios as $medio)
                                            <option value={{$medio->idMedio}}>{{$medio->nomMedio}}</option>
                                        @endforeach
                                    </select>   
                                    <div id="contMedImp" style="display:none"><!--1:Medios Impresos-->
                                        {{-- <label for="evidencia">Anexo</label>
                                        <div class="input-field">                                                                                                            
                                            <input id="documento" name="documento" class="dropify" type="file" data-allowed-file-extensions="pdf">
                                        </div> --}}
                                        <div class="input-field">
                                            <label for="linkImp">Link de la nota</label>
                                            <input id="linkImp" {{-- name="linkTv" --}} type="text" data-parsley-type="url"
                                                required>
                                        </div>
                                    </div><!--Medios Impresos-->    
                                    <div id="contTv" style="display:none"> <!--2:Televisión-->
                                        <label for="imgTv">Imagen</label>                                        
                                        <div class="input-file">
                                            <div id="contImgTv" class="span4 target"></div> 
                                            <input hidden id="imgTv" type="text" name="imgTv" required>                                               
                                        </div>
                                        {{-- <div class="input-field">                            
                                            <label for="linkTv">Link de la nota</label>
                                            <input id="linkTv"  type="text" data-parsley-type="url" required>
                                        </div> --}}
                                    </div> <!--Fin Televisión-->  
                                    <div id="contRedSoc" style="display:none"> <!--4:Red Social-->
                                        {{-- <div class="input-field">
                                            <label for="linkRedSoc">Link de la nota</label>
                                            <input id="linkRedSoc"  type="text" data-parsley-type="url" required>
                                        </div> --}}
                                        <label for="imgRedSoc">Imagen</label>                                        
                                        <div class="input-file">
                                            <div id="contImgRedSoc" class="span4 target"></div> 
                                            <input hidden id="imgRedSoc" type="text" name="imgRedSoc" required>                                               
                                        </div>
                                        <div class="input-field">
                                            <div class="switch m-b-md">
                                                <label>
                                                    No verificada
                                                    <input id="ctaVerif" type="checkbox" >
                                                    <span class="lever"></span>
                                                    Verificada
                                                </label>
                                            </div>
                                            <br>
                                            <input hidden id="ctaVer" {{-- name="ctaVer" --}} type="text" value="0">
                                        </div>
                                    </div><!--Fin Red Social-->   
                                    <div>
                                        <br>
                                        <br>
                                        <p>Categoria de la nota:</p>
                                        <br>
                                        <div class="contSemf">
                                            <p class="p-v-xs option option-1">
                                                <input class="validar miRadio" name="categoria" type="radio" value="verde"
                                                    id="verde" data-parsley-errors-container="#cbErrorContainerRadio"
                                                    required checked />
                                                <label for="verde" style="color: black">Verde</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                            <p class="p-v-xs option option-2">
                                                <input class="validar miRadio" name="categoria" type="radio" value="amarillo"
                                                    id="amarillo" data-parsley-errors-container="#cbErrorContainerRadio"
                                                    required />
                                                <label for="amarillo" style="color: black">Amarillo</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                            <p class="p-v-xs option option-3">
                                                <input class="validar miRadio" name="categoria" type="radio" value="rojo" id="rojo"
                                                    data-parsley-errors-container="#cbErrorContainerRadio" required />
                                                <label for="rojo" style="color: black">Rojo</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                        </div>
                                        <div id="cbErrorContainerRadio"></div>
                                        <br>
                                        <br>
                                        <div>
                                            <p>Agregar a la Síntesis</p>
                                            <br>
                                            <input type="checkbox" id="addPdf" name="addPdf" value="1">
                                            <label for="addPdf"> Agregar a PDF</label>
                                        </div>
                                    </div>                                                                                                       
                                </div>                                    
                                <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                            </div>
                        </div>
                    </section>
                    <!---->
                </div>
                <div class="row right-align">
                    <a id="btnModifNotasCSV" class="waves-effect waves-light btn indigo miA">Guardar Cambios</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{URL::asset('assets/js/customNotasCSV.js')}}?{{rand()}}"></script>
<script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
<script>
    /// upload image file ///
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastra y suelta un archivo aquí o haz click',
            'replace': 'Arrastra y suelta o haz click para reemplazar',
            'remove':  'Eliminar',
            'error':   'Ooops, Algo salió mal.'
        },
        error: {
            'fileExtension': 'Solo se aceptan archivos pdf.'
        }
    });
    /// paste image ///
    (function($) {
        	var defaults;
        	$.event.fix = (function(originalFix) {
        		return function(event) {
        			event = originalFix.apply(this, arguments);
        			if (event.type.indexOf("copy") === 0 || event.type.indexOf("paste") === 0) {
        				event.clipboardData = event.originalEvent.clipboardData;
        			}
        			return event;
        		};
        	})($.event.fix);
        	defaults = {
        		callback: $.noop,
        		matchType: /image.*/
        	};
        	return ($.fn.pasteImageReader = function(options) {
        		if (typeof options === "function") {
        			options = {
        				callback: options
        			};
        		}
        		options = $.extend({}, defaults, options);
        		return this.each(function() {
        			var $this, element;
        			element = this;
        			$this = $(this);
        			return $this.bind("paste", function(event) {
        				var clipboardData, found;
        				found = false;
        				clipboardData = event.clipboardData;
        				return Array.prototype.forEach.call(clipboardData.types, function(type, i) {
        					var file, reader;
        					if (found) {
        						return;
        					}
        					if (
        						type.match(options.matchType) ||
        						clipboardData.items[i].type.match(options.matchType)
        					) {
        						file = clipboardData.items[i].getAsFile();
        						reader = new FileReader();
        						reader.onload = function(evt) {
        							return options.callback.call(element, {
        								dataURL: evt.target.result,
        								event: evt,
        								file: file,
        								name: file.name
        							});
        						};
        						reader.readAsDataURL(file);                                
        						snapshoot();
        						return (found = true);
        					}
        				});
        			});
        		});
        	});
        })(jQuery);
    var dataURL, filename;
        $("html").pasteImageReader(function(results) {            
        	filename = results.filename, dataURL = results.dataURL;
            var imgText = dataURL.replace(/^data\:image\/\w+\;base64\,/, '');
            $('#imgTv').val(imgText);
            $('#imgRedSoc').val(imgText);
        	$data.text(dataURL);
        	$size.val(results.file.size);
        	$type.val(results.file.type);
        	var img = document.createElement("img");
        	img.src = dataURL;
        	var w = img.width;
        	var h = img.height;
        	$width.val(w);
        	$height.val(h);
        	return $(".activeImg")
        		.css({
        			backgroundImage: "url(" + dataURL + ")"
        		})
        		.data({ width: w, height: h });
        });
        
        var $data, $size, $type, $width, $height;
        $(function() {
        	$data = $(".data");
        	$size = $(".size");
        	$type = $(".type");
        	$width = $("#width");
        	$height = $("#height");
        	$(".target").on("click", function() {
        		var $this = $(this);
        		var bi = $this.css("background-image");
        		if (bi != "none") {
        			$data.text(bi.substr(4, bi.length - 6));
        		}
            
        		$(".activeImg").removeClass("activeImg");
        		$this.addClass("activeImg");
            
        		$this.toggleClass("contain");
            
        		$width.val($this.data("width"));
        		$height.val($this.data("height"));
        		if ($this.hasClass("contain")) {
        			$this.css({
        				width: $this.data("width"),
        				height: $this.data("height"),
        				"z-index": "10"
        			});
        		} else {
        			$this.css({ width: "", height: "", "z-index": "" });
        		}
        	});
        });
</script>
@endsection