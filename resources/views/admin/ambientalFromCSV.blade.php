@extends('admin.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Cargar Archivo CSV Información Ambiental</h3>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">            
                <form id="csvForm" action="">
                    <label for="documento">Archivo CSV</label>
                    <div class="input-field">                                                                                                            
                        <input id="documento" name="documento" class="dropify" type="file" data-allowed-file-extensions="csv">
                    </div> 
                    <br>
                    <div class="row right-align">
                        <a id="cargaCsvAmbiental" class="waves-effect waves-light btn indigo miA">Cargar Archivo</a>
                    </div> 
                </form>              
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
    <script>
        $('.dropify').dropify({
        messages: {
            'default': 'Arrastra y suelta un archivo aquí o haz click',
            'replace': 'Arrastra y suelta o haz click para reemplazar',
            'remove':  'Eliminar',
            'error':   'Ooops, Algo salió mal.'
        },
        error: {
            'fileExtension': 'Solo se aceptan archivos csv.'
        }
    });
    </script>
@endsection