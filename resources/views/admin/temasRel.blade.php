@extends('admin.master')
@section('css')
    
@endsection
@section('contenido')
    <h1>Temas Relevantes</h1>
    <div class="row right-align">
        <a href="/nuevoTemaRelevante" class="waves-effect waves-light btn" style="background-color: #235b4e"><i
                class="material-icons right">add_circle</i>Nuevo Tema</a>
    </div>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableTemasRel" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            {{-- <th>&nbsp&nbsp&nbsp</th> --}}
                            <th>ID</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Entidad</th>
                            <th scope="col">Estatus</th>
                            <th scope="col">Más</th>
                            {{--<th scope="col">Medio</th>
                            <th scope="col">Entidad</th>
                            <th scope="col">SubProcuraduría</th>
                            <th scope="col">Link</th>
                            <th scope="col">Fecha de Publicación</th> --}}
                            {{-- <th scope="col">Fecha de Alta</th>
                        <th scope="col">Más</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Acción</th> --}}
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>    
@endsection
@section('css')
    
@endsection