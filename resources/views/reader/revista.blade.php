@extends('reader.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/treeview/default/style.min.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i class="material-icons left">skip_previous</i>Regresar</a>
    <h1 style="color: #bc955c">MI PROFEPA</h1>
    <div class="row">
        <div class="col s6">
            <div class="card white">
                <div class="card-content center">
                    <div id="jstree">
                        <ul>
                          <li data-jstree='{"opened":true}'>MI PROFEPA
                            {{-- <ul>
                              <li data-jstree='{"opened":true}'>2020 --}}
                                  <ul>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_1.pdf')}}" target="_blanck">Número 1</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_2.pdf')}}" target="_blanck">Número 2</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_3.pdf')}}" target="_blanck">Número 3</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_4.pdf')}}" target="_blanck">Número 4</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_5.pdf')}}" target="_blanck">Número 5</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_6.pdf')}}" target="_blanck">Número 6</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_7.pdf')}}" target="_blanck">Número 7</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_8.pdf')}}" target="_blanck">Número 8</a>                                       
                                    </li>
                                    <li data-jstree='{"icon":"{{URL::asset('assets/plugins/treeview/default/pdf_mini.png')}}"}'>
                                        <a href="{{URL::asset('storage/revista/Revista_9.pdf')}}" target="_blanck">Número 9</a>                                       
                                    </li>
                                  </ul>
                              {{-- </li>                              
                            </ul> --}}
                          </li>
                        </ul>
                      </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col s6">
            <div id="file-content"></div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/treeview/jstree.min.js')}}"></script>
    <script>       
        $("#jstree").jstree().bind("select_node.jstree", function (e, data) {
             var href = data.node.a_attr.href;
             if (href[0] !== "#"){
                window.open(href);
             }             
        });
    </script>
@endsection