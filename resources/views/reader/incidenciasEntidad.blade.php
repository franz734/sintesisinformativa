@extends('reader.master')
@section('css')
    
@endsection
@section('contenido')
    <div class="row right-align">
        <a id="reportIncidencia" class="waves-effect waves-light btn" style="background-color: #9f2241"><i class="material-icons left">report</i>Reportar Incidencia</a>
    </div>
    <style>
        td.details-control {
          background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
          cursor: pointer;
        }
          tr.shown td.details-control {
          background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }
    </style>
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i class="material-icons left">skip_previous</i>Regresar</a>    
    <h1 style="color: #bc955c">{{$entidad->nomEntidad}}</h1>
    @if ($incidencias == 'noIncidencias')
      <h3>Sin Incidencias</h3>
    @else
      {{-- <h1 style="color: #bc955c">{{$incidencias[0]->entidadRel->nomEntidad}}</h1> --}}
      <div class="row">
        <div class="row no-m-t no-m-b">
            <div class="card">
                <div class="card-content">
                    <table id="tableIncidenciasEdo" class="display responsive-table datatable-example">
                        <thead>
                            <tr>
                                <th>&nbsp&nbsp&nbsp</th>
                                <th>ID</th>
                                <th scope="col">Tema</th>                                                    
                                <th scope="col">Fecha de Suceso</th>
                                <th scope="col">Más</th>                        
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
      </div> 
    @endif
    
@endsection
@section('scripts')
    @if ($incidencias != 'noIncidencias')
      <script>
          function format2 ( d ) {  
            var ret = d.actuacionHtml;
            ret = ret.replace(/&gt;/g, '>');
            ret = ret.replace(/&lt;/g, '<');
            ret = ret.replace(/&quot;/g, '"');
            ret = ret.replace(/&apos;/g, "'");
            ret = ret.replace(/&amp;/g, '&');    
            //console.log(ret);
              return ret;
          
          }
          var spanish = {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    '<i class="material-icons miTi">chevron_left</i>',
                "sLast":     '<i class="material-icons miTi">chevron_right</i>',
                "sNext":     '<i class="material-icons miTi">chevron_right</i>',
                "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
          }
          var idEntidad = '<?php echo $incidencias[0]->idEntidad;?>';
          console.log(idEntidad);
          /// datatable incidencias ///
          var tableIncidenciasEdo = $('#tableIncidenciasEdo').DataTable({
            "language":spanish,
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "ajax": "/incidencias/getIncidenciasByEdo/"+idEntidad,              
            "columns": [
              { "data":""},
              {"data":"idIncidencia"},
              {"data":"tema"},                      
                {"data":"fcSuceso"}
          
            ],
            "columnDefs": [
              {
                "targets": 0,
                "data": null,
                "orderable": false,
                "className": 'details-control',
                "defaultContent": ''
              },  
             {
                "targets":4,
                "data":null,
                "visible":true,
                "className": "dt-center",
                "render": function(dta, type, row){
                  return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>'                      
                }
              },        
            ],
          });
          $('.dataTables_length select').addClass('browser-default');
          $('#tableIncidenciasEdo tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            //tr.addClass('miText');
              var row = tableIncidenciasEdo.row( tr );
          
            if ( row.child.isShown() ) {
              // This row is already open - close it
              row.child.hide();
              tr.removeClass('shown');
            }
            else {
              // Open this row
              row.child( format2(row.data()) ).show();
              tr.addClass('shown');
            }
          });
          $('#tableIncidenciasEdo tbody').on('click','.btnVerIncidente', function(){
            var tr = $(this).closest('tr');  
            var idIncidencia = tr[0].childNodes[1].firstChild.data;
            //console.log(idIncidente)
            window.location.href ='../verIncidencia/'+idIncidencia;
          });
      </script>
        
    @endif
@endsection