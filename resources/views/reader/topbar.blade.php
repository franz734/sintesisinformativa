@php
    $routesS = ['sintesisInformativa/inicio', 'sintesisInformativa/entidad/{id}'];
    $routesI = ['incidencias/inicio', 'incidencia/nuevaIncidencia', 'incidencias/entidad/{id}',
                'incidencias/verIncidencia/{id}','denunciante/nuevaDenuncia'];
@endphp
{{-- <p>{{$uri->uri}}</p> --}}
<header class="mn-header">
    <ul id="dropdown1" class="dropdown-content">
        <li><a class="aTopD" href="{{url('/sintesisInformativa/ochoColumnas')}}" style="color: #9f2241">Ocho Columnas</a></li>
        <li class="divider"></li>
        <li><a class="aTopD" href="{{url('/sintesisInformativa/boletines')}}" style="color: #9f2241">Boletines</a></li>
        <li class="divider"></li>
        <li><a class="aTopD" href="{{url('/sintesisInformativa/informacionAmbiental')}}" style="color: #9f2241">Información Ambiental</a></li>
        
    </ul>
    <nav class="miNavHeader">
        <div class="nav-wrapper row">            
            <div class="header-title col s3 m3">                      
                @if (in_array($uri->uri, $routesS))
                    <a href="{{url('/sintesisInformativa/inicio')}}">
                        <img src="{{URL::asset('assets/images/Logo_gob.png')}}" height="45px">
                        <img src="{{URL::asset('assets/images/LogoMedioAmbiente- Profepa.png')}}" height="45px">
                    </a>
                @elseif(in_array($uri->uri,$routesI))
                    <a {{-- href="{{url('/incidencias/inicio')}}" --}}>
                        <img src="{{URL::asset('assets/images/Logo_gob.png')}}" height="45px">
                        <img src="{{URL::asset('assets/images/LogoMedioAmbiente- Profepa.png')}}" height="45px">
                    </a>
                @endif
                {{-- <img src="{{URL::asset('assets/images/LogoMedioAmbiente- Profepa.png')}}" height="45px"> --}}
            </div>   
            <ul id="nav-mobile" class="right hide-on-med-and-down">   
                @if (in_array($uri->uri,$routesS))
                    @hasanyrole ('SuperAdmin')
                        <li><a class="aTop" href="/incidencias/inicio">Incidencias</a></li>
                    @endhasanyrole
                    {{-- <li><a class="dropdown-trigger aTop" href="#!" data-activates="dropdown1">Sintesis<i class="material-icons right">arrow_drop_down</i></a></li> --}}
                @elseif(in_array($uri->uri,$routesI))
                    <li><a class="aTop" href="/sintesisInformativa/inicio">Sintesis Informativa</a></li>
                    @if ($uri->uri == 'incidencia/nuevaIncidencia' || $uri->uri='denunciante/nuevaDenuncia')
                        <li class="aTop">                        
                            <a class="waves-effect waves-grey" href="{{ route('logoutColab') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                Salir
                            </a>
                            <form id="logout-form" action="{{ route('logoutColab') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    @endif
                @endif        
                {{-- <li><a class="aTop" href="{{URL::asset('storage/revista/revista_no1.pdf')}}" target="_blanck">Procuración Ambiental</a></li> --}}
                <li><a class="aTop" href="{{url('/sintesisInformativa/revista')}}">Mi PROFEPA</a></li>
            </ul>       
        </div>
    </nav>
</header>
