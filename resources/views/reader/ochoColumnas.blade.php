@extends('reader.master')
@section('css')
    
@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i class="material-icons left">skip_previous</i>Regresar</a>
    <h1>Ocho Columnas</h1>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                @foreach ($columnas as $columna)
                    @php
                        $img = '';
                        if($columna->fuente == 'El Universal'){
                            $img = 'assets/images/reader/logoElUniversal.png';
                        }
                        elseif ($columna->fuente == 'Reforma') {
                            $img = 'assets/images/reader/logoReforma.png';
                        }
                        elseif ($columna->fuente == 'Milenio') {
                            $img = 'assets/images/reader/logoMilenio.png';
                        }
                        elseif ($columna->fuente == 'Excelsior') {
                            $img = 'assets/images/reader/logoExcelsior.png';
                        }
                        elseif ($columna->fuente == 'La Jornada') {
                            $img = 'assets/images/reader/logoJornada.png';
                        }
                    @endphp                   
                    <div class="col s12 m4 l8">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s12 m4 l2">
                                        <img src="{{URL::asset($img)}}" width="150" height="100">
                                    </div> <!--ofsset izquierdo-->
                                    <div class="col s8">
                                        <!--Ocho columnas-->                                                                            
                                        <blockquote class="miBlock">
                                            <p class="upper"><span>{{$columna->encabezado}}</span></p>
                                            {!!$columna->textoHtml!!}
                                        </blockquote>                                
                                        <!---->
                                    </div>            
                                </div>                                
                            </div>
                        </div>                                                                                                                        
                    </div>
                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->                    
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    
@endsection