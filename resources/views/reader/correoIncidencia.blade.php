<p>Se ha reportado una nueva incidencia del estado de {{$incidencia->entidadRel->nomEntidad}}.</p>
<p>
    <b>Tema</b>
    <br>
    {{$incidencia->tema}}
</p>
<p>
    <b>Area</b>
    <br>
    {{$incidencia->areaRel->nomArea.'/'.$incidencia->subAreaRel->nomSubArea}}
</p>
<p>
    <b>Resumen</b>
    <br>
    {{$incidencia->resumen}}
</p>
<p><a href="{{$dir}}">ver detalles</a></p>
@if (!is_null($propuesta))
@php
    $txtProp = json_decode($propuesta->propuesta);
@endphp
    <hr>
    <p>Se propone dicha incidencia como {{$propuesta->tipoPropRel->nomTipoPropuesta}}</p>
    @if ($propuesta->idTipoPropuesta == 1)
        <p><i>{{$txtProp->texto}}.</i></p>
    @elseif ($propuesta->idTipoPropuesta == 2)
        <p>
            <b><i>{{$txtProp->titulo}}</i></b>
            <br>
            <i>{{$txtProp->texto}}</i>
        </p>
    
    @endif

@endif