@extends('reader.master')
@section('css')
    
@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i class="material-icons left">skip_previous</i>Regresar</a>
    <h1>Boletines</h1>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                @foreach ($boletines as $boletin)
                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                    <div class="col s12 m4 l8">
                        <div class="card">
                            <div class="card-content">
                                <div class="row">
                                    <div class="col s8">
                                        <!--Boletin-->                                        
                                        <blockquote class="miBlock">
                                            {!!$boletin->textoHtml!!}
                                        </blockquote>                                
                                        <!---->
                                    </div>            
                                </div>
                                @if (!is_null($boletin->link) || $boletin->link != '')
                                    <div class="row">            
                                        <div class="col s4">
                                            <a href="{{$boletin->link}}" target="_blank">CONTINUAR LEYENDO</a>                
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>                                                                                                                        
                    </div>
                    <div class="col s12 m4 l2"></div><!--ofsset derecho--> 
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    
@endsection