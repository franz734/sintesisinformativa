@extends('reader.master')
@section('css')
    
@endsection
@section('contenido')  
  @php
      //dd($notas);
      $ctl = 0;
      $ctlC = 0;
      $cad = '<br>';
  @endphp  
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i class="material-icons left">skip_previous</i>Regresar</a>
    <h1 style="color: #bc955c">{{$entidad->nomEntidad}}</h1>
    <div class="row">
      @if ($notas == 'noNotas')
          <h3>Sin Notas</h3>
      @else
        <div class="col s12">
          <ul class="tabs">
            @foreach ($notas as $nota)
                @php
                  $active = ($ctl == 0) ? 'active' : '';         
                @endphp 
                <li class="tab col s3"><a class="{{$active}}" style="color: #9f2241" href="#tab{{$ctl}}">{{$nota[0]->medioRel->nomMedio}}</a></li>
                @php
                    $ctl++;
                @endphp
            @endforeach
          </ul>
        </div>
        <!--contenedor de la nota-->
        @foreach ($notas as $nota)        
          <div id="tab{{$ctlC}}" class="col s12 ">
            <div class="card">
              <div class="card-content">
                @foreach ($nota as $info)
                    @if ($info->idMedio == 1)
                        @include('reader.tipoImpreso',['nota'=>$info])                        
                    @elseif ($info->idMedio == 2)
                        @include('reader.tipoTv',['nota'=>$info])                        
                    @elseif ($info->idMedio == 3)
                        @include('reader.tipoWeb',['nota'=>$info])                        
                    @elseif ($info->idMedio == 4)                       
                        @include('reader.tipoRedSoc',['nota'=>$info]) 
                        @php
                            $cad = ''
                        @endphp                       
                    @endif
                  {{--  {!!$cad!!} --}}
                @endforeach
              </div>
            </div>
          </div>                      
          @php
              $ctlC++;
          @endphp
        @endforeach
        <!---->
      @endif      
    </div>
    

@endsection
@section('scripts')
<script>
    function goBack() {
      window.history.back();
    }
</script>
@endsection