{{-- <h1>{{$nota->encabezado}}</h1> --}}
<div class="card notaCont">
    <div class="card-coontent">        
        <div class="row">
            <!--Entidad-->
            <div class="col s12 m4 l5">
                {{-- <h5 class="upper"><span style="color:blue">{{$nota->entidadRel->nomEntidad}}</span></h5> --}}
            </div> 
            <!---->
            <div class="col s12 m4 l2"></div> <!--ofsset central-->
            <!--Area-->
            <div class="col s12 m4 l5">
                <h5 class="upper"><span style="color: #235b4e">{{$nota->areaRel->nomArea}}</span></h5>
            </div> 
            <!---->
        </div>
        <div class="row">
            <div class="col s8">
                <!--Nota-->
                <p class="upper"><bold>{{$nota->encabezado}}</bold></p>
                <blockquote class="miBlock">
                    {!!$nota->textoHtml!!}
                </blockquote>                                
                <!---->
            </div>            
        </div>
        <div class="row">
            <div class="col s12 m4 l8"></div> <!--ofsset izquierdo-->
            <div class="col s4">
                <p class="upper">{{$nota->fuente}}</p>
            </div>
        </div>
        <br>
    </div>
</div>