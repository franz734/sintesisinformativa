<style>
    html { margin: 0 }
    .page-break {
        page-break-after: always;
    }    
    #overlay {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        /* background-image: url("/var/www/html/sintesisInformativa/public/assets/images/Fondo.png"); */
        background-position: center top;
        background-repeat: no-repeat;
        z-index: -1;
    }
    #titulo{
        color: #891f36;
        text-align: center;
        font-size: 50px;
    }
    #titulo2{
        color: #891f36;
        text-align: center;
       /*  font-size: 50px; */
    }
    .centrar{
        text-align: center;
        font-size: 20px;
    }
    #center-page{
        margin-top: 450px;
    }
    table, th, td {
      border: 1px solid black;
    }
    table thead{
        background-color: #ddc9a3; 
    }
    .entidades, .medios, .areas{
        margin-left: 70px;
    }
    .imgEntidad{
       margin-top: -200px;
       margin-left: 200px;
    }
    .imgMedios, .imgAreas{
       margin-top: -200px;
       margin-left: 250px;
    }
    .contNota{
        margin-left: 70px; 
    }
</style>
<!----------->
<!DOCTYPE html>
<html>
<head>
	<title>SINTESIS INFORMATIVA</title>
</head>
<body>
    <div id="overlay">    
        <div id="center-page">
            <h1 id="titulo"> {!! $title !!}</h1>   
            <p class="centrar" style="color: #847d74">COORDINACIÓN DE COMUNICACIÓN SOCIAL</p>   
            <P class="centrar" style="color: 235b4e"><span>{{$hoy}}</span></P>
        </div>      
    </div>
    <div class="page-break"></div>
    <!--Graficas-->
    <br>
    <br>
    <h1 id="titulo2">....EN NÚMEROS</h1>
    <br>
    <div class="entidades">
        <div class="tabEntidad">
            <table>
                <thead>
                    <tr>
                      <th>Estados</th>
                      <th>Notas</th>              
                    </tr>  
                </thead>          
                <tbody>
                    @foreach ($entidades as $entidad)
                        <tr>
                          <td>{{$entidad->nomEntidad}}</td>
                          <td>{{$entidad->notas}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="background-color: #ddc9a3">Total</td>
                        <td style="background-color: #ddc9a3">{{$total}}</td>
                    </tr>
                </tbody>            
            </table>
        </div>
        <div class="imgEntidad">
            <img src="https://quickchart.io/chart?c={{$entidadesG}}" width="400" height="200">
        </div>
    </div>
    <br>
    <br>
    <div class="medios">
        <div class="tabMedios">
            <table>
                <thead>
                    <tr>
                     <th>Medios</th>
                     <th>Notas</th>
                    </tr>  
                </thead>          
                <tbody>
                    @foreach ($medios as $medio)
                        <tr>
                          <td>{{$medio->nomMedio}}</td>
                          <td>{{$medio->notas}}</td>                          
                        </tr>
                    @endforeach
                    <tr>
                        <td style="background-color: #ddc9a3">Total</td>
                        <td style="background-color: #ddc9a3">{{$total}}</td>
                    </tr>
                </tbody>            
            </table>
        </div>
        <div class="imgMedios">
            <img src="https://quickchart.io/chart?c={{$mediosG}}" width="350" height="200">
        </div>
    </div>
    <br>
    <br>
    <div class="areas">
        <div class="tabArea">
            <table>
                <thead>
                    <tr>
                      <th>Área</th>
                      <th>Notas</th>              
                    </tr>  
                </thead>          
                <tbody>
                    @foreach ($areas as $area)
                        <tr>
                          <td>{{$area->nomArea}}</td>
                          <td>{{$area->notas}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td style="background-color: #ddc9a3">Total</td>
                        <td style="background-color: #ddc9a3">{{$total}}</td>
                    </tr>
                </tbody>            
            </table>
        </div>
        <div class="imgAreas">
            <img src="https://quickchart.io/chart?c={{$areasG}}" width="350" height="200">
        </div>
    </div>
    <div class="page-break"></div>
    <!--NOtas-->
    @foreach ($mediosCtl as $medio)
        <br>
        <br>
        <h1 id="titulo2">{{$medio->nomMedio}}</h1>
        <div class="contNota">
            @foreach ($notas as $nota)
                @if ($medio->idMedio == $nota->idMedio)
                    @if ($nota->idMedio == 4)
                        {{-- <a href="{{$nota->link}}" title="{{$nota->link}}" target="_blank">                        
                            <img class="notaCont" style="margin-right: 10px" src="{{URL::asset($nota->imgNota)}}" width="310" height="400">
                        </a> --}}
                    @elseif ($nota->idMedio == 2)
                        {{-- <img src="{{URL::asset($nota->imgNota)}}" height="80"> --}}
                        {!!$nota->textoHtml!!}
                        <a href="{{$nota->link}}" target="_blank">VER NOTA</a>
                    @else
                        {!!$nota->textoHtml!!}   
                    @endif
                                       
                @endif
                
            @endforeach
        </div>
        <br>
        <div class="page-break"></div>
    @endforeach
</body>
</html>