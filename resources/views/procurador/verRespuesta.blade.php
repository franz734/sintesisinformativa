@php
if ($respuesta->subAreaRel == null) {
    $cadSubArea = '';
} else {
    $cadSubArea = $respuesta->subAreaRel->nomSubArea;
}
@endphp
@extends('procurador.master')
@section('css')

@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5>Respuesta No. {{ $respuesta->idRespuesta }}</h5>
                <h5 style="color: #bc955c">{{ $respuesta->entidadRel->nomEntidad }}</h5>
                <h5>{{ $respuesta->fcSuceso }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $respuesta->notaRel->encabezado }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Área</b></h5>
                        <blockquote>{{ $respuesta->areaRel->nomArea . '/' . $cadSubArea }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Resumen</b></h5>
                        <blockquote>{{ $respuesta->resumen }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Antecedentes y/o Problematica</b></h5>
                        <blockquote>{{ $respuesta->notaRel->texto }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Actuación de la Delegación PROFEPA</b></h5>
                        <blockquote>{!! $respuesta->actuacionHtml !!}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Acciones a Realizar</b></h5>
                        <blockquote>{!! $respuesta->acciones !!}</blockquote>
                        <br>
                    </div>
                </div>
                @if (!is_null($respuesta->evidencia))
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Multimedia</b></h5>
                            @php
                                $ctl = 0;
                            @endphp
                            @foreach ($respuesta->evidencia as $img)
                                @php
                                    if ($ctl % 2 == 0) {
                                        $inicio = '<div class="row center-align">';
                                        $fin = '';
                                    } else {
                                        $inicio = '';
                                        $fin = '</div><br>';
                                    }
                                    $ctl++;
                                @endphp
                                {!! $inicio !!}
                                {{-- <div class="imgCont"> --}}
                                <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">
                                {{-- </div> --}}
                                {!! $fin !!}
                            @endforeach
                            <br>
                            <br>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
