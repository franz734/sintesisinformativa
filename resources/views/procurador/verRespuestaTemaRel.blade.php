@php
    if($respuestas[0]->entidadRel == null){        
        $cad = 'Oficina Central';
    }
    else{        
        $cad = $respuestas[0]->entidadRel->nomEntidad;
    }
@endphp
@extends('procurador.master')
@section('css')

@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5 style="color: #bc955c">{{ $cad }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $respuestas[0]->temaRelevanteRel->temaRel }}</blockquote>
                        <br>
                    </div>
                </div>
                {{-- <div class="card notaCont">
                <div class="card-coontent cont">   
                    <br>                      
                    <h5><b>Área</b></h5>       
                    <blockquote>{{$respuesta->areaRel->nomArea.'/'.$respuesta->subAreaRel->nomSubArea}}</blockquote>
                    <br>
                </div>
            </div> --}}
                @foreach ($respuestas as $respuesta)
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Respuesta</b></h5>
                            <p>{{$respuesta->fecha}}</p>
                            <blockquote>{{ $respuesta->respuesta }}</blockquote>
                            <br>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
