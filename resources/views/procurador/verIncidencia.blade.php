@extends('procurador.master')
@section('css')

@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5>Incidencia No. {{ $incidencia->idIncidencia }}</h5>
                <h5 style="color: #bc955c">{{ $incidencia->entidadRel->nomEntidad }}</h5>
                <h5>{{ $incidencia->fcSuceso }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $incidencia->tema }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Área</b></h5>
                        <blockquote>{{ $incidencia->areaRel->nomArea . '/' . $incidencia->subAreaRel->nomSubArea }}
                        </blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Resumen</b></h5>
                        <blockquote>{{ $incidencia->resumen }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Antecedentes y/o Problematica</b></h5>
                        <blockquote>{{ $incidencia->problematica }}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Actuación de la Delegación PROFEPA</b></h5>
                        <blockquote>{!! $incidencia->actuacionHtml !!}</blockquote>
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Acciones a Realizar</b></h5>
                        <blockquote>{!! $incidencia->acciones !!}</blockquote>
                        <br>
                    </div>
                </div>
                @if (!is_null($incidencia->evidencia))
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Multimedia</b></h5>
                            @php
                                $ctl = 0;
                            @endphp
                            @foreach ($incidencia->evidencia as $img)
                                @php
                                    $img = str_replace('Queru00e9taro', 'Querétaro', $img);
                                    $img = str_replace('Michoacu00e1n', 'Michoacán', $img);
                                    $img = str_replace('NuevoLeu00f3n', 'NuevoLeón', $img);
                                    $img = str_replace('Mu00e9xico', 'México', $img);
                                    $img = str_replace('Yucatu00e1n', 'Yucatán', $img);
                                    if ($ctl % 2 == 0) {
                                        $inicio = '<div class="row center-align">';
                                        $fin = '';
                                    } else {
                                        $inicio = '';
                                        $fin = '</div><br>';
                                    }
                                    $ctl++;
                                @endphp
                                {!! $inicio !!}
                                {{-- <div class="imgCont"> --}}
                                <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">
                                {{-- </div> --}}
                                {!! $fin !!}
                            @endforeach
                            <br>
                            <br>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
