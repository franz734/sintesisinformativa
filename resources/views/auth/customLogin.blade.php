@include('admin.header')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SINTESIS INFORMATIVA</title>
</head>
<style>
    .delegates {
        /*height: 100% !important;
        width: 100% !important;
        background: url("/assets/images/fondoLogin1.jpeg") no-repeat !important;
        background-size: auto !important;
        background-size: cover !important;
        */
    }
    .loginCard{
        background-color: rgba(255, 255, 255, .7) !important;
        box-shadow: 0 1rem 3rem rgba(0,0,0,.175) !important;
    }
    .login-container{
        max-width: 300px;
        margin: 0 auto;
    }
    .btn-login{
        width: 100%;
        background: #0a4403; 
    }
    .btn-login:hover, .btn-login:active{
        background: #15590d; 
    }    
    .msg-error{
        color: #f03030;
        padding-top: 231px;
        text-align: center;
    }
    body{
        font-family: 'MontserratCustom'!important;
    }
</style>

<body class="delegates" id="bg-login">
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container">
            <div class="valign">
                <div class="row">
                    {{-- <div style="text-align: center">
                        <img src="{{ URL::asset('/assets/images/logo_profepa.png') }}" width="300px">
                    </div> --}}
                    <!-- <div class="col s12 m6 l4 offset-l4 offset-m3"> -->
                    <div class="login-container">
                        <div class="card white darken-1 loginCard" >
                            <div class="card-content ">
                                <div class="row">
                                    <div style="text-align: center">
                                        <img src="{{ URL::asset('/assets/images/logo_profepa.png') }}" width="100%">
                                        <h5>Síntesis Informativa</h5>
                                    </div>                                    
                                    <form class="col s12" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="input-field col s12">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror" name="email"
                                                value="{{ old('email') }}" required autocomplete="email">
                                            <label for="email" style="color: black !important">Usuario</label>
                                        </div>
                                        <div class="input-field col s12">
                                            <input id="password" type="password"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password" required autocomplete="current-password">
                                            <label for="password" style="color: black !important">Contraseña</label>
                                        </div>
                                        <div class="col s12 right-align m-t-sm">
                                            {{-- <a href="sign-up.html" class="waves-effect waves-grey btn-flat">sign up</a> --}}
                                            <button type="submit" class="btn btn-login btn-primary">
                                                INGRESAR
                                            </button>
                                        </div>
                                        
                                            @error('password')                                                
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            @error('email')
                                                <div class="msg-error">
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Los datos ingresados son incorrectos.</strong>
                                                    </span>
                                                </div>
                                            @enderror                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    @include('admin.footer')
    <script>
        var url = document.URL;
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function() {
            history.pushState(null, null, url);
        });

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/r121/three.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vanta@latest/dist/vanta.birds.min.js"></script>
    <script>
        VANTA.BIRDS({
            el: "#bg-login",
            mouseControls: true,
            touchControls: true,
            gyroControls: false,
            minHeight: 200.00,
            minWidth: 200.00,
            scale: 1.00,
            scaleMobile: 1.00,
            backgroundColor: 0xffffff,
            color1: 0x646262,
            color2: 0xfcfcc2,
            colorMode: "lerp",
            wingSpan: 40.00,
            speedLimit: 3.00,
            separation: 100.00,
            alignment: 100.00,
            cohesion: 100.00,
            quantity: 4.00
        })

    </script>
</body>

</html>
