@include('admin.header')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SINTESIS INFORMATIVA</title>
</head>
<body>
    <div class="mn-content valign-wrapper">
        <main class="mn-inner container ">
            <div class="valign">
                  <div class="row">
                    <div style="text-align: center">
                        <img src="{{URL::asset('/assets/images/logo_profepa.png')}}" width="300px">
                      </div>
                      <div class="col s12 m6 l4 offset-l4 offset-m3">
                          <div class="card white darken-1">
                              <div class="card-content ">
                                  <span class="card-title">Registrar</span>
                                   <div class="row">
                                       <form class="col s12" method="POST" action="{{ route('register') }}">
                                        @csrf
                                           <div class="input-field col s12">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                               <label for="name">Nombre</label>
                                           </div>
                                           <div class="input-field col s12">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                               <label for="email">Correo</label>
                                           </div>
                                           <div class="input-field col s12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                               <label for="password">Contraseña</label>
                                           </div>
                                           <div class="input-field col s12">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                               <label for="password-confirm">Confirmar Contraseña</label>
                                           </div>
                                           <div class="col s12 right-align m-t-sm">
                                                {{-- <button type="submit" class="btn btn-primary">
                                                    {{ __('Register') }}
                                                </button> --}}
                                                <button class="btn waves-effect waves-light" type="submit">{{ __('Register') }}
                                                    <i class="material-icons right">send</i>
                                                  </button>
                                           </div>
                                       </form>
                                  </div>
                              </div>
                          </div>
                      </div>
                </div>
            </div>
        </main>
    </div>
    @include('admin.footer')
</body>
</html>