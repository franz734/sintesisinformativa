@extends('denunciante.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Registro de Denuncias</h3>
    <div class="col s12 m12 l12">
        <div class="card" >
            <div class="card-content">
                <form id="denunciaForm" action="">
                    <div>
                        <!--Datos Generales-->
                        <h3>Denuncia</h3>
                        <section>
                            <div class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="row">
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="expediente">Expediente Asignado</label>
                                                    <input id="expediente" class="validar" name="expediente" type="text" placeholder="PFPA/EDO/DQ/XX/XXXX-XX" required>
                                                </div>                                                
                                            </div>
                                            <div class="col s6">
                                                <div class="input-field">                                            
                                                    <select id="selectFuente" name="fuente" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                        <option disabled selected value="def">Tipo de Recepción</option>                                                
                                                        @foreach ($fuentes as $fuente)
                                                            <option value={{$fuente->idFuente}}>{{$fuente->nomFuente}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>    
                                        </div>  
                                        <br>
                                        <div class="row">
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="fechaRec">Fecha de Recepción</label>
                                                    <input id="fechaRec"  name="fechaRec" type="text" class="datepicker" disabled value="{{date('d/m/Y')}}">
                                                </div>
                                            </div>
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="horaRec">Hora de Recepción</label>
                                                    <input placeholder="" id="horaRec" name="horaRec" class="masked" type="text" required {{-- data-inputmask="'mask': 'h:s'" --}}>
                                                </div>
                                            </div>
                                        </div>   
                                        <div class="input-field">                                            
                                            <select id="selectAtendio" name="atendio" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Atendió Denuncia</option>                                                                                                
                                                <option value="1">Persona1</option>
                                                <option value="2">Persona2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectArea" name="area" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Area</option>                                                
                                                @foreach ($areas as $area)
                                                    <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                                @endforeach
                                            </select>
                                        </div>   
                                        <div class="input-field">                                            
                                            <select id="selectSubArea" name="subArea" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Sub-Área</option>                                    
                                            </select>
                                        </div>    
                                        <div class="input-field">                                            
                                            <select id="selectProcedencia1" name="procedencia1" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Procedencia 1</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectProcedencia2" name="procedencia2" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Procedencia 2</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectProcedencia3" name="procedencia3" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Procedencia 3</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectProcedencia4" name="procedencia4" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Procedencia 4</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>                                                                                                                                         
                                    </div>
                                    <div class="col s12 m4 l2"></div><!--ofsset derecho-->                               
                                </div>
                            </div>
                        </section>
                        <!---->
                        <!--Datos del Denunciante-->
                        <h3>Denunciante</h3>
                        <section>
                            <div class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">                                            
                                            <select id="selectTipoPer_1" name="tipoPersona" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Persona</option>                                                
                                                @foreach ($tiposPer as $tipoPer)
                                                    <option value={{$tipoPer->idTipoPersona}}>{{$tipoPer->tipoPersona}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="contDenunciante" style="display: none">
                                            <div class="input-field">                                            
                                                <select id="selectSubTipo_1" name="subTipo" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Genero</option>                                                    
                                                </select>
                                            </div>
                                            <div id="contName">
                                                    
                                            </div>                                            
                                            <div class="input-field">
                                                <label for="calle">Calle/Predio</label>
                                                <input id="calle" class="validar" name="calle" type="text" required>
                                            </div>
                                            <div class="row">
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="numero">Número</label>
                                                        <input id="numero" class="validar" name="numero" type="text" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="cp">C.P.</label>
                                                        <input id="cp" class="validar" name="cp" type="text" size="5" maxlength="5" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="tel">Teléfono</label>
                                                        <input id="tel" class="validar" name="tel" type="text" size="10" maxlength="10" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-field">                                            
                                                <select id="selectEdo" name="entidadFed" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Entidad Federativa</option>                                                
                                                    @foreach ($entidades as $entidad)
                                                        <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-field">                                            
                                                <select id="selectMunicipio" name="municipio" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Delegación o Municipio</option>                                    
                                                </select>
                                            </div> 
                                            <div class="input-field">
                                                <label for="localidad">Localidad</label>
                                                <input id="localidad" class="validar" name="localidad" type="text" required>
                                            </div>
                                            <div class="input-field">
                                                <label for="notificar">C.C. Señalados para Oir y Recibir Notificaciones</label>
                                                <input id="notificar" class="validar" name="notificar" type="text" required>
                                            </div>
                                            <div class="row">                                                
                                                <div class="input-field">
                                                    <div class="switch m-b-md">
                                                        <label>
                                                            No Notificar por Correo
                                                            <input id="notif" type="checkbox" >
                                                            <span class="lever"></span>
                                                            Notificar por Correo
                                                        </label>
                                                    </div>                                                    
                                                    <input hidden id="notifI" name="notifI" type="text">
                                                </div>                                                
                                            </div>
                                            <br>
                                            <div class="input-field">
                                                <label for="correo">Correo Electrónico</label>
                                                <input id="correo" class="validar" name="correo" type="email" required>
                                            </div>                                            
                                            <div class="row">                                                
                                                <div class="input-field">
                                                    <div class="switch m-b-md">
                                                        <label>
                                                            No Guardar Identidad
                                                            <input id="anonimo" type="checkbox" >
                                                            <span class="lever"></span>
                                                            Guardar Identidad
                                                        </label>
                                                    </div>                                                    
                                                    <input hidden id="anonimoI" name="anonimoI" type="text">
                                                </div>                                                
                                            </div>
                                        </div>
                                    </div>     
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                        <!--Denunciado-->
                        <h3>Denunciado</h3>
                        <section>
                            <div class="wizard-content">
                               <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">                                            
                                            <select id="selectTipoPer_2" name="tipoPersonaDdo" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Tipo de Persona</option>                                                
                                                @foreach ($tiposPer as $tipoPer)
                                                    <option value={{$tipoPer->idTipoPersona}}>{{$tipoPer->tipoPersona}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="contDenunciado" style="display: none">
                                            <div class="input-field">                                            
                                                <select id="selectSubTipo_2" name="subTipoDdo" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Genero</option>                                                    
                                                </select>
                                            </div>
                                            <div id="contNameDdo">
                                                    
                                            </div>
                                            <div class="input-field">
                                                <label for="calleDdo">Calle/Predio</label>
                                                <input id="calleDdo" class="validar" name="calleDdo" type="text" required>
                                            </div>
                                            <div class="row">
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="numeroDdo">Número</label>
                                                        <input id="numeroDdo" class="validar" name="numeroDdo" type="text" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="cpDdo">C.P.</label>
                                                        <input id="cpDdo" class="validar" name="cpDdo" type="text" size="5" maxlength="5" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">                                                
                                                <div class="col s6">
                                                    <div class="input-field">
                                                        <label for="telDdo">Teléfono</label>
                                                        <input id="telDdo" class="validar" name="telDdo" type="text" size="10" maxlength="10" onKeyPress="return soloNumeros(event)" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-field">                                            
                                                <select id="selectEdoDdo" name="entidadFedDdo" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Entidad Federativa</option>                                                
                                                    @foreach ($entidades as $entidad)
                                                        <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="input-field">                                            
                                                <select id="selectMunicipioDdo" name="municipioDdo" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                    <option disabled selected value="def">Delegación o Municipio</option>                                    
                                                </select>
                                            </div> 
                                            <div class="input-field">
                                                <label for="localidadDdo">Localidad</label>
                                                <input id="localidadDdo" class="validar" name="localidadDdo" type="text" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                                </div> 
                            </div>
                        </section>
                        <!---->
                        <!--Acciones-->
                        <h3>Acciones</h3>
                        <section>
                            <div class="wizard-content">                                
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">                                            
                                            <select id="selectRecAfect" name="recursoAfectado" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Recurso Afectado</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectAccDen" name="accionDenunciada" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Acción Dnunciada</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectElemAfec" name="elemenrosAfectacion" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Elementos de Afectación</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>  
                                        <div class="row">
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="descripcion">Descripción</label>
                                                    <textarea id="descripcion" name="descripcion" class="materialize-textarea validar" {{-- length="500" --}} required></textarea>                                
                                                </div>
                                            </div>
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="pruebaDte">Prueba que Aporta Denunciante</label>
                                                    <textarea id="pruebaDte" name="pruebaDte" class="materialize-textarea validar" {{-- length="500" --}} required></textarea>                                
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-field">
                                            <label for="calleAcc">Calle/Predio</label>
                                            <input id="calleAcc" class="validar" name="calleAcc" type="text" required>
                                        </div>
                                        <div class="row">
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="numeroAcc">Número</label>
                                                    <input id="numeroAcc" class="validar" name="numeroAcc" type="text" onKeyPress="return soloNumeros(event)" required>
                                                </div>
                                            </div>
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="cpAcc">C.P.</label>
                                                    <input id="cpAcc" class="validar" name="cpAcc" type="text" size="5" maxlength="5" onKeyPress="return soloNumeros(event)" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">                                                
                                            <div class="col s6">
                                                <div class="input-field">
                                                    <label for="telAcc">Teléfono</label>
                                                    <input id="telAcc" class="validar" name="telAcc" type="text" size="10" maxlength="10" onKeyPress="return soloNumeros(event)" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectEdoAcc" name="entidadFedAcc" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Entidad Federativa</option>                                                
                                                @foreach ($entidades as $entidad)
                                                    <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectMunicipioAcc" name="municipioAcc" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Delegación o Municipio</option>                                    
                                            </select>
                                        </div> 
                                        <div class="input-field">
                                            <label for="localidadAcc">Localidad</label>
                                            <input id="localidadAcc" class="validar" name="localidadAcc" type="text" required>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectRegimen" name="regimen" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Regimen</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">                                            
                                            <select id="selectSisTerr" name="sistemaTrritorial" class="js-states browser-default sel2" tabindex="-1" style="width: 100%" required>
                                                <option disabled selected value="def">Sistema Territorial</option>                                                                                                
                                                <option value="1">Opcion 1</option>
                                                <option value="2">Opcion 2</option>
                                            </select>
                                        </div>
                                        <div class="input-field">    
                                            <p class="p-v-xs">
                                                <input type="checkbox" class="filled-in" id="areaProtegida" name="areaProtegida" />
                                                <label for="areaProtegida">Áreas Naturales Protegidas</label>
                                            </p>
                                        </div>    
                                    </div>
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                        <!--Finalizar-->
                        <h3>Terminar</h3>
                        <section>
                            <div class="wizard-content">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div> <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        Verifique que todo sea correcto y presione finalizar.
                                    </div>
                                    <div class="col s12 m4 l2"></div> <!--ofsset derecho-->                                    
                                </div>
                            </div>
                        </section>
                        <!---->
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
<script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
<script>
    // Formulario nueva nota //
    var form = $("#denunciaForm");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        autoFocus: true,
        //transitionEffect: "fade",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if(currentIndex < newIndex){
                // Step 1 form validation
                if(currentIndex === 0){                    
                    return true;                    
                }
                // Step 2 form validation
                if(currentIndex === 1){
                    return true;
                }
                // Step 3 form validation
                if(currentIndex === 2){
                    return true;
                }
                // Step 4 form validation
                if(currentIndex === 3){
                    return true;
                }
            }
            else{
                return true;
            }
        },        
        onFinished: function (event, currentIndex)
        {            
            var formdata = new FormData($("#denunciaForm")[0]);   
            Swal.fire({
              title: 'Guardando...',
              allowEscapeKey: false,
              allowOutsideClick: false,
            })
            Swal.showLoading();         
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
                url: "",
                type: 'POST',
                dataType: 'json',
                data: formdata,                
                cache:false,
                processData: false,
                contentType: false,
            })
            .done(function(result) {
               console.log(result)               
                Swal.fire(
                  'Guardado',
                  'Los datos han sido registrados',
                  'success'
                )
                var delayInMilliseconds = 2000; //1 second
                setTimeout(function() {
                    
                }, delayInMilliseconds);
            })
            .fail(function(result) {
                console.log(result);
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: result.responseJSON,
                })                
            })
            .always(function() {
                console.log('complete');
            });
        }
    });
    
    $(".wizard .actions ul li a").addClass("waves-effect waves-blue btn-flat");
    $(".wizard .steps ul").addClass("tabs z-depth-1");
    $(".wizard .steps ul li").addClass("tab");
    $('ul.tabs').tabs();
    $('select').material_select();
    $('.select-wrapper.initialized').prev( "ul" ).remove();
    $('.select-wrapper.initialized').prev( "input" ).remove();
    $('.select-wrapper.initialized').prev( "span" ).remove();
    
    /// Datepicker //
    $(function(){
	    $('.datepicker').pickadate({
            format: 'dd/mm/yyyy',        
	    	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	    	monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
	    	weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	    	weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
	    	selectMonths: true,
	    	selectYears: 15, // Puedes cambiarlo para mostrar más o menos años
	    	today: 'Hoy',
	    	clear: 'Limpiar',
	    	close: 'Ok',
	    	labelMonthNext: 'Siguiente mes',
	    	labelMonthPrev: 'Mes anterior',
	    	labelMonthSelect: 'Selecciona un mes',
	    	labelYearSelect: 'Selecciona un año',
	    });
    });    
    /// upload image file ///
    $('.dropify').dropify({
        messages: {
            'default': 'Arrastra y suelta un archivo aquí o haz click',
            'replace': 'Arrastra y suelta o haz click para reemplazar',
            'remove':  'Eliminar',
            'error':   'Ooops, Algo salió mal.'
        }
    });
</script> 
@endsection