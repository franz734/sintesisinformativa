@extends('delegado.master')
@section('css')
    
@endsection
@section('contenido')
<h1>{{$entidad}}</h1>
<!-- Numeros-->
<div class="row no-m-t no-m-b">
    <div class="col col s12 m12 l4">
        <a href="{{ url('/delegado/notas') }}" title="ver notas">
            <div class="card stats-card">
                <div class="card-content">                    
                    <span class="card-title">Total de Notas en Medios</span>
                    <span class="stats-counter"><span class="counter">{{$notasAnio}}</span><small>en 
                        @php
                            echo date('Y')
                        @endphp
                    </small></span>
                    <div class="percent-info green-text"><img class="miCardImg" src="{{URL::asset('assets/images/iconDoc.png')}}"></div>
                </div>
                
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </a>        
    </div>
    <div class="col col s12 m12 l4">
        <a href="{{ url('/delegado/incidencias') }}" title="ver incidencias">
            <div class="card stats-card">
                <div class="card-content">                    
                    <span class="card-title">Total de Incidencias</span>
                    <span class="stats-counter"><span class="counter">{{$totalIncidencias}}</span><small>Incidencias en 
                        @php
                            echo date('Y')
                        @endphp
                    </small></span>
                    <div class="percent-info green-text"><img class="miCardImg" src="{{URL::asset('assets/images/iconDoc.png')}}"></div>
                </div>
                
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </a>
    </div>
    <div class="col col s12 m12 l4">
        <a href="{{ url('/delegado/solicitudes') }}" title="ver solicitudes">
            <div class="card stats-card">
                <div class="card-content">                    
                    <span class="card-title">Solicitudes Pendientes</span>
                    <span class="stats-counter"><span class="counter">{{$totalSolicitudes}}</span>{{-- <small>Notas en 
                        @php
                            echo date('Y')
                        @endphp
                    </small> --}}</span>
                    <div class="percent-info green-text"><img class="miCardImg" src="{{URL::asset('assets/images/iconDoc.png')}}"></div>
                </div>
                
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </a>
    </div>
    <div class="col col s12 m12 l4">
        <a href="{{ url('/delegado/temasRelevantes') }}" title="ver temas relevantes">
            <div class="card stats-card">
                <div class="card-content">                    
                    <span class="card-title">Temas Relevantes</span>
                    <span class="stats-counter"><span class="counter">{{$totalTemasRel}}</span><small>Temas activos</small></span>
                    <div class="percent-info green-text"><img class="miCardImg" src="{{URL::asset('assets/images/iconDoc.png')}}"></div>
                </div>
                
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </a>
    </div>
    <div class="col col s12 m12 l4">
        <a href="{{ url('/delegado/notasInformativas') }}" title="ver temas relevantes">
            <div class="card stats-card">
                <div class="card-content">                    
                    <span class="card-title">Notas Informativas</span>
                    <span class="stats-counter"><span class="counter">{{$totalNotasInfo}}</span>{{-- <small>Temas activos</small> --}}</span>
                    <div class="percent-info green-text"><img class="miCardImg" src="{{URL::asset('assets/images/iconDoc.png')}}"></div>
                </div>
                
                <div class="progress stats-card-progress">
                    <div class="determinate" style="width: 100%"></div>
                </div>
            </div>
        </a>
    </div>
</div>
<!---->
    
@endsection
@section('scripts')
    
@endsection