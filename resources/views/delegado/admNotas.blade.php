@php
//dd(Auth::user()->roles[0]->name);
@endphp
@extends('delegado.master')
@section('css')

@endsection
@section('contenido')
    <h1>Notas</h1>
    <style>
        td.details-control {
            background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }

    </style>
    <input hidden id="idNota" type="text">
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableNotas" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Encabezado</th>
                            <th scope="col">Medio</th>
                            <th scope="col">Entidad</th>
                            <th scope="col">SubProcuraduría</th>
                            <th scope="col">Link</th>
                            <th scope="col">Fecha de Publicación</th>
                            {{-- <th scope="col">Fecha de Alta</th>
                        <th scope="col">Más</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Acción</th> --}}
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Structure -->
    {{-- <div id="modalSelect" class="modal">
    <div class="modal-content">
        <div class="input-field">                                            
            <select id="selectTipo" name="subProc" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                <option disabled selected value="def">Tipo</option>                                
                @foreach ($tipo as $tipo)
                    <option value={{$tipo->idTipo}}>{{$tipo->nomTipo}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#!" id="btnAsigTipo" class=" waves-effect waves-green btn-flat">Asignar</a>
    </div>
  </div> --}}
@endsection
@section('scripts')

@endsection
