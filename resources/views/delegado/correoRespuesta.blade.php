<p>Se ha dado respuesta a una solicitud de información del estado de {{$respuesta->entidadRel->nomEntidad}} con id = {{$respuesta->idRespuesta}}</p>
@if (!is_null($propuesta))
@php
    $txtProp = json_decode($propuesta->propuesta);
@endphp
    <p>Se propone dicha respuesta como {{$propuesta->tipoPropRel->nomTipoPropuesta}}</p>
    @if ($propuesta->idTipoPropuesta == 1)
        <p>{{$txtProp->texto}}.</p>
    @elseif ($propuesta->idTipoPropuesta == 2)
        <p><b>{{$txtProp->titulo}}</b></p>
        <p>{{$txtProp->texto}}</p>
    
    @endif

@endif