@php
//dd(url());
@endphp
<style>
    .side-nav.fixed{
    width: 240px !important;
}
</style>
<header class="mn-header navbar-fixed">
    <nav class="miNavHeader">
        <div class="nav-wrapper row">
            <section class="material-design-hamburger navigation-toggle">
                <a href="javascript:void(0)" data-activates="slide-out"
                    class="button-collapse show-on-large material-design-hamburger__icon">
                    <span class="material-design-hamburger__layer"></span>
                </a>
            </section>
            <div class="header-title col s3 m3">                
                <img src="{{ URL::asset('assets/images/Logo_gob.png') }}" height="45px">
            </div>
        </div>
    </nav>
</header>
<aside id="slide-out" class="side-nav white fixed">
    <div class="side-nav-wrapper">
        <div class="sidebar-profile">
            <div class="sidebar-profile-image">
                <img src="{{ URL::asset('assets/images/user.png') }}" class="circle" alt="">
            </div>
            <div class="sidebar-profile-info">                
                <p>{{-- {{ Auth::user()->name }} --}}</p>                
            </div>
        </div>
        <ul class="sidebar-menu collapsible collapsible-accordion" data-collapsible="accordion">
            {{-- ////////////////////////////////// --}}
            {{-- <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">account_balance</i>Jurídico<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>                        
                        <li><a href="#">Denuncias</a></li>                                       
                    </ul>
                </div>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">person_search</i>Inspección Industrial<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">                   
                </div>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">landscape</i>Recursos Naturales<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">                    
                </div>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">nature_people</i>Auditoría Ambiental<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">                   
                </div>
            </li> --}}
            {{-- <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">connect_without_contact</i>Comunicación Social<i class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{url('/delegado/inicioCS')}}">Inicio</a></li>
                        <li><a href="{{url('/delegado/nuevaIncidencia')}}">Reportar Incidencia</a></li>
                        <li><a href="{{url('/sintesisInformativa/inicio')}}" target="_blank">Ir a la Síntesis</a></li>                                       
                    </ul>
                </div>
            </li> --}}
            {{-- ////////////////////////////////// --}}
            <li class="no-padding">
                <a class="waves-effect waves-grey" href="{{ url('/delegado/inicioCS') }}"><i
                        class="material-icons">settings_input_svideo</i>Inicio</a>
            </li>
            <li class="no-padding">
                <a class="waves-effect waves-grey" href="{{ url('/delegado/nuevaIncidencia') }}"><i
                        class="material-icons">report</i>Reportar Incidencia</a>
            </li>
            <li class="no-padding">
                <a class="waves-effect waves-grey" href="{{ url('/delegado/nuevaNotaInformativa') }}"><i
                        class="material-icons">info_outline</i>Cargar Nota Informativa</a>
            </li>
            <li class="no-padding">
                <a class="collapsible-header waves-effect waves-grey"><i class="material-icons">visibility</i>Ver<i
                        class="nav-drop-icon material-icons">keyboard_arrow_right</i></a>
                <div class="collapsible-body">
                    <ul>
                        <li><a href="{{ url('/delegado/notas') }}">Notas en Medios</a></li>
                        <li><a href="{{ url('/delegado/notasInformativas') }}">Notas Informativas</a></li>
                        <li><a href="{{ url('/delegado/incidencias') }}">Incidencias</a></li>
                        <li><a href="{{ url('/delegado/solicitudes') }}">Solicitudes <br> pendientes</a></li>
                    </ul>
                </div>
            </li>            
            {{-- <li class="no-padding">
                <a class="waves-effect waves-grey" href="{{ url('/sintesisInformativa/inicio') }}" target="_blank"><i
                        class="material-icons">article</i>Ir a la Síntesis</a>
            </li> --}}                      
            <li class="no-padding">
                <a class="waves-effect waves-grey" href="{{ url('/delegado/temasRelevantes') }}"><i
                        class="material-icons">reorder</i>Temas Relevantes</a>
            </li>            
            <li class="divider"></li>
            <li class="no-padding">                
                <a class="waves-effect waves-grey" href="{{ route('logoutDeleg') }}" onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                    <i class="material-icons">exit_to_app</i>Salir{{-- </a> --}}
                </a>
                <form id="logout-form" action="{{ route('logoutDeleg') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</aside>
