@extends('delegado.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('assets/plugins/dropify/css/dropify.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Incidencia</h3>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <form id="incidenciaForm" action="">
                    <div class="row" id="incidenciaCont">                       
                        <div class="col s6">
                            {{-- <div class="col s12 m4 l8"> --}}                            
                            <div class="input-field">                                            
                                <select id="selectEdo" name="entidadFed" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option selected value="{{$entidad->entidadRel->idEntidad}}">{{$entidad->entidadRel->nomEntidad}}</option>                                                
                                    {{-- @foreach ($entidades as $entidad)
                                        <option value={{$entidad->idEntidad}}>{{$entidad->nomEntidad}}</option>
                                    @endforeach --}}
                                </select>
                            </div>                            
                            <div class="input-field">
                                <label for="tema">Tema</label>
                                <input id="tema" class="validar" name="tema" type="text" required>
                            </div>
                            <div class="input-field">                                            
                                <select id="selectMateria" name="materia" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Acción específica</option>                                                
                                    @foreach ($materias as $materia)
                                        <option value={{$materia->idMateria}}>{{$materia->nomMateria}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field">
                                <label for="fechaSuc">Fecha de suceso</label>
                                <input id="fechaSuc"  name="fechaSuc" type="text" class="datepicker validar" required>
                            </div>                            
                            {{-- <div class="input-field">
                                <label for="materia">Materia</label>
                                <input id="materia" name="materia" type="text" class="validar" required>
                            </div> --}}
                            <div class="input-field">                                            
                                <select id="selectArea" name="area" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Área</option>                                                
                                    @foreach ($areas as $area)
                                        <option value={{$area->idArea}}>{{$area->nomArea}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-field">                                            
                                <select id="selectSubArea" name="subArea" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Sub-Área</option>                                    
                                </select>
                            </div>
                            <div id="contSelAccVS" class="input-field" style="display: none">
                                <select name="accionVS" id="selectAccioVS" class="js-states browser-default validar" tabindex="-1" style="width: 100%">
                                    <option disabled selected value="def">Acción</option>
                                    <option value="aseguramiento">Aseguramiento Precautorio</option>
                                    <option value="avistamiento">Avistamiento</option>
                                    <option value="decomision">Decomiso</option>
                                    <option value="liberacion">Liberación</option>
                                    <option value="deposito">Deposito</option>
                                    <option value="devolucion">Devolución</option>
                                    <option value="monitoreo">Monitoreo</option>
                                    <option value="muerte">Muerte de Ejemplar</option>
                                    <option value="reunion">Reunión</option>
                                    <option value="traslado">Traslado</option>
                                </select>
                            </div>
                            <div class="input-field">
                                <label for="problematica">Antecedentes y/o problematica</label>
                                <textarea id="problematica" name="problematica" class="materialize-textarea validar" {{-- length="500" --}} required></textarea>                                
                            </div>                                                                                                                          
                            <div class="input-field">                            
                                <label for="texto">Actuación de la delegación PROFEPA</label>                                    
                                <br>
                                <br>                                
                                <div id="incidenciaTexto" class="mi_editor-container"></div> 
                                <input hidden id="textoHtml" class="validar" name="textoHtml" >
                                <input hidden id="texto" class="validar" name="texto" required>                            
                            </div>     
                            <div class="input-field">
                                <label for="acciones">Acciones a realizar</label>
                                <input id="acciones" name="acciones" type="text" class="validar" required>
                            </div>  
                        </div>  
                        <div class="col s6">
                            {{-- <div class="input-field">                            
                                <label for="texto">Actuación de la delegación PROFEPA</label>                                    
                                <br>
                                <br>                                
                                <div id="incidenciaTexto" class="mi_editor-container"></div> 
                                <input hidden id="textoHtml" class="validar" name="textoHtml" >
                                <input hidden id="texto" class="validar" name="texto" required>                            
                            </div> --}}
                            {{-- <div class="input-field">
                                <label for="acciones">Acciones a realizar</label>
                                <input id="acciones" name="acciones" type="text" class="validar" required>
                            </div> --}}
                            <div class="input-field">
                                <label for="resumen">Resumen</label>
                                <textarea id="resumen" name="resumen" class="materialize-textarea validar" {{-- length="500" --}} required></textarea>                                
                            </div>
                            <label for="evidencia">Multimedia</label>
                            <div class="input-field">                                                                                                            
                                <input id="evidencia" name="evidencia[]" class="dropify" type="file" multiple>
                            </div>
                            <br>
                            <label for="evidencia">Documentos</label>
                            <div class="input-field">                                                                                                            
                                <input id="documento" name="documento" class="dropify" type="file">
                            </div>
                            <br>
                            <div class="input-field">
                                <label for="propuesta">¿Propondría esta incidencia como tweet o boletín?</label>
                                <div class="switch m-b-md">
                                    <label>
                                        No
                                        <input id="propuestaS" type="checkbox" >
                                        <span class="lever"></span>
                                        Si
                                    </label>
                                </div>
                                <br>
                                <input hidden id="flgPropuesta" name="flgPropuesta" type="text" value="0">
                            </div>
                        </div>           
                    </div>
                    <div class="row propuesta" style="display: none">
                        <div class="col s12 m4 l2"></div> <!-- Offset izquierdo -->
                        <div class="col s12 m4 l8">
                            <div class="input-field">                                            
                                <select id="selectProp" name="propuesta" class="js-states browser-default" tabindex="-1" style="width: 100%" required>
                                    <option disabled selected value="def">Tipo</option>                                                
                                    @foreach ($tiposProp as $tipoProp)
                                        <option value={{$tipoProp->idTipoPropuesta}}>{{$tipoProp->nomTipoPropuesta}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="tipoTweet" style="display: none">
                                <div class="input-field">
                                    <label for="txtTweet">Tweet</label>
                                    <textarea id="txtTweet" {{-- name="txtTweet" --}} class="materialize-textarea {{-- validar --}}" length="140" required></textarea>                                
                                </div>
                            </div>
                            <div id="tipoBoletin" style="display: none">
                                <div class="input-field">
                                    <label for="titBoletin">Titulo</label>
                                    <input id="titBoletin" {{-- name="titBoletin" --}} type="text" {{-- class="validar" --}} required>
                                </div>
                                <div class="input-field">
                                    <label for="txtBoletin">Texto</label>
                                    {{-- <input id="txtBoletin" name="txtBoletin" type="text" class="validar" required> --}}
                                    <br>
                                    <br>                                
                                    <div id="txtBoletin" class="mi_editor-container"></div> 
                                    <input hidden id="textoHtmlBoletin" {{-- class="validar" name="textoHtmlBoletin" --}} >
                                    <input hidden id="textoBoletin" {{-- class="validar" name="textoBoletin" --}} required> 
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="row right-align">
                        <a id="guardaIncidecnia" class="waves-effect waves-light btn indigo miA">Enviar Incidencia</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/dropify/js/dropify.min.js')}}"></script>
    <script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
    <script>        
        /// Datepicker //
        $(function(){
	        $('.datepicker').pickadate({
                format: 'dd/mm/yyyy',        
	        	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	        	monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
	        	weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	        	weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
	        	selectMonths: true,
	        	selectYears: 15, // Puedes cambiarlo para mostrar más o menos años
	        	today: 'Hoy',
	        	clear: 'Limpiar',
	        	close: 'Ok',
	        	labelMonthNext: 'Siguiente mes',
	        	labelMonthPrev: 'Mes anterior',
	        	labelMonthSelect: 'Selecciona un mes',
	        	labelYearSelect: 'Selecciona un año',
	        });
        });

        // Initialize QUill editor
        var quillNota = new Quill('#incidenciaTexto', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quillNota.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quillNota.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtml').val(html);
                var contenido = $('#textoHtml').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#texto').val(texto);  
            }
        updateHtmlOutput_msj();

        /* // Initialize QUill editor boletin
        var quillBoletin = new Quill('#txtBoletin', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quillBoletin.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quillBoletin.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtmlBoletin').val(html);
                var contenido = $('#textoHtmlBoletin').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#textoBoletin').val(texto);  
            }
        updateHtmlOutput_msj(); */

        /// upload image file ///
        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz click',
                'replace': 'Arrastra y suelta o haz click para reemplazar',
                'remove':  'Eliminar',
                'error':   'Ooops, Algo salió mal.'
            }
        });
    </script>
@endsection