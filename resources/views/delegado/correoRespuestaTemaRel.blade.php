@php
    if($respuesta->entidadRel == null){
        $comp = ' correspondiente a ';
        $cad = 'Oficina Central';
    }
    else{
        $comp = ' del estado de ';
        $cad = $respuesta->entidadRel->nomEntidad;
    }
@endphp
<p>Se ha dado respuesta al tema <b>{{$respuesta->temaRelevanteRel->temaRel}}</b> {{$comp}} <b>{{$cad}}</b> </p>
