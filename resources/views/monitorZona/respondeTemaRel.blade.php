@extends('monitorZona.master')
@section('css')

@endsection
@section('contenido')
    <h3>Respuesta</h3>
    <div class="col s12 m12 l12">
        <div class="card">
            <div class="card-content">
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $tema->temaRel }}</blockquote>
                        <br>
                    </div>
                </div>
                <form id="respTemaRel">
                    <div class="row">
                        <input hidden id="idTema" name="idTema" class="validar" type="text" value="{{$tema->idTemaRel}}" required>
                        <div class="input-field col s12">
                            <textarea id="respuesta" name="respuesta" class="materialize-textarea validar" required></textarea>
                            <label for="respuesta">Respuesta</label>
                        </div>
                    </div>
                    <div class="row right-align">
                        <a id="guardaRespTemaRel" class="waves-effect waves-light btn indigo miA">Enviar Información</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
