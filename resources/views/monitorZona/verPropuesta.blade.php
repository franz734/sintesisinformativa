@extends('monitorZona.master')
@section('css')
    <link href="{{URL::asset('assets/plugins/quill/quill.snow.css')}}" rel="stylesheet" />
@endsection
@section('contenido')   
@php
    $propuesta->propuesta = Json_decode($propuesta->propuesta);
    if($propuesta->notaRel != null){
        $tema = $propuesta->notaRel->encabezado;
    }
    elseif ($propuesta->incidenciaRel != null) {
        $tema = $propuesta->incidenciaRel->tema;
    }
@endphp 
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5>Propuesta para {{$propuesta->tipoPropRel->nomTipoPropuesta}}</h5>                                
                <h5 style="color: #bc955c">{{$propuesta->entidadRel->nomEntidad}}</h5>
                <h5>{{$propuesta->areaRel->nomArea}}/{{$propuesta->subAreaRel->nomSubArea}}</h5>
                <h5>Tema: {{$tema}}</h5>
            </div>
        </div>
    </div>
    @if ($propuesta->idTipoPropuesta == 1) <!--Tweet-->
        <div class="row">        
            <div class="col s6">
                <div class="card white">
                    <div class="card-content center">                       
                        <div class="card notaCont">
                            <div class="card-coontent cont" {{-- style="text-align: justify" --}}>   
                                <br>                      
                                {{-- <h5><b>Tweet</b></h5> --}}       
                                <p>{{$propuesta->propuesta->texto}}</p>
                                <br>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            {{-- <div class="col s6">
                <div class="card white">
                    <div class="card-content">
                        <form id="editPropuestaForm" action="">
                            <div class="input-field">
                                <label for="txtTweet">Tweet</label>
                                <textarea id="txtTweet" name="txtTweet" class="materialize-textarea validar" length="140" required></textarea> 
                            </div>
                            <input hidden type="text" name="idPropuesta" value="{{$propuesta->idPropuesta}}">
                            <div class="row right-align">
                                <a id="modPropuesta" class="waves-effect waves-light btn indigo miA">Enviar a Revisión</a>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div> --}}
        </div>
    @elseif($propuesta->idTipoPropuesta == 2) <!--Boletín-->                     
        <div class="row">        
            <div class="col s6">
                <div class="card white">
                    <div class="card-content center">                       
                        <div class="card notaCont">
                            <div class="card notaCont">
                                <div class="card-coontent cont" style="text-align: left">   
                                    <br>                                                           
                                    <h5><b>{{$propuesta->propuesta->titulo}}</b></h5>
                                    <p>{!!$propuesta->propuesta->textHtml!!}</p>
                                    <br>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
            </div>
            {{-- <div class="col s6">
                <div class="card white">
                    <div class="card-content">
                        <form id="editPropuestaForm" action="">
                            <div class="input-field">
                                <label for="titBoletin">Titulo</label>
                                <input id="titBoletin" name="titBoletin" type="text" class="validar" required>
                            </div>
                            <div class="input-field">
                                <label for="txtBoletin">Texto</label>                                
                                <br>
                                <br>                                
                                <div id="txtBoletin" class="mi_editor-container"></div> 
                                <input hidden id="textoHtmlBoletin" class="validar" name="textoHtmlBoletin" >
                                <input hidden id="textoBoletin" class="validar" name="textoBoletin" required> 
                                <input hidden type="text" name="idPropuesta" value="{{$propuesta->idPropuesta}}">
                            </div>    
                            <br>                        
                            <div class="row right-align">
                                <a id="modPropuesta" class="waves-effect waves-light btn indigo miA">Enviar a Revisión</a>
                            </div> 
                        </form>
                    </div>
                </div>
            </div> --}}
        </div>
    @endif
@endsection
@section('scripts')
    <script src="{{URL::asset('assets/plugins/quill/quill.min.js')}}"></script>
    <script>
        // Initialize QUill editor boletin
        var quillBoletin = new Quill('#txtBoletin', {
          modules: {
            toolbar: [
              [{ header: [1, 2, 3, 4, 5, 6,  false] }],
              ['bold', 'italic', 'underline','strike'],
              [{ 'color': [] }, { 'background': [] }],
              ['link'],
              [{ 'script': 'sub'}, { 'script': 'super' }],
              [{ 'list': 'ordered'}, { 'list': 'bullet' }], 
                       
            ]
          },      
          theme: 'snow'  // or 'bubble'
        });      
        $('.ql-snow select').addClass('browser-default');  
        quillBoletin.on('text-change', function(delta, source) {
            updateHtmlOutput_msj()
        })        
        $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })        
            function getQuillHtml_msj() { return quillBoletin.root.innerHTML; }
            function updateHtmlOutput_msj()
            {
                let html = getQuillHtml_msj();
                //console.log ( html );            
                $('#textoHtmlBoletin').val(html);
                var contenido = $('#textoHtmlBoletin').val();
                var texto = contenido.replace(/<[^>]*>?/g, '');
                $('#textoBoletin').val(texto);  
            }
        updateHtmlOutput_msj();
    </script>
@endsection