@php
    if($respuestas[0]->entidadRel == null){        
        $cad = 'Oficina Central';
    }
    else{        
        $cad = $respuestas[0]->entidadRel->nomEntidad;
    }
@endphp
@extends('monitorZona.master')
@section('css')
<link href="{{URL::asset('assets/plugins/datepicker/datepicker.css')}}" rel="stylesheet" />
@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5 style="color: #bc955c">{{ $cad }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Tema</b></h5>
                        <blockquote>{{ $respuestas[0]->temaRelevanteRel->temaRel }}</blockquote>
                        <div class="right-align" style="padding-right: 10px">
                            <a class="waves-effect waves-light btn" style="background-color: #9f2241"
                                title="Solicitar más información"
                                onclick="masInfoTema({{ $respuestas[0]->temaRelevanteRel->idTemaRel }},'temaRel')">
                                <i class="material-icons">info</i>
                            </a>
                        </div>
                        <br>
                    </div>
                </div>
                {{-- <div class="card notaCont">
                <div class="card-coontent cont">   
                    <br>                      
                    <h5><b>Área</b></h5>       
                    <blockquote>{{$respuesta->areaRel->nomArea.'/'.$respuesta->subAreaRel->nomSubArea}}</blockquote>
                    <br>
                </div>
            </div> --}}
                @foreach ($respuestas as $respuesta)
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Respuesta</b></h5>
                            <p>{{$respuesta->fecha}}</p>
                            <blockquote>{{ $respuesta->respuesta }}</blockquote>
                            <br>
                            <div class="right-align" style="padding-right: 10px">
                                <a class="waves-effect waves-light btn" style="background-color: #235b4e"
                                    title="Enviar por Correo"
                                    onclick="enviaRespuesta({{ $respuesta->idRespuestaTemaRel }})">
                                    <i class="material-icons">email</i>
                                </a>
                            </div>
                            <br>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{URL::asset('assets/plugins/datepicker/datepicker.js')}}"></script>
<script src="{{URL::asset('assets/plugins/datepicker/datepicker.es-ES.js')}}"></script>
@endsection
