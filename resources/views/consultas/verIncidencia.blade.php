@php
//dd($incidencia, $cel);
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4>Incidencia No. {{ $incidencia->idIncidencia }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4 style="color: #bc955c">{{ $incidencia->entidadRel->nomEntidad }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: right;">
                                    <h4>{{ $incidencia->fcSuceso }}</h4>
                                </div>
                            </div>

                            <br>
                            <h4><b>Tema</b></h4>
                            <blockquote class="quote-secondary">{{ $incidencia->tema }}</blockquote>
                            {{-- <br>
                            <h5><b>Área</b></h5>
                            <blockquote>{{ $incidencia->areaRel->nomArea . '/' . $incidencia->subAreaRel->nomSubArea }}
                            </blockquote> --}}
                            <br>
                            <h4><b>Resumen</b></h4>
                            <blockquote class="quote-secondary">{{ $incidencia->resumen }}</blockquote>
                            <br>
                            <h4><b>Antecedentes y/o Problematica</b></h4>
                            <blockquote class="quote-secondary">{{ $incidencia->problematica }}</blockquote>
                            <br>
                            <h4><b>Actuación de la Delegación PROFEPA</b></h4>
                            <blockquote class="quote-secondary">{!! $incidencia->actuacionHtml !!}</blockquote>
                            <br>
                            <h4><b>Acciones a Realizar</b></h4>
                            <blockquote class="quote-secondary">{!! $incidencia->acciones !!}</blockquote>
                            <br>
                            @if (!is_null($incidencia->evidencia))
                                <div class="card notaCont">
                                    <div class="card-coontent cont">
                                        <br>
                                        <h4><b>Multimedia</b></h4>
                                        {{-- @php
                                    $ctl = 0;
                                @endphp
                                @foreach ($incidencia->evidencia as $img)
                                    @php
                                        if ($ctl % 2 == 0) {
                                            $inicio = '<div class="row text-center">';
                                            $fin = '';
                                        } else {
                                            $inicio = '';
                                            $fin = '</div><br>';
                                        }
                                        $ctl++;
                                    @endphp
                                    {!! $inicio !!}                                    
                                    <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">                                    
                                    {!! $fin !!}
                                @endforeach
                                <br>
                                <br> --}}
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                @php
                                                    $ctl = 0;
                                                @endphp
                                                @foreach ($incidencia->evidencia as $img)
                                                    @if ($ctl == 0)
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}" class="active"></li>
                                                    @else
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}"></li>
                                                    @endif
                                                    @php
                                                        $ctl++;
                                                    @endphp
                                                @endforeach
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="container">
                                                <div class="carousel-inner" role="listbox">
                                                    @php
                                                        $ctl = 0;
                                                    @endphp
                                                    @foreach ($incidencia->evidencia as $img)
                                                        @php
                                                            $img = str_replace('Queru00e9taro', 'Querétaro', $img);
                                                            $img = str_replace('Michoacu00e1n', 'Michoacán', $img);
                                                            $img = str_replace('NuevoLeu00f3n', 'NuevoLeón', $img);
                                                            $img = str_replace('Mu00e9xico', 'México', $img);
                                                            $img = str_replace('Yucatu00e1n', 'Yucatán', $img);
                                                        @endphp
                                                        @if ($ctl == 0)
                                                            <div class="item active">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="item">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @php
                                                            $ctl++;
                                                        @endphp
                                                    @endforeach
                                                </div>
                                            </div>
                                            <!-- Controls -->
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="float-right">
                                <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                    style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                    Llamar</a>
                                <a class="btn btnBck" onclick="comparteInfo({{ $incidencia->idIncidencia }}, 'incidencia')"
                                    style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                    compartir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
