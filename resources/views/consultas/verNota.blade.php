@php
//dd($nota, $respuesta, $cel);
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{--  --}}
                            <h4>{{ $nota->encabezado }}</h4>
                            <blockquote class="quote-secondary">
                                <p>{{ $nota->texto }}</p>
                            </blockquote>
                            {{--  --}}
                            <div class="float-right">
                            @if (!is_null($respuesta))
                                {{-- <div class="float-right"> --}}
                                    <a class="btn btnBck"
                                        href="{{ '/consultas/verRespuesta/' . $respuesta->idRespuesta }}"
                                        style="background-color: #235b4e"><i class="far fa-bookmark"></i>&nbsp; Ver
                                        Respuesta</a>
                                {{-- </div> --}}
                            @else
                                {{-- <div class="float-right"> --}}
                                    @if ($nota->idEntidad != 33)
                                        <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                            style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                            Llamar</a>
                                        {{-- </div> --}}
                                    @endif
                                    {{-- <div class="float-right"> --}}
                                    <a class="btn btnBck" onclick="solInfo({{ $nota->idNota }},'nota')"
                                        style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp; Solicitar
                                        info.</a>

                                {{-- </div> --}}
                            @endif
                            {{-- <div class="float-right" style="padding-right: 0.5%"> --}}
                                <a class="btn btnBck" onclick="comparteInfo({{ $nota->idNota }}, 'nota')"
                                    style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                    compartir</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
