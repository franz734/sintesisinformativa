@php
//dd($incidencias);
$presentaciones = $presentaciones->data;
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{--  --}}
                            <table id="tablePresentaciones" class="table table-bordered table-striped">
                                <thead>
                                    <tr>                                        
                                        <th>Titulo</th>                                                                                
                                        <th>Archivo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($presentaciones as $presentacion)
                                        <tr>                                            
                                            <td>{{ $presentacion->titulo }}</td>                                                                                        
                                            <td class="icon-td">                                                
                                                <a type="button" href="{{'/'.$presentacion->pathFile}}">
                                                        <i class="fas fa-file-powerpoint"></i>
                                                    </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{--  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //creaTableNotas(dataNotas);
        //console.log(dataNotas);
        $('#tablePresentaciones').DataTable({
            language: spanish,
            ordering:false,
            responsive: {                
                details: {                    
                }
            },
            lengthMenu: [10, 25, 50, 100]
        });
    </script>
@endsection
