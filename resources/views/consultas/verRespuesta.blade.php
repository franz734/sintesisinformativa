@php
//dd($respuesta, $cel);
if ($respuesta->subAreaRel == null) {
    $cadSubArea = '';
} else {
    $cadSubArea = $respuesta->subAreaRel->nomSubArea;
}
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4>Respuesta No. {{ $respuesta->idRespuesta }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4 style="color: #bc955c">{{ $respuesta->entidadRel->nomEntidad }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: right;">
                                    <h4>{{ $respuesta->fcAlta }}</h4>
                                </div>
                                <div class="col-lg-12">
                                    @if ($respuesta->docRespuesta != '')
                                        <a href="{{ '../../' . $respuesta->docRespuesta }}" target="_blanck">Ver
                                            Documento</a>
                                    @endif
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Tema</b></h5>
                                    <blockquote class="quote-secondary">{{ $respuesta->notaRel->encabezado }}
                                    </blockquote>
                                    <br>
                                    <div class="float-right"
                                        style="padding-bottom: 5px !important; padding-right: 5px !important">
                                        @if ($respuesta->idEntidad != 33)
                                            <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                                style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                                Llamar</a>
                                            {{-- </div> --}}
                                        @endif
                                        <a class="btn btnBck" onclick="solInfo({{ $respuesta->idNota }},'nota')"
                                            style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                            Solicitar info.</a>
                                        <a class="btn btnBck" onclick="comparteInfo({{ $respuesta->idRespuesta }}, 'respSolInfo')"
                                            style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                            compartir</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Área</b></h5>
                                    <blockquote class="quote-secondary">
                                        {{ $respuesta->areaRel->nomArea . '/' . $cadSubArea }}</blockquote>
                                    <br>
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Resumen</b></h5>
                                    <blockquote class="quote-secondary">{{ $respuesta->resumen }}</blockquote>
                                    <br>
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Antecedentes y/o Problematica</b></h5>
                                    <blockquote class="quote-secondary">{{ $respuesta->notaRel->texto }}</blockquote>
                                    <br>
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Actuación de la Delegación PROFEPA</b></h5>
                                    <blockquote class="quote-secondary">{!! $respuesta->actuacionHtml !!}</blockquote>
                                    <br>
                                </div>
                            </div>
                            <div class="card notaCont">
                                <div class="card-coontent cont">
                                    <br>
                                    <h5><b>Acciones a Realizar</b></h5>
                                    <blockquote class="quote-secondary">{!! $respuesta->acciones !!}</blockquote>
                                    <br>
                                </div>
                            </div>
                            @if (!is_null($respuesta->evidencia))
                                <div class="card notaCont">
                                    <div class="card-coontent cont">
                                        <br>
                                        <h5><b>Multimedia</b></h5>
                                        {{-- @php
                                            $ctl = 0;
                                        @endphp
                                        @foreach ($respuesta->evidencia as $img)
                                            @php
                                                if ($ctl % 2 == 0) {
                                                    $inicio = '<div class="row center-align">';
                                                    $fin = '';
                                                } else {
                                                    $inicio = '';
                                                    $fin = '</div><br>';
                                                }
                                                $ctl++;
                                            @endphp
                                            {!! $inicio !!}
                                            <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">
                                            {!! $fin !!}
                                        @endforeach
                                        <br>
                                        <br> --}}
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                @php
                                                    $ctl = 0;
                                                @endphp
                                                @foreach ($respuesta->evidencia as $img)
                                                    @if ($ctl == 0)
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}" class="active"></li>
                                                    @else
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}"></li>
                                                    @endif
                                                    @php
                                                        $ctl++;
                                                    @endphp
                                                @endforeach
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="container">
                                                <div class="carousel-inner" role="listbox">
                                                    @php
                                                        $ctl = 0;
                                                    @endphp
                                                    @foreach ($respuesta->evidencia as $img)
                                                        @if ($ctl == 0)
                                                            <div class="item active">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="item">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @php
                                                            $ctl++;
                                                        @endphp
                                                    @endforeach
                                                </div>
                                            </div>
                                            <!-- Controls -->
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
