<!-- /.content-wrapper -->
  {{-- -------------- --}}
  
<!-- jQuery -->
<script src="{{URL::asset('assetsM/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{URL::asset('assetsM/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{URL::asset('assetsM/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{URL::asset('assetsM/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{URL::asset('assetsM/plugins/sparklines/sparkline.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{URL::asset('assetsM/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{URL::asset('assetsM/plugins/moment/moment.min.js')}}"></script>
<script src="{{URL::asset('assetsM/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
{{-- <script src="{{URL::asset('assetsM/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script> --}}
<script src="{{URL::asset('assetsM/plugins/bootstrapS/js/bootstrap.js')}}"></script>
<!-- Summernote -->
<script src="{{URL::asset('assetsM/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{URL::asset('assetsM/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- Select2 -->
<script src="{{URL::asset('assetsM/plugins/select2/js/select2.full.min.js')}}""></script>
<!-- DataTables -->
<script src="{{URL::asset('assetsM/plugins/datatables/jquery.dataTables.min.js')}}""></script>
<script src="{{URL::asset('assetsM/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}""></script>
<script src="{{URL::asset('assetsM/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}""></script>
<script src="{{URL::asset('assetsM/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}""></script>
<!-- Sweetalert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!-- Parsley -->
<script src="{{URL::asset('assetsM/plugins/parsleyjs/parsley.min.js')}}"></script>
<script src="{{URL::asset('assetsM/plugins/parsleyjs/i18n/es.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::asset('assetsM/js/adminlte.js')}}""></script>
<!-- AdminLTE for demo purposes -->
<script src="{{URL::asset('assetsM/js/demo.js')}}"></script>
{{-- Custom --}}
<script src="{{URL::asset('assetsM/js/custom.js')}}"></script>
@yield('scripts')