@php
if (!empty($respuestas[0])) {
    if ($respuestas[0]->entidadRel == null) {
        $cad = 'Oficina Central';
    } else {
        $cad = $respuestas[0]->entidadRel->nomEntidad;
    }
}

@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            @if (empty($respuestas[0]))
                                <h3>Cargando Informacion</h3>
                                <div class="float-right">
                                    @if ($tema->idEntidad != 33)
                                        <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                            style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                            Llamar</a>
                                        {{-- </div> --}}
                                    @endif
                                    {{-- <div class="float-right"> --}}
                                    <a class="btn btnBck" onclick="solInfo({{ $tema->idTemaRel }},'temaRel')"
                                        style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp; Solicitar
                                        info.</a>
                                </div>
                            @else
                                <h4 style="color: #bc955c">{{ $cad }}</h4>
                                <h4><b>Tema</b></h4>
                                <blockquote class="quote-secondary">{{ $respuestas[0]->temaRelevanteRel->temaRel }}
                                </blockquote>
                                <div class="float-right">
                                    @if ($tema->idEntidad != 33)
                                        <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                            style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                            Llamar</a>
                                        {{-- </div> --}}
                                    @endif
                                    {{-- <div class="float-right"> --}}
                                    <a class="btn btnBck" onclick="solInfo({{ $tema->idTemaRel }},'temaRel')"
                                        style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp; Solicitar
                                        info.</a>
                                </div>
                                <br>
                                <br>
                                <br>
                                @foreach ($respuestas as $respuesta)
                                    <div class="card-body notaCont">
                                        <h4><b>Respuesta</b></h4>
                                        <p>{{ $respuesta->fecha }}</p>
                                        <blockquote class="quote-secondary">{{ $respuesta->respuesta }}</blockquote>
                                        <div class="float-right">
                                            <a class="btn btnBck" onclick="comparteInfo({{ $respuesta->idRespuestaTemaRel }}, 'respTemaRel')"
                                                style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                                compartir</a>
                                        </div>
                                    </div>                                    
                                    <br>                                    
                                @endforeach                                
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
