@php
$ctl = 0;
$ctl2 = 0;
//dd($normas[0]->data->data);
@endphp
@extends('consultas.master')
@section('css')
    <style>
        .nav-tabs .nav-link.active,
        .nav-tabs .nav-item.show .nav-link {            
            background-color: antiquewhite !important;        
        }

    </style>

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <!-- Nav tabs -->                            
                            <ul class="nav nav-tabs">
                                @foreach ($normas as $item)
                                    @php
                                        $active = $ctl == 0 ? ' active' : '';
                                    @endphp
                                    <li class="nav-item">
                                        <a class="nav-link{{ $active }}" data-toggle="tab"
                                            href="#ley{{ $item->idLey }}">{{ $item->ley }}: <b>{{$item->cant}}</b></a>
                                    </li>
                                    @php
                                        $ctl++;
                                    @endphp
                                @endforeach
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                @foreach ($normas as $item)
                                    @php
                                        $active2 = $ctl2 == 0 ? ' active' : '';
                                    @endphp
                                    <div id="ley{{ $item->idLey }}" class="container tab-pane{{ $active2 }}"><br>
                                        <h6>{{ $item->ley }}</h6>
                                        <table id="" class="table table-bordered table-striped tableNormas" style="width: 100%">
                                            <thead>
                                                <tr>                                                    
                                                    <th>Articulo</th>                                        
                                                    <th >Fracción</th>
                                                    <th >Inciso</th>
                                                    <th>Contenido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {{-- @foreach ($normas as $item) --}}
                                                    @foreach ($item->data->data as $data)
                                                        <tr>
                                                            <td>{{ $data->articulo }}</td>
                                                            <td>{{ $data->fraccion }}</td>                                            
                                                            <td>{{ $data->inciso }}</td>
                                                            <td>{{ str_replace('&quot;','',$data->contenido) }}</td>

                                                        </tr>
                                                    @endforeach
                                                {{-- @endforeach --}}
                                            </tbody>
                                        </table>
                                    </div>
                                    @php
                                        $ctl2++;
                                    @endphp
                                @endforeach
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
@section('scripts')
<script>
    //creaTableNotas(dataNotas);
    //console.log(dataNotas);
    $('.tableNormas').DataTable({
        language: spanish,
        ordering:false,
        responsive:true,
        responsive: {                
            details: {                    
            }
        },
        lengthMenu: [10, 25, 50, 100]
    });
</script>
@endsection
