<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <title>SINTESIS INFORMATIVA</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">    
    <!-- Tempusdominus Bbootstrap 4 -->    
    {{-- <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}"> --}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assetsM/plugins/bootstrapS/css/bootstrap.css') }}">
    <!-- iCheck -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- JQVMap -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/jqvmap/jqvmap.min.css')}}">
    <!-- Theme style -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/css/adminlte.css')}}">
    <!-- overlayScrollbars -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- Daterange picker -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/daterangepicker/daterangepicker.css')}}">
    <!-- summernote -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">    
    <!-- Select2 -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/select2/css/select2.min.css')}}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assetsM/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <!-- custom -->    
    <link rel="stylesheet" href="{{URL::asset('assetsM/css/custom.css')}}">
    @yield('css')
</head>
