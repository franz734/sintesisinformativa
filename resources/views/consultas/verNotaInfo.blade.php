@php
if ($nota->subAreaRel == null) {
    $cadSubArea = '';
} else {
    $cadSubArea = $nota->subAreaRel->nomSubArea;
}
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4>{{ $nota->encabezado }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <h4 style="color: #bc955c">{{ $nota->entidadRel->nomEntidad }}</h4>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12" style="text-align: right;">
                                    <h4>{{ $nota->fcSuceso }}</h4>
                                </div>
                            </div>

                            <br>
                            <h4><b>Contenido</b></h4>
                            <blockquote class="quote-secondary">{{ $nota->texto }}</blockquote>                            
                            <h4><b>Área</b></h4>
                            <blockquote class="quote-secondary">{{ $nota->areaRel->nomArea . '/' . $cadSubArea }}</blockquote>
                            <br>
                            @if (!is_null($nota->imgNota))
                                <div class="card notaCont">
                                    <div class="card-coontent cont">
                                        <br>
                                        <h4><b>Multimedia</b></h4>
                                        {{-- @php
                                    $ctl = 0;
                                @endphp
                                @foreach ($incidencia->evidencia as $img)
                                    @php
                                        if ($ctl % 2 == 0) {
                                            $inicio = '<div class="row text-center">';
                                            $fin = '';
                                        } else {
                                            $inicio = '';
                                            $fin = '</div><br>';
                                        }
                                        $ctl++;
                                    @endphp
                                    {!! $inicio !!}                                    
                                    <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">                                    
                                    {!! $fin !!}
                                @endforeach
                                <br>
                                <br> --}}
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                @php
                                                    $ctl = 0;
                                                @endphp
                                                @foreach ($nota->imgNota as $img)
                                                    @if ($ctl == 0)
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}" class="active"></li>
                                                    @else
                                                        <li data-target="#carousel-example-generic"
                                                            data-slide-to="{{ $ctl }}"></li>
                                                    @endif
                                                    @php
                                                        $ctl++;
                                                    @endphp
                                                @endforeach
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="container">
                                                <div class="carousel-inner" role="listbox">
                                                    @php
                                                        $ctl = 0;
                                                    @endphp
                                                    @foreach ($nota->imgNota as $img)
                                                        @php
                                                            $img = str_replace('Queru00e9taro', 'Querétaro', $img);
                                                            $img = str_replace('Michoacu00e1n', 'Michoacán', $img);
                                                            $img = str_replace('NuevoLeu00f3n', 'NuevoLeón', $img);
                                                            $img = str_replace('Mu00e9xico', 'México', $img);
                                                            $img = str_replace('Yucatu00e1n', 'Yucatán', $img);
                                                        @endphp
                                                        @if ($ctl == 0)
                                                            <div class="item active">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="item">
                                                                <img src="{{ URL::asset($img) }}" {{-- width="300" height="300" --}}
                                                                    alt="...">
                                                                <div class="carousel-caption">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        @php
                                                            $ctl++;
                                                        @endphp
                                                    @endforeach
                                                </div>
                                            </div>
                                            <!-- Controls -->
                                            <a class="left carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control" href="#carousel-example-generic" role="button"
                                                data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="float-right">
                                <a class="btn btnBck" href="tel:+52{{ $cel }}"
                                    style="background-color: #98989A"><i class="fas fa-phone-square"></i>&nbsp;
                                    Llamar</a>
                                <a class="btn btnBck" onclick="comparteInfo({{ $nota->idNotaInformativa }}, 'notaInfo')"
                                    style="background-color: #6f7271"><i class="fas fa-info-circle"></i>&nbsp;
                                    compartir</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
