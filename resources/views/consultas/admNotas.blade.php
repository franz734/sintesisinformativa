@php
//dd($notas);
//$notas = json_encode($notas);
$notas = $notas->data;
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{--  --}}
                            <table id="tableNotas" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        <th>Encabezado</th>
                                        {{-- <th>Texto</th> --}}
                                        <th class="cell-hide">Entidad</th>
                                        <th class="cell-hide">Fecha Publicación</th>
                                        <th>Link</th>
                                        <th>Detalle</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($notas as $nota)
                                        <tr>
                                            {{-- <td>{{ $nota->idNota }}</td> --}}
                                            <td>{{ $nota->encabezado }}</td>
                                            {{-- <td>{{$nota->texto}}</td> --}}
                                            <td class="cell-hide">{{ $nota->nomEntidad }}</td>
                                            <td class="cell-hide">{{ $nota->fcPublicacion }}</td>
                                            <td class="icon-td">
                                                <a href="{{ $nota->link }}" target="_blank"><i class="fas fa-link"></i></a>
                                            </td>
                                            <td class="icon-td">
                                                <input hidden id="idNota" type="text" value="{{ $nota->idNota }}">
                                                <a type="button" onclick="detallesNota({{ $nota->idNota }})"><i class="fas fa-info-circle"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{--  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //creaTableNotas(dataNotas);
        //console.log(dataNotas);
        $('#tableNotas').DataTable({
            language: spanish,
            ordering:false,
            responsive: {                
                details: {                    
                }
            },
            lengthMenu: [10, 25, 50, 100]
        });

    </script>
@endsection
