<!DOCTYPE html>
<html lang="en" style="background: #F4F6F9 !important">
@include('consultas.header')

<body class="hold-transition layout-top-nav" style="background: #F4F6F9 !important">
    <div class="wrapper">
        @include('consultas.navbar')
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container">
                    <div class="row mb-2">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                            <div>
                                <img src="{{ URL::asset('/assets/images/logo_profepa.png') }}" style="max-width: 200px; width: 100%">
                            </div>
                        </div><!-- /.col -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                            <div style="text-align: right;">
                                <div>
                                    <a class=" btn btn-secondary" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->
            @yield('contenido')
        </div>
        <footer class="main-footer miPleca">

        </footer>
    </div>
    @include('consultas.footer')
</body>

</html>
