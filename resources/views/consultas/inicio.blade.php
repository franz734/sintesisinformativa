@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="">
                                <label for="keyWord">Ingresa una palabra o tema para comenzar la búsqueda</label>
                                <div class="input-group input-group">
                                    <input id="keyWord" name="keyWord" type="text" class="form-control validar" data-parsley-errors-container="#errCont" required>
                                    <span class="input-group-append">
                                        <a id="buscaKeyWord" class="btn btn-info btn-flat miBtn">Buscar</a>
                                    </span>
                                </div>
                                <div id="errCont" class="errorBlock"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Info Obtenida --}}
            <div class="row" id="contResult" style="display: none">
                <div class="col-md-12">
                    <p>Resultados para: <b><span id="labelKeyWord" style="color: #bc955c"></span></b></p>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordNotas" class="keyWordS" type="text">
                            <a id="aNotas">
                                <p><b>Notas en Medios:</b> <span id="countNotas"></span></p>
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordNotasInfo" class="keyWordS" type="text">
                            <a id="aNotasInfo">
                                <p><b>Notas Informativas:</b> <span id="countNotasInfo"></span></p>
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordIncidencias" class="keyWordS" type="text">
                            <a id="aIncidencias">
                                <p><b>Incidencias:</b> <span id="countIncidencias"></span></p>
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordTemasRel" class="keyWordS" type="text">
                            <a id="aTemasRel">
                                <p><b>Temas Relevantes:</b> <span id="countTemasRel"></span></p>
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordNormas" class="keyWordS" type="text">
                            <a id="aNormas">
                                <p><b>Normatividad:</b> <span id="countNormas"></span></p>
                            </a>
                        </div>
                    </div>
                    <div class="card">
                        <div class="aSearch">
                            <input hidden id="keyWordPresentaciones" class="keyWordS" type="text">
                            <a id="aPresentaciones">
                                <p><b>Presentaciones:</b> <span id="countPresentaciones"></span></p>
                            </a>
                        </div>
                    </div>                                        
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
