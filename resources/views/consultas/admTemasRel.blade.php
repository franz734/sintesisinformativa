@php
$temasRel = $temasRel->data;
@endphp
@extends('consultas.master')
@section('css')

@endsection
@section('contenido')
    <div class="content">
        <div class="container">
            <a class="btn btnBck" style="background-color: #9f2241" onclick="goBack()"><i class="fas fa-arrow-left"></i>
                Regresar</a>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            {{--  --}}
                            <table id="tableIncidencias" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th>ID</th> --}}
                                        <th>Tema</th>
                                        <th class="cell-hide">Entidad</th>
                                        {{-- <th class="cell-hide">Fecha de Suceso</th> --}}
                                        <th>Ver</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($temasRel as $tema)
                                        <tr>
                                            {{-- <td>{{ $tema->idTemaRel }}</td> --}}
                                            <td>{{ $tema->temaRel }}</td>
                                            <td class="cell-hide">{{ $tema->nomEntidad }}</td>
                                            {{-- <td class="cell-hide">{{ $tema->fcSuceso }}</td> --}}
                                            <td class="icon-td">
                                                <input hidden id="idIncidencia" type="text"
                                                    value="{{ $tema->idTemaRel }}">
                                                <a type="button" onclick="detallesTemaRel({{ $tema->idTemaRel }})">
                                                    <i class="fas fa-info-circle"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{--  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        //creaTableNotas(dataNotas);
        //console.log(dataNotas);
        $('#tableIncidencias').DataTable({
            language: spanish,
            ordering: false,
            responsive: {
                details: {}
            },
            lengthMenu: [10, 25, 50, 100]
        });

    </script>
@endsection
