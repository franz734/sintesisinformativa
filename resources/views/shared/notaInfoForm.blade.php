@php
$uri = Route::current();
$prfx = getPrefix($uri->uri);
$idEntidad = '';
$nomEntidad = '';
if($prfx == 'delegado'){
    $mail = Auth::user()->email;
    $delegacion = getEntidad($mail);
    $idEntidad = $delegacion->idEntidad;
    $nomEntidad = trim($delegacion->entidadRel->nomEntidad);
}
@endphp
@extends($prfx.'.master')
@section('css')
    <link href="{{ URL::asset('assets/plugins/dropify/css/dropify.css') }}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Nota Informativa</h3>
    @hasanyrole ('MonitorZona|Delegado')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    @endrole
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <form id="notasInfoForm" action="">
                    <div>
                        <section>
                            <div id="contNota">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">
                                            <label for="encabezado">Titulo</label>
                                            <input id="encabezado" class="validar" name="encabezado" type="text" required>
                                        </div>
                                        <div class="input-field">
                                            <label for="texto">Texto</label>
                                            <textarea id="texto" name="texto" class="materialize-textarea validar"
                                                required></textarea>
                                        </div>
                                        {{-- <div class="input-field">
                                            <label for="resumen">Resumen</label>
                                            <textarea id="resumen" name="resumen" class="materialize-textarea validar"
                                                required></textarea>
                                        </div> --}}
                                        <div class="input-field">
                                            <label for="fechaSuc">Fecha de suceso</label>
                                            <input id="fechaSuc" name="fechaSuc" type="text" class="datepicker validar"
                                                required>
                                        </div>
                                        <div class="input-field">
                                            <select id="selectEdo" name="entidadFed"
                                                class="js-states browser-default validar edoNotaAPI sel2" tabindex="-1"
                                                style="width: 100%" required>                                                
                                                @if ($idEntidad == '')
                                                    <option disabled selected value="def">Entidad Federativa</option>
                                                    @foreach ($entidades as $entidad)
                                                        <option value={{ $entidad->idEntidad }}>{{ $entidad->nomEntidad }}
                                                        </option>
                                                    @endforeach
                                                @else
                                                    <option value={{ $idEntidad }}>{{ $nomEntidad }}
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="input-field">
                                            <select id="selectSubProc" name="subProc"
                                                class="js-states browser-default anid validar sel2" tabindex="-1"
                                                style="width: 100%" required>
                                                <option disabled selected value="def">SubProcuraduría</option>
                                                @foreach ($areas as $area)
                                                    <option value={{ $area->idArea }}>{{ $area->nomArea }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="input-field contSA">
                                        </div>
                                        <label for="imgNota">Multimedia</label>
                                        <div class="input-field">
                                            <input id="imgNota" name="imgNota[]" class="dropify" type="file" multiple
                                                data-allowed-file-extensions="png jpg jpeg">
                                        </div>
                                        {{-- <div>
                                        <br>
                                        <br>
                                        <p>Categoria de la nota:</p>
                                        <br>
                                        <div class="contSemf">
                                            <p class="p-v-xs option option-1">
                                                <input class="validar miRadio" name="categoria" type="radio" value="verde"
                                                    id="verde" data-parsley-errors-container="#cbErrorContainerRadio"
                                                    required checked />
                                                <label for="verde" style="color: black">Verde</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                            <p class="p-v-xs option option-2">
                                                <input class="validar miRadio" name="categoria" type="radio" value="amarillo"
                                                    id="amarillo" data-parsley-errors-container="#cbErrorContainerRadio"
                                                    required />
                                                <label for="amarillo" style="color: black">Amarillo</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                            <p class="p-v-xs option option-3">
                                                <input class="validar miRadio" name="categoria" type="radio" value="rojo" id="rojo"
                                                    data-parsley-errors-container="#cbErrorContainerRadio" required />
                                                <label for="rojo" style="color: black">Rojo</label>
                                                &nbsp;
                                                &nbsp;
                                            </p>
                                        </div>                                        
                                        <div id="cbErrorContainerRadio"></div>
                                        <br>
                                        <br>
                                        <div>
                                            <p>Agregar a la Síntesis</p>
                                            <br>
                                            <input type="checkbox" id="addPdf" name="addPdf" value="1">
                                            <label for="addPdf"> Agregar a PDF</label>
                                        </div>
                                    </div> --}}
                                    </div>
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                        <!---->
                    </div>
                    <div class="row right-align">
                        <a id="guardaNotaInfo" class="waves-effect waves-light btn indigo miA">Guardar Nota Informativa</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="{{ URL::asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>
    <script>
        /// Datepicker //
        $(function() {
            $('.datepicker').pickadate({
                format: 'dd/mm/yyyy',
                monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto',
                    'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'
                ],
                monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov',
                    'Dic'
                ],
                weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                weekdaysShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
                selectMonths: true,
                selectYears: 15, // Puedes cambiarlo para mostrar más o menos años
                today: 'Hoy',
                clear: 'Limpiar',
                close: 'Ok',
                labelMonthNext: 'Siguiente mes',
                labelMonthPrev: 'Mes anterior',
                labelMonthSelect: 'Selecciona un mes',
                labelYearSelect: 'Selecciona un año',
            });
        });
        /// upload image file ///
        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz click',
                'replace': 'Arrastra y suelta o haz click para reemplazar',
                'remove': 'Eliminar',
                'error': 'Ooops, Algo salió mal.'
            },
            error: {
                'fileExtension': 'Solo se aceptan archivos jpg o png'
            }
        });

    </script>

@endsection
