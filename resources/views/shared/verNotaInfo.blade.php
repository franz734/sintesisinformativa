@php
$uri = Route::current();
$prfx = getPrefix($uri->uri);
//dd($nota);
if ($nota->subAreaRel == null) {
    $cadSubArea = '';
} else {
    $cadSubArea = $nota->subAreaRel->nomSubArea;
}
@endphp
@extends($prfx.'.master')
@section('css')

@endsection
@section('contenido')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <h5>{{ $nota->encabezado }}</h5>
                <h5 style="color: #bc955c">{{ $nota->entidadRel->nomEntidad }}</h5>
                <h5>{{ $nota->fcSuceso }}</h5>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Contenido</b></h5>
                        <blockquote>{{ $nota->texto }}</blockquote>
                        {{-- <div class="right-align" style="padding-right: 10px">
                            <a class="waves-effect waves-light btn" style="background-color: #9f2241"
                                title="Solicitar más información"
                                onclick="masInfoTema({{ $nota->idNotaInformativa }}, 'notaInfo')">
                                <i class="material-icons">info</i>
                            </a>
                        </div> --}}
                        <br>
                    </div>
                </div>
                <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Área</b></h5>
                        <blockquote>{{ $nota->areaRel->nomArea . '/' . $cadSubArea }}</blockquote>
                        <br>
                    </div>
                </div>
                {{-- <div class="card notaCont">
                    <div class="card-coontent cont">
                        <br>
                        <h5><b>Resumen</b></h5>
                        <blockquote>{{ $nota->resumen }}</blockquote>
                        <br>
                    </div>
                </div> --}}
                @if (!is_null($nota->imgNota))
                    <div class="card notaCont">
                        <div class="card-coontent cont">
                            <br>
                            <h5><b>Multimedia</b></h5>
                            @php
                                $ctl = 0;
                            @endphp
                            @foreach ($nota->imgNota as $img)
                                @php
                                    if ($ctl % 2 == 0) {
                                        $inicio = '<div class="row center-align">';
                                        $fin = '';
                                    } else {
                                        $inicio = '';
                                        $fin = '</div><br>';
                                    }
                                    $ctl++;
                                @endphp
                                {!! $inicio !!}
                                {{-- <div class="imgCont"> --}}
                                <img class="imgEvi" src="{{ URL::asset($img) }}" width="300" height="300">
                                {{-- </div> --}}
                                {!! $fin !!}
                            @endforeach
                            <br>
                            <br>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
@section('scripts')

@endsection
