@php
$uri = Route::current();
$prfx = getPrefix($uri->uri);
@endphp
@extends($prfx.'.master')
@section('css')
    <link href="{{ URL::asset('assets/plugins/dropify/css/dropify.css') }}" rel="stylesheet" />
@endsection
@section('contenido')
    <h3>Presentaciones</h3>
    @hasanyrole ('MonitorZona|Delegado')
    <a class="waves-effect waves-light btn" style="background-color: #9f2241" onclick="goBack()"><i
            class="material-icons left">skip_previous</i>Regresar</a>
    <br>
    <br>
    @endrole
    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <form id="presentacionForm" action="">
                    <div>
                        <section>
                            <div id="contNota">
                                <div class="row">
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset izquierdo-->
                                    <div class="col s12 m4 l8">
                                        <div class="input-field">
                                            <label for="titulo">Titulo</label>
                                            <input id="titulo" class="validar" name="titulo" type="text" required>
                                        </div>
                                        <div class="input-field">
                                            <label for="texto">Texto</label>
                                            <textarea id="texto" name="texto" class="materialize-textarea validar"
                                                required></textarea>
                                        </div>
                                        <label for="imgNota">Multimedia</label>
                                        <div class="input-field">
                                            <input id="imgNota" name="archivo" class="dropify" type="file" multiple
                                                data-allowed-file-extensions="pptx ppt">
                                        </div>
                                    </div>
                                    <div class="col s12 m4 l2"></div>
                                    <!--ofsset derecho-->
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="row right-align">
                        <a id="guardaPresentacion" class="waves-effect waves-light btn indigo miA">Guardar Presentación</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ URL::asset('assets/plugins/dropify/js/dropify.min.js') }}"></script>
    <script>
        /// upload image file ///
        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz click',
                'replace': 'Arrastra y suelta o haz click para reemplazar',
                'remove': 'Eliminar',
                'error': 'Ooops, Algo salió mal.'
            },
            error: {
                'fileExtension': 'Solo se aceptan archivos ppt o pptx'
            }
        });

    </script>
@endsection
