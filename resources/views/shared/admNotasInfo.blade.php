@php
$uri = Route::current();
$prfx = getPrefix($uri->uri);
@endphp
@extends($prfx.'.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('contenido')
    <h1>Notas Informativas</h1>
    <style>
        td.details-control {
            background: url('{{ URL::asset('assets/images/details_open.png') }}') no-repeat center center;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('{{ URL::asset('assets/images/details_close.png') }}') no-repeat center center;
        }

    </style>
    @hasanyrole ('SuperAdmin|Administrador|MonitorZona')
    <div class="row no-m-t no-m-b">
        <!--Filtro de busqueda-->
        <div class="col s12 m4 l2"></div>
        <!--ofsset izquierdo-->
        <div class="col s12 m4 l8">
            <form id="filterFormNotasInfo" action="">
                <div class="card stats-card">
                    <div class="card-content">
                        <span class="card-title">Busqueda</span>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectEdoF" name="entidadFed" class="js-states browser-default sel2"
                                        tabindex="-1" style="width: 100%">
                                        <option disabled selected value="def">Entidad Federativa</option>
                                        @foreach ($entidades as $entidad)
                                            <option value={{ $entidad->idEntidad }}>{{ $entidad->nomEntidad }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l6">

                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectArea" name="area" class="js-states browser-default sel2" tabindex="-1"
                                        style="width: 100%">
                                        <option disabled selected value="def">Área</option>
                                        @foreach ($areas as $area)
                                            <option value={{ $area->idArea }}>{{ $area->nomArea }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <select id="selectSubArea" name="subArea" class="js-states browser-default sel2"
                                        tabindex="-1" style="width: 100%">
                                        <option disabled selected value="def">Sub-Área</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="encabezado">Encabezado</label>
                                    <input id="encabezado" name="encabezado" type="text" class="validar">
                                </div>
                            </div>
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="texto">Texto</label>
                                    <input id="texto" name="texto" type="text" class="validar">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s12 m4 l6">
                                <div class="input-field">
                                    <label for="datefilter">Fecha de Alta</label>
                                    <br>
                                    <input id="datefilter" type="text" name="datefilter" />
                                </div>
                            </div>
                        </div>
                        <div class="right-align">
                            <a id="excelNotasInfo" class="waves-effect waves-light btn green" style="display: none"><i
                                    class="material-icons right">description</i>Excel</a>
                            <a id="limpiaFilterNotasInfo" class="waves-effect waves-light btn"
                                style="background-color: #235b4e">Limpiar</a>
                            <a id="filterNotasInfo" class="waves-effect waves-light btn"
                                style="background-color: #9f2241">Buscar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col s12 m4 l2"></div>
        <!--ofsset derecho-->
    </div>
    <!--Fin del filtro-->
    @endrole
    @hasanyrole ('MonitorZona')
    <div class="row right-align">
        <a href="{{ url('/monitorZona/nuevaNotaInformativa') }}" class="waves-effect waves-light btn"
            style="background-color: #9f2241"><i class="material-icons right">add_circle</i>Nueva Nota Informativa</a>
    </div>
    @endrole

    <div class="row no-m-t no-m-b">
        <div class="card">
            <div class="card-content">
                <table id="tableNotasInfo" class="display responsive-table datatable-example">
                    <thead>
                        <tr>
                            <th>&nbsp&nbsp&nbsp</th>
                            <th>ID</th>
                            <th scope="col">Encabezado</th>
                            {{-- <th scope="col">Resumen</th> --}}
                            <th scope="col">Entidad</th>
                            <th scope="col">SubProcuraduría</th>
                            <th scope="col">Fecha de Suceso</th>
                            <th scope="col">Ver</th>
                            {{-- @hasanyrole ('SuperAdmin|Administrador')
                                <th scope="col">Más</th>
                            @endrole --}}
                            {{-- <th scope="col">Fecha de Alta</th>
                        <th scope="col">Más</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Acción</th> --}}
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('input[name="datefilter"]').daterangepicker({
                opens: 'center',
                drops: 'up',
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });
            $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format(
                    'DD/MM/YYYY'));
            });
            $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });

    </script>
@endsection
