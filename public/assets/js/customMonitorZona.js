// Funciones //
function format(d) {
  var ret = d.textoHtml;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;
}
function format2(d) {
  var ret = d.actuacionHtml;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;
}
var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="material-icons miTi">chevron_left</i>',
    "sLast": '<i class="material-icons miTi">chevron_right</i>',
    "sNext": '<i class="material-icons miTi">chevron_right</i>',
    "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}
function destory_editor(selector) {
  if ($(selector)[0]) {
    var content = $(selector).find('.ql-editor').html();
    $(selector).html(content);

    $(selector).siblings('.ql-toolbar').remove();
    $(selector + " *[class*='ql-']").removeClass(function (index, css) {
      return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
    });

    $(selector + "[class*='ql-']").removeClass(function (index, css) {
      return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
    });
  }
  else {
    console.error('editor not exists');
  }
}
function goBack() {
  window.history.back();
}

function masInfoTema(id, task) {
  /* console.log(id,task);
  return false; */
  let date = new Date()

  let day = date.getDate()
  let month = date.getMonth() + 1
  let year = date.getFullYear()

  if (month < 10) {
    var hoy = `${day}/0${month}/${year}`
  } else {
    var hoy = `${day}/${month}/${year}`
  }
  //console.log(hoy);
  Swal.fire({
    title: 'Elija la fecha de solicitud',
    html: '<input id="datepicker">',
    text: "Se enviara un correo con la solicitud",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Enviar',
    cancelButtonText: 'Cancelar',
    onOpen: function () {
      $('#datepicker').datepicker({
        //startView: 2,
        language: 'es-ES',
        autoHide: true,
        inline: true,
        zIndex: 999999,
        startDate: hoy
      });
    },
  }).then((result) => {
    if (result.isConfirmed) {
      fecha = document.getElementById('datepicker').value,
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/monitorZona/masInfoTemaRel",
        type: 'POST',
        dataType: 'json',
        data: { 'id': id, 'fecha': fecha, 'task': task },
        cache: false,
      })
        .done(function (result) {
          Swal.fire(
            'Enviado',
            'La información ha sido solicitada',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }
  })
}

function enviaRespuesta(idResp) {
  //console.log(idResp);
  (async () => {
    const { value: email } = await Swal.fire({
      title: 'Correo de destino:',
      input: 'email',
      //inputLabel: 'Your email address',      
    })
    if (email) {
      //console.log(email);
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/monitorZona/enviaRespuestaTemaRel",
        type: 'POST',
        dataType: 'json',
        data: { 'idRespuesta': idResp, 'mail': email },
        cache: false,
      })
        .done(function (result) {
          Swal.fire(
            'Enviado',
            'La información ha sido enviada',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se pudo enviar la información',
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  })()
}
/////////////////////////////////

// Ready //
$(document).ready(function () {
  $('.sel2').select2();
  ///datatable temas relevantes///
  var tableTemasRel = $('#tableTemasRel').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/monitorZona/getTemasRel",
    "columns": [
      //{ "data": "" },
      { "data": "idTemaRel" },
      { "data": "temaRel" },
      { "data": "nomEntidad" },
      { "data": "status" },
      //{ "data": "fcCrea" }
    ],
    "columnDefs": [
      /* {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      }, */
      {
        "targets": 3,
        "data": "status",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return 'Con respuesta'
          }
          else if (row.status == 2) {
            return 'Cerrado'
          }
          else if (row.status == null) {
            return 'Activo'
          }
        }
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            if (row.idEntidad == null) {
              return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>' +
                '<a class="waves-effect waves-light btn btnRespondeTema" style="background-color:#215a4c" title="Responder"><i class="material-icons">question_answer</i></a>'
            }
            else {
              return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'
            }
          }
          else if (row.status == 2) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'//+
            //'<a class="waves-effect waves-light btn btnAbrirTemaRel" style="background-color:#ddc9a3" title="abrir tema"><i class="material-icons">open_in_new</i></a>'
          }
          else if (row.status == null && row.idEntidad != null) {
            return '<a class="waves-effect waves-light btn btnSolInfo" style="background-color:#10312b" title="solicitar información">' +
              '<i class="material-icons">pending_actions</i>' +
              '</a>'
          }
          else if (row.idEntidad == null) {
            return '<a class="waves-effect waves-light btn btnRespondeTema" style="background-color:#215a4c" title="Responder"><i class="material-icons">question_answer</i></a>'
          }
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableTemasRel tbody').on('click', '.btnVerRespTemarel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    window.location.href = '/monitorZona/verRespuesta/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnRespondeTema', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    console.log(idTema)
    $('#idTema').val(idTema);
    window.location.href = '/monitorZona/responderTemaRelevante/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnSolInfo', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    var task = 'temaRel';
    masInfoTema(idTema, task)
  });
  $('#guardaRespTemaRel').click(function (e) {
    e.preventDefault();
    var campos = $('#respTemaRel').find('.validar').parsley();
    console.log(campos);
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid()) {
        ctl++
      }
      else {
        campos[i].validate();
      }
    }
    if (ctl === campos.length) {
      var formdata = new FormData($("#respTemaRel")[0]);
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/monitorZona/guardaRespuestaTemaRel",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            window.location.href = '/monitorZona/temasRelevantes';
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log(result);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  });

  ///datatable notas///
  var tableNotas = $('#tableNotas').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "getNotas",
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "medio_rel.nomMedio" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "link" },
      { "data": "fcPublicacion" },
      { "data": "fcAlta" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      /* {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNota" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNota" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      }, */
      {
        "targets": 9,
        "data": "tipo_rel",
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null) {
            return "S/T";
          }
          else {
            return dta.nomTipo;
          }
        }
      },
      {
        "targets": 10,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          var show = (row.idTipo === null) ? "" : 'display:none';
          var showSend = (row.idTipo != null && row.statusEnvio === null) ? "" : "display:none";
          var showStatus = (row.idTipo != null && row.statusEnvio != null) ? "" : "display:none";
          return '<a class="waves-effect waves-light btn btnSelectTipo" style="background-color:#6f7271; ' + show + '" title="asignar tipo"><i class="material-icons">indeterminate_check_box</i></a>' +
            '<a class="waves-effect waves-light btn btnEnviaNota" style="background-color:#215a4c; ' + showSend + '" title="enviar nota"><i class="material-icons">email</i></a>' +
            '<i class="material-icons iconGreen" style="' + showStatus + '">check_circle_outline</i>';
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotas tbody').on('click', '.btnSelectTipo', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(idNota)
    $('#selectTipo').select2();
    $('#modalSelect').openModal();
    $('#idNota').val(idNota);
  });
  $('#tableNotas tbody').on('click', '.btnEnviaNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(tr[0]);
    //return false;
    Swal.fire({
      title: '¿Enviar la nota por correo electrónico?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "enviaNota",
          type: 'POST',
          dataType: 'json',
          data: { idNota: idNota },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'La nota ha sido enviada',
              'success'
            )
            tableNotas.ajax.reload();
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });

  $('#btnAsigTipo').on('click', function () {
    var selectTipo = $('#selectTipo').parsley();
    if (selectTipo.isValid()) {
      var idNota = $('#idNota').val();
      var idTipo = $('#selectTipo').val();
      $('#selectTipo').val('def').trigger('change');
      console.log(idNota + ' ' + idTipo);
      //return false;
      $('#modalSelect').closeModal();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "asignaTipo",
        type: 'POST',
        dataType: 'json',
        data: { idNota: idNota, idTipo: idTipo },
        /* cache:false,
        processData: false,
        contentType: false, */
      })
        .done(function (result) {
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido guardados',
            'success'
          )
          tableNotas.ajax.reload();
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      selectTipo.validate();
    }

  });

  /// Filtro Notas ///
  //Lugar Filtro notas//
  $('#selectEdoF').change(function () {
    idEntidad = $(this).val();
    var lugaresArr = [];
    $.ajax({
      url: "getLugares/" + idEntidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            lugaresArr.push({
              id: result[i].idLugar,
              lugar: result[i].nomLugar,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Lugar Específico</option>';
        for (const i in lugaresArr) {
          cadCom += '<option value="' + lugaresArr[i].id + '">' + lugaresArr[i].lugar
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectLugarF').empty()
        $('#selectLugarF').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  })
  //Btn filter Notas//
  $('#filterNotas').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormInc")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/monitorZona/filtraNotas",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelNotas').show();
        url = "/monitorZona/getFilteredNotas/" + result
        tableNotas.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Notas//
  $('#limpiaFilterNotas').click(function (e) {
    e.preventDefault();
    $('#selectEdoF').val('def').trigger('change');
    $('#selectLugarF').val('def').trigger('change');
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');
    $('#selectCategoria').val('def').trigger('change')
    $('#excelNotas').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    tableNotas.ajax.url("getNotas").load();
  });
  //Excel Notas//
  $('#excelNotas').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdoF').val();
    var lugar = $('#selectLugarF').val();
    var area = $('#selectArea').val();
    var subArea = $('#selectSubArea').val()
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();
    var categoria = $('#selectCategoria').val();
    var datefilter = $('#datefilter').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,
      lugar: lugar,
      area: area,
      subArea: subArea,
      encabezado: encabezado,
      texto: texto,
      categoria: categoria,
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelNotas/" + dta //comentar en producción        
    window.location = url
  });

  /// datatable respuestas ///
  var tableRespuestas = $('#tableRespuestas').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getRespuestas",
    "columns": [
      { "data": "" },
      { "data": "idRespuesta" },
      { "data": "nota_rel.encabezado" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "nota_rel.fcEnvio" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 7,
        "data": "propuesta_rel",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === 0) {
            var cad = 'S/P'
          }
          else if (dta !== null) {
            if (dta.idTipoPropuesta === 1) {
              var cad = '<a href="/monitorZona/verPropuesta/' + dta.idPropuesta + '">Tweet</a>'
            }
            else if (dta.idTipoPropuesta === 2) {
              var cad = '<a href="/monitorZona/verPropuesta/' + dta.idPropuesta + '">Boletín</a>'
            }
          }
          return cad
        }
      },
      {
        "targets": 8,
        "data": 'docRespuesta',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerRespuesta" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableRespuestas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableRespuestas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableRespuestas tbody').on('click', '.btnVerRespuesta', function () {
    var tr = $(this).closest('tr');
    var idRespuesta = tr[0].childNodes[1].firstChild.data;
    window.location.href = 'verRespuestaSolInfo/' + idRespuesta;
  });

  /// Filtro respuestas ///
  //Btn filter Respuestas//
  $('#filterRespuestas').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormResp")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/monitorZona/filtraRespuestas",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelRespuestas').show();
        url = "/monitorZona/getFilteredRespuestas/" + result
        tableRespuestas.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Respuestas//
  $('#limpiaFilterRespuestas').click(function (e) {
    e.preventDefault();
    $('#selectEdo').val('def').trigger('change');
    /* $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change'); */
    $('#excelRespuestas').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    $('#datefilter2').val(null);
    tableRespuestas.ajax.url("getRespuestas").load();
  });
  //Excel Respuestas//
  $('#excelRespuestas').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdo').val()
    /* var area = $('#selectArea').val()
    var subArea = $('#selectSubArea').val() */
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();
    var datefilter = $('#datefilter').val();
    var datefilter2 = $('#datefilter2').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,
      /* area: area,
      subArea: subArea, */
      encabezado: encabezado,
      texto: texto,
      datefilter: datefilter.replaceAll('/', '.'),
      datefilter2: datefilter2.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelRespuestas/" + dta //comentar en producción        
    window.location = url
  });

  /// datatable incidencias ///
  var tableIncidencias = $('#tableIncidencias').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getIncidencias",
    "columns": [
      { "data": "" },
      { "data": "idIncidencia" },
      { "data": "tema" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "propuesta_rel",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === 0) {
            var cad = 'S/P'
          }
          else if (dta !== null) {
            if (dta.idTipoPropuesta === 1) {
              var cad = '<a href="/monitorZona/verPropuesta/' + dta.idPropuesta + '">Tweet</a>'
            }
            else if (dta.idTipoPropuesta === 2) {
              var cad = '<a href="/monitorZona/verPropuesta/' + dta.idPropuesta + '">Boletín</a>'
            }
          }
          return cad
        }
      },
      {
        "targets": 7,
        "data": 'docIncidencia',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 8,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableIncidencias tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableIncidencias.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableIncidencias tbody').on('click', '.btnVerIncidente', function () {
    var tr = $(this).closest('tr');
    var idIncidencia = tr[0].childNodes[1].firstChild.data;
    //console.log(idIncidente)
    window.location.href = 'verIncidencia/' + idIncidencia;
  });

  //Filter Incidencias//
  $('#selectArea').change(function () {
    $('#selectSubArea').val('def').trigger('change');
    var subAreaArr = [];
    var area = $(this).val();
    if (area == null || area == '') {
      return false;
    }
    //console.log(area);
    $.ajax({
      url: "/incidencia/listaSubAreas/" + area,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            subAreaArr.push({
              id: result[i].idSubArea,
              subArea: result[i].nomSubArea,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Área</option>';
        for (const i in subAreaArr) {
          cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectSubArea').empty()
        $('#selectSubArea').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Btn filter Incidencias//
  $('#filterIncidencias').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormInc")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/monitorZona/filtraIncidencias",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelInc').show();
        url = "/monitorZona/getFilteredIncidencias/" + result
        tableIncidencias.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Incidencias//
  $('#limpiaFilter').click(function (e) {
    e.preventDefault();
    $('#selectEdo').val('def').trigger('change');
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');
    $('#selectMateria').val('def').trigger('change');
    $('#excelInc').hide();
    $('#tema').val(null);
    $('#problematica').val(null);
    $('#datefilter').val(null);
    tableIncidencias.ajax.url("getIncidencias").load();
  });
  //Excel Incidencias//
  $('#excelInc').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdo').val()
    var area = $('#selectArea').val()
    var subArea = $('#selectSubArea').val()
    var tema = $('#tema').val();
    var problematica = $('#problematica').val();
    var materia = $('#selectMateria').val();
    var accionVS = $('#selectAccioVS').val();
    var datefilter = $('#datefilter').val();
    dta = {
      entidad: entidad,
      area: area,
      subArea: subArea,
      tema: tema,
      problematica: problematica,
      materia: materia,
      accionVS: accionVS,
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelIncidencias/" + dta //comentar en producción        
    window.location = url
  });

  //Notas informativas//
  //Select Sub-Area//
  $('.anid').change(function () {
    $('.contSA').empty();
    var cadSel = '<select id="selectSubArea" name="subArea" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
      '<option disabled selected value="def">Sub-Área</option>' +
      '</select>';
    $('#selectSubArea').val('def').trigger('change');
    $('.contSA').append(cadSel);
    $('#selectSubArea').select2();
    var subAreaArr = [];
    var area = $(this).val();
    /* console.log(area);
    return false; */
    if (area === '4') {
      $('.contSA').empty();
    }
    else {
      $.ajax({
        url: "../incidencia/listaSubAreas/" + area,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          //console.log(result)
          //return false;
          for (const i in result) {
            if (result[i] != null) {
              subAreaArr.push({
                id: result[i].idSubArea,
                subArea: result[i].nomSubArea,
              })
            }
          }
          var cadCom = '<option disabled selected value="def">Sub-Área</option>';
          for (const i in subAreaArr) {
            cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
              + '</option>';
          }
          //console.log(cadCom);
          $('#selectSubArea').empty()
          $('#selectSubArea').append(cadCom);

        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    }
  });

  ///Datatable Notas Informativas///
  var tableNotasInfo = $('#tableNotasInfo').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "/monitorZona/getNotasInformativas",
    "columns": [
      { "data": "" },
      { "data": "idNotaInformativa" },
      { "data": "encabezado" },
      //{ "data": "resumen" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "fcSuceso" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerNotaInfo" style="background-color:#6f7271" title="ver nota"><i class="material-icons">visibility</i></a>'
        }
      },
      /* {
        "targets": 8,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNotaInfo" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNotaInfo" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      }, */
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotasInfo tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasInfo.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format3(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasInfo tbody').on('click', '.btnVerNotaInfo', function () {
    var tr = $(this).closest('tr');
    var idNotaInfo = tr[0].childNodes[1].firstChild.data;
    window.location.href = '/monitorZona/verNotaInformativa/' + idNotaInfo;
  });

  ///Guarda Notas Informativas///
  $('#guardaNotaInfo').click(function (e) {
    e.preventDefault();
    var estado = $('#selectEdo').parsley();
    var area = $('#selectSubProc').parsley();
    if ($('#selectSubProc').val() !== 4) {
      var subArea = $('#selectSubArea').parsley();
    }
    var campos = $('#notasInfoForm').find('.validar').parsley();
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid() && estado.isValid() && area.isValid()) {
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          if (subArea.isValid()) {
            ctl++;
          }
        }
        else {
          ctl++
        }
      }
      else {
        campos[i].validate();
        estado.validate();
        area.validate();
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          subArea.validate();
        }
      }
      if (ctl === campos.length) {
        var formdata = new FormData($("#notasInfoForm")[0]);
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/monitorZona/guardaNotaInformativa",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido registrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              //location.reload();
              $("#notasInfoForm").trigger('reset');
              $('#selectEdo').val('def').trigger('change');
              $('#selectArea').val('def').trigger('change');
              $('#selectSubArea').val('def').trigger('change');
              $('.dropify-clear').click();
              location.reload(true);
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log(result);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
          })
          .always(function () {
            console.log('complete');
          })
      }
    }
  });

  //Btn filter Notas Informativas//
  $('#filterNotasInfo').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormNotasInfo")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/monitorZona/filtraNotasInfo",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelNotasInfo').show();
        url = "/monitorZona/getFilteredNotasInfo/" + result
        tableNotasInfo.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });

  //Limpiar Filtro Notas informativas//
  $('#limpiaFilterNotasInfo').click(function (e) {
    e.preventDefault();
    $('#selectEdoF').val('def').trigger('change');    
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');    
    $('#excelNotasInfo').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    tableNotasInfo.ajax.url("/monitorZona/getNotasInformativas").load();
  });

  //Excel Notas Informativas//
  $('#excelNotasInfo').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdoF').val();    
    var area = $('#selectArea').val();
    var subArea = $('#selectSubArea').val()
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();    
    var datefilter = $('#datefilter').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,      
      area: area,
      subArea: subArea,
      encabezado: encabezado,
      texto: texto,      
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "/monitorZona/getExcelNotasInfo/" + dta //comentar en producción        
    window.location = url
  });

});