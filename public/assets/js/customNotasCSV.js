var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="material-icons miTi">chevron_left</i>',
    "sLast": '<i class="material-icons miTi">chevron_right</i>',
    "sNext": '<i class="material-icons miTi">chevron_right</i>',
    "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}
function format(d) {
  var ret = d.texto;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;

}
/////////////////////////////////
// Ready //
$(document).ready(function () {
  //Notas//
  flgSuceso = 0;
  flgLugar = 0;
  $('#selectMedioCSV').select2();
  ///datatable notas///
  var tableNotasCSV = $('#tableNotasCSV').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getNotasCSV",
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "link" },
      { "data": "fcPublicacion" },
      { "data": "fcAlta" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },

      {
        "targets": 7,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNota" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNota" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotasCSV tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasCSV.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasCSV tbody').on('click', '.btnEditNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    window.location.href = 'editaNotaCSV/' + idNota;
  });
  $('#tableNotasCSV tbody').on('click', '.btnBorrarNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    //window.location.href ='editaNota/'+idNota;
    Swal.fire({
      title: '¿Borrar esta nota?',
      text: "Eliminara el registro de la nota",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraNota",
          type: 'POST',
          dataType: 'json',
          data: { idNota: idNota },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/completaNotasCSV';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  $('#selectMedioCSV').change(function () {
    $('.dropify-clear').click();
    tipoMedio = $(this).val();
    console.log(tipoMedio);
    if (tipoMedio == 1) {
      // para Tv //
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //                                 
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $('#contImgRedSoc').removeClass(['activeImg', 'contain']);
      $('#contImgRedSoc').removeAttr('style');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Medios Impresos //
      $('#contMedImp').show();
      $('#linkImp').attr('name', 'link');
      $('#linkImp').addClass('validar');
    }
    if (tipoMedio == 2) {
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $('#contImgRedSoc').removeClass(['activeImg', 'contain']);
      $('#contImgRedSoc').removeAttr('style');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Tv //
      $('#contTv').show();
      $('#linkTv').attr('name', 'link');
      $('#linkTv').addClass('validar');
      $('#imgTv').addClass('validar');
      $('#imgTv').attr('name', 'imgTv');
    }
    if (tipoMedio == 3) {
      $('.dropify-clear').click();
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Tv //          
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para RedSoc //
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Web //
      /* $('#contWeb').show();
      $('#linkWeb').attr('name', 'link');
      $('#linkWeb').addClass('validar'); */
    }
    if (tipoMedio == 4) {
      $('.dropify-clear').click();
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Tv //         
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //
      $('#contRedSoc').show();
      $('#linkRedSoc').attr('name', 'link');
      $('#linkRedSoc').addClass('validar');
      $('#imgRedSoc').addClass('validar');
      $('#imgRedSoc').attr('name', 'imgRedSoc');
      $('#ctaVer').attr('name', 'ctaVer');
    }
  });
  $('#btnModifNotasCSV').click(function (e) {
    e.preventDefault();
    var campos = $('#editNotasCSVForm').find('.validar').parsley();
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid()) {
        ctl++
      }
      else {
        campos[i].validate();

      }
    }
    if (campos.length == ctl) {
      var formdata = new FormData($("#editNotasCSVForm")[0]);
      if (suceso === 'otro') {
        flgSuceso = 1;
      }
      if (lugar === 'otro') {
        flgLugar = 1;
      }
      formdata.append('flagNvoSuceso', flgSuceso)
      formdata.append('flagNvoLugar', flgLugar)
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../modificaNotaCSV",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido modificados',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            window.location.href = '/completaNotasCSV';
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }

  });

  ///Info Ambiental///
  var tableAmbietalesCSV = $('#tableAmbietalesCSV').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getAmbientalesCSV",
    "columns": [
      { "data": "" },
      { "data": "idInfoAmbiental" },
      { "data": "encabezado" },
      { "data": "link" },
      { "data": "fcAlta" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 3,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      {
        "targets": 5,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditAmbiental" style="background-color:#6f7271" title="editar ambiental"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarAmbiental" style="background-color:#9f2241" title="borrar ambiental"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableAmbietalesCSV tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableAmbietalesCSV.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableAmbietalesCSV tbody').on('click', '.btnEditAmbiental', function () {
    var tr = $(this).closest('tr');
    var idAmbiental = tr[0].childNodes[1].firstChild.data;
    //console.log(idAmbiental)
    window.location.href = 'editaAmbientalCSV/' + idAmbiental;
  });

  $('#btnModifAmbientalCSV').click(function (e) {
    e.preventDefault();
    var campos = $('#editAmbientalCSVForm').find('.validar').parsley();
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid()) {
        ctl++
      }
      else {
        campos[i].validate();
      }
    }
    if (campos.length == ctl) {
      var formdata = new FormData($("#editAmbientalCSVForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../modificaAmbientalCSV",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido modificados',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            window.location.href = '/completaInfoAmbientalCSV';
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }
  });
  $('#tableAmbietalesCSV tbody').on('click', '.btnBorrarAmbiental', function () {
    var tr = $(this).closest('tr');
    var idAmbiental = tr[0].childNodes[1].firstChild.data;
    /* console.log(idAmbiental)
    return false; */
    Swal.fire({
      title: '¿Borrar información ambiental?',
      text: "Eliminara el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraAmbiental",
          type: 'POST',
          dataType: 'json',
          data: { idAmbiental: idAmbiental },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/completaInfoAmbientalCSV';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });

});