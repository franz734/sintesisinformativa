// Funciones //
function cargarEstado(id_estado) {
  //que quiero hacercuando se seleccione un estado //
  console.log('Se selecciono el id_estado: ' + id_estado);
  window.location = 'entidad/' + id_estado;
}
function limpiaDiv(divID) {
  $(divID).empty();
}
function restartColumna() {
  $('#selectFuente').val('def').trigger('change');
  $('#texto').val('');
  $('#textoHtml').val('');
  $('#encabezado').val('');
  quill.setContents([{ insert: '' }]);
}
function format(d) {
  var ret = d.textoHtml;
  if (!ret) {
    ret = d.texto
  }
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;

}
function format2(d) {
  var ret = d.actuacionHtml;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;

}

function format3(d) {
  var ret = d.texto;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;
}

var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="material-icons miTi">chevron_left</i>',
    "sLast": '<i class="material-icons miTi">chevron_right</i>',
    "sNext": '<i class="material-icons miTi">chevron_right</i>',
    "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}

function goBack() {
  window.history.back();
}

function enviaRespuesta(idResp) {
  //console.log(idResp);
  (async () => {
    const { value: email } = await Swal.fire({
      title: 'Correo de destino:',
      input: 'email',
      //inputLabel: 'Your email address',      
    })
    if (email) {
      //console.log(email);
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/enviaRespuestaTemaRel",
        type: 'POST',
        dataType: 'json',
        data: { 'idRespuesta': idResp, 'mail': email },
        cache: false,
      })
        .done(function (result) {
          Swal.fire(
            'Enviado',
            'La información ha sido enviada',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se pudo enviar la información',
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  })()
}

function masInfoTema(id, task) {
  /* console.log(id,task);
  return false; */
  let date = new Date()

  let day = date.getDate()
  let month = date.getMonth() + 1
  let year = date.getFullYear()

  if (month < 10) {
    var hoy = `${day}/0${month}/${year}`
  } else {
    var hoy = `${day}/${month}/${year}`
  }
  //console.log(hoy);
  Swal.fire({
    title: 'Elija la fecha de solicitud',
    html: '<input id="datepicker">',
    text: "Se enviara un correo con la solicitud",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Enviar',
    cancelButtonText: 'Cancelar',
    onOpen: function () {
      $('#datepicker').datepicker({
        //startView: 2,
        language: 'es-ES',
        autoHide: true,
        inline: true,
        zIndex: 999999,
        startDate: hoy
      });
    },
  }).then((result) => {
    if (result.isConfirmed) {
      fecha = document.getElementById('datepicker').value,
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/masInfoTemaRel",
        type: 'POST',
        dataType: 'json',
        data: { 'id': id, 'fecha': fecha, 'task': task },
        cache: false,
      })
        .done(function (result) {
          Swal.fire(
            'Enviado',
            'La información ha sido solicitada',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }
  })
}

/////////////////////////////////
var tipoMedio;
var j = 0;
var ctlColumnas = 0;
columnas = [];
var fnte;
var flg = 0;
var suceso;
var flgSuceso = 0;
var lugar;
var flgLugar = 0;
var idEntidad;

// Ready //
$(document).ready(function () {
  // contador dinamico //
  $('.counter').each(function () {
    $(this).prop('Counter', 0).animate({
      Counter: $(this).text()
    }, {
      duration: 2000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.ceil(now));
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
      }
    });
  });
  //$('#modalSelect').modal();
  // select2 //
  $('#selectMedio').select2();
  $('#selectEdo').select2();
  $('#selectSubProc').select2();
  $('#selectFuente').select2();
  $('#selectFuenteNota').select2();
  $('#selectRef').select2();
  $('#selectArea').select2();
  $('#selectSubArea').select2();
  $('#selectMateria').select2();
  $('#selectAccioVS').select2();
  $('.sel2').select2();
  $('#selectLugarF').select2();
  // Switch //
  $('#ctaVerif').change(function () {
    if ($(this).prop('checked')) {
      $('#ctaVer').val("1");
    }
    else {
      $('#ctaVer').val("0");
    }
  });
  // Mapa dinamico //
  /* State Names */
  var state_names = new Array();
  var state_class = new Array();
  state_names[1] = "Aguascalientes"; state_names[2] = "Baja California"; state_names[3] = "Baja California Sur"; state_names[4] = "Campeche"; state_names[5] = "Coahuila"; state_names[6] = "Colima"; state_names[7] = "Chiapas"; state_names[8] = "Chihuahua"; state_names[9] = "Distrito Federal"; state_names[10] = "Durango"; state_names[11] = "Guanajuato"; state_names[12] = "Guerrero"; state_names[13] = "Hidalgo"; state_names[14] = "Jalisco"; state_names[15] = "Estado de M&eacute;xico"; state_names[16] = "Michoac&aacute;n"; state_names[17] = "Morelos"; state_names[18] = "Nayarit"; state_names[19] = "Nuevo Le&oacute;n"; state_names[20] = "Oaxaca"; state_names[21] = "Puebla"; state_names[22] = "Quer&eacute;taro"; state_names[23] = "Quintana roo"; state_names[24] = "San Luis Potos&iacute;"; state_names[25] = "Sinaloa"; state_names[26] = "Sonora"; state_names[27] = "Tabasco"; state_names[28] = "Tamaulipas"; state_names[29] = "Tlaxcala"; state_names[30] = "Veracruz"; state_names[31] = "Yucat&aacute;n"; state_names[32] = "Zacatecas";
  state_class[1] = "AGU"; state_class[2] = "BCN"; state_class[3] = "BCS"; state_class[4] = "CAM"; state_class[5] = "COA"; state_class[6] = "COL"; state_class[7] = "CHP"; state_class[8] = "CHH"; state_class[9] = "DIF"; state_class[10] = "DUR"; state_class[11] = "GUA"; state_class[12] = "GRO"; state_class[13] = "HID"; state_class[14] = "JAL"; state_class[15] = "MEX"; state_class[16] = "MIC"; state_class[17] = "MOR"; state_class[18] = "NAY"; state_class[19] = "NLE"; state_class[20] = "OAX"; state_class[21] = "PUE"; state_class[22] = "QUE"; state_class[23] = "ROO"; state_class[24] = "SLP"; state_class[25] = "SIN"; state_class[26] = "SON"; state_class[27] = "TAB"; state_class[28] = "TAM"; state_class[29] = "TLA"; state_class[30] = "VER"; state_class[31] = "YUC"; state_class[32] = "ZAC";
  $(function () {
    $('.listaEdos').mouseover(function (e) {
      $($(this).data('parent-map')).mouseover();
    }).mouseout(function (e) {
      $($(this).data('parent-map')).mouseout();
    }).click(function (e) {
      e.preventDefault();
      $($(this).data('parent-map')).click();
    });


    $('.area').hover(function () {
      var id_estado = $(this).data('id-estado');
      var meid = $(this).attr('id');
      $('#edo').html(state_names[id_estado]);
      $('#letras' + meid).addClass('listaEdosHover');
      $('.escudo').addClass('escudo_img');
      if (last_selected_id_estado !== null) {
        $('.escudo').removeClass(state_class[last_selected_id_estado]);
      }
      $('.escudo').addClass(meid);
    }).mouseout(function () {
      var meid = $(this).attr('id');
      $('#letras' + meid).removeClass('listaEdosHover');
      $('.escudo').removeClass(meid);
      if (last_selected_id_estado !== null) {
        $('#edo').html(state_names[last_selected_id_estado]);
        $('.escudo').addClass(state_class[last_selected_id_estado]);
      } else {
        $('#edo').html("&nbsp;");
        $('.escudo').removeClass('escudo_img');
        //$('.escudo').attr('class','escudo');
      }
    });
    //$('#map_ID').imageMapResize();//funciona perfectamente
    var areaLastClicked = null;
    var last_selected_id_estado = null;
    $('.area').click(function (e) {
      e.preventDefault();
      var $area = $(this);
      var meid = $area.attr('id');
      //$('.area').mouseout();
      var data = $area.data('maphilight') || {};
      if (areaLastClicked !== null) {
        var lastData = areaLastClicked.data('maphilight') || {};
        lastData.alwaysOn = false;
        $('#letras' + areaLastClicked.attr('id')).removeClass('listaEdosActive');
        $('.escudo').removeClass(state_class[last_selected_id_estado]);
      }
      $('#letras' + meid).addClass('listaEdosActive');
      areaLastClicked = $area;
      //data.alwaysOn = !data.alwaysOn;
      data.alwaysOn = true;
      $(this).data('maphilight', data).trigger('alwaysOn.maphilight');
      last_selected_id_estado = $(this).data('id-estado');
      cargarEstado(last_selected_id_estado);
    });

    $('.map').maphilight({ fade: true, strokeColor: '950054', fillColor: '950054', fillOpacity: 0.3 });//funciona, pero no cuando se redimienciona la imagen (cuando se cambia el estylo de la img con widt o height)                        
  });
  // Entidad de la nota//
  $('.edoNota').change(function () {
    //Lugares//
    $('#contSelLugar').empty();
    var cadSel = '<select id="selectLugar" name="lugar" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
      '<option disabled selected value="def">Lugar Específico</option>' +
      '<option value="otro">Otro Lugar</option>' +
      '</select>';
    $('#selectLugar').val('def').trigger('change');
    $('#contSelLugar').append(cadSel);
    $('#selectLugar').select2();
    idEntidad = $(this).val();
    var lugaresArr = [];
    $.ajax({
      url: "getLugares/" + idEntidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            lugaresArr.push({
              id: result[i].idLugar,
              lugar: result[i].nomLugar,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Lugar Específico</option>' +
          '<option value="otro">Otro Lugar</option>';
        for (const i in lugaresArr) {
          cadCom += '<option value="' + lugaresArr[i].id + '">' + lugaresArr[i].lugar
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectLugar').empty()
        $('#selectLugar').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
    //Sucesos//
    $('#contSelSuceso').empty();
    var cadSel2 = '<select id="selectSuceso" name="suceso" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
      '<option disabled selected value="def">Suceso</option>' +
      '<option value="otro">Otro Suceso</option>' +
      '</select>';
    $('#selectSuceso').val('def').trigger('change');
    $('#contSelSuceso').append(cadSel2);
    $('#selectSuceso').select2();
    var sucesosArr = [];
    $.ajax({
      url: "getSucesos/" + idEntidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            sucesosArr.push({
              id: result[i].idSuceso,
              suceso: result[i].nomSuceso,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Suceso Específico</option>' +
          '<option value="otro">Otro Suceso</option>';
        for (const i in sucesosArr) {
          cadCom += '<option value="' + sucesosArr[i].id + '">' + sucesosArr[i].suceso
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectSuceso').empty()
        $('#selectSuceso').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
    //console.log(idEntidad);
  })
  // Tipo de medio //
  $('#selectMedio').change(function () {
    tipoMedio = $(this).val();
    console.log(tipoMedio);
    if (tipoMedio == 1) {
      $('.dropify-clear').click();
      // para Tv //
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //                                 
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $('#contImgRedSoc').removeClass(['activeImg', 'contain']);
      $('#contImgRedSoc').removeAttr('style');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Medios Impresos //
      $('#contMedImp').show();
      $('#linkImp').attr('name', 'link');
      $('#linkImp').addClass('validar');
    }
    if (tipoMedio == 2) {
      $('.dropify-clear').click();
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $('#contImgRedSoc').removeClass(['activeImg', 'contain']);
      $('#contImgRedSoc').removeAttr('style');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Tv //
      $('#contTv').show();
      $('#linkTv').attr('name', 'link');
      $('#linkTv').addClass('validar');
      $('#imgTv').addClass('validar');
      $('#imgTv').attr('name', 'imgTv');
    }
    if (tipoMedio == 3) {
      $('.dropify-clear').click();
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Tv //          
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para RedSoc //
      $('#contRedSoc').hide();
      $('#linkRedSoc').val('');
      $('#linkRedSoc').removeAttr('name');
      $('#linkRedSoc').removeClass('validar');
      $('#imgRedSoc').val('');
      $('#imgRedSoc').removeAttr('name');
      $('#imgRedSoc').removeClass('validar');
      $("#ctaVerif").prop("checked", false);
      $('#ctaVer').val('0');
      $('#ctaVer').removeAttr('name');
      // para Web //
      $('#contWeb').show();
      $('#linkWeb').attr('name', 'link');
      $('#linkWeb').addClass('validar');
    }
    if (tipoMedio == 4) {
      $('.dropify-clear').click();
      // para Medios Impresos //
      $('#contMedImp').hide();
      $('#linkImp').val('');
      $('#linkImp').removeAttr('name');
      $('#linkImp').removeClass('validar');
      // para Tv //         
      $('#contTv').hide();
      $('#linkTv').val('');
      $('#linkTv').removeAttr('name');
      $('#linkTv').removeClass('validar');
      $('#imgTv').val('');
      $('#imgTv').removeAttr('name');
      $('#imgTv').removeClass('validar');
      $('#contImgTv').removeClass(['activeImg', 'contain']);
      $('#contImgTv').removeAttr('style');
      // para Web //
      $('#contWeb').hide();
      $('#linkWeb').val('');
      $('#linkWeb').removeAttr('name');
      $('#linkWeb').removeClass('validar');
      // para RedSoc //
      $('#contRedSoc').show();
      $('#linkRedSoc').attr('name', 'link');
      $('#linkRedSoc').addClass('validar');
      $('#imgRedSoc').addClass('validar');
      $('#imgRedSoc').attr('name', 'imgRedSoc');
      $('#ctaVer').attr('name', 'ctaVer');
    }
  });
  //Select Sub-Area//
  $('.anid').change(function () {
    $('.contSA').empty();
    var cadSel = '<select id="selectSubArea" name="subArea" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
      '<option disabled selected value="def">Sub-Área</option>' +
      '</select>';
    $('#selectSubArea').val('def').trigger('change');
    $('.contSA').append(cadSel);
    $('#selectSubArea').select2();
    var subAreaArr = [];
    var area = $(this).val();
    /* console.log(area);
    return false; */
    if (area === '4') {
      $('.contSA').empty();
    }
    else {
      $.ajax({
        url: "../incidencia/listaSubAreas/" + area,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          //console.log(result)
          //return false;
          for (const i in result) {
            if (result[i] != null) {
              subAreaArr.push({
                id: result[i].idSubArea,
                subArea: result[i].nomSubArea,
              })
            }
          }
          var cadCom = '<option disabled selected value="def">Sub-Área</option>';
          for (const i in subAreaArr) {
            cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
              + '</option>';
          }
          //console.log(cadCom);
          $('#selectSubArea').empty()
          $('#selectSubArea').append(cadCom);

        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    }
  });
  //Select Fuente//
  $("#selectFuenteNota").change(function () {
    fnte = $(this).val();
    if (fnte === 'otra') {
      $("#contSel").hide();
      var cad = '<label for="fuente">Fuente</label>' +
        '<input id="fuente" class="validar" name="fuente" type="text" required>';
      $("#contOtra").html(cad);
    }
  });

  /// Agregar columnas ///
  $(document).on('click', '#btnAgColumna', function (e) {
    e.preventDefault();
    var fuente = $('#selectFuente').parsley();
    var textoColum = $('#texto').parsley();
    var encabezado = $('#encabezado').parsley();
    if (fuente.isValid() && textoColum.isValid() && encabezado.isValid()) {
      var fuenteA = $('#selectFuente option:selected').text();
      var textoA = $('#texto').val();
      var textoHtmlA = $('#textoHtml').val()
      var encabezadoA = $('#encabezado').val();
      $('#listaColumnas').append('<li data-id="' + j + '" class="miLi">' + fuenteA + ' <a class="waves-effect waves-light red btn eliminaColum" data-id="' + j + '" style="color:#fff;">X</a></li>');
      columnas.push({
        id: j,
        fuente: fuenteA,
        encabezado: encabezadoA,
        texto: textoA,
        textoHtml: textoHtmlA,
        fcAlta: $('#fechaAlta').val()

      })
      console.log(columnas);
      j++;
      ctlColumnas++
      restartColumna();
    }
    else {
      fuente.validate();
      textoColum.validate();
      encabezado.validate();
    }

  });
  $('#btnfinColumnas').click(function (e) {
    e.preventDefault();
    if (ctlColumnas === 0) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No se agregaron columnas',
      })
    }
    else {
      var jColumnas = JSON.stringify(columnas);
      console.log(jColumnas);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../guardaOchoColumnas",
        type: 'POST',
        dataType: 'json',
        data: { 'columnas': jColumnas },
        cache: false,
      })
        .done(function (result) {
          $('#encabezado').val('');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se pudieron registrar los datos',
          })
        })
        .always(function () {
          console.log('complete');
        });
    }
  });
  /// Guardar Boletín ///
  $('#guardaBoletin').click(function (e) {
    e.preventDefault();
    var texto = $('#texto').parsley();
    var link = $('#link').parsley();
    var encabezado = $('#encabezado').parsley();
    if (texto.isValid()  /* && link.isValid() */ && encabezado.isValid()) {
      $('#fechaAlta').prop("disabled", false);
      var formdata = new FormData($("#boletinForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../guardaBoletin",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          $('#fechaAlta').prop("disabled", false);
          $('#link').val('');
          $('#encabezado').val('');
          console.log(result)
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          )
          $('#selectMedio').val('def').trigger('change');
          $('#selectEdo').val('def').trigger('change');
          $('#selectSubProc').val('def').trigger('change');
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      texto.validate();
      link.validate();
      encabezado.validate();
    }

  });

  /// Guarda Información Ambiental ///
  $('#guardaAmbiental').click(function (e) {
    e.preventDefault();
    var texto = $('#texto').parsley();
    var link = $('#link').parsley();
    var encabezado = $('#encabezado').parsley();
    var referencia = $('#selectRef').parsley()
    if (texto.isValid() /* && link.isValid() */ && encabezado.isValid() && referencia.isValid()) {
      $('#fechaAlta').prop("disabled", false);
      var formdata = new FormData($("#ambientalForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../guardaAmbiental",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          $('#fechaAlta').prop("disabled", true);
          $('#link').val('');
          $('#encabezado').val('');
          console.log(result)
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          )
          $('#selectMedio').val('def').trigger('change');
          $('#selectEdo').val('def').trigger('change');
          $('#selectSubProc').val('def').trigger('change');
          $('#selectRef').val('def').trigger('change');
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      texto.validate();
      link.validate();
      encabezado.validate();
      referencia.validate();
    }

  });
  ///datatable notas///
  var tableNotas = $('#tableNotas').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "getNotas",
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "medio_rel.nomMedio" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "link" },
      { "data": "fcPublicacion" },
      { "data": "fcAlta" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNota" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNota" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      },
      {
        "targets": 10,
        "data": "tipo_rel",
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null) {
            return "S/T";
          }
          else {
            return dta.nomTipo;
          }
        }
      },
      {
        "targets": 11,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          var show = (row.idTipo === null) ? "" : 'display:none';
          var showSend = (row.idTipo != null && row.statusEnvio === null) ? "" : "display:none";
          var showStatus = (row.idTipo != null && row.statusEnvio != null) ? "" : "display:none";
          return '<a class="waves-effect waves-light btn btnSelectTipo" style="background-color:#6f7271; ' + show + '" title="asignar tipo"><i class="material-icons">indeterminate_check_box</i></a>' +
            '<a class="waves-effect waves-light btn btnEnviaNota" style="background-color:#215a4c; ' + showSend + '" title="enviar nota"><i class="material-icons">email</i></a>' +
            '<i class="material-icons iconGreen" style="' + showStatus + '">check_circle_outline</i>';
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotas tbody').on('click', '.btnEditNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    window.location.href = 'editaNota/' + idNota;
  });
  $('#btnModifNotas').on('click', function (e) {
    e.preventDefault();
    //alert('hola');
    var formdata = new FormData($("#editNotasForm")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../modificaNota",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        //alert('Guardada');
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido modificados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/notas';
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  $('#tableNotas tbody').on('click', '.btnBorrarNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    //window.location.href ='editaNota/'+idNota;
    Swal.fire({
      title: '¿Borrar esta nota?',
      text: "Eliminara el registro de la nota",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraNota",
          type: 'POST',
          dataType: 'json',
          data: { idNota: idNota },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/notas';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  $('#tableNotas tbody').on('click', '.btnSelectTipo', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(idNota)
    $('#selectTipo').select2();
    $('#modalSelect').openModal();
    $('#idNota').val(idNota);
  });
  $('#tableNotas tbody').on('click', '.btnEnviaNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(tr[0]);
    //return false;
    Swal.fire({
      title: '¿Enviar la nota por correo electrónico?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../enviaNota",
          type: 'POST',
          dataType: 'json',
          data: { idNota: idNota },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'La nota ha sido enviada',
              'success'
            )
            tableNotas.ajax.reload();
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })

  });
  /// Datatable Notas dado un estado ///
  var idEstado = $('#idEntidad').val();
  //console.log(idEstado);
  cad = '../getNotasByEdo/' + idEstado;
  //return false;
  var tableNotasEdo = $('#tableNotasEdo').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": cad,
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "medio_rel.nomMedio" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "fcPublicacion" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 7,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      {
        "targets": 8,
        "data": "tipo_rel",
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null) {
            return "S/T";
          }
          else {
            return dta.nomTipo;
          }
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          var show = (row.idTipo === null) ? "" : 'display:none';
          var showSend = (row.idTipo != null && row.statusEnvio === null) ? "" : "display:none";
          var showStatus = (row.idTipo != null && row.statusEnvio != null) ? "" : "display:none";
          return '<a class="waves-effect waves-light btn btnSelectTipo" style="background-color:#6f7271; ' + show + '" title="asignar tipo"><i class="material-icons">indeterminate_check_box</i></a>' +
            '<a class="waves-effect waves-light btn btnEnviaNota" style="background-color:#215a4c; ' + showSend + '" title="enviar nota"><i class="material-icons">email</i></a>' +
            '<i class="material-icons iconGreen" style="' + showStatus + '">check_circle_outline</i>';
        }
      },
    ],
  });

  $('#selectSubProc').change(function () {
    var idArea = $('#selectSubProc').val();
    cad = '../getNotasByEdoArea/' + idEstado + '/' + idArea;
    $('#tableNotasEdo').DataTable().destroy();
    tableNotasEdo = $('#tableNotasEdo').DataTable({
      "language": spanish,
      "responsive": true,
      "processing": true,
      "serverSide": true,
      "ajax": cad,
      "columns": [
        { "data": "" },
        { "data": "idNota" },
        { "data": "encabezado" },
        { "data": "medio_rel.nomMedio" },
        { "data": "entidad_rel.nomEntidad" },
        { "data": "area_rel.nomArea" },
        { "data": "fcPublicacion" },
      ],
      "columnDefs": [
        {
          "targets": 0,
          "data": null,
          "orderable": false,
          "className": 'details-control',
          "defaultContent": ''
        },
        {
          "targets": 7,
          "data": "link",
          "orderable": false,
          "className": 'dt-center',
          "render": function (dta, type, row) {
            if (dta == "" || dta == null) {
              return "S/L";
            }
            else {
              return ' <a href="' + dta + '" target="_blank">link</a> ';
            }
          }
        },
        {
          "targets": 8,
          "data": "tipo_rel",
          "className": "dt-center",
          "render": function (dta, type, row) {
            if (dta === null) {
              return "S/T";
            }
            else {
              return dta.nomTipo;
            }
          }
        },
        {
          "targets": 9,
          "data": null,
          "visible": true,
          "className": "dt-center",
          "render": function (dta, type, row) {
            var show = (row.idTipo === null) ? "" : 'display:none';
            var showSend = (row.idTipo != null && row.statusEnvio === null) ? "" : "display:none";
            var showStatus = (row.idTipo != null && row.statusEnvio != null) ? "" : "display:none";
            return '<a class="waves-effect waves-light btn btnSelectTipo" style="background-color:#6f7271; ' + show + '" title="asignar tipo"><i class="material-icons">indeterminate_check_box</i></a>' +
              '<a class="waves-effect waves-light btn btnEnviaNota" style="background-color:#215a4c; ' + showSend + '" title="enviar nota"><i class="material-icons">email</i></a>' +
              '<i class="material-icons iconGreen" style="' + showStatus + '">check_circle_outline</i>';
          }
        },
      ],
    });
    //console.log(cad);
    //alert(idArea);
  })
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotasEdo tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasEdo.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasEdo tbody').on('click', '.btnSelectTipo', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(idNota)
    $('#selectTipo').select2();
    $('#modalSelect').openModal();
    $('#idNota').val(idNota);
  });
  $('#btnAsigTipo').on('click', function () {
    var selectTipo = $('#selectTipo').parsley();
    if (selectTipo.isValid()) {
      var idNota = $('#idNota').val();
      var idTipo = $('#selectTipo').val();
      $('#selectTipo').val('def').trigger('change');
      console.log(idNota + ' ' + idTipo);
      //return false;
      $('#modalSelect').closeModal();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "../asignaTipo",
        type: 'POST',
        dataType: 'json',
        data: { idNota: idNota, idTipo: idTipo },
        /* cache:false,
        processData: false,
        contentType: false, */
      })
        .done(function (result) {
          //alert('Guardada');
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido guardados',
            'success'
          )
          tableNotasEdo.ajax.reload();
          tableNotas.ajax.reload();
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
          //alert(result.responseJSON)
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      selectTipo.validate();
    }

  })
  $('#tableNotasEdo tbody').on('click', '.btnEnviaNota', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    console.log(idNota);
    Swal.fire({
      title: '¿Enviar la nota por correo electrónico?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../enviaNota",
          type: 'POST',
          dataType: 'json',
          data: { idNota: idNota },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'La nota ha sido enviada',
              'success'
            )
            tableNotasEdo.ajax.reload();
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })

  });
  /// Datatable ocho columnas ///
  var tableOchoColumnas = $('#tableOchoColumnas').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getOchoColumnas",
    "columns": [
      { "data": "" },
      { "data": "idOchoColumna" },
      { "data": "encabezado" },
      { "data": "fuente" },
      { "data": "fcAlta" },

    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 5,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditOcho" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarOcho" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableOchoColumnas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableOchoColumnas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableOchoColumnas tbody').on('click', '.btnEditOcho', function () {
    var tr = $(this).closest('tr');
    var idOcho = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    window.location.href = 'editaOchoColumnas/' + idOcho;
  });
  $('#btnModifColumnas').on('click', function (e) {
    e.preventDefault();
    /* alert('hola');
    return false; */
    var fuenteA = $('#selectFuente option:selected').text();
    var formdata = new FormData($("#editColumnasForm")[0]);
    formdata.append('fuenteA', fuenteA);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../modificaColumnas",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        //alert('Guardada');
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido modificados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/ochoColumnas';
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  $('#tableOchoColumnas tbody').on('click', '.btnBorrarOcho', function () {
    var tr = $(this).closest('tr');
    var idOcho = tr[0].childNodes[1].firstChild.data;
    //console.log(idOcho)
    //window.location.href ='editaOchoColumnas/'+idOcho;
    Swal.fire({
      title: '¿Borrar esta columna?',
      text: "Eliminara el registro de la columna",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraColumnas",
          type: 'POST',
          dataType: 'json',
          data: { idColumna: idOcho },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/ochoColumnas';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  /// Datatable boletines ///
  var tableBoletines = $('#tableBoletines').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getBoletines",
    "columns": [
      { "data": "" },
      { "data": "idBoletin" },
      { "data": "encabezado" },
      { "data": "fcAlta" },

    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditBoletin" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarBoletin" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableBoletines tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableBoletines.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableBoletines tbody').on('click', '.btnEditBoletin', function () {
    var tr = $(this).closest('tr');
    var idBoletin = tr[0].childNodes[1].firstChild.data;
    //console.log(idBoletin)
    window.location.href = 'editaBoletin/' + idBoletin;
  });
  $('#btnModifBoletin').on('click', function (e) {
    e.preventDefault();
    /* alert('hola');
    return false; */
    var formdata = new FormData($("#editBoletinForm")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../modificaBoletin",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        //alert('Guardada');
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido modificados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/boletines';
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  $('#tableBoletines tbody').on('click', '.btnBorrarBoletin', function () {
    var tr = $(this).closest('tr');
    var idBoletin = tr[0].childNodes[1].firstChild.data;
    //console.log(idOcho)
    //window.location.href ='editaOchoColumnas/'+idOcho;
    Swal.fire({
      title: '¿Borrar este Boletín?',
      text: "Eliminara el registro del boletín",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraBoletin",
          type: 'POST',
          dataType: 'json',
          data: { idBoletin: idBoletin },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/boletines';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  /// Datatable ambientales ///
  var tableAmbientales = $('#tableAmbientales').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getAmbientales",
    "columns": [
      { "data": "" },
      { "data": "idInfoAmbiental" },
      { "data": "encabezado" },
      { "data": "fcAlta" },

    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditAmbiental" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarAmbiental" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableAmbientales tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableAmbientales.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableAmbientales tbody').on('click', '.btnEditAmbiental', function () {
    var tr = $(this).closest('tr');
    var idAmbiental = tr[0].childNodes[1].firstChild.data;
    //console.log(idBoletin)
    window.location.href = 'editaAmbiental/' + idAmbiental;
  });
  $('#btnModifAmbiental').on('click', function (e) {
    e.preventDefault();
    /* alert('hola');
    return false; */
    var formdata = new FormData($("#editaAmbientalForm")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../modificaAmbiental",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        //alert('Guardada');
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido modificados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/ambiental';
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  $('#tableAmbientales tbody').on('click', '.btnBorrarAmbiental', function () {
    var tr = $(this).closest('tr');
    var idAmbiental = tr[0].childNodes[1].firstChild.data;
    //console.log(idOcho)
    //window.location.href ='editaOchoColumnas/'+idOcho;
    Swal.fire({
      title: '¿Borrar Información Ambiental?',
      text: "Eliminara el registro de información ambiental",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraAmbiental",
          type: 'POST',
          dataType: 'json',
          data: { idAmbiental: idAmbiental },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/ambiental';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  /// enviar sintesis ///
  $('#sendSintesis').click(function (e) {
    e.preventDefault();
    //alert('hola');
    Swal.fire({
      title: '¿Enviar síntesis Matutina?',
      //text: "La síntesis será enviada a todas las direcciones de correo registradas junto a losarchivos seleccionados",
      //icon: 'warning',
      html: '<h6>Selecciona los archivos que se enviaran junto con la síntesis</h6>' +
        '<input type="checkbox" id="archivo1" name="incidencias" value="inc"><label for="archivo1"> Incidencias</label><br>' +
        '<input type="checkbox" id="archivo2" name="respuestas" value="resp"><label for="archivo2"> Respuestas</label><br>',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        var file1 = Swal.getPopup().querySelector('#archivo1').checked
        if (file1 == true) {
          file1 = 'inc'
        }
        else if (file1 == false) {
          file1 = null
        }
        var file2 = Swal.getPopup().querySelector('#archivo2').checked
        if (file2 == true) {
          file2 = 'resp'
        }
        else if (file2 == false) {
          file2 = null
        }
        //console.log(file1+' '+file2);
        //return false;
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/sintesisInformativa/enviaSintesis/m/" + file1 + "/" + file2,
          type: 'GET',
          /* dataType: 'json',
          data: {idBoletin:idBoletin}, */
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Enviada',
              '',
              'success'
            )
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  $('#sendSintesisVesp').click(function (e) {
    e.preventDefault();
    //alert('hola');
    Swal.fire({
      title: '¿Enviar síntesis Vespertina?',
      //text: "La síntesis será enviada a todas las direcciones de correo registradas junto a losarchivos seleccionados",
      //icon: 'warning',
      html: '<h6>Selecciona los archivos que se enviaran junto con la síntesis</h6>' +
        '<input type="checkbox" id="archivo1" name="incidencias" value="inc"><label for="archivo1"> Incidencias</label><br>' +
        '<input type="checkbox" id="archivo2" name="respuestas" value="resp"><label for="archivo2"> Respuestas</label><br>',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        var file1 = Swal.getPopup().querySelector('#archivo1').checked
        if (file1 == true) {
          file1 = 'inc'
        }
        else if (file1 == false) {
          file1 = null
        }
        var file2 = Swal.getPopup().querySelector('#archivo2').checked
        if (file2 == true) {
          file2 = 'resp'
        }
        else if (file2 == false) {
          file2 = null
        }
        //console.log(file1+' '+file2);
        //return false;
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/sintesisInformativa/enviaSintesis/v/" + file1 + "/" + file2,
          type: 'GET',
          /* dataType: 'json',
          data: {idBoletin:idBoletin}, */
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Enviada',
              '',
              'success'
            )
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  /// datatable incidencias ///
  var tableIncidencias = $('#tableIncidencias').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getIncidencias",
    "columns": [
      { "data": "" },
      { "data": "idIncidencia" },
      { "data": "tema" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "propuesta_rel",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === 0) {
            var cad = 'S/P'
          }
          else if (dta !== null) {
            if (dta.idTipoPropuesta === 1) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Tweet</a>'
            }
            else if (dta.idTipoPropuesta === 2) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Boletín</a>'
            }
          }
          return cad
        }
      },
      {
        "targets": 7,
        "data": 'docIncidencia',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 8,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarIncidencia" style="background-color:#9f2241" title="borrar incidencia"><i class="material-icons">delete</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableIncidencias tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableIncidencias.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableIncidencias tbody').on('click', '.btnVerIncidente', function () {
    var tr = $(this).closest('tr');
    var idIncidencia = tr[0].childNodes[1].firstChild.data;
    //console.log(idIncidente)
    window.location.href = 'verIncidencia/' + idIncidencia;
  });
  $('#tableIncidencias tbody').on('click', '.btnBorrarIncidencia', function () {
    var tr = $(this).closest('tr');
    var idIncidencia = tr[0].childNodes[1].firstChild.data;
    console.log(idIncidencia)
    Swal.fire({
      title: '¿Borrar esta incidencia?',
      text: "Eliminara el registro de incidencia",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Borrar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        //console.log(idNota);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../borraIncidencia",
          type: 'POST',
          dataType: 'json',
          data: { idIncidencia: idIncidencia },
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido borrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/incidencias';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });

  // Envia revista //
  $('#sendRevista').click(function (e) {
    e.preventDefault();
    /* alert('hola');
    return false; */
    Swal.fire({
      title: '¿Enviar Revista?',
      text: "La revista será enviada a todas las direcciones de correo registradas",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Enviar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/enviaRevista",
          type: 'GET',
          /* dataType: 'json',
          data: {idBoletin:idBoletin}, */
          /* cache:false,
          processData: false,
          contentType: false, */
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Enviada',
              '',
              'success'
            )
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
      }
    })
  });
  /// datatable respuestas ///
  var tableRespuestas = $('#tableRespuestas').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getRespuestas",
    "columns": [
      { "data": "" },
      { "data": "idRespuesta" },
      { "data": "nota_rel.encabezado" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "nota_rel.fcEnvio" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 7,
        "data": "propuesta_rel",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === 0) {
            var cad = 'S/P'
          }
          else if (dta !== null) {
            if (dta.idTipoPropuesta === 1) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Tweet</a>'
            }
            else if (dta.idTipoPropuesta === 2) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Boletín</a>'
            }
          }
          return cad
        }
      },
      {
        "targets": 8,
        "data": 'docRespuesta',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerRespuesta" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableRespuestas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableRespuestas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableRespuestas tbody').on('click', '.btnVerRespuesta', function () {
    var tr = $(this).closest('tr');
    var idRespuesta = tr[0].childNodes[1].firstChild.data;
    window.location.href = 'verRespuesta/' + idRespuesta;
  });
  $('#descargaRespuestas').click(function (e) {
    e.preventDefault();
    var href = '/generaPDFRespuestas/b';
    window.open(href);
  });
  // actions pdf //
  $('#validaPdf').click(function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Generando...',
      allowEscapeKey: false,
      allowOutsideClick: false,
    })
    Swal.showLoading();
    $.ajax({
      url: "/validaPdf",
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result) 
        Swal.close();
        var href = result;
        window.open(href);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
    //alert('hola');
    //window.open(href);
  });
  ///Carga CSV///
  $('#cargaCsv').click(function (e) {
    e.preventDefault();
    var formdata = new FormData($("#csvForm")[0]);
    Swal.fire({
      title: 'Guardando...',
      allowEscapeKey: false,
      allowOutsideClick: false,
    })
    Swal.showLoading();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "cargaCSV",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido registrados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/completaNotasCSV';
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log(result);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Ambientales//
  $('#cargaCsvAmbiental').click(function (e) {
    e.preventDefault();
    var formdata = new FormData($("#csvForm")[0]);
    Swal.fire({
      title: 'Guardando...',
      allowEscapeKey: false,
      allowOutsideClick: false,
    })
    Swal.showLoading();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "cargaCSVAmbiental",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        console.log(result)
        Swal.fire(
          'Guardado',
          'Los datos han sido registrados',
          'success'
        )
        var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {
          window.location.href = '/completaInfoAmbientalCSV';
          //window.reload();
        }, delayInMilliseconds);
      })
      .fail(function (result) {
        console.log(result);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
      })
      .always(function () {
        console.log('complete');
      });
  })
  //Filter Incidencias//
  $('#selectArea').change(function () {
    $('#selectSubArea').val('def').trigger('change');
    var subAreaArr = [];
    var area = $(this).val();
    if (area == null || area == '') {
      return false;
    }
    //console.log(area);
    $.ajax({
      url: "/incidencia/listaSubAreas/" + area,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            subAreaArr.push({
              id: result[i].idSubArea,
              subArea: result[i].nomSubArea,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Área</option>';
        for (const i in subAreaArr) {
          cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectSubArea').empty()
        $('#selectSubArea').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Btn filter Incidencias//
  $('#filterIncidencias').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormInc")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../filtraIncidencias",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelInc').show();
        url = "getFilteredIncidencias/" + result
        tableIncidencias.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Incidencias//
  $('#limpiaFilter').click(function (e) {
    e.preventDefault();
    $('#selectEdo').val('def').trigger('change');
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');
    $('#selectMateria').val('def').trigger('change');
    $('#excelInc').hide();
    $('#tema').val(null);
    $('#problematica').val(null);
    $('#datefilter').val(null);
    tableIncidencias.ajax.url("getIncidencias").load();
  });

  //Lugar Filtro notas//
  $('#selectEdoF').change(function () {
    idEntidad = $(this).val();
    var lugaresArr = [];
    $.ajax({
      url: "getLugares/" + idEntidad,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            lugaresArr.push({
              id: result[i].idLugar,
              lugar: result[i].nomLugar,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Lugar Específico</option>';
        for (const i in lugaresArr) {
          cadCom += '<option value="' + lugaresArr[i].id + '">' + lugaresArr[i].lugar
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectLugarF').empty()
        $('#selectLugarF').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  })
  //Btn filter Notas//
  $('#filterNotas').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormInc")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../filtraNotas",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelNotas').show();
        url = "getFilteredNotas/" + result
        tableNotas.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Notas//
  $('#limpiaFilterNotas').click(function (e) {
    e.preventDefault();
    $('#selectEdoF').val('def').trigger('change');
    $('#selectLugarF').val('def').trigger('change');
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');
    $('#selectCategoria').val('def').trigger('change')
    $('#excelNotas').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    tableNotas.ajax.url("getNotas").load();
  });

  //Btn filter Respuestas//
  $('#filterRespuestas').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormResp")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../filtraRespuestas",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelRespuestas').show();
        url = "getFilteredRespuestas/" + result
        tableRespuestas.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Respuestas//
  $('#limpiaFilterRespuestas').click(function (e) {
    e.preventDefault();
    $('#selectEdo').val('def').trigger('change');
    /* $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change'); */
    $('#excelRespuestas').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    $('#datefilter2').val(null);
    tableRespuestas.ajax.url("getRespuestas").load();
  });
  //Excel Incidencias//
  $('#excelInc').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdo').val()
    var area = $('#selectArea').val()
    var subArea = $('#selectSubArea').val()
    var tema = $('#tema').val();
    var problematica = $('#problematica').val();
    var materia = $('#selectMateria').val();
    var accionVS = $('#selectAccioVS').val();
    var datefilter = $('#datefilter').val();
    dta = {
      entidad: entidad,
      area: area,
      subArea: subArea,
      tema: tema,
      problematica: problematica,
      materia: materia,
      accionVS: accionVS,
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelIncidencias/" + dta //comentar en producción        
    window.location = url
  });
  //Excel Notas//
  $('#excelNotas').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdoF').val();
    var lugar = $('#selectLugarF').val();
    var area = $('#selectArea').val();
    var subArea = $('#selectSubArea').val()
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();
    var categoria = $('#selectCategoria').val();
    var datefilter = $('#datefilter').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,
      lugar: lugar,
      area: area,
      subArea: subArea,
      encabezado: encabezado,
      texto: texto,
      categoria: categoria,
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelNotas/" + dta //comentar en producción        
    window.location = url
  });
  //Excel Respuestas//
  $('#excelRespuestas').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdo').val()
    /* var area = $('#selectArea').val()
    var subArea = $('#selectSubArea').val() */
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();
    var datefilter = $('#datefilter').val();
    var datefilter2 = $('#datefilter2').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,
      /* area: area,
      subArea: subArea, */
      encabezado: encabezado,
      texto: texto,
      datefilter: datefilter.replaceAll('/', '.'),
      datefilter2: datefilter2.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelRespuestas/" + dta //comentar en producción        
    window.location = url
  });
  //accion Vida silvestre//
  $('#selectSubArea').change(function () {
    $('#contSelAccVS').hide();
    $('#selectAccioVS').prop('required', false)
    $('#selectAccioVS').val('def').trigger('change');
    var subArea = $(this).val();
    if (subArea == 2) {
      $('#contSelAccVS').show();
      $('#selectAccioVS').prop('required', true);
    }
  });
  //semaforo nota//
  $('input:radio[name="categoria"]').change(function () {
    var valor = $(this).val();
    if (valor == 'verde') {
      $('.option-1').css("background-color", "green");
      $('.option-2').css("background-color", "#fff");
      $('.option-3').css("background-color", "#fff");
    }
    else if (valor == 'amarillo') {
      $('.option-2').css("background-color", "yellow");
      $('.option-1').css("background-color", "#fff");
      $('.option-3').css("background-color", "#fff");
    }
    else if (valor == 'rojo') {
      $('.option-3').css("background-color", "red");
      $('.option-1').css("background-color", "#fff");
      $('.option-2').css("background-color", "#fff");
    }
  });
  //notas desde feeder//
  $('#btnFeeder').click(function (e) {
    e.preventDefault;
    $.ajax({
      url: "/getNotasAPI",
      type: 'GET',
      dataType: 'json',
    })
      .done(function (result) {
        console.log(result)
        return false;

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  ///datatable temas relevantes///
  var tableTemasRel = $('#tableTemasRel').DataTable({
    "language": spanish,
    "lengthMenu": [25, 1, 10, 50, 100],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/getTemasRel",
    "columns": [
      //{ "data": "" },
      { "data": "idTemaRel" },
      { "data": "temaRel" },
      { "data": "nomEntidad" },
      { "data": "status" },
      //{ "data": "fcCrea" }
    ],
    "columnDefs": [
      /* {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      }, */
      {
        "targets": 3,
        "data": "status",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return 'Con respuesta'
          }
          else if (row.status == 2) {
            return 'Cerrado'
          }
          else if (row.status == null) {
            return 'Activo'
          }
        }
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>' +
              '<a class="waves-effect waves-light btn btnFinTemaRel" style="background-color:#9f2241" title="cerrar tema"><i class="material-icons">block</i></a>'
          }
          else if (row.status == 2) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>' +
              '<a class="waves-effect waves-light btn btnAbrirTemaRel" style="background-color:#ddc9a3" title="abrir tema"><i class="material-icons">open_in_new</i></a>'
          }
          else if (row.status == null) {
            return '<a class="waves-effect waves-light btn btnSolInfo" style="background-color:#10312b" title="solicitar información">' +
              '<i class="material-icons">pending_actions</i>' +
              '</a>'
          }
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableTemasRel tbody').on('click', '.btnVerRespTemarel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    window.location.href = '/verRespuestaTemaRel/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnFinTemaRel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    //console.log(idTema);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/cierraTemaRel",
      type: 'POST',
      dataType: 'json',
      data: { idTema: idTema },
    })
      .done(function (result) {
        console.log(result)
        Swal.fire(
          'Guardado',
          'Tema cerrado',
          'success'
        )
        /* var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {            
          window.location.href = '/delegado/temasRelevantes';
        }, delayInMilliseconds); */
        tableTemasRel.ajax.reload();
      })
      .fail(function (result) {
        console.log(result);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
      })
      .always(function () {
        console.log('complete');
      });
    //window.location.href = '/delegado/verRespuestaTemaRel/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnAbrirTemaRel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/abreTemaRel",
      type: 'POST',
      dataType: 'json',
      data: { idTema: idTema },
    })
      .done(function (result) {
        console.log(result)
        Swal.fire(
          'Guardado',
          'Tema Abierto',
          'success'
        )
        /* var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {            
          window.location.href = '/delegado/temasRelevantes';
        }, delayInMilliseconds); */
        tableTemasRel.ajax.reload();
      })
      .fail(function (result) {
        console.log(result);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
      })
      .always(function () {
        console.log('complete');
      });
    //window.location.href = '/delegado/verRespuestaTemaRel/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnSolInfo', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    var task = 'temaRel';
    masInfoTema(idTema, task)
  });
  //guardar tema relevante//
  $('#guardaTemaRel').click(function (e) {
    var tema = $('#temaRel').parsley();
    var edo = $('#selectEdo').parsley();
    if (tema.isValid() && edo.isValid()) {
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      var formdata = new FormData($("#temaRelForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/guardaTemaRel",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          console.log(result)
          Swal.fire(
            'Guardado',
            '',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            window.location.href = '/temasRelevantes';
          }, delayInMilliseconds);

        })
        .fail(function (result) {
          console.log(result);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      tema.validate();
      edo.validate();
    }

  });

  //Notas Informativas//
  ///Datatable Notas Informativas///
  var tableNotasInfo = $('#tableNotasInfo').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "getNotasInformativas",
    "columns": [
      { "data": "" },
      { "data": "idNotaInformativa" },
      { "data": "encabezado" },
      //{ "data": "resumen" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "fcSuceso" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerNotaInfo" style="background-color:#6f7271" title="ver nota"><i class="material-icons">visibility</i></a>'
        }
      },
      /* {
        "targets": 8,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNotaInfo" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNotaInfo" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      }, */
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotasInfo tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasInfo.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format3(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasInfo tbody').on('click', '.btnVerNotaInfo', function () {
    var tr = $(this).closest('tr');
    var idNotaInfo = tr[0].childNodes[1].firstChild.data;
    window.location.href = '/verNotaInformativa/' + idNotaInfo;
  });

  ///Guarda Notas Informativas///
  $('#guardaNotaInfo').click(function (e) {
    e.preventDefault();
    var estado = $('#selectEdo').parsley();
    var area = $('#selectSubProc').parsley();    
    if ($('#selectSubProc').val() !== 4) {
      var subArea = $('#selectSubArea').parsley();
    }
    var campos = $('#notasInfoForm').find('.validar').parsley();
    var ctl = 0;    
    for (const i in campos) {
      if (campos[i].isValid() && estado.isValid() && area.isValid()) {
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          if (subArea.isValid()) {
            ctl++;
          }
        }
        else {
          ctl++
        }
      }
      else {
        campos[i].validate();
        estado.validate();
        area.validate();
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          subArea.validate();
        }
      }
      if (ctl === campos.length) {        
        var formdata = new FormData($("#notasInfoForm")[0]);
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "guardaNotaInformativa",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido registrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              //location.reload();
              $("#notasInfoForm").trigger('reset');
              $('#selectEdo').val('def').trigger('change');
              $('#selectArea').val('def').trigger('change');
              $('#selectSubArea').val('def').trigger('change');
              $('.dropify-clear').click();
              location.reload(true);
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log(result);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
          })
          .always(function () {
            console.log('complete');
          })
      }
    }
  });

  //Btn filter Notas Informativas//
  $('#filterNotasInfo').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormNotasInfo")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "../filtraNotasInfo",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelNotasInfo').show();
        url = "getFilteredNotasInfo/" + result
        tableNotasInfo.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });

  //Limpiar Filtro Notas informativas//
  $('#limpiaFilterNotasInfo').click(function (e) {
    e.preventDefault();
    $('#selectEdoF').val('def').trigger('change');
    $('#selectArea').val('def').trigger('change');
    $('#selectSubArea').val('def').trigger('change');
    $('#excelNotasInfo').hide();
    $('#encabezado').val(null);
    $('#texto').val(null);
    $('#datefilter').val(null);
    tableNotasInfo.ajax.url("getNotasInformativas").load();
  });

  //Excel Notas Informativas//
  $('#excelNotasInfo').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdoF').val();
    var area = $('#selectArea').val();
    var subArea = $('#selectSubArea').val()
    var encabezado = $('#encabezado').val();
    var texto = $('#texto').val();
    var datefilter = $('#datefilter').val();
    datefilter.replaceAll('/', '-')
    /* console.log(datefilter.replaceAll('/','-'));
    return false; */
    dta = {
      entidad: entidad,
      area: area,
      subArea: subArea,
      encabezado: encabezado,
      texto: texto,
      datefilter: datefilter.replaceAll('/', '.')
    }
    dta = JSON.stringify(dta);
    var url = "getExcelNotasInfo/" + dta //comentar en producción        
    window.location = url
  });

  //Presentaciones//
  //Carga presentaciones//
  $('#guardaPresentacion').click(function (e) {
    e.preventDefault();
    var campos = $('#presentacionForm').find('.validar').parsley();
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid()) {
        ctl++
      }
      else {
        campos[i].validate();

      }
    }
    if (campos.length == ctl) {
      var formdata = new FormData($("#presentacionForm")[0]);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/guardaPresentacion",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            //location.reload();
            $("#presentacionForm").trigger('reset');            
            $('.dropify-clear').click();
            location.reload(true);
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  });

  ///Datatable Presentaciones///
  var tablePres = $('#tablePres').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "getPresentaciones",
    "columns": [
      { "data": "idPresentacion" },
      { "data": "titulo" },
      /* { "data": "encabezado" }, */      
    ],
    "columnDefs": [
      /* {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      }, */
      {
        "targets": 2,
        "data": "pathFile",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          return ' <a href="' + dta + '" target="_blank">Archivo</a> ';
        }
      },      
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  /* $('#tableNotasInfo tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasInfo.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format3(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasInfo tbody').on('click', '.btnVerNotaInfo', function () {
    var tr = $(this).closest('tr');
    var idNotaInfo = tr[0].childNodes[1].firstChild.data;
    window.location.href = '/verNotaInformativa/' + idNotaInfo;
  }); */

});

/////////////////////////////
$(document).on('click', '.eliminaColum', function (e) {
  e.preventDefault();
  $(this).parent('li').remove();
  var id = $(this).data('id');
  delete columnas[id];
  console.log(columnas);
});
//Select Lugar//
$(document).on('change', '#selectLugar', function () {
  lugar = $(this).val();
  if (lugar === 'otro') {
    $("#contSelLugar").hide();
    var cad = '<label for="flugar">Lugar Especfico</label>' +
      '<input id="lugar" class="validar" name="lugar" type="text" required>';
    $("#contOtroLugar").html(cad);
  }
});
//Select Suceso//
$(document).on('change', '#selectSuceso', function () {
  suceso = $(this).val();
  console.log(suceso);
  if (suceso === 'otro') {
    $("#contSelSuceso").hide();
    var cad = '<label for="suceso">Suceso</label>' +
      '<input id="suceso" class="validar" name="suceso" type="text" required>';
    $("#contOtroSuceso").html(cad);
  }
});
