// Funciones //
function format(d) {
  var ret = d.textoHtml;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;
}
function format2(d) {
  var ret = d.actuacionHtml;
  ret = ret.replace(/&gt;/g, '>');
  ret = ret.replace(/&lt;/g, '<');
  ret = ret.replace(/&quot;/g, '"');
  ret = ret.replace(/&apos;/g, "'");
  ret = ret.replace(/&amp;/g, '&');
  //console.log(ret);
  return ret;
}
var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="material-icons miTi">chevron_left</i>',
    "sLast": '<i class="material-icons miTi">chevron_right</i>',
    "sNext": '<i class="material-icons miTi">chevron_right</i>',
    "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}
function destory_editor(selector) {
  if ($(selector)[0]) {
    var content = $(selector).find('.ql-editor').html();
    $(selector).html(content);

    $(selector).siblings('.ql-toolbar').remove();
    $(selector + " *[class*='ql-']").removeClass(function (index, css) {
      return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
    });

    $(selector + "[class*='ql-']").removeClass(function (index, css) {
      return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
    });
  }
  else {
    console.error('editor not exists');
  }
}

function goBack() {
  window.history.back();
}
/////////////////////////////////

// Ready //
$(document).ready(function () {
  $('.sel2').select2();
  // contador dinamico //
  $('.counter').each(function () {
    $(this).prop('Counter', 0).animate({
      Counter: $(this).text()
    }, {
      duration: 2000,
      easing: 'swing',
      step: function (now) {
        $(this).text(Math.ceil(now));
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
      }
    });
  });

  $('#selectEdo').select2();
  $('#selectArea').select2();
  $('#selectSubArea').select2();
  $('#selectProp').select2();
  $('#selectMateria').select2();
  $('#selectAccioVS').select2();
  //select anidado incidencias //
  $('#selectArea').change(function () {
    $('#selectSubArea').val('def').trigger('change');
    $('#selectAccioVS').val('def').trigger('change');
    var subAreaArr = [];
    var area = $(this).val();
    if (area == '' || area == null) {
      return false;
    }
    //console.log(area);
    $.ajax({
      url: "/incidencia/listaSubAreas/" + area,
      type: 'GET',
      cache: false,
    })
      .done(function (result) {
        //console.log(result)
        //return false;
        for (const i in result) {
          if (result[i] != null) {
            subAreaArr.push({
              id: result[i].idSubArea,
              subArea: result[i].nomSubArea,
            })
          }
        }
        var cadCom = '<option disabled selected value="def">Sub-Área</option>';
        for (const i in subAreaArr) {
          cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
            + '</option>';
        }
        //console.log(cadCom);
        $('#selectSubArea').empty()
        $('#selectSubArea').append(cadCom);

      })
      .fail(function (result) {
        console.log(result)
      })
      .always(function () {
        console.log('complete');
      });
  });
  $('#selectSubArea').change(function () {
    $('#contSelAccVS').hide();
    $('#selectAccioVS').prop('required', false)
    $('#selectAccioVS').val('def').trigger('change');
    var subArea = $(this).val();
    if (subArea == 2) {
      $('#contSelAccVS').show();
      $('#selectAccioVS').prop('required', true);
    }
  });
  // Switch //
  $('#propuestaS').change(function () {
    if ($(this).prop('checked')) {
      $('#flgPropuesta').val("1");
      $('.propuesta').show();
    }
    else {
      $('#flgPropuesta').val("0");
      //tweet//
      $('#txtTweet').val('');
      $('#txtTweet').removeAttr('name');
      $('#txtTweet').removeClass('validar');
      $('#tipoTweet').hide();
      //boletin//
      destory_editor('#txtBoletin');
      $('#textoHtmlBoletin').val('');
      $('#textoHtmlBoletin').removeAttr('name');
      $('#textoHtmlBoletin').removeClass('validar');
      $('#textoBoletin').val('');
      $('#textoBoletin').removeAttr('name');
      $('#textoBoletin').removeClass('validar');
      $('#titBoletin').val('');
      $('#titBoletin').removeAttr('name');
      $('#titBoletin').removeClass('validar');
      $('#txtBoletin').empty();
      $('#tipoBoletin').hide();
      //select//
      $('.propuesta').hide();
      $('#selectProp').val('def').trigger('change');
    }
  });
  // Tipo propuesta //
  $('#selectProp').change(function () {
    var propuesta = $(this).val();
    console.log(propuesta);
    if (propuesta === '1') { //Tweet
      //Boletin//
      $('#tipoBoletin').hide();
      destory_editor('#txtBoletin');
      $('#titBoletin').val('');
      $('#titBoletin').removeAttr('name');
      $('#titBoletin').removeClass('validar');
      $('#textoHtmlBoletin').val('');
      $('#textoHtmlBoletin').removeAttr('name');
      $('#textoHtmlBoletin').removeClass('validar');
      $('#textoBoletin').val('');
      $('#textoBoletin').removeAttr('name');
      $('#textoBoletin').removeClass('validar');
      //Tweet//
      $('#tipoTweet').show();
      $('#txtTweet').attr('name', 'txtTweet');
      $('#txtTweet').addClass('validar');
      $('#txtBoletin').empty();
    }
    if (propuesta === '2') { //Boletin
      //Tweet//
      $('#tipoTweet').hide();
      $('#txtTweet').val('');
      $('#txtTweet').removeAttr('name');
      $('#txtTweet').removeClass('validar');
      //Boletin//
      $('#tipoBoletin').show();
      $('#titBoletin').attr('name', 'titBoletin');
      $('#titBoletin').addClass('validar');
      $('#textoHtmlBoletin').attr('name', 'textoHtmlBoletin');
      $('#textoHtmlBoletin').addClass('validar');
      $('#textoBoletin').attr('name', 'textoBoletin');
      $('#textoBoletin').addClass('validar');
      var quillBoletin = new Quill('#txtBoletin', {
        modules: {
          toolbar: [
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            ['bold', 'italic', 'underline', 'strike'],
            [{ 'color': [] }, { 'background': [] }],
            ['link'],
            [{ 'script': 'sub' }, { 'script': 'super' }],
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],

          ]
        },
        theme: 'snow'  // or 'bubble'
      });
      $('.ql-snow select').addClass('browser-default');
      quillBoletin.on('text-change', function (delta, source) {
        updateHtmlOutput_msj()
      })
      $('#btn-convert').on('click', () => { updateHtmlOutput_msj() })
      function getQuillHtml_msj() { return quillBoletin.root.innerHTML; }
      function updateHtmlOutput_msj() {
        let html = getQuillHtml_msj();
        //console.log ( html );            
        $('#textoHtmlBoletin').val(html);
        var contenido = $('#textoHtmlBoletin').val();
        var texto = contenido.replace(/<[^>]*>?/g, '');
        $('#textoBoletin').val(texto);
      }
      updateHtmlOutput_msj();
      //quillBoletin.setContents([{ insert: '' }]);
    }
  })
  //Guarda Incidencia//
  $('#guardaIncidecnia').click(function (e) {
    e.preventDefault()
    var estado = $('#selectEdo').parsley();
    var area = $('#selectArea').parsley();
    var subArea = $('#selectSubArea').parsley();
    var materia = $('#selectMateria').parsley();
    if ($('#textoHtml').val() === '<p><br></p>') {
      $('#textoHtml').val('');
    }
    var campos = $('#incidenciaForm').find('.validar').parsley();
    //console.log(campos);
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid() && estado.isValid()) {
        ctl++
      }
      else {
        campos[i].validate();
        estado.validate();
        area.validate();
        subArea.validate();
        materia.validate();
      }
      if (ctl === campos.length) {
        var formdata = new FormData($("#incidenciaForm")[0]);
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../incidencia/guardaIncidencia",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido registrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              //location.reload();
              $("#incidenciaForm").trigger('reset');
              $('#selectEdo').val('def').trigger('change');
              $('#selectArea').val('def').trigger('change');
              $('#selectSubArea').val('def').trigger('change');
              quillNota.setContents([{ insert: '' }]);
              $('.dropify-clear').click();
              location.reload(true);
              /* $( "#notasForm" ).steps('reset'); */
              //var form = $("#notasForm").steps();
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log(result);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
          })
          .always(function () {
            console.log('complete');
          });
      }
    }
  });
  ///datatable notas///
  var tableNotas = $('#tableNotas').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/delegado/getNotas",
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "medio_rel.nomMedio" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "link" },
      { "data": "fcPublicacion" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  /// datatable incidencias ///
  var tableIncidencias = $('#tableIncidencias').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/delegado/getIncidencias",
    "columns": [
      { "data": "" },
      { "data": "idIncidencia" },
      { "data": "tema" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": 'docIncidencia',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 7,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableIncidencias tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableIncidencias.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableIncidencias tbody').on('click', '.btnVerIncidente', function () {
    var tr = $(this).closest('tr');
    var idIncidencia = tr[0].childNodes[1].firstChild.data;
    //console.log(idIncidente)
    window.location.href = 'verIncidencia/' + idIncidencia;
  });
  ///datatable solicitudes///
  var tableSolicitudes = $('#tableSolicitudes').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/delegado/getSolicitudes",
    "columns": [
      { "data": "" },
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "medio_rel.nomMedio" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "link" },
      { "data": "fcPublicacion" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      {
        "targets": 8,
        "data": "tipo_rel",
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null) {
            return "S/T";
          }
          else {
            return dta.nomTipo;
          }
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnRespondeSolicitud" style="background-color:#215a4c" title="Responder"><i class="material-icons">question_answer</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableSolicitudes tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableSolicitudes.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableSolicitudes tbody').on('click', '.btnRespondeSolicitud', function () {
    var tr = $(this).closest('tr');
    var idNota = tr[0].childNodes[1].firstChild.data;
    //console.log(idNota)
    window.location.href = '/delegado/responderSolicitud/' + idNota;
  });
  //Guarda Respuesta//
  $('#guardaRespuesta').click(function (e) {
    e.preventDefault()
    /* alert('guardar respuesta');
    return false; */
    var estado = $('#selectEdo').parsley();
    var area = $('#selectArea').parsley();
    var subArea = $('#selectSubArea').parsley();
    if ($('#textoHtml').val() === '<p><br></p>') {
      $('#textoHtml').val('');
    }
    var campos = $('#respuestaForm').find('.validar').parsley();
    //console.log(campos);
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid() && estado.isValid() && area.isValid() && subArea.isValid()) {
        ctl++
      }
      else {
        campos[i].validate();
        estado.validate();
        area.validate();
        subArea.validate();
      }
      if (ctl === campos.length) {
        var formdata = new FormData($("#respuestaForm")[0]);
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/delegado/guardaRespuesta",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido registrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              //location.reload();
              /* $("#incidenciaForm").trigger('reset');
              $('#selectEdo').val('def').trigger('change');
              $('#selectArea').val('def').trigger('change');
              $('#selectSubArea').val('def').trigger('change');
              quillNota.setContents([{ insert: '' }]);
              $('.dropify-clear').click();
              location.reload(); */
              window.location.href = '/delegado/solicitudes';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            //console.log(result);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
          })
          .always(function () {
            console.log('complete');
          });
      }
    }
  });
  ///datatable temas relevantes///
  var tableTemasRel = $('#tableTemasRel').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/delegado/getTemasRel",
    "columns": [
      //{ "data": "" },
      { "data": "idTemaRel" },
      { "data": "temaRel" },
      { "data": "status" },
      //{ "data": "entidad_rel.nomEntidad" },
      //{ "data": "fcSuceso" },
      //{ "data": "fcCrea" }
    ],
    "columnDefs": [
      /* {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      }, */
      {
        "targets": 2,
        "data": "status",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return 'Con respuesta'
          }
          else if (row.status == 2) {
            return 'Cerrado';
          }
          else if (row.status == null) {
            return 'Activo'
          }
        }
      },
      {
        "targets": 3,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == null || row.status == 1) {
            return '<a class="waves-effect waves-light btn btnRespondeTema" style="background-color:#215a4c" title="Responder"><i class="material-icons">question_answer</i></a>'
          }
          else {
            return '<i class="material-icons iconGreen" >check_circle_outline</i>';
          }

        }
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>' +
              '<a class="waves-effect waves-light btn btnFinTemaRel" style="background-color:#9f2241" title="cerrar tema"><i class="material-icons">block</i></a>'
          }
          else if (row.status == 2) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'
          }
          else if (row.status == null) {
            return '<span class="material-icons">pending_actions</span>'
          }
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableTemasRel tbody').on('click', '.btnRespondeTema', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    console.log(idTema)
    $('#idTema').val(idTema);
    window.location.href = '/delegado/responderTemaRelevante/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnVerRespTemarel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    window.location.href = '/delegado/verRespuestaTemaRel/' + idTema;
  });
  $('#tableTemasRel tbody').on('click', '.btnFinTemaRel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    //console.log(idTema);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/delegado/cierraTemaRel",
      type: 'POST',
      dataType: 'json',
      data: { idTema: idTema },
    })
      .done(function (result) {
        console.log(result)
        Swal.fire(
          'Guardado',
          'Tema cerrado',
          'success'
        )
        /* var delayInMilliseconds = 2000; //1 second
        setTimeout(function () {            
          window.location.href = '/delegado/temasRelevantes';
        }, delayInMilliseconds); */
        tableTemasRel.ajax.reload();
      })
      .fail(function (result) {
        console.log(result);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
      })
      .always(function () {
        console.log('complete');
      });
    //window.location.href = '/delegado/verRespuestaTemaRel/' + idTema;
  });
  $('#guardaRespTemaRel').click(function (e) {
    e.preventDefault();
    var campos = $('#respTemaRel').find('.validar').parsley();
    console.log(campos);
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid()) {
        ctl++
      }
      else {
        campos[i].validate();
      }
    }
    if (ctl === campos.length) {
      var formdata = new FormData($("#respTemaRel")[0]);
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/delegado/guardaRespuestaTemaRel",
        type: 'POST',
        dataType: 'json',
        data: formdata,
        cache: false,
        processData: false,
        contentType: false,
      })
        .done(function (result) {
          console.log(result)
          Swal.fire(
            'Guardado',
            'Los datos han sido registrados',
            'success'
          )
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            window.location.href = '/delegado/temasRelevantes';
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log(result);
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: result.responseJSON,
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  });

  //Notas informativas//
  //Select Sub-Area//
  $('.anid').change(function () {
    $('.contSA').empty();
    var cadSel = '<select id="selectSubArea" name="subArea" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
      '<option disabled selected value="def">Sub-Área</option>' +
      '</select>';
    $('#selectSubArea').val('def').trigger('change');
    $('.contSA').append(cadSel);
    $('#selectSubArea').select2();
    var subAreaArr = [];
    var area = $(this).val();
    /* console.log(area);
    return false; */
    if (area === '4') {
      $('.contSA').empty();
    }
    else {
      $.ajax({
        url: "../incidencia/listaSubAreas/" + area,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          //console.log(result)
          //return false;
          for (const i in result) {
            if (result[i] != null) {
              subAreaArr.push({
                id: result[i].idSubArea,
                subArea: result[i].nomSubArea,
              })
            }
          }
          var cadCom = '<option disabled selected value="def">Sub-Área</option>';
          for (const i in subAreaArr) {
            cadCom += '<option value="' + subAreaArr[i].id + '">' + subAreaArr[i].subArea
              + '</option>';
          }
          //console.log(cadCom);
          $('#selectSubArea').empty()
          $('#selectSubArea').append(cadCom);

        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    }
  });

  ///Datatable Notas Informativas///
  var tableNotasInfo = $('#tableNotasInfo').DataTable({
    "language": spanish,
    "responsive": true,
    "lengthMenu": [25, 1, 10, 50, 100],
    "processing": true,
    "serverSide": true,
    "ajax": "/delegado/getNotasInformativas",
    "columns": [
      { "data": "" },
      { "data": "idNotaInformativa" },
      { "data": "encabezado" },
      //{ "data": "resumen" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "area_rel.nomArea" },
      { "data": "fcSuceso" },
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerNotaInfo" style="background-color:#6f7271" title="ver nota"><i class="material-icons">visibility</i></a>'
        }
      },
      /* {
        "targets": 8,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnEditNotaInfo" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
            '<a class="waves-effect waves-light btn btnBorrarNotaInfo" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
        }
      }, */
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableNotasInfo tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableNotasInfo.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format3(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableNotasInfo tbody').on('click', '.btnVerNotaInfo', function () {
    var tr = $(this).closest('tr');
    var idNotaInfo = tr[0].childNodes[1].firstChild.data;
    window.location.href = '/delegado/verNotaInformativa/' + idNotaInfo;
  });

  ///Guarda Notas Informativas///
  $('#guardaNotaInfo').click(function (e) {
    e.preventDefault();
    var estado = $('#selectEdo').parsley();
    var area = $('#selectSubProc').parsley();
    if ($('#selectSubProc').val() !== 4) {
      var subArea = $('#selectSubArea').parsley();
    }
    var campos = $('#notasInfoForm').find('.validar').parsley();
    var ctl = 0;
    for (const i in campos) {
      if (campos[i].isValid() && estado.isValid() && area.isValid()) {
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          if (subArea.isValid()) {
            ctl++;
          }
        }
        else {
          ctl++
        }
      }
      else {
        campos[i].validate();
        estado.validate();
        area.validate();
        if ($('#selectSubProc').val() !== 4 && subArea !== undefined) {
          subArea.validate();
        }
      }
      if (ctl === campos.length) {
        var formdata = new FormData($("#notasInfoForm")[0]);
        Swal.fire({
          title: 'Enviando...',
          allowEscapeKey: false,
          allowOutsideClick: false,
        })
        Swal.showLoading();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "/delegado/guardaNotaInformativa",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido registrados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              //location.reload();
              $("#notasInfoForm").trigger('reset');
              $('#selectEdo').val('def').trigger('change');
              $('#selectArea').val('def').trigger('change');
              $('#selectSubArea').val('def').trigger('change');
              $('.dropify-clear').click();
              location.reload(true);
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log(result);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
          })
          .always(function () {
            console.log('complete');
          })
      }
    }
  });

});

//ON//
