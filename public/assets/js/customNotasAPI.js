var spanish = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
      "sFirst": '<i class="material-icons miTi">chevron_left</i>',
      "sLast": '<i class="material-icons miTi">chevron_right</i>',
      "sNext": '<i class="material-icons miTi">chevron_right</i>',
      "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
    },
    "oAria": {
      "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
      "copy": "Copiar",
      "colvis": "Visibilidad"
    }
  }
  function format(d) {
    var ret = d.texto;
    ret = ret.replace(/&gt;/g, '>');
    ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
    //console.log(ret);
    return ret;
  
  }
  /////////////////////////////////
  // Ready //
  $(document).ready(function () {
    flgSuceso = 0;    
    flgLugar = 0;
    var tableNotasAPI = $('#tableNotasAPI').DataTable({
        "language": spanish,
        "lengthMenu": [25, 1, 10, 50, 100],
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "getNotasAPIt",
        "columns": [
          { "data": "" },
          { "data": "idNota" },
          { "data": "encabezado" },      
          { "data": "medio_rel.nomMedio" },      
          { "data": "link" },          
          { "data": "fcPublicacion" },
          { "data": "fcAlta" },
        ],
        "columnDefs": [
          {
            "targets": 0,
            "data": null,
            "orderable": false,
            "className": 'details-control',
            "defaultContent": ''
          },
          {
            "targets": 4,
            "data": "link",
            "orderable": false,
            "className": 'dt-center',
            "render": function (dta, type, row) {
              if (dta == "" || dta == null) {
                return "S/L";
              }
              else {
                return ' <a href="' + dta + '" target="_blank">link</a> ';
              }
            }
          },          
          {
            "targets": 7,
            "data": null,
            "visible": true,
            "className": "dt-center",
            "render": function (dta, type, row) {
              return '<a class="waves-effect waves-light btn btnEditNota" style="background-color:#6f7271" title="editar nota"><i class="material-icons">edit</i></a>' +
                '<a class="waves-effect waves-light btn btnBorrarNota" style="background-color:#9f2241" title="borrar nota"><i class="material-icons">delete</i></a>'
            }
          },      
        ],
      });
      $('.dataTables_length select').addClass('browser-default');
      $('#tableNotasAPI tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //tr.addClass('miText');
        var row = tableNotasAPI.row(tr);
    
        if (row.child.isShown()) {
          // This row is already open - close it
          row.child.hide();
          tr.removeClass('shown');
        }
        else {
          // Open this row
          row.child(format(row.data())).show();
          tr.addClass('shown');
        }
      });
      $('#tableNotasAPI tbody').on('click', '.btnEditNota', function () {
        var tr = $(this).closest('tr');
        var idNota = tr[0].childNodes[1].firstChild.data;
        //console.log(idNota)
        window.location.href = 'editaNotaAPI/' + idNota;
      });
      $('#tableNotasAPI tbody').on('click', '.btnBorrarNota', function () {
        var tr = $(this).closest('tr');
        var idNota = tr[0].childNodes[1].firstChild.data;
        //console.log(idNota)
        //window.location.href ='editaNota/'+idNota;
        Swal.fire({
          title: '¿Borrar esta nota?',
          text: "Eliminara el registro de la nota",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Borrar',
          cancelButtonText: 'Cancelar'
        }).then((result) => {
          if (result.isConfirmed) {
            //console.log(idNota);
            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
            });
            $.ajax({
              url: "../borraNotaAPI",
              type: 'POST',
              dataType: 'json',
              data: { idNota: idNota },
              /* cache:false,
              processData: false,
              contentType: false, */
            })
              .done(function (result) {
                //alert('Guardada');
                console.log(result)
                Swal.fire(
                  'Guardado',
                  'Los datos han sido borrados',
                  'success'
                )
                var delayInMilliseconds = 2000; //1 second
                setTimeout(function () {
                  window.location.href = '/notasAPI';
                }, delayInMilliseconds);
              })
              .fail(function (result) {
                console.log('Fail');
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: result.responseJSON,
                })
                //alert(result.responseJSON)
              })
              .always(function () {
                console.log('complete');
              });
          }
        })
      });      
      $('#btnModifNotasAPI').click(function(e){
        e.preventDefault();
        var campos = $('#editNotasAPIForm').find('.validar').parsley();    
          var ctl = 0;
          for (const i in campos) {
            if (campos[i].isValid()) {
              ctl++
            }
            else {
              campos[i].validate();
              
            }
          }
          if(campos.length == ctl){
            var formdata = new FormData($("#editNotasAPIForm")[0]);
            if(suceso === 'otro'){
                flgSuceso = 1;
            }
            if(lugar === 'otro'){
                flgLugar = 1;
            }
            formdata.append('flagNvoSuceso', flgSuceso)
            formdata.append('flagNvoLugar', flgLugar)
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          url: "../modificaNotaAPI",
          type: 'POST',
          dataType: 'json',
          data: formdata,
          cache: false,
          processData: false,
          contentType: false,
        })
          .done(function (result) {
            //alert('Guardada');
            console.log(result)
            Swal.fire(
              'Guardado',
              'Los datos han sido modificados',
              'success'
            )
            var delayInMilliseconds = 2000; //1 second
            setTimeout(function () {
              window.location.href = '/notasAPI';
            }, delayInMilliseconds);
          })
          .fail(function (result) {
            console.log('Fail');
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: result.responseJSON,
            })
            //alert(result.responseJSON)
          })
          .always(function () {
            console.log('complete');
          });
          }
        
      });
      //Entidad de la nota//
      $('.edoNotaAPI').change(function(){
        //Lugares//
        $('#contSelLugar').empty();
        var cadSel = '<select id="selectLugar" name="lugar" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
          '<option disabled selected value="def">Lugar Específico</option>' +
          '<option value="otro">Otro Lugar</option>'+      
          '</select>';
        $('#selectLugar').val('def').trigger('change');
        $('#contSelLugar').append(cadSel);
        $('#selectLugar').select2();
        idEntidad = $(this).val();
        var lugaresArr = [];    
        $.ajax({
          url: "../getLugares/" + idEntidad,
          type: 'GET',
          cache: false,
        })
          .done(function (result) {
            //console.log(result)
            //return false;
            for (const i in result) {
              if (result[i] != null) {
                lugaresArr.push({
                  id: result[i].idLugar,
                  lugar: result[i].nomLugar,
                })
              }
            }
            var cadCom = '<option disabled selected value="def">Lugar Específico</option>' +
                         '<option value="otro">Otro Lugar</option>';
            for (const i in lugaresArr) {
              cadCom += '<option value="' + lugaresArr[i].id + '">' + lugaresArr[i].lugar
                + '</option>';
            }
            //console.log(cadCom);
            $('#selectLugar').empty()
            $('#selectLugar').append(cadCom);
    
          })
          .fail(function (result) {
            console.log(result)
          })
          .always(function () {
            console.log('complete');
          });
        //Sucesos//
        $('#contSelSuceso').empty();
        var cadSel2 = '<select id="selectSuceso" name="suceso" class="js-states browser-default validar" tabindex="-1" style="width: 100%" required>' +
          '<option disabled selected value="def">Suceso</option>' +
          '<option value="otro">Otro Suceso</option>'+      
          '</select>';
        $('#selectSuceso').val('def').trigger('change');
        $('#contSelSuceso').append(cadSel2);
        $('#selectSuceso').select2();
        var sucesosArr = [];
        $.ajax({
          url: "../getSucesos/" + idEntidad,
          type: 'GET',
          cache: false,
        })
          .done(function (result) {
            //console.log(result)
            //return false;
            for (const i in result) {
              if (result[i] != null) {
                sucesosArr.push({
                  id: result[i].idSuceso,
                  suceso: result[i].nomSuceso,
                })
              }
            }
            var cadCom = '<option disabled selected value="def">Suceso Específico</option>' +
                         '<option value="otro">Otro Suceso</option>';
            for (const i in sucesosArr) {
              cadCom += '<option value="' + sucesosArr[i].id + '">' + sucesosArr[i].suceso
                + '</option>';
            }
            //console.log(cadCom);
            $('#selectSuceso').empty()
            $('#selectSuceso').append(cadCom);
    
          })
          .fail(function (result) {
            console.log(result)
          })
          .always(function () {
            console.log('complete');
          });
        //console.log(idEntidad);
    })
  }) //rady