// Funciones //
function format(d) {
    var ret = d.textoHtml;
    ret = ret.replace(/&gt;/g, '>');
    ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
    //console.log(ret);
    return ret;
}
function format2(d) {
    var ret = d.actuacionHtml;
    ret = ret.replace(/&gt;/g, '>');
    ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
    //console.log(ret);
    return ret;
}
var spanish = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": '<i class="material-icons miTi">chevron_left</i>',
        "sLast": '<i class="material-icons miTi">chevron_right</i>',
        "sNext": '<i class="material-icons miTi">chevron_right</i>',
        "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
}
function destory_editor(selector) {
    if ($(selector)[0]) {
        var content = $(selector).find('.ql-editor').html();
        $(selector).html(content);

        $(selector).siblings('.ql-toolbar').remove();
        $(selector + " *[class*='ql-']").removeClass(function (index, css) {
            return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
        });

        $(selector + "[class*='ql-']").removeClass(function (index, css) {
            return (css.match(/(^|\s)ql-\S+/g) || []).join(' ');
        });
    }
    else {
        console.error('editor not exists');
    }
}

function goBack() {
    window.history.back();
}
/////////////////////////////////

// Ready //
$(document).ready(function () {

    ///datatable notas///
    var tableNotas = $('#tableNotas').DataTable({
        "language": spanish,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "/procurador/getNotas",
        "columns": [
            { "data": "" },
            { "data": "idNota" },
            { "data": "encabezado" },
            { "data": "medio_rel.nomMedio" },
            { "data": "entidad_rel.nomEntidad" },
            { "data": "area_rel.nomArea" },
            { "data": "link" },
            { "data": "fcPublicacion" },
        ],
        "columnDefs": [
            {
                "targets": 0,
                "data": null,
                "orderable": false,
                "className": 'details-control',
                "defaultContent": ''
            },
            {
                "targets": 6,
                "data": "link",
                "orderable": false,
                "className": 'dt-center',
                "render": function (dta, type, row) {
                    if (dta == "" || dta == null) {
                        return "S/L";
                    }
                    else {
                        return ' <a href="' + dta + '" target="_blank">link</a> ';
                    }
                }
            },
        ],
    });
    $('.dataTables_length select').addClass('browser-default');
    $('#tableNotas tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //tr.addClass('miText');
        var row = tableNotas.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
    /// datatable incidencias ///
  var tableIncidencias = $('#tableIncidencias').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "/procurador/getIncidencias",
    "columns": [
      { "data": "" },
      { "data": "idIncidencia" },
      { "data": "tema" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 6,
        "data": 'docIncidencia',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 7,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableIncidencias tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableIncidencias.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableIncidencias tbody').on('click', '.btnVerIncidente', function () {
    var tr = $(this).closest('tr');
    var idIncidencia = tr[0].childNodes[1].firstChild.data;
    //console.log(idIncidente)
    window.location.href = 'verIncidencia/' + idIncidencia;
  });
  /// datatable respuestas ///
  var tableRespuestas = $('#tableRespuestas').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getRespuestas",
    "columns": [
      { "data": "" },
      { "data": "idRespuesta" },
      { "data": "nota_rel.encabezado" },
      { "data": "entidad_rel.nomEntidad" },
      { "data": "fcSuceso" },
      { "data": "nota_rel.fcEnvio" },
      { "data": "fcCrea" }
    ],
    "columnDefs": [
      {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      },
      {
        "targets": 7,
        "data": "propuesta_rel",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === 0) {
            var cad = 'S/P'
          }
          else if (dta !== null) {
            if (dta.idTipoPropuesta === 1) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Tweet</a>'
            }
            else if (dta.idTipoPropuesta === 2) {
              var cad = '<a href="/verPropuesta/' + dta.idPropuesta + '">Boletín</a>'
            }
          }
          return cad
        }
      },
      {
        "targets": 8,
        "data": 'docRespuesta',
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (dta === null || dta === '') {
            var cad = 'S/D'
          }
          else if (dta !== null) {
            var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
          }
          return cad
        }
      },
      {
        "targets": 9,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerRespuesta" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableRespuestas tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    //tr.addClass('miText');
    var row = tableRespuestas.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    }
    else {
      // Open this row
      row.child(format2(row.data())).show();
      tr.addClass('shown');
    }
  });
  $('#tableRespuestas tbody').on('click', '.btnVerRespuesta', function () {
    var tr = $(this).closest('tr');
    var idRespuesta = tr[0].childNodes[1].firstChild.data;
    window.location.href = 'verRespuesta/' + idRespuesta;
  });
  ///datatable temas relevantes///
  var tableTemasRel = $('#tableTemasRel').DataTable({
    "language": spanish,
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": "getTemasRel",
    "columns": [
      //{ "data": "" },
      { "data": "idTemaRel" },
      { "data": "temaRel" },
      { "data": "nomEntidad" },
      { "data": "status" },
      //{ "data": "fcCrea" }
    ],
    "columnDefs": [
      /* {
        "targets": 0,
        "data": null,
        "orderable": false,
        "className": 'details-control',
        "defaultContent": ''
      }, */
      {
        "targets": 3,
        "data": "status",
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return 'Con respuesta'
          }
          else if(row.status == 2){
            return 'Cerrado'
          }
          else if (row.status == null) {
            return 'Activo'
          }
        }
      },
      {
        "targets": 4,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          if (row.status == 1) {
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'//+
                   //'<a class="waves-effect waves-light btn btnFinTemaRel" style="background-color:#9f2241" title="cerrar tema"><i class="material-icons">block</i></a>'
          }
          else if(row.status == 2){
            return '<a class="waves-effect waves-light btn btnVerRespTemarel" style="background-color:#6f7271" title="ver respuesta"><i class="material-icons">visibility</i></a>'//+
                   //'<a class="waves-effect waves-light btn btnAbrirTemaRel" style="background-color:#ddc9a3" title="abrir tema"><i class="material-icons">open_in_new</i></a>'
          }
          else if (row.status == null) {
            return '<span class="material-icons">pending_actions</span>'
          }
        }
      },
    ],
  });
  $('.dataTables_length select').addClass('browser-default');
  $('#tableTemasRel tbody').on('click', '.btnVerRespTemarel', function () {
    var tr = $(this).closest('tr');
    var idTema = tr[0].childNodes[0].firstChild.data;
    window.location.href = 'verRespuestaTemaRel/' + idTema;
  });
});