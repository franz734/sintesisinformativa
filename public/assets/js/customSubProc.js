// Funciones //
function format(d) {
    var ret = d.textoHtml;
    ret = ret.replace(/&gt;/g, '>');
    ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
    //console.log(ret);
    return ret;
}
function format2(d) {
    var ret = d.actuacionHtml;
    ret = ret.replace(/&gt;/g, '>');
    ret = ret.replace(/&lt;/g, '<');
    ret = ret.replace(/&quot;/g, '"');
    ret = ret.replace(/&apos;/g, "'");
    ret = ret.replace(/&amp;/g, '&');
    //console.log(ret);
    return ret;
}
var spanish = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla =(",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": '<i class="material-icons miTi">chevron_left</i>',
        "sLast": '<i class="material-icons miTi">chevron_right</i>',
        "sNext": '<i class="material-icons miTi">chevron_right</i>',
        "sPrevious": '<i class="material-icons miTi">chevron_left</i>'
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
}

function goBack() {
    window.history.back();
}
/////////////////////////////////

// Ready //
$(document).ready(function () {
    // contador dinamico //
    $('.counter').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 3000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
                $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
    });

    $('.sel2').select2();

    ///datatable notas///
    var tableNotas = $('#tableNotas').DataTable({
        "language": spanish,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "/subProc/getNotas",
        "columns": [
            { "data": "" },
            { "data": "idNota" },
            { "data": "encabezado" },
            { "data": "medio_rel.nomMedio" },
            { "data": "entidad_rel.nomEntidad" },
            { "data": "area_rel.nomArea" },
            { "data": "link" },
            { "data": "fcPublicacion" },
        ],
        "columnDefs": [
            {
                "targets": 0,
                "data": null,
                "orderable": false,
                "className": 'details-control',
                "defaultContent": ''
            },
            {
                "targets": 6,
                "data": "link",
                "orderable": false,
                "className": 'dt-center',
                "render": function (dta, type, row) {
                    if (dta == "" || dta == null) {
                        return "S/L";
                    }
                    else {
                        return ' <a href="' + dta + '" target="_blank">link</a> ';
                    }
                }
            },
        ],
    });
    $('.dataTables_length select').addClass('browser-default');
    $('#tableNotas tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //tr.addClass('miText');
        var row = tableNotas.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });
    //Btn filter Notas//
    $('#filterNotas').click(function (e) {
        e.preventDefault();
        /* console.log($('#selectEdo').val());
        return false; */
        var formdata = new FormData($("#filterFormInc")[0]);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/subProc/filtraNotas",
            type: 'POST',
            dataType: 'json',
            data: formdata,
            cache: false,
            processData: false,
            contentType: false,
        })
            .done(function (result) {
                /* console.log(result);
                return false; */
                $('#excelNotas').show();
                url = "/subProc/getFilteredNotas/" + result
                tableNotas.ajax.url(url).load();

            })
            .fail(function (result) {
                console.log('Fail');
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: result.responseJSON,
                })
                return false;
                //alert(result.responseJSON)
            })
            .always(function () {
                console.log('complete');
            });
    });
    //Limpiar Filtro Notas//
    $('#limpiaFilterNotas').click(function (e) {
        e.preventDefault();
        $('#selectEdo').val('def').trigger('change');
        $('#selectArea').val('def').trigger('change');
        $('#selectSubArea').val('def').trigger('change');
        $('#selectCategoria').val('def').trigger('change')
        $('#excelNotas').hide();
        $('#encabezado').val(null);
        $('#texto').val(null);
        $('#datefilter').val(null);
        tableNotas.ajax.url("getNotas").load();
    });
    //Excel Notas//
    $('#excelNotas').click(function (e) {
        e.preventDefault();
        var entidad = $('#selectEdo').val()
        //var area = $('#selectArea').val()
        //var subArea = $('#selectSubArea').val()
        var encabezado = $('#encabezado').val();
        var texto = $('#texto').val();
        var categoria = $('#selectCategoria').val();
        var datefilter = $('#datefilter').val();
        datefilter.replaceAll('/', '-')
        /* console.log(datefilter.replaceAll('/','-'));
        return false; */
        dta = {
            entidad: entidad,
            /* area: area,
            subArea: subArea, */
            encabezado: encabezado,
            texto: texto,
            categoria: categoria,
            datefilter: datefilter.replaceAll('/', '.')
        }
        dta = JSON.stringify(dta);
        var url = "/subProc/getExcelNotas/" + dta //comentar en producción        
        window.location = url
    });

    /// datatable incidencias ///
    var tableIncidencias = $('#tableIncidencias').DataTable({
        "language": spanish,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": "/subProc/getIncidencias",
        "columns": [
            { "data": "" },
            { "data": "idIncidencia" },
            { "data": "tema" },
            { "data": "entidad_rel.nomEntidad" },
            { "data": "fcSuceso" },
            { "data": "fcCrea" }
        ],
        "columnDefs": [
            {
                "targets": 0,
                "data": null,
                "orderable": false,
                "className": 'details-control',
                "defaultContent": ''
            },
            {
                "targets": 6,
                "data": 'docIncidencia',
                "visible": true,
                "className": "dt-center",
                "render": function (dta, type, row) {
                    if (dta === null || dta === '') {
                        var cad = 'S/D'
                    }
                    else if (dta !== null) {
                        var cad = '<a href="/' + dta + '" target="_blank">Ver</a>'
                    }
                    return cad
                }
            },
            {
                "targets": 7,
                "data": null,
                "visible": true,
                "className": "dt-center",
                "render": function (dta, type, row) {
                    return '<a class="waves-effect waves-light btn btnVerIncidente" style="background-color:#6f7271" title="ver incidente"><i class="material-icons">visibility</i></a>'
                }
            },
        ],
    });
    $('.dataTables_length select').addClass('browser-default');
    $('#tableIncidencias tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        //tr.addClass('miText');
        var row = tableIncidencias.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child(format2(row.data())).show();
            tr.addClass('shown');
        }
    });
    $('#tableIncidencias tbody').on('click', '.btnVerIncidente', function () {
        var tr = $(this).closest('tr');
        var idIncidencia = tr[0].childNodes[1].firstChild.data;
        //console.log(idIncidente)
        window.location.href = 'verIncidencia/' + idIncidencia;
    });

    //Btn filter Incidencias//
  $('#filterIncidencias').click(function (e) {
    e.preventDefault();
    /* console.log($('#selectEdo').val());
    return false; */
    var formdata = new FormData($("#filterFormInc")[0]);
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      url: "/subProc/filtraIncidencias",
      type: 'POST',
      dataType: 'json',
      data: formdata,
      cache: false,
      processData: false,
      contentType: false,
    })
      .done(function (result) {
        /* console.log(result);
        return false; */
        $('#excelInc').show();
        url = "/subProc/getFilteredIncidencias/" + result
        tableIncidencias.ajax.url(url).load();

      })
      .fail(function (result) {
        console.log('Fail');
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: result.responseJSON,
        })
        return false;
        //alert(result.responseJSON)
      })
      .always(function () {
        console.log('complete');
      });
  });
  //Limpiar Filtro Incidencias//
  $('#limpiaFilter').click(function (e) {
    e.preventDefault();
    $('#selectEdo').val('def').trigger('change');
    //$('#selectArea').val('def').trigger('change');
    //$('#selectSubArea').val('def').trigger('change');
    $('#selectMateria').val('def').trigger('change');
    $('#excelInc').hide();
    $('#tema').val(null);
    $('#problematica').val(null);
    $('#datefilter').val(null);
    tableIncidencias.ajax.url("getIncidencias").load();
  });
  //Excel Incidencias//
  $('#excelInc').click(function (e) {
    e.preventDefault();
    var entidad = $('#selectEdo').val()
    //var area = $('#selectArea').val()
    //var subArea = $('#selectSubArea').val()
    var tema = $('#tema').val();
    var problematica = $('#problematica').val();
    var materia = $('#selectMateria').val();
    var accionVS = $('#selectAccioVS').val();
    var datefilter = $('#datefilter').val();
    dta = {
      entidad: entidad,
      //area: area,
      //subArea: subArea,
      tema: tema,
      problematica: problematica,
      materia: materia,
      accionVS: accionVS,
      datefilter: datefilter.replaceAll('/','.')
    }
    dta = JSON.stringify(dta);
    var url = "/subProc/getExcelIncidencias/" + dta //comentar en producción        
    window.location = url
  });
})