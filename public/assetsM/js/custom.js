var spanish = {
  "sProcessing": "Procesando...",
  "sLengthMenu": "Mostrar _MENU_ registros",
  "sZeroRecords": "No se encontraron resultados",
  "sEmptyTable": "Ningún dato disponible en esta tabla =(",
  "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
  "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
  "sInfoPostFix": "",
  "sSearch": "Buscar:",
  "sUrl": "",
  "sInfoThousands": ",",
  "sLoadingRecords": "Cargando...",
  "oPaginate": {
    "sFirst": '<i class="fas fa-chevron-left"></i>',
    "sLast": '<i class="fas fa-chevron-right"></i>',
    "sNext": '<i class="fas fa-chevron-right"></i>',
    "sPrevious": '<i class="fas fa-chevron-left"></i>'
  },
  "oAria": {
    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
  },
  "buttons": {
    "copy": "Copiar",
    "colvis": "Visibilidad"
  }
}

function goBack() {
  window.history.back();
}

function detallesNota(idNota) {
  window.location.href = '../verNota/' + idNota;
}

function detallesIncidencia(idIncidencia) {
  window.location.href = '../verIncidencia/' + idIncidencia;
}

function detallesTemaRel($idTemaRel) {
  window.location.href = '../verTemaRel/' + $idTemaRel;
}

function detallesNotaInfo(idNotaInfo) {
  window.location.href = '../verNotaInformativa/' + idNotaInfo;
}

function creaTableNotas(data) {
  console.log(data);
  //return false;
  var tableNotas = $('#tableNotas').DataTable({
    "language": spanish,
    "responsive": true,
    "data": data.data,
    "columns": [
      { "data": "idNota" },
      { "data": "encabezado" },
      { "data": "texto" },
      { "data": "nomEntidad" },
      { "data": "link" },
    ],
    "columnDefs": [
      {
        "targets": 4,
        "data": "link",
        "orderable": false,
        "className": 'dt-center',
        "render": function (dta, type, row) {
          if (dta == "" || dta == null) {
            return "S/L";
          }
          else {
            return ' <a href="' + dta + '" target="_blank">link</a> ';
          }
        }
      },
      {
        "targets": 5,
        "data": null,
        "visible": true,
        "className": "dt-center",
        "render": function (dta, type, row) {
          return '<a class="waves-effect waves-light btn btnVerNota" style="background-color:#6f7271" title="ver nota"><i class="fa fa-eye"></i></a>'
        }
      },
    ],
  });

}

function solInfo(id, task) {
  Swal.showLoading();
  let date = new Date()

  let day = date.getDate()
  let month = date.getMonth() + 1
  let year = date.getFullYear()

  if (month < 10) {
    var hoy = `${day}/0${month}/${year}`
  } else {
    var hoy = `${day}/${month}/${year}`
  }
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "/consultas/masInfoTemaRel",
    type: 'POST',
    dataType: 'json',
    data: { 'id': id, 'fecha': hoy, 'task': task },
    cache: false,
  })
    .done(function (result) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
      })
      Toast.fire({
        icon: 'success',
        title: 'Solicitud Enviada'
      })
    })
    .fail(function (result) {
      console.log('Fail');
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: result.responseJSON,
      })
    })
    .always(function () {
      console.log('complete');
    });
}

function comparteInfo(id, task) {
  (async () => {
    const { value: email } = await Swal.fire({
      title: 'Correo de destino:',
      input: 'email',
      //inputLabel: 'Your email address',      
    })
    if (email) {
      //console.log(email);
      Swal.fire({
        title: 'Enviando...',
        allowEscapeKey: false,
        allowOutsideClick: false,
      })
      Swal.showLoading();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        url: "/consultas/comparteInfo",
        type: 'POST',
        dataType: 'json',
        data: { 'id': id, 'mail': email, 'task': task },
        cache: false,
      })
        .done(function (result) {
          Swal.fire(
            'Enviado',
            'La información ha sido enviada',
            'success'
          );
          var delayInMilliseconds = 2000; //1 second
          setTimeout(function () {
            location.reload();
          }, delayInMilliseconds);
        })
        .fail(function (result) {
          console.log('Fail');
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'No se pudo enviar la información',
          })
        })
        .always(function () {
          console.log('complete');
        });
    }

  })()
}

var dataNotas;
// Ready //
$(document).ready(function () {
  $('.carousel').carousel();

  $('#keyWord').keypress(function (e) {    
    if (e.keyCode == 13){
      e.preventDefault();
      $('#buscaKeyWord').click();
    }      
  });

  $('#buscaKeyWord').click(function (e) {
    e.preventDefault();
    var keyWord = $('#keyWord').parsley();
    if (keyWord.isValid()) {
      var key = $('#keyWord').val();
      $('.keyWordS').val(key);
      $.ajax({
        url: "getResultados/" + key,
        type: 'GET',
        cache: false,
      })
        .done(function (result) {
          //console.log(result)
          //return false;
          for (const i in result) {
            var sel = '#' + i;
            $(sel).html(result[i]);
          }
          $('#labelKeyWord').html(key);
          $('#contResult').show();
        })
        .fail(function (result) {
          console.log(result)
        })
        .always(function () {
          console.log('complete');
        });
    }
    else {
      keyWord.validate();
    }
  });
});

// ON //
$('#aNotas').on('click', function (e) {
  e.preventDefault();
  //alert('buscar notas');
  var key = $('.keyWordS').val()
  window.location.href = 'notas/' + key;
});

$('#aIncidencias').on('click', function (e) {
  e.preventDefault();
  //alert('buscar incidencias');
  var key = $('.keyWordS').val()
  window.location.href = 'incidencias/' + key;
});

$('#aTemasRel').on('click', function (e) {
  e.preventDefault();
  //alert('buscar temasRel');
  var key = $('.keyWordS').val()
  window.location.href = 'temasRel/' + key;
});

$('#aNotasInfo').on('click', function (e) {
  e.preventDefault();
  //alert('buscar notas informativas');
  var key = $('.keyWordS').val()
  window.location.href = 'notasInformativas/' + key;
});

$('#aNormas').on('click', function (e) {
  e.preventDefault();
  //alert('buscar notas informativas');
  var key = $('.keyWordS').val()
  window.location.href = 'normas/' + JSON.stringify(key);
});

$('#aPresentaciones').on('click', function (e) {
  e.preventDefault();
  //alert('buscar notas informativas');
  var key = $('.keyWordS').val()
  window.location.href = 'presentaciones/' + key;
});